<?php

class Event_model extends CI_Model{

    public function createEvent($data){
        $this->db->insert('event', $data);
        if($this->db->affected_rows() > 0){
            $insert_id = $this->db->insert_id();
            return  $insert_id;
        }
        return 0;
    }

    public function deleteEvent($id){
        $deleted = $this->db->delete('event', array('id' => $id));
        return $deleted;
    }

    public function updateEventStatut($data, $id){
        $updated = $this->db->update('event', $data, array('id' => $id));
        return $updated;
    }

}

?>
