<?php

class Categories extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('category_model','category');
    }

    public function index()
	{
		
		$this->load->view('welcome');

	}

	public function insert()
	{
		
	}

	public function showAll(){
		$query = $this->category->showAll();
			if($query){
				$result['category']  = $this->category->showAll();
			}
		echo json_encode($result);
	}
	
	public function updateCategory(){

		$result['error'] = true;
		$result['msg'] = "failed when update category";
		$result['code_error'] = 0;

		$id = $this->input->post('id_category');
		$label = $this->input->post('label_category');
		$has_image = $this->input->post('has_image');
		$new_image_name = $this->input->post('new_image_name_category');
		$old_image_name = $this->input->post('old_image_name_category');
		
		$data = array(
			'categoryLabel' => $label,
			'categoryImage' => $new_image_name
        );
		$query = $this->category->updateCategory($data, $id);

		if($has_image === 'TRUE'){
			
			$file = "uploads/categories/" . $old_image_name;
 			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				
				//add category 
				$config['upload_path']          = './uploads/categories/';
				$config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['max_size']             = 500;
				$config['max_width']            = 2500;
				$config['max_height']           = 1400;
	
				$this->load->library('upload', $config);
	
				if ( ! $this->upload->do_upload('image_category')) {
			
					$result['msg'] = 'Erreur lors du upload';
					$result['code_error'] = 2;
					
				} else {

					$result['msg'] = 'Upload success';
					$result['code_error'] = 1;

				}
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
				$result['code_error'] = 3;
			}

		}

		if($query){
			$result['error'] = false;
		}
		echo json_encode($result);

	}

	public function deleteCategory(){

		$result['error'] = true;
		$result['file_deleted'] = false;
		$result['msg'] = 'error when deleting!';

		//FCPATH . "uploads/" . $picname
		$image = $this->input->post('image_category');
		$id = $this->input->post('id_category');
		$file = "uploads/categories/" . $image;
 
		if($this->category->containSubcategory($id)){
			$result['msg'] = "Cette catégorie contient un ou plusieurs sous-catégorie(s) et ne peut pas être supprimé";
		}else{
			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				
				$query = $this->category->deleteCategory($id);
				if($query){
					$result['error'] = false;
				}
				$result['msg'] = "file has been deleted.";
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
			}
		}
		
		echo json_encode($result);

	}

}
    
?>