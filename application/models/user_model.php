<?php

class User_model extends CI_Model
{

    public function addUser($data)
    {
        $insert = $this->db->insert('users', $data);
        return $insert;
    }

    public function showByAdmin($admin)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('admin_id', $admin);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function searchUser($match)
    {
        $field = array('first_name', 'last_name', 'email');
        $this->db->like('concat(' . implode(',', $field) . ')', $match);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function loginUser($username, $password)
    {
        $query = $this->db->query("SELECT * FROM users WHERE username='$username' AND password=md5('$password')");
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function selectUser($userId)
    {
        $query = $this->db->query("SELECT * FROM users WHERE id=$userId");
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function updateCredential($data, $id)
    {
        $this->db->update('users', $data, array('id' => $id));
        if ($this->db->affected_rows() > 0) {
            $result = $this->selectUser($id);
            return $result;
        }
        return false;
    }

    public function checkCredential($userId, $token_url)
    {

        $query = $this->db->query("SELECT * FROM users WHERE id='$userId' AND token='$token_url'");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function deleteUser($id)
    {
        $deleted = $this->db->delete('users', array('id' => $id));
        return $deleted;
    }

    public function updateUser($data, $id)
    {
        $this->db->update('users', $data, array('id' => $id));
        if ($this->db->affected_rows() > 0) {
            $this->session->set_userdata('user', $this->selectUser($id));
            return true;
        }
        return false;
    }

    public function isExist($username, $email)
    {
        $query = $this->db->query("SELECT * FROM users WHERE username='$username' OR email='$email'");
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

}
