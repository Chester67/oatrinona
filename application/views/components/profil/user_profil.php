<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

.field {
    height: 50px;
}

.hidden-placeholder {
    width:120px;
    color:transparent;
}

</style>

<script type="text/x-template" id="template-user-profil">

    <div>
        <v-card v-if="!data_no_user" style="margin-left: 20px; margin-right: 20px; display: flex; justify-content: center; align-items: center; flex-direction: column; margin-top: 23px; background: rgb(236, 239, 241)">
                <v-card-title style="width: 100%; height: 55px; background: #BDBDBD; font-size: 19px; font-weight: bold; color: #455A64">
                    Profil utilisateur
                </v-card-title>

                <v-layout row wrap style="margin-bottom: 7px; padding-top: 7px; padding-bottom: 7px">
                    <v-flex xs12 md3>
                        <div style="height: 100%">
                                <v-layout justify-center fill-height align-center style="margin-left: 25px">
                                    <div style="display: flex; flex-direction: column; justify-content: center; align-items: center;">

                                        <v-avatar
                                            :tile="tile"
                                            :size="avatarSize"
                                            color="transparent">
                                            <!--<img src="https://via.placeholder.com/150x150" alt="">-->

                                            <template v-if="!isImageChanged">
                                                <template v-if="file_name_update && !!file_name_update">
                                                    <img :src="urlBaseImage+'uploads/utilisateurs/'+file_name_update" alt="" >
                                                </template>
                                                <template v-else>
                                                    <v-icon style="font-size: 120px; background: #fafafa">face</v-icon>
                                                </template>
                                            </template>
                                            <template v-else>
                                                <!--<img :src="urlBaseImage+'uploads/utilisateurs/'+file_name_update" alt="" >-->
                                                <img id="target" ref="target" :src="changedUrl"/>
                                            </template>

                                        </v-avatar>

                                    </div>
                                </v-layout>

                        </div>
                    </v-flex>

                    <v-flex xs12 md9>

                        <v-container grid-list-sm style="margin-right: 25px">
                            <v-layout row wrap>
                                <v-flex xs4 class="field">
                                    <v-text-field prepend-icon="face" placeholder="Nom" v-model="newUser.last_name"></v-text-field>
                                </v-flex>
                                <v-flex xs5 class="field">
                                    <v-text-field placeholder="Prénoms" v-model="newUser.first_name"></v-text-field>
                                </v-flex>
                                <v-flex xs3 class="field">
                                    <v-text-field placeholder="Nom utilisateur" v-model="newUser.username" disabled></v-text-field>
                                </v-flex>
                                <v-flex xs6 class="field">
                                    <v-text-field prepend-icon="vpn_key" type="password" placeholder="Mot de passe" v-model="newUser.password"></v-text-field>
                                </v-flex>
                                <v-flex xs6 class="field">
                                    <v-text-field type="password" placeholder="Confirmation mot de passe" v-model="confirm_password"></v-text-field>
                                </v-flex>
                                <v-flex xs6 class="field">
                                    <v-text-field prepend-icon="business" placeholder="Société" v-model="newUser.company"></v-text-field>
                                </v-flex>
                                <v-flex xs6 class="field">
                                    <v-text-field placeholder="Métier" v-model="newUser.job"></v-text-field>
                                </v-flex>
                                <v-flex xs12 class="field">
                                    <v-text-field prepend-icon="mail" placeholder="Email" v-model="newUser.email"></v-text-field>
                                </v-flex>
                                <v-flex xs12 class="field">
                                    <v-text-field
                                        type="tel"
                                        prepend-icon="phone"
                                        placeholder="Téléphone"
                                        v-model="newUser.phone"></v-text-field>
                                </v-flex>

                                <div style="display: flex; flex-direction: row; margin-top: 15px; align-items: center;" class="field">
                                    <v-icon>add_a_photo</v-icon>
                                    <div style="display: flex; flex-direction: row; align-items: center">
                                        <input id="file" style="margin-left: 8px" type="file" :class="class_name_state" @change="onFileSelected">
                                        <label style="margin-left: 4px" id="fileLabel">{{ file_name_update }}</label>
                                    </div>
                                </div>

                            </v-layout>
                        </v-container>

                        <v-btn style="margin-left: 25px" @click="wsUpdateUser">Enregistrer</v-btn>

                    </v-flex>
                </v-layout>

                <!--<v-card-actions style="background: red; width: 100%">
                    <v-spacer></v-spacer>
                    <v-btn flat @click="wsUpdateUser">Enregistrer</v-btn>
                </v-card-actions>-->

                <div v-if="!!errormessage" style="display: flex; flex-direction: row; margin-left: 23px; height: 60px">
                    <v-icon style="display: flex; align-items: center; vertical-align: middle">error_outline</v-icon>
                    <span style="display: flex; align-items: center;">{{ errormessage }}</span>
                </div>

        </v-card>

        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>

        <v-alert
            class="centered_flash"
            :value="alert"
            type="success"
            transition="scale-transition">
            {{ flashMessage }}
        </v-alert>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_update_user = '<?php echo URL_UPDATE_USER ?>';
    var url_update_user_final = `${url_base}${url_update_user}`;

    Vue.component('user-profil', {
        template: '#template-user-profil',
        mixins: [common_mixin],
        data: function () {

            return{
                flashMessage:'',
                alert: false,
                newUser:{
                    first_name:'',
                    last_name:'',
                    email:'',
                    username:'',
                    password:'',
                    register_date:'',
                    company:'',
                    job:'',
                    phone:'',
                    role:'USER',
                    admin_id: undefined
                },
                confirm_password: '',
                errormessage: '',
                user_logged_in: undefined,
                tile: false,
                avatarsize: 200,
                file_name_update: '',
                old_file_name_update: '',
                class_name_state: '',
                selectedFile: null,
                urlBaseImage: url_base,
                isImageChanged: false,
                changedUrl: '',
            }

        },
        methods: {
            afterCheckLogged(connexion){
                if(connexion.is_logged){

                    /*if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }*/
                    this.user_logged_in = connexion.user_logged[0];
                    console.log(`USER PROFIL PAGE USER LOGGED IN ===>${JSON.stringify(this.user_logged_in)}`);
                    this.copyUser(this.user_logged_in);
                    this.data_loader = false;

                }else{
                    console.log("USER PROFIL PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
           },
           copyUser(user){
               /*
                {"id":"4","first_name":"Admin","last_name":"admin","email":"admin@gmail.com","username":"admin","password":"21232F297A57A5A743894A0E4A801FC3","company":"ovitech","job":"manager","phone":"+2614588565","register_date":"2018-12-24 15:11:31","admin_id":"0","role":"ADMINISTRATOR","token":"1547719296","last_connected":"2019-01-17 01:01:36","fcm_id":null,"user_image":null}
               */
                this.newUser.id = user.id;
                this.newUser.first_name = user.first_name;
                this.newUser.last_name = user.last_name;
                this.newUser.email = user.email;
                this.newUser.username = user.username;
                this.newUser.password = 'placeholder';
                this.newUser.company = user.company;
                this.newUser.job = user.job;
                this.newUser.phone = user.phone;
                this.newUser.register_date = user.register_date;
                this.newUser.role = user.role;
                this.newUser.user_image = user.user_image;
                this.confirm_password = 'placeholder';

                if(user.user_image && !! user.user_image)
                    this.file_name_update = user.user_image;
           },
           onFileSelected(event){
                this.selectedFile = event.target.files[0];
                console.log(`CHANGED IMG=====>${URL.createObjectURL(this.selectedFile)}`)
                this.file_name_update = this.selectedFile.name;
                this.changedUrl = URL.createObjectURL(this.selectedFile);
                this.isImageChanged = true;
           },
           wsUpdateUser(){

                if(this.allFieldChecked()){

                    if(this.newUser.password === this.confirm_password){
                        this.data_loader = true;
                        this.errormessage = '';
                        var formData = new FormData();
                        formData.append('id_user', this.newUser.id);
                        formData.append('first_name', this.newUser.first_name);
                        formData.append('last_name', this.newUser.last_name);
                        formData.append('email', this.newUser.email);
                        formData.append('password', this.newUser.password);
                        formData.append('company', this.newUser.company);
                        formData.append('job', this.newUser.job);
                        formData.append('phone', this.newUser.phone);

                        if(this.selectedFile && this.selectedFile.name){
                            console.log("========>UPDATE USER IMAGE AND ALL LABEL");
                            formData.append('has_image', 'TRUE');
                            formData.append('image_user', this.selectedFile);

                        }else{
                            console.log("========>UPDATE ONLY ALL LABEL");
                            formData.append('has_image', 'FALSE');
                            formData.append('image_user', null);
                        }

                        axios.post(url_update_user_final, formData).then((response) => {
                            if (!response.data.error) {
                                    //alert("SUCCESS");
                                    this.dialog = false;
                                    this.manageFlashMSG("Utilisateur modifié avec succès !");

                            }else{
                                //alert(`ERROR====>${response.data.error}`);
                                this.errormessage = response.data.msg;
                            }
                        })
                        .finally(() => {

                            this.closeLoaderAfterDelay();

                        })
                    }else{
                        this.errormessage = "Les deux mots de passe ne sont pas identiques !"
                    }

                }else{
                    this.errormessage = "Veuillez completer les champs !"
                }
            },
            allFieldChecked(){
                let check = this.newUser;
                if(check.first_name && check.last_name && check.username && check.email && check.password && this.confirm_password && check.company && check.job && check.phone){
                    return true;
                }
                return false;
            },
            manageFlashMSG(message){
                this.flashMessage = message;
                this.alert = true;
                setTimeout(() =>{
                    this.alert = false;
			        this.checkIfLogged(url_is_logged_final);
			    },  3000); // disappearing message success in 3 sec
            },


        },
        mounted: function () {
            console.log('userprofil : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
            this.class_name_state = 'hidden-placeholder';

        },
        created: function () {
            //console.log(document.querySelector(".target"));
            //console.log(document.getElementById("target"));
        },
        computed: {
            avatarSize () {
                return `${this.avatarsize}px`
            }
        },

    });

</script>