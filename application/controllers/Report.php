<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('spent_model','spent');
    }

	public function index()
	{
		$this->load->view('welcome');
	}
	
	public function category()
	{
		$this->load->view('welcome');
	}
    
    public function day()
	{
		$this->load->view('welcome');
	}

	public function spentglobal()
	{
		$this->load->view('welcome');
	}

	public function reportSimpleSpentGlobal()
	{
		$result['error'] = true;
		$userId = $this->input->post('user_id');
		$spentMonth = $this->input->post('spent_month');
		$spentYear = $this->input->post('spent_year');
		$query_result = $this->spent->reportSimpleSpentGlobal($userId, $spentMonth, $spentYear); 
		
		if($query_result){
			$result['error'] = false;
			$result['report'] = $query_result;
		}

		echo json_encode($result);
	}

	public function reportSimpleTaskGlobal()
	{
		$result['error'] = true;
		$userId = $this->input->post('user_id');
		$spentMonth = $this->input->post('spent_month');
		$spentYear = $this->input->post('spent_year');
		$query_result = $this->spent->reportSimpleTaskGlobal($userId, $spentMonth, $spentYear); 
		
		if($query_result){
			$result['error'] = false;
			$result['report'] = $query_result;
		}

		echo json_encode($result);
	}

	public function reportCategorySpent()
	{
		$result['error'] = true;
		$userId = $this->input->post('user_id');
		$spentMonth = $this->input->post('spent_month');
		$spentYear = $this->input->post('spent_year');
		$query_result = $this->spent->reportCategorySpent($userId, $spentMonth, $spentYear); 
		
		if($query_result){
			$result['error'] = false;
			$result['report'] = $query_result;
		}

		echo json_encode($result);
	}

	public function reportCategoryTask()
	{
		$result['error'] = true;
		$userId = $this->input->post('user_id');
		$spentMonth = $this->input->post('spent_month');
		$spentYear = $this->input->post('spent_year');
		$query_result = $this->spent->reportCategoryTask($userId, $spentMonth, $spentYear); 
		
		if($query_result){
			$result['error'] = false;
			$result['report'] = $query_result;
		}

		echo json_encode($result);
	}

	public function reportDetailedSpentGlobal()
	{

	}
	
}
