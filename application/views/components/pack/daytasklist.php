<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_empty');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

    .text-day-task {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        line-height: 16px;     /* fallback */
        white-space: normal;
        word-wrap: break-word;
        -webkit-line-clamp: 2; /* number of lines to show */
        -webkit-box-orient: vertical;
        max-width: 90px;
        color: white;
        font-weight: normal;
        background: #78909C;
        border-radius: 19px;
        padding-left: 10px;
        padding-right: 7px;
        padding-top: 4px;
        padding-bottom: 4px;
        font-size: 13px;
    }

    hr {
        background: #e0d9d9;
        height: 1px;
        border: 0;
        border-top: 1px solid #e0d9d9;
        margin: 1em 0;
        padding: 0;
    }

    .task-right-text {
        font-size: 15px;
        color: #757575;
        font-weight: normal;
    }

    h3 {
        font: 33px sans-serif;
        text-align: left;
        display: flex;
        flex-direction: row;
    }

    h3:after{
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #e0d9d9;
        margin: auto;
    }

    h3:before {
        content: ' ';
        background-image: url('<?php echo base_url(); ?>assets/img/coin.png');
        background-size:contain;
        height: 17px;
        width: 17px;
        margin-right: 8px;
        margin-left: 5px;
    }
    /*h3:before{
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #e0d9d9;
        margin: auto;
    }*/

</style>

<script type="text/x-template" id="template-day-tasklist">

    <div>

        <div v-if="!data_no_user">

            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timeline/reset.css"> <!-- CSS reset -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timeline/style.css"> <!-- Resource style -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timeline/demo.css"> <!-- Demo style -->

            <div style="padding-top: 12px; text-align: center; background: #FAFAFA; font-weight: bold; font-size: 19px; color: #757575;">Aujourd'hui</div>

            <template v-if="alltasks && alltasks.length > 0">

                <section class="cd-timeline js-cd-timeline">

                    <div class="cd-timeline__container">

                        <template v-for="task in alltasks">
                            <div class="cd-timeline__block js-cd-block">
                                <div class="cd-timeline__img cd-timeline__img--picture" style="display: flex; justify-content: center; align-items: center">
                                    <!--<img src="<?php echo base_url(); ?>assets/img/cd-icon-location.svg" alt="Picture">-->
                                    <v-icon style=" vertical-align: middle;">access_time</v-icon>
                                </div> <!-- cd-timeline__img -->

                                <v-card class="cd-timeline__content js-cd-content elevation-4">

                                    <div style="float: right;">
                                        <v-icon @click="deleteEntireTask" :data-index="task.details.eventId" style="font-size: 22px; color: grey; cursor: pointer">clear</v-icon>
                                    </div>

                                    <h2 style="margin-left: 5px; font-size: 16px; color: #757575; font-weight: bold;">Tâche : {{ task.details.subtaskLabel }} ({{ task.details.taskLabel }})</h2>

                                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                                        <v-icon>assignment_ind</v-icon>
                                        <h1 class="task-right-text" style="margin-left: 5px">{{ getAssignedUserName(task.details.assignedUser, task.details.assignedUserId) }} ({{ getStatutLetter(task.details.eventStatut) }})</h1>

                                    </div>
                                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                                        <v-icon>place</v-icon>
                                        <h1 class="task-right-text" style="margin-left: 5px">{{ task.details.place_description }}</h1>

                                    </div>
                                    <div style="margin-top: 13px">
                                        <h3 class="task-right-text">Dépenses</h3>
                                    </div>

                                    <v-layout fluid wrap>

                                        <v-flex xs12 md12>

                                            <template v-for="taskspent in task.taskspents">

                                                <v-flex xs12>

                                                    <v-layout row wrap>

                                                        <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                                                            <div>
                                                                <v-layout justify-center fill-height align-center>
                                                                    <div style="display:block; margin:0 auto; text-align: center;">

                                                                        <img :src="urlBaseImage+'/uploads/categories/'+taskspent.category.categoryImage" alt="" style="margin-top: 5px;width:42%;height:42%;object-fit:contain">
                                                                        <p class="text-xs-center" style="margin-top: -4px; font-weight: bold; font-size: 12px">{{ taskspent.category.categoryLabel }}</p>

                                                                    </div>
                                                                </v-layout>
                                                            </div>
                                                        </v-flex>

                                                        <v-flex d-flex xs12 sm6 md9>

                                                            <v-layout row wrap >

                                                                <template v-for="subcategory in taskspent.subcategories">

                                                                    <v-layout row style="min-height: 27px; width: 100%; margin-top: 2px">
                                                                        <v-flex xs12 sm6 md1 style="display: flex; align-items: center;">
                                                                            <div :style="`border-radius: 50%; width: 15px; height: 15px; background: ${subcategory.utility.contextImage}`"></div>
                                                                        </v-flex>
                                                                        <v-flex xs12 sm6 md5 style="display: flex; align-items: center;">
                                                                            <div>

                                                                                <div class="text-day-task">{{ subcategory.subcategory_label }}</div>

                                                                            </div>
                                                                        </v-flex>
                                                                        <v-flex xs12 sm6 md4>
                                                                            <div class="prix_report">
                                                                                <span style="height: 100%; display: flex; align-items: center; font-size: 13px; color: #78909C">{{ subcategory.price }} Ar</span>
                                                                            </div>
                                                                        </v-flex>
                                                                        <v-flex xs12 sm6 md2>
                                                                            <span style="height: 100%; text-align: left; display: flex; align-items: center; font-size: 13px; color: #546E7A">x {{ subcategory.number }}</span>
                                                                        </v-flex>

                                                                    </v-layout>


                                                                </template>

                                                            </v-layout>

                                                        </v-flex>

                                                    </v-layout>

                                                    <hr/>
                                                </v-flex>

                                            </template>

                                        </v-flex>

                                    </v-layout>
                                    <!--<a href="#0" class="cd-timeline__read-more">Read more</a>-->
                                    <span class="cd-timeline__date">{{ task.details.eventDebut | getHourOfDate}}</span>

                                </v-card> <!-- cd-timeline__content -->

                            </div> <!-- cd-timeline__block -->
                        </template>

                    </div>

                </section> <!-- cd-timeline -->
            </template>
            <template v-else>
                <data-empty></data-empty>
            </template>

        </div>

        <v-btn
            fab
            bottom
            right
            color="blue-grey lighten-1"
            dark
            fixed
            @click.stop="showAdd"
            v-if="!data_no_user">
            <v-icon>add</v-icon>
        </v-btn>

        <v-dialog v-model="dialog_remove" persistent max-width="290">
            <v-card>
                <v-card-title>Voulez-vous vraiment supprimer cette tâche ?</v-card-title>
                <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn color="green darken-1" flat @click="dialog_remove = false">ANNULER</v-btn>
                    <v-btn color="green darken-1" flat @click="deleteTask">SUPPRIMER</v-btn>
                </v-card-actions>
            </v-card>
        </v-dialog>

        <data-no-user v-if="data_no_user"></data-no-user>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_list_day_task = '<?php echo URL_LIST_DAY_TASK ?>';
    var url_list_day_task_final = `${url_base}${url_list_day_task}`;

    var url_delete_day_task = '<?php echo URL_DELETE_SPENT_BY_EVENT ?>';
    var url_delete_day_task_final = `${url_base}${url_delete_day_task}`;

    var url_delete_event = '<?php echo URL_DELETE_EVENT ?>';
    var url_delete_event_final = `${url_base}${url_delete_event}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    Vue.component('day-tasklist', {
        template: '#template-day-tasklist',
        mixins: [common_mixin],
        data: function () {

            return{
                name: '',
                rowsPerPageItems: [100],
                pagination: {
                    rowsPerPage: 100
                },
                tasks: [1,2,3,4,5,6],
                alltasks: [],
                urlBaseImage: url_base,
                user_logged_in: undefined,
                user_choosed_by_admin: undefined,
                dialog_remove: false,
                event_id_to_delete: 0,
            }

        },
        methods: {
           showAdd () {

            this.$router.push('/day/task');

           },
           wsTaskList (spentDate) {
                var userobj = this.user_logged_in;
                if(typeof choosed !== 'undefined'){
                    userobj = this.user_choosed_by_admin;
                }
                var formData = new FormData();
                formData.append('user_id', userobj.id);
                formData.append('spent_date', spentDate);
                window.axios.post(url_list_day_task_final, formData).then((response) => {

                    if(response.data.taskspents){
                        //console.log(`BEFORE REGROUP=====>${JSON.stringify(response.data.taskspents)}`);
                        var ids_event = this.takeAllEventId(response.data.taskspents);
                        var filtered_ids_event = ids_event.filter((v,i) => ids_event.indexOf(v) == i);
                        for(var i= 0; i < filtered_ids_event.length; i++){
                            this.alltasks.push(this.regroupTaskByEvent(this.regroupSpentByEvent(response.data.taskspents, filtered_ids_event[i])));
                        }
                        this.alltasks.sort(function(a,b) {return a.hoursort - b.hoursort});
                        //console.log(`AFTER REGROUP=====>${JSON.stringify(this.alltasks)}`);

                    }else{
                        console.log("DAY TASK LIST=====> UNDEFINED");
                        this.alltasks = [];
                    }

                }).catch(err => {
                    console.log(err)
                })
           },
           checkIfMe: function(assigned_user_id, logged, choosed){
                var userobj;
                if(typeof choosed !== 'undefined'){
                    userobj = choosed;
                }else{
                    userobj = logged;
                }
                if(assigned_user_id === userobj.id){
                    return true;
                }

                return false;
           },
           getAssignedUserName: function(assigned_user_name, assigned_user_id){
                var fullname = 'Moi';
                if(!this.checkIfMe(assigned_user_id, this.user_logged_in, this.user_choosed_by_admin)){
                    fullname = assigned_user_name;
                }
                return fullname;
           },
           deleteEntireTask: function(e){

                this.dialog_remove = true;
                let event_id = e.target.getAttribute('data-index');
                this.event_id_to_delete = event_id;
                console.log(`delete_entire_task======>${event_id}`);

           },
           deleteTask: function(){
                this.dialog_remove = false;
                console.log("delete that");
                var formData = new FormData();
                formData.append('event_id', this.event_id_to_delete);
                window.axios.post(url_delete_day_task_final, formData).then((response) => {

                    if(response.data.deleted){
                        console.log('DELETE task SUCCESS');
                        this.deleteEvent();
                    }else{
                        console.log('DELETE task failure');
                    }

                }).catch(err => {
                    console.log(err)
                })
           },
           deleteEvent: function(){
                var formData = new FormData();
                formData.append('event_id', this.event_id_to_delete);
                window.axios.post(url_delete_event_final, formData).then((response) => {

                    if(response.data.deleted){
                        console.log('DELETE event SUCCESS');
                        this.alltasks = [];
                        let today = new Date().toISOString().slice(0, 10);
                        this.wsTaskList(today);
                    }else{
                        console.log('DELETE event failure');
                    }

                }).catch(err => {
                    console.log(err)
                })
           },
           afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`DAYTASKLIST PAGE USER LOGGED  ===> ${JSON.stringify(connexion.user_logged[0])}`);

                    this.user_logged_in = connexion.user_logged[0];
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_choosed_by_admin = connexion.user_choosed[0];
                    }
                    let today = new Date().toISOString().slice(0, 10);
                    this.wsTaskList(today);

                }else{
                    console.log("DAYTASK PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
           },

        },
        mounted: function () {
            console.log('daytasklist : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },

    });

</script>