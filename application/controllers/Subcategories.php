<?php

class Subcategories extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('subcategory_model','subcategory');
    }

    public function index()
	{
		$this->load->view('welcome');
	}

	public function insert()
	{
		
	}

	public function showAll(){
		$query = $this->subcategory->showAll();
		if($query){
			$result['subcategory'] = $this->subcategory->showAll();
		}
		echo json_encode($result);
	}
	
	public function showByCategoryId(){
		$categoryId = $this->input->post('category_id');
		$query = $this->subcategory->showByCategoryId($categoryId);
		if($query){
			$result['subcategory'] = $this->subcategory->showByCategoryId($categoryId);
		}
		echo json_encode($result);
	}

	public function updateSubcategory(){

		$result['error'] = true;
		$result['msg'] = "failed when update subcategory";
		$result['code_error'] = 0;

		$id = $this->input->post('id_subcategory');
		$label = $this->input->post('label_subcategory');
		$has_image = $this->input->post('has_image');
		$new_image_name = $this->input->post('new_image_name_subcategory');
		$old_image_name = $this->input->post('old_image_name_subcategory');
		
		$data = array(
			'subcategoryLabel' => $label,
			'subcategoryImage' => $new_image_name
        );
		$query = $this->subcategory->updateSubcategory($data, $id);

		if($has_image === 'TRUE'){
			
			$file = "uploads/sous-categories/" . $old_image_name;
 			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				
				//add subcategory 
				$config['upload_path']          = './uploads/sous-categories/';
				$config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['max_size']             = 500;
				$config['max_width']            = 2500;
				$config['max_height']           = 1400;
	
				$this->load->library('upload', $config);
	
				if ( ! $this->upload->do_upload('image_subcategory')) {
			
					$result['msg'] = 'Erreur lors du upload';
					$result['code_error'] = 2;
					
				} else {

					$result['msg'] = 'Upload success';
					$result['code_error'] = 1;

				}
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
				$result['code_error'] = 3;
			}

		}

		if($query){
			$result['error'] = false;
		}
		echo json_encode($result);

	}

	public function deleteSubcategory(){

		$result['error'] = true;
		$result['file_deleted'] = false;
		$result['msg'] = 'error when deleting!';

		//FCPATH . "uploads/" . $picname
		$image = $this->input->post('image_subcategory');
		$id = $this->input->post('id_subcategory');
		$file = "uploads/sous-categories/" . $image;
 
		if($this->subcategory->isExistInSpent($id)){

			$result['msg'] = "Ce sous-catégorie est utilisé dans votre dépense et ne peut pas être supprimé !";

		}else{
			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				$query = $this->subcategory->deleteSubcategory($id);
				if($query){
					$result['error'] = false;
				}
				$result['msg'] = "file has been deleted.";
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
			}
		}

		echo json_encode($result);

	}

}
    
?>