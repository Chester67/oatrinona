<?php

?>

<script type="text/x-template" id="template-application-detail">

    <div>
        <div style="display: flex; flex-direction: column; justify-content: center; align-items: center; ">
            
            <v-icon size="72px">people</v-icon>
            <label>Application test</label>
        
        </div>
    </div>

</script>

<script type="text/javascript">

    Vue.component('application-detail', {
        template: '#template-application-detail',
        data: function () {
            
            return{
                title: '',
            }
            
        },
        methods: {
           
           
        },
        mounted: function () {
            
        },
        
    });

</script>