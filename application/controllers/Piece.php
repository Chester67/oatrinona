<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piece extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('piece_model', 'piece');
    }

    public function createPiece()
    {
        $result['error'] = true;
        $result['msg'] = 'Error when adding piece';
        $id_inserted = 0;
        //$jsonPiece = $this->input->post('name_piece');
        $pieceArray = json_decode($jsonPiece, true);

        $this->load->library('upload', $config);

        for ($i = 0; $i < count($_FILES['files']['name']); $i++) {
            //Get the temp file path
            $tmpFilePath = $_FILES['files']['tmp_name'][$i];

            //Make sure we have a filepath
            if ($tmpFilePath != "") {
                //Setup our new file path
                $newFilePath = "./uploads/pieces/" . $_FILES['files']['name'][$i];

                //Upload the file into the temp dir
                if (move_uploaded_file($tmpFilePath, $newFilePath)) {

                    $newpiece = array(
                        'namepiece' => $_FILES['files']['name'][$i],
                        'eventId' => $this->input->post('event_id'),
                        'userId' => $this->input->post('user_id'),
                    );
                    $id_inserted = $this->piece->createPiece($newpiece);

                }
            }
        }

        if ($id_inserted != 0) {
            $result['error'] = false;
            $result['inserted_id'] = $id_inserted;
            $result['msg'] = 'Piece added successfully';
        }
        echo json_encode($result);
    }

    public function showPieceByEvent()
    {
        $eventId = $this->input->post('id_event');
        $query = $this->piece->showPieceByEvent($eventId);
        if ($query) {
            $result['pieces'] = $query;
        }
        echo json_encode($result);
    }

}
