<style type="text/css">
    html { height: 100%; }
    body {height: 100%; }
</style>

<script type="text/x-template" id="template-data-empty">

    <div style="display: flex; align-items: center; justify-content: center; height: 400px;">
        <span style="font-weight: bold">Oops! Aucune donnée disponible !</span> 
    </div>

</script>

<script type="text/javascript">

    Vue.component('data-empty', {
        template: '#template-data-empty',
        data: function () {
            
            return{
                name: '',
            }
            
        },
        methods: {
           
        },
        
    });

</script>