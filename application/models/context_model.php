<?php

class Context_model extends CI_Model{

    public function showAll(){
        $query = $this->db->get('context');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

}
