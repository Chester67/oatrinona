<?php

class Task_model extends CI_Model{

    public function showAll(){
        $query = $this->db->get('task');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function addTask($data){
        $insert = $this->db->insert('task', $data);
        return $insert;
    }

    public function isExist($label_task){
        $this->db->select('*');
        $this->db->from('task');
        $this->db->where('taskLabel', $label_task);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

    public function updateTask($data, $id){
        $updated = $this->db->update('task', $data, array('id' => $id));
        return $updated;
    }

    public function deleteTask($id){
        $deleted = $this->db->delete('task', array('id' => $id));
        return $deleted;
    }
    
    public function containSubtask($id_task){
        $this->db->select('*');
        $this->db->from('subtask');
        $this->db->where('taskId', $id_task);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

}

?>
