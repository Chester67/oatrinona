<?php

class WsResponse {

	public function getError($message, $errorcode)
	{
		$error = array(
            'success' => false,
            'message' => $message,
            'code' => $errorcode,
        );
        return $error;
    }

    public function getSuccess($message, $code, $objkey, $objvalue)
	{
		$success = array(
            'success' => true,
            'message' => $message,
            'code' => $code,
            $objkey => $objvalue
        );
        return $success;
    }

    
}

?>