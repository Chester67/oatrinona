<?php

class Subtasks extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('subtask_model','subtask');
    }

    public function index()
	{
		$this->load->view('welcome');
	}

	public function showByTaskId(){
		$taskId = $this->input->post('task_id');
		$query = $this->subtask->showByTaskId($taskId);
		if($query){
			$result['subtask'] = $query;
		}
		echo json_encode($result);
	}

	public function showAll(){
		$query = $this->subtask->showAll();
		if($query){
			$result['subtask']  = $query;
		}
		echo json_encode($result);
	}

	public function updateSubtask(){

		$result['error'] = true;
		$result['msg'] = "failed when update subtask";
		$result['code_error'] = 0;

		$id = $this->input->post('id_subtask');
		$label = $this->input->post('label_subtask');
		$has_image = $this->input->post('has_image');
		$new_image_name = $this->input->post('new_image_name_subtask');
		$old_image_name = $this->input->post('old_image_name_subtask');
		
		$data = array(
			'subtaskLabel' => $label,
			'subtaskImage' => $new_image_name
        );
		$query = $this->subtask->updateSubtask($data, $id);

		if($has_image === 'TRUE'){
			
			$file = "uploads/sous-taches/" . $old_image_name;
 			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				
				//add subtask img 
				$config['upload_path']          = './uploads/sous-taches/';
				$config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['max_size']             = 500;
				$config['max_width']            = 2500;
				$config['max_height']           = 1400;
	
				$this->load->library('upload', $config);
	
				if ( ! $this->upload->do_upload('image_subtask')) {
			
					$result['msg'] = 'Erreur lors du upload';
					$result['code_error'] = 2;
					
				} else {

					$result['msg'] = 'Upload success';
					$result['code_error'] = 1;

				}
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
				$result['code_error'] = 3;
			}

		}

		if($query){
			$result['error'] = false;
		}
		echo json_encode($result);

	}

	public function deleteSubtask(){

		$result['error'] = true;
		$result['file_deleted'] = false;
		$result['msg'] = 'error when deleting!';

		$image = $this->input->post('image_subtask');
		$id = $this->input->post('id_subtask');
		$file = "uploads/sous-taches/" . $image;
 
		if($this->subtask->isExistInSpent($id)){

			$result['msg'] = "Cette sous-tâche est utilisée dans votre dépense et ne peut pas être supprimé !";

		}else{
			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				$query = $this->subtask->deleteSubtask($id);
				if($query){
					$result['error'] = false;
				}
				$result['msg'] = "file has been deleted.";
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
			}
		}

		echo json_encode($result);

	}

}

?>