<?php
$this->load->view('components/empty/data_empty');
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

    .sticky-report-day-context {
        position: -webkit-sticky;
        position: sticky;
        top: 60px;
        top-max: 60px;
        top-min: 60px;

    }

    .sticky-report-day {
        position: -webkit-sticky;
        position: sticky;
        width: 100%;
        top: 70px;
        top-max: 70px;
        top-min: 70px;
        height: 60px;
    }

    div.centre-div {
        width: 200px;
        display: block;
        background-color: #eee;
        height: 30px;
        margin-left: auto;
        margin-right: auto;
        text-align: center;

    }

    .v-stepper, .v-stepper__header {
        box-shadow: 0 0 0 0 transparent, 0 0 0 0 transparent, 0 0 0 0 transparent;
    }

    .theme--light .v-stepper {
         background: #ffffff;
    }

    .text {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        line-height: 16px;     /* fallback */
        white-space: normal;
        word-wrap: break-word;
        -webkit-line-clamp: 2; /* number of lines to show */
        -webkit-box-orient: vertical;
        max-width: 85px;
        color: white;
        font-weight: normal;
        background: #78909C;
        border-radius: 19px;
        padding-left: 10px;
        padding-right: 7px;
        padding-top: 4px;
        padding-bottom: 4px;

    }

    .prix_report {
        position: relative;
        margin-left: 0px;
        height: 100%;
    }

    .textprice_report {
        position: absolute;
        top: 7px;
        left: 3px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .card-report {
        background: #fff;
        border-radius: 1px;
        margin: 1rem;
        position: relative;
        padding-top: 14px;
        padding-bottom: 14px;
    }

    .card-1-report {
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    }

    .a {
        width: 100%;
        margin: 0 auto;
        position: relative;
        display: table;
    }

    .c {
        display: table-cell;
        width:83%;
        height: 20px !important;
        margin-top: -5px;
    }

    .d {
        text-align: right;
        float: right;
    }

    .e {
        position: absolute;
        left: 50%;
        display: inline;
        width: auto;
        transform: translateX(-50%);
    }

    .centered-text {
        line-height: 30px;
        vertical-align: middle;
        text-align: center;
        font-size: 13sp;
        font-weight: bold;
        color: #90A4AE;
    }

</style>

<script type="text/x-template" id="template-report-day">

    <div style="background:#ffffff; height: 100%">
        <v-container v-if="!data_no_user" fluid>

            <v-stepper v-if="selected_report === 'context'" class="sticky-report-day-context" style="margin-top: -6px; padding-right: 15px; padding-left: 15px; margin-left: -16px; margin-right: -16px">

                <v-toolbar style="height: 60px;">

                    <v-dialog
                        ref="dialog"
                        v-model="modal"
                        :return-value.sync="date"
                        persistent
                        lazy
                        full-width
                        width="520px"
                        justify-center
                        style="width: 100% !important">

                            <!--<div slot="activator" style="display: flex; flex-direction: row; justify-content: center; align-items: center">
                                <v-icon>event</v-icon>
                                <h1 style="margin-left: 20px">Date : {{ date | format_date }}</h1>
                            </div>-->

                            <div slot="activator" class="a">
                                <div class="c">
                                    <h1>Date : {{ getDayOfDate(date) }} {{ getMonthByInt(getMonthOfDate(date)) }} {{ getYearOfDate(date) }}</h1>
                                </div>
                                <div class="hidden-sm-and-down" style="display: table-cell; width:83%; height: 20px !important; margin-top: -5px; text-align: right; float: right;">
                                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 3px; margin-right: 90px">
                                        <v-icon>event</v-icon>
                                        <h1 style="margin-left: 5px">{{ date | format_date }}</h1>
                                    </div>
                                </div>
                            </div>

                            <v-date-picker v-model="date" scrollable full-width locale="fr-fr">
                                <v-spacer></v-spacer>
                                <v-btn flat color="primary" @click="modal = false">Annuler</v-btn>
                                <v-btn flat color="primary" @click="requestByDate(date)">OK</v-btn>
                            </v-date-picker>
                    </v-dialog>

                    <v-menu style="position: absolute; right: 25px">
                        <v-btn
                            slot="activator"
                            dark
                            icon>
                            <v-icon style="color: grey">more_vert</v-icon>
                        </v-btn>
                        <v-list>
                            <v-list-tile v-for="(item, i) in menus" :key="i" @click="manageReportContextualMenu(item)">
                                <v-list-tile-action>
                                    <v-icon>{{ item.icon }}</v-icon>
                                </v-list-tile-action>
                                <v-list-tile-content>
                                    <v-list-tile-title>
                                        <span class="menu-text-font">{{ item.title }}</span>
                                    </v-list-tile-title>
                                </v-list-tile-content>
                            </v-list-tile>
                        </v-list>
                    </v-menu>

                </v-toolbar>

                <v-stepper-header style="width: 100%; ">
                    <!--morning-->
                    <v-stepper-step step="1">Matin</v-stepper-step>

                    <v-divider></v-divider>

                    <!--midday-->
                    <v-stepper-step step="2">Midi</v-stepper-step>

                    <v-divider></v-divider>

                    <!--evening-->
                    <v-stepper-step step="3">Soir</v-stepper-step>
                </v-stepper-header>

            </v-stepper>

            <v-toolbar v-else class="sticky-report-day-context" style="margin-top: -6px; height: 60px;">

                <v-dialog
                    ref="dialog"
                    v-model="modal"
                    :return-value.sync="date"
                    persistent
                    lazy
                    full-width
                    width="520px"
                    justify-center
                    style="width: 100% !important">

                        <div slot="activator">
                            <div style="vertical-align: middle; float: right; margin-right: 60px">
                                <div class="hidden-sm-and-down" style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 3px">
                                    <v-icon>event</v-icon>
                                    <h1 style="margin-left: 5px">{{ date | format_date }}</h1>
                                </div>
                            </div>
                            <!--<div class="hidden-sm-and-down" style="display: table-cell; width:83%; height: 20px !important; margin-top: -5px; text-align: right; float: right;">
                                <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 3px; margin-right: 90px">
                                    <v-icon>event</v-icon>
                                    <h1 style="margin-left: 5px">{{ date | format_date }}</h1>
                                </div>
                            </div>-->
                        </div>

                        <v-date-picker v-model="date" scrollable full-width locale="fr-fr">
                            <v-spacer></v-spacer>
                            <v-btn flat color="primary" @click="modal = false">Annuler</v-btn>
                            <v-btn flat color="primary" @click="requestByDate(date)">OK</v-btn>
                        </v-date-picker>

                </v-dialog>

                <v-menu style="position: absolute; right: 25px">
                    <v-btn
                        slot="activator"
                        dark
                        icon>
                        <v-icon style="color: grey">more_vert</v-icon>
                    </v-btn>
                    <v-list>
                        <v-list-tile v-for="(item, i) in menus" :key="i" @click="manageReportContextualMenu(item)">
                            <v-list-tile-action>
                                <v-icon>{{ item.icon }}</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                                <v-list-tile-title>
                                    <span class="menu-text-font">{{ item.title }}</span>
                                </v-list-tile-title>
                            </v-list-tile-content>
                        </v-list-tile>
                    </v-list>
                </v-menu>

                <div style="position: absolute; left: 25px; cursor: pointer" @click="modal = true">
                    <h1>Date : {{ getDayOfDate(date) }} {{ getMonthByInt(getMonthOfDate(date)) }} {{ getYearOfDate(date) }}</h1>
                </div>

            </v-toolbar>

            <template v-if="selected_report === 'context'">

                <template v-if="category_expenses.morning.length > 0 || category_expenses.midday.length > 0 || category_expenses.evening.length > 0">

                    <v-layout fluid wrap>
                        <v-flex xs12 md4 :key="category.id" v-for="category in category_expenses">

                            <v-data-iterator
                                no-data-text=""
                                :items="category"
                                :rows-per-page-items="rowsPerPageItems"
                                :pagination.sync="pagination"
                                content-tag="v-layout"
                                row
                                hide-actions
                                wrap>

                                <v-flex
                                    slot="item"
                                    slot-scope="props"
                                    xs12>

                                    <v-layout row wrap class="card-report card-1-report" style="background:#ECEFF1;">

                                        <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                                            <div>
                                                <v-layout justify-center fill-height align-center>
                                                    <div style="display:block; margin:0 auto; text-align: center;">

                                                        <img :src="urlBaseImage+'/uploads/categories/'+props.item.category.categoryImage" alt="" style="margin-top: 5px;width:42%;height:42%;object-fit:contain">
                                                        <p class="text-xs-center" style="margin-top: -4px; font-weight: bold; font-size: 13px">{{ props.item.category.categoryLabel }}</p>

                                                    </div>
                                                </v-layout>
                                            </div>
                                        </v-flex>

                                        <v-flex d-flex xs12 sm6 md9>

                                            <v-layout row wrap >

                                                <template v-for="subcategory in props.item.subcategories">

                                                    <v-layout row style="min-height: 27px; max-height: 41px; width: 100%; margin-top: 2px">
                                                        <v-flex xs12 sm6 md1 style="display: flex; align-items: center;">
                                                            <div :style="`border-radius: 50%; width: 15px; height: 15px; background: ${subcategory.utility.contextImage}`"></div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md5 style="display: flex; align-items: center;">
                                                            <div>

                                                                <div class="text">{{ subcategory.subcategory_label }}</div>

                                                            </div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md4>
                                                            <div class="prix_report">
                                                                <span style="height: 100%; display: flex; align-items: center; font-size: 13px; color: #78909C">{{ subcategory.price }} Ar</span>
                                                            </div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md2>
                                                            <span style="height: 100%; text-align: left; display: flex; align-items: center; font-size: 13px; color: #546E7A">x {{ subcategory.number }}</span>
                                                        </v-flex>
                                                    </v-layout>

                                                </template>

                                            </v-layout>

                                        </v-flex>

                                    </v-layout>

                                </v-flex>

                            </v-data-iterator>

                        </v-flex>

                    </v-layout>

                </template>
                <template v-else>
                    <data-empty></data-empty>
                </template>

            </template>
            <template v-else-if="selected_report === 'task'">


                <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timeline/style.css">
                <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timeline/demo.css">
                <template v-if="alltasks && alltasks.length > 0">

                    <section class="cd-timeline js-cd-timeline">

                        <div class="cd-timeline__container">

                            <template v-for="task in alltasks">
                                <div class="cd-timeline__block js-cd-block">
                                    <div class="cd-timeline__img cd-timeline__img--picture" style="display: flex; justify-content: center; align-items: center">
                                        <!--<img src="<?php echo base_url(); ?>assets/img/cd-icon-location.svg" alt="Picture">-->
                                        <v-icon style=" vertical-align: middle;">access_time</v-icon>
                                    </div> <!-- cd-timeline__img -->

                                    <v-card class="cd-timeline__content js-cd-content elevation-4">
                                        <h2 style="margin-left: 5px; font-size: 16px; color: #757575; font-weight: bold;">Tâche : {{ task.details.subtaskLabel }} ({{ task.details.taskLabel }})</h2>
                                        <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                                            <v-icon>assignment_ind</v-icon>
                                            <h1 class="task-right-text" style="margin-left: 5px">{{ getAssignedUserName(task.details.assignedUser, task.details.assignedUserId) }} ({{ getStatutLetter(task.details.eventStatut) }})</h1>

                                        </div>
                                        <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                                            <v-icon>place</v-icon>
                                            <h1 class="task-right-text" style="margin-left: 5px">{{ task.details.place_description }}</h1>

                                        </div>
                                        <div style="margin-top: 13px">
                                            <h3 class="task-right-text">Dépenses</h3>
                                        </div>

                                        <v-layout fluid wrap>

                                            <v-flex xs12 md12>

                                                <template v-for="taskspent in task.taskspents">

                                                    <v-flex xs12>

                                                        <v-layout row wrap>

                                                            <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                                                                <div>
                                                                    <v-layout justify-center fill-height align-center>
                                                                        <div style="display:block; margin:0 auto; text-align: center;">

                                                                            <img :src="urlBaseImage+'/uploads/categories/'+taskspent.category.categoryImage" alt="" style="margin-top: 5px;width:42%;height:42%;object-fit:contain">
                                                                            <p class="text-xs-center" style="margin-top: -4px; font-weight: bold; font-size: 12px">{{ taskspent.category.categoryLabel }}</p>

                                                                        </div>
                                                                    </v-layout>
                                                                </div>
                                                            </v-flex>

                                                            <v-flex d-flex xs12 sm6 md9>

                                                                <v-layout row wrap >

                                                                    <template v-for="subcategory in taskspent.subcategories">

                                                                        <v-layout row style="min-height: 27px; max-height: 41px; width: 100%; margin-top: 2px">
                                                                            <v-flex xs12 sm6 md1 style="display: flex; align-items: center;">
                                                                                <div :style="`border-radius: 50%; width: 15px; height: 15px; background: ${subcategory.utility.contextImage}`"></div>
                                                                            </v-flex>
                                                                            <v-flex xs12 sm6 md5 style="display: flex; align-items: center;">
                                                                                <div>

                                                                                    <div class="text-day-task">{{ subcategory.subcategory_label }}</div>

                                                                                </div>
                                                                            </v-flex>
                                                                            <v-flex xs12 sm6 md4>
                                                                                <div class="prix_report">
                                                                                    <span style="height: 100%; display: flex; align-items: center; font-size: 13px; color: #78909C">{{ subcategory.price }} Ar</span>
                                                                                </div>
                                                                            </v-flex>
                                                                            <v-flex xs12 sm6 md2>
                                                                                <span style="height: 100%; text-align: left; display: flex; align-items: center; font-size: 13px; color: #546E7A">x {{ subcategory.number }}</span>
                                                                            </v-flex>

                                                                        </v-layout>


                                                                    </template>

                                                                </v-layout>

                                                            </v-flex>

                                                        </v-layout>

                                                        <hr/>
                                                    </v-flex>

                                                </template>

                                            </v-flex>

                                        </v-layout>
                                        <!--<a href="#0" class="cd-timeline__read-more">Read more</a>-->
                                        <span class="cd-timeline__date">{{ task.details.eventDebut | getHourOfDate}}</span>
                                    </v-card> <!-- cd-timeline__content -->
                                </div> <!-- cd-timeline__block -->
                            </template>

                        </div>

                    </section> <!-- cd-timeline -->
                </template>
                <template v-else>
                    <data-empty></data-empty>
                </template>
            </template>
            <template v-else-if="selected_report === 'both'">
                <div>both</div>
            </template>

            <div style="position: absolute; bottom: 0; right: 0; left: 0; margin-left: 20px; margin-right: 20px">

                <div v-if="selected_report === 'context'" style="height: 65px; display: flex; flex-direction: column; background: #ECEFF1">
                    <div style="height: 30px;">
                        <v-flex xs12 md12 >
                            <div class="centered-text"><span style="color: #78909C">Total</span> : {{ total_price_matin + total_price_midi + total_price_soir }} Ar</div>
                        </v-flex>
                    </div>
                    <div style="height: 30px;">
                        <v-layout wrap>
                            <v-flex xs12 md4>
                                <div class="centered-text"><span style="color: #78909C">Matin</span> : {{ total_price_matin }} Ar</div>
                            </v-flex>
                            <v-flex xs12 md4>
                                <div class="centered-text"><span style="color: #78909C">Midi</span> : {{ total_price_midi }} Ar</div>
                            </v-flex>
                            <v-flex xs12 md4>
                                <div class="centered-text"><span style="color: #78909C">Soir</span> : {{ total_price_soir }} Ar</div>
                            </v-flex>
                        </v-layout>
                    </div>
                </div>
                <div v-else-if="selected_report === 'task'" style="height: 35px; display: flex; flex-direction: column; background: #ECEFF1">
                    <div style="height: 30px;">
                        <v-flex xs12 md12 >
                            <div class="centered-text"><span style="color: #78909C">Total</span> : {{ total_price_task }} Ar</div>
                        </v-flex>
                    </div>
                </div>

            </div>

        </v-container>

        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_list_spent = '<?php echo URL_LIST_SPENT ?>';
    var url_list_spent_final = `${url_base}${url_list_spent}`;

    var url_list_day_task = '<?php echo URL_LIST_DAY_TASK ?>';
    var url_list_day_task_final = `${url_base}${url_list_day_task}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    Vue.component('report-day', {
        template: '#template-report-day',
        mixins: [common_mixin],
        data: function () {

            return{
                date: new Date().toISOString().substr(0, 10),
                modal: false,
                rowsPerPageItems: [100],
                pagination: {
                    rowsPerPage: 100
                },

                urlBaseImage: url_base,
                category_expenses : {
                    morning: [],
                    midday: [],
                    evening: []
                },
                user_logged_in: undefined,

                menus: [
                    //{ id: 1, icon: 'timeline', title: 'Contexte & Tâche' },
                    { id: 2, icon: 'equalizer', title: 'Contexte' },
                    { id: 3, icon: 'equalizer', title: 'Tâche' },
                ],
                selected_report: 'context',

                alltasks: [],

                total_price_matin: 0,
                total_price_midi: 0,
                total_price_soir: 0,

                total_price_task: 0,

            }

        },
        methods: {
            manageReportContextualMenu: function (item){
               if(item.id == 1){
                 this.selected_report = 'both';
                 this.$router.app.$emit('updateWelcomeSubtitle', "par contexte et tâche");
               }else if(item.id == 2){
                 this.selected_report = 'context';
                 this.$router.app.$emit('updateWelcomeSubtitle', "par contexte");
               }else if(item.id == 3){
                 this.selected_report = 'task';
                 this.$router.app.$emit('updateWelcomeSubtitle', "par tâche");
               }
            },
            wsDepense: function(spentDate) {
                this.total_price_matin = 0;
                this.total_price_midi = 0;
                this.total_price_soir = 0;
                var formData = new FormData();
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_date', spentDate);
                window.axios.post(url_list_spent_final, formData).then((response) => {
                    //console.log(`SPENT LIST=====>${JSON.stringify(response.data.spents)}`);
                    if(response.data.spents){

                        //-----------------------MORNING LIST------------------------------
                        let by_typeday_morning = this.regroupSpentByTypeDay(response.data.spents, 'morning');
                        var ids_morning = this.takeAllCatId(by_typeday_morning);
                        var filtered_morning = ids_morning.filter((v,i) => ids_morning.indexOf(v) == i);
                        if(filtered_morning && filtered_morning.length > 0){
                            for(var i= 0; i < filtered_morning.length; i++){
                                this.category_expenses.morning.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_morning, filtered_morning[i])));
                            }
                        }
                        if(by_typeday_morning && by_typeday_morning.length > 0){
                            this.total_price_matin = this.regroupPriceBySpent(by_typeday_morning);
                        }

                        //---------------------MIDDAY LIST----------------------------------
                        let by_typeday_midday = this.regroupSpentByTypeDay(response.data.spents, 'midday');
                        var ids_midday = this.takeAllCatId(by_typeday_midday);
                        var filtered_midday = ids_midday.filter((v,i) => ids_midday.indexOf(v) == i);
                        if(filtered_midday && filtered_midday.length > 0){
                            for(var i= 0; i < filtered_midday.length; i++){
                                this.category_expenses.midday.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_midday, filtered_midday[i])));
                            }
                        }
                        if(by_typeday_midday && by_typeday_midday.length > 0){
                            this.total_price_midi = this.regroupPriceBySpent(by_typeday_midday);
                        }

                        //---------------------EVENING LIST----------------------------------
                        let by_typeday_evening = this.regroupSpentByTypeDay(response.data.spents, 'evening');
                        var ids_evening = this.takeAllCatId(by_typeday_evening);
                        var filtered_evening = ids_evening.filter((v,i) => ids_evening.indexOf(v) == i);
                        if(filtered_evening && filtered_evening.length > 0){
                            for(var i= 0; i < filtered_evening.length; i++){
                                this.category_expenses.evening.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_evening, filtered_evening[i])));
                            }
                        }
                        if(by_typeday_evening && by_typeday_evening.length > 0){
                            this.total_price_soir = this.regroupPriceBySpent(by_typeday_evening);
                        }

                        //pagination control
                        var total_length = this.category_expenses.morning.length + this.category_expenses.midday.length + this.category_expenses.evening.length;
                        var arr_rowsPerPageItems = [];
                        arr_rowsPerPageItems.push(total_length);
                        this.rowsPerPageItems = arr_rowsPerPageItems;
                        this.pagination.rowsPerPage = total_length;

                    }else{
                        console.log("+++++UNDEFINED+++++")
                    }
                })
                .catch(err => {
                    console.log(err)
                })
                .finally(() => this.data_loader = false)

            },
            requestByDate: function(calendarDate){
                this.$refs.dialog.save(calendarDate);
                console.log(`this is the date=====>${calendarDate}`);
                this.category_expenses.morning = [];
                this.category_expenses.midday = [];
                this.category_expenses.evening = [];
                this.alltasks = [];
                this.wsDepense(calendarDate);
                this.wsTaskList(calendarDate);
            },
            wsTaskList (spentDate) {
                this.total_price_task = 0;
                var formData = new FormData();
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_date', spentDate);
                window.axios.post(url_list_day_task_final, formData).then((response) => {

                    if(response.data.taskspents){
                        //console.log(`BEFORE REGROUP=====>${JSON.stringify(response.data.taskspents)}`);
                        var ids_event = this.takeAllEventId(response.data.taskspents);
                        var filtered_ids_event = ids_event.filter((v,i) => ids_event.indexOf(v) == i);

                        for(var i= 0; i < filtered_ids_event.length; i++){
                            this.alltasks.push(this.regroupTaskByEvent(this.regroupSpentByEvent(response.data.taskspents, filtered_ids_event[i])));
                            console.log(`FOR PRICE SUM=====>${JSON.stringify(this.regroupSpentByEvent(response.data.taskspents, filtered_ids_event[i]))}`);
                            this.total_price_task += this.regroupPriceBySpent(this.regroupSpentByEvent(response.data.taskspents, filtered_ids_event[i]));
                        }
                        this.alltasks.sort(function(a,b) {return a.hoursort - b.hoursort});
                        //console.log(`AFTER REGROUP=====>${JSON.stringify(this.regroupSpentByEvent(response.data.taskspents, filtered_ids_event[i]))}`);

                    }else{
                        console.log("DAY TASK LIST=====> UNDEFINED");
                        this.alltasks = [];
                    }

                }).catch(err => {
                    console.log(err)
                }).finally(() => this.data_loader = false)
            },
            checkIfMe: function(assigned_user_id, userobj){

                if(assigned_user_id === userobj.id){
                    return true;
                }

                return false;
            },
            getAssignedUserName: function(assigned_user_name, assigned_user_id){
                var fullname = 'Moi';
                if(!this.checkIfMe(assigned_user_id, this.user_logged_in)){
                    fullname = assigned_user_name;
                }
                return fullname;
            },
            regroupPriceBySpent: function(spents){
                var total = 0;
                for(var i= 0; i < spents.length; i++){

                    var montant = 0;
                    let pr = Number(spents[i].number);
                    let pu = Number(spents[i].unitPrice);
                    montant = pu * pr;
                    total += montant;

                }
                return total;
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`REPORT DAY PAGE USER LOGGED  ===> ${JSON.stringify(connexion.user_logged[0])}`);

                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }
                    let today = new Date().toISOString().slice(0, 10);
                    this.wsDepense(today);
                    this.wsTaskList(today);
                }else{
                    console.log("REPORT DAY PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            }

        },
        mounted: function () {
            console.log('report day : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },
        created: function () {
            this.$router.app.$emit('updateWelcomeSubtitle', "par contexte");
        }

    });

</script>