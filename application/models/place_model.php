<?php

class Place_model extends CI_Model
{

    public function createPlace($data)
    {
        $this->db->insert('place', $data);
        if ($this->db->affected_rows() > 0) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        return 0;
    }

    public function isExist($lat, $lng)
    {
        $this->db->select('*');
        $this->db->from('place');
        $this->db->where('place_lat', $lat);
        $this->db->where('place_lng', $lng);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if ($numrows > 0) {
            return $query->result();
        }
        return false;
    }

    public function isExistByDesc($desc)
    {
        $this->db->select('*');
        $this->db->from('place');
        $this->db->where('place_description', $desc);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if ($numrows > 0) {
            return $query->result();
        }
        return false;
    }

    public function showAll()
    {
        $query = $this->db->get('place');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
