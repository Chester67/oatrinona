<?php
$this->load->view('components/inputs/expense'); 

?>

<script type="text/x-template" id="template-time-expense">

    <div>
    
        <p>time expenses session user {{ savedUserIdSession }}</p>
        <expense></expense>
    
    </div>

</script>

<script type="text/javascript">

    Vue.component('time-expense', {
        template: '#template-time-expense',
        data: function(){
            return {
                savedUserIdSession: ''
                
            }
        },
        methods: {
           displaySessionVar () {
               
           },
           
        },
        created: function () {
            this.$forceUpdate();
        },
    });

</script>