<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Place extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('place_model', 'place');
    }

    public function createPlace()
    {
        $result['error'] = true;
        $result['msg'] = 'Error when adding place';
        $place_lat = $this->input->post('place_lat');
        $place_lng = $this->input->post('place_lng');
        $place_desc = $this->input->post('place_description');
        $type = $this->input->post('type');
        if ($type === 'WITH_CARTE') {
            $result = $this->place->isExist($place_lat, $place_lng);
            if ($result && $result[0]) {
                $result['error'] = false;
                $result['inserted_id'] = $result[0]->id;
                $result['msg'] = 'Place already exist';
            } else {
                $place = array(
                    'place_id' => $this->input->post('place_id'),
                    'place_description' => $place_desc,
                    'place_type' => $this->input->post('place_type'),
                    'place_lat' => $place_lat,
                    'place_lng' => $place_lng,
                    'place_formated_adress' => $this->input->post('place_formated_adress'),
                );
                $id_inserted = $this->place->createPlace($place);
                if ($id_inserted != 0) {
                    $result['error'] = false;
                    $result['inserted_id'] = $id_inserted;
                    $result['msg'] = 'Place added successfully';
                }
            }
        } else if ($type === 'NO_CARTE') {
            $result = $this->place->isExistByDesc($place_desc);
            if ($result && $result[0]) {
                $result['error'] = false;
                $result['inserted_id'] = $result[0]->id;
                $result['msg'] = 'Place already exist';
            } else {
                $place = array(
                    'place_id' => $this->input->post('place_id'),
                    'place_description' => $place_desc,
                    'place_type' => $this->input->post('place_type'),
                    'place_lat' => 0,
                    'place_lng' => 0,
                    'place_formated_adress' => $this->input->post('place_formated_adress'),
                );
                $id_inserted = $this->place->createPlace($place);
                if ($id_inserted != 0) {
                    $result['error'] = false;
                    $result['inserted_id'] = $id_inserted;
                    $result['msg'] = 'Place added successfully';
                }
            }
        }
        echo json_encode($result);
    }

    public function showAll()
    {
        $result['places'] = [];
        $query = $this->place->showAll();
        if ($query) {
            $result['places'] = $this->place->showAll();
        }
        echo json_encode($result);
    }

}
