<?php

class Category_model extends CI_Model{

    public function showAll(){
        $query = $this->db->get('category');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function addCategory($data){
        $insert = $this->db->insert('category', $data);
        return $insert;
    }

    public function isExist($label_category){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('categoryLabel', $label_category);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

    public function updateCategory($data, $id){
        $updated = $this->db->update('category', $data, array('id' => $id));
        return $updated;
    }

    public function deleteCategory($id){
        $deleted = $this->db->delete('category', array('id' => $id));
        return $deleted;
    }
    
    public function containSubcategory($id_category){
        $this->db->select('*');
        $this->db->from('subcategory');
        $this->db->where('categoryId', $id_category);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

}

?>