<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

    .event-todo{
        background: #f9944a !important;
    }

    .event-inprogress{
        background: #4A9FF9 !important;
    }

    .event-finished{
        background: #2ac06d !important;
    }

    .event-validated{
        background: #BDBDBD !important;
    }

    .event-rejected{
        background: #c91010 !important;
    }

    .comment-accordion {
        margin-top: 5px !important;
        margin-bottom: 10px !important;
        margin-left: 5px !important;
        margin-right: 5px !important;
    }

    .comment-accordion .header {
        height: 40px !important;
        line-height: 40px !important;
    }

    .comment-accordion .header-icon {
        top: 4px !important;
        display: none;
    }

    .header-comment {
        padding-left: 19px;
        padding-top: 8px;
        padding-bottom: 8px;
    }

    table.v-table tbody td, table.v-table tbody th {
        height: 34px !important;
    }

    table.v-table thead tr {
        height: 34px;
    }

    .application .theme--light.v-table, .theme--light .v-table {
        background-color: #F7F8F8 !important;
    }

    .statut-item a {
        text-transform: uppercase !important;
        height: 25px !important;
        font-size: 13px;
        font-weight: bold;
    }

</style>

<script type="text/x-template" id="template-task-planning">

    <div>
        <template v-if="!data_no_user">
            <fullcalendar @update_loading="updateLoading" @show_dialog_planning="showDialogPlanning" ref="calendar" :user_id="user_logged_in.id" style="margin-top: 20px; margin-left: 30px; margin-right: 30px">
            </fullcalendar>
        </template>
        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>
        <v-dialog v-model="dialog_planning" width="600px" style="background: #ffffff;">

                <v-card class="elevation-4" style="padding: 25px">
                    <div style="display: flex; flex-direction: row; height: 30px;">
                        <h2 v-if="planning_event" style="margin-left: 5px; font-size: 16px; color: #757575; font-weight: bold; line-height:30px; vertical-align:middle">
                            Tâche : {{ planning_event.subtaskLabel }}
                            <!--<span :class="getClassNameByStatut(planning_event.state)" style="border-radius: 2px; margin-left: 5px; font-weight: bold; color: white; padding: 4px"> {{ planning_event.eventStatut }} </span>-->
                        </h2>
                        <v-menu v-if="planning_event" offset-y>
                            <template slot="activator">
                                <v-btn
                                style="height: 25px !important; color: white !important; font-weight: bold"
                                :class="getClassNameByStatut(planning_event.state)"
                                >
                                {{ planning_event.eventStatut }}
                                </v-btn>
                            </template>
                            <v-list style="padding: 3px 0 !important;">
                                <v-list-tile
                                    class="statut-item"
                                    v-for="(item, index) in available_next_status"
                                    :key="index"
                                    @click="updateEventStatut(planning_event.ticketId, item.state)"
                                >
                                    <v-list-tile-title>{{ item.stateLabel }}</v-list-tile-title>
                                </v-list-tile>
                            </v-list>
                        </v-menu>
                    </div>
                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                        <v-icon>assignment_ind</v-icon>
                        <div v-if="planning_event" >
                            <h1 class="task-right-text" style="margin-left: 5px">{{ planning_event.assignedUser }}</h1>
                        </div>


                    </div>
                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                        <v-icon>place</v-icon>
                        <h1 v-if="planning_event" class="task-right-text" style="margin-left: 5px">{{ planning_event.placeDescription }}</h1>

                    </div>

                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 5px">
                        <v-icon>attach_money</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px"><span style="width: 150px">Dépenses effectuées</span><hr style='display:inline-block; width:350px;' /></div>
                    </div>

                    <v-layout fluid wrap>

                        <v-flex xs12 md12 v-if="planning_event" style="margin-top: 10px">

                            <template v-for="(taskspent, index) in planning_event.taskspents">

                                <v-flex xs12>

                                    <v-layout row wrap>

                                        <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                                            <div>
                                                <v-layout justify-center fill-height align-center>
                                                    <div style="display:block; margin:0 auto; text-align: center;">

                                                        <img :src="urlBaseImage+'/uploads/categories/'+taskspent.category.categoryImage" alt="" style="margin-top: 5px;width:42%;height:42%;object-fit:contain">
                                                        <p class="text-xs-center" style="margin-top: -4px; font-weight: bold; font-size: 12px">{{ taskspent.category.categoryLabel }}</p>

                                                    </div>
                                                </v-layout>
                                            </div>
                                        </v-flex>

                                        <v-flex d-flex xs12 sm6 md9>

                                            <v-layout row wrap >

                                                <template v-for="subcategory in taskspent.subcategories">

                                                    <v-layout row style="min-height: 27px; max-height: 41px; width: 100%; margin-top: 2px">
                                                        <v-flex xs12 sm6 md1 style="display: flex; align-items: center;">
                                                            <div :style="`border-radius: 50%; width: 15px; height: 15px; background: ${subcategory.utility.contextImage}`"></div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md5 style="display: flex; align-items: center;">
                                                            <div>

                                                                <div class="text-day-task">{{ subcategory.subcategory_label }}</div>

                                                            </div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md4>
                                                            <div class="prix_report">
                                                                <span style="height: 100%; display: flex; align-items: center; font-size: 13px; color: #78909C">{{ subcategory.price }} Ar</span>
                                                            </div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md2>
                                                            <span style="height: 100%; text-align: left; display: flex; align-items: center; font-size: 13px; color: #546E7A">x {{ subcategory.number }}</span>
                                                        </v-flex>

                                                    </v-layout>


                                                </template>

                                            </v-layout>

                                        </v-flex>

                                    </v-layout>

                                    <hr v-if="index != (planning_event.taskspents.length - 1)" />

                                </v-flex>

                            </template>

                        </v-flex>

                    </v-layout>

                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 5px">
                        <v-icon>attach_file</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px;"><span style="width: 125px">Pièce(s) jointe(s)</span><hr style='display:inline-block; width:375px;' /></div>
                    </div>

                    <div>
                        <template v-if="list_pieces.length > 0" v-for="(piece, index) in list_pieces">
                            <v-tooltip top>
                                <v-chip
                                    slot="activator"
                                    class="white--text"
                                    style="height: 33px; background: #BDBDBD; cursor: pointer;"
                                    @click="downloadWithAxios(piece.namepiece)">

                                    <v-icon left>mdi-coin</v-icon>
                                    <span style="font-weight: normal; font-size: 13px; color: #455A64; overflow:hidden; text-overflow:ellipsis; max-width:180px;">{{ piece.namepiece }}</span>

                                </v-chip>
                                <span> <b>Auteur : </b>{{ piece.first_name }} {{ piece.last_name }} <br> <b>Date : </b>{{ formatDateToMMDDYYYY(piece.datepiece) }} </span>
                            </v-tooltip>
                        </template>
                        <div style="display: flex; align-items: center">

                            <v-chip
                                class="white--text"
                                style="height: 33px; background: #BDBDBD; cursor: pointer"
                                @click="pickFile()">

                                <v-icon left>add</v-icon>
                                <span style="font-weight: bold; font-size: 15px; color: #fff; margin-left: -10px">Ajouter</span>

                            </v-chip>

                        </div>
                        <input
                            type="file"
                            style="display: none"
                            ref="addfile"
                            multiple
                            @change="onFilePicked"
                        >
                    </div>

                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 13px">
                        <v-icon>comment</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px"><span style="width: 120px">Commentaire(s)</span><hr style='display:inline-block; width:380px;' /></div>
                    </div>

                    <v-layout fluid wrap style="margin-top: 10px">

                        <v-flex xs12>
                            <template v-for="(comment, index) in list_comments">

                                <v-card v-if="comment" :ripple="true" class="elevation-0" style="border-radius: 5px;margin-top: 6px; margin-left: 6px; margin-right: 6px; padding-top: 10px; padding-bottom: 17px; padding-left: 8px; padding-right: 8px; background: #bcc4c61F">

                                    <div style="padding-top: 6px; padding-left: 10px; padding-right: 50px; padding-bottom: 20px">
                                        {{ comment.comment }}
                                    </div>

                                    <div style="position: absolute; right: 5px; bottom: 1px; height: 40px; display: flex; align-items: center;">
                                        <div style="display: inline-block; float: left; height: 15px; margin-right: 7px; font-size: 10px">
                                        {{ getLocalLongDate(comment.datecomment) }}
                                        </div>
                                        <div style="display: inline-block; float: left; height: 34px;">
                                            <img v-if="!!comment.user_image" :src="urlBaseImage+'uploads/utilisateurs/'+comment.user_image" alt="" width="34" height="34" style="margin-right: 7px">
                                            <v-icon v-else style="font-size: 40px; margin-right: 4px">face</v-icon>
                                        </div>
                                    </div>

                                </v-card>

                            </template>

                            <accordion-comment class="comment-accordion" theme="grey lighten-5" ref="refresh_week" style="border-radius:5px;">
                                <div slot="header" style="display: flex; flex-direction: row">
                                    <v-icon style="font-size: 22px; ">add_circle_outline</v-icon>
                                    <span style="font-weight: bold; color: rgb(117, 117, 117); margin-left: 8px">Ajouter un commentaire</span>
                                </div>
                                <v-layout row wrap style="padding-bottom: 10px">
                                    <v-flex xs10>
                                        <v-card class="elevation-2" style="margin-left: 7px; margin-top: 10px">
                                            <v-textarea
                                                style="padding-left: 10px; padding-right: 10px; padding-top: 10px"
                                                v-model="newtextcomment"
                                                >
                                            </v-textarea>
                                        </v-card>
                                    </v-flex>

                                    <v-flex xs2>
                                        <div style="display: flex; justify-content: center; margin-top: 14px">
                                            <v-btn
                                                v-if="planning_event"
                                                fab
                                                small
                                                color="blue-grey lighten-1"
                                                dark
                                                @click="addComment(planning_event.ticketId)">
                                                    <v-icon>add</v-icon>
                                            </v-btn>
                                        </div>
                                    </v-flex>
                                </v-layout>
                            </accordion-comment>

                        </v-flex>

                    </v-layout>

                    <div v-if="list_histories.length > 0" style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 5px">
                        <v-icon>history</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px"><span style="width: 100px">Historique(s)</span><hr style='display:inline-block; width:400px;' /></div>
                    </div>

                    <v-data-table
                        v-if="list_histories.length > 0"
                        :headers="headers"
                        :items="list_histories"
                        class="elevation-2"
                        :pagination.sync="pagination_history"
                        :total-items="list_histories.length"
                        :hide-actions="true"
                        style="margin-top: 7px; margin-left: 7px; margin-right: 7px; background: #F7F8F8; border-radius: 6px"
                    >
                        <template
                            slot="items"
                            slot-scope="props"
                            >
                            <td style="color: rgba(0,0,0,.54);">{{ props.item.first_name }} {{ props.item.last_name }}</td>
                            <td style="color: rgba(0,0,0,.54);">{{ formatDateToMMDDYYYY(props.item.datehistory) }}</td>
                            <td style="color: rgba(0,0,0,.54);">{{ getStatutLetter(props.item.statuthistory) }}</td>

                        </template>
                    </v-data-table>

                </v-card>

        </v-dialog>
    </div>

</script>

<template id="template-accordion-comment">
    <div style="margin-left: 4px; margin-right: 4px" v-bind:class="theme">
        <div class="header-comment" @click="toggle">
            <slot name="header">HINT</slot>
            <!--<i class="fa fa-2x fa-angle-down header-icon" v-bind:class="{ rotate: show }"></i>-->
            <v-icon class="header-icon" style="font-size: 33px;" v-bind:class="{ rotate: show }">keyboard_arrow_down</v-icon>
        </div>
        <transition name="accordion"
            v-on:before-enter="beforeEnter" v-on:enter="enter"
            v-on:before-leave="beforeLeave" v-on:leave="leave">
            <div class="body" v-show="show">
                <div class="body-inner">
                <slot></slot>
                </div>
            </div>
        </transition>
    </div>
</template>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_range_notification_task = '<?php echo URL_GET_RANGE_NOTIFICATION ?>';
    var url_range_notification_task_final = `${url_base}${url_range_notification_task}`;

    var url_other_range_notification_task = '<?php echo URL_GET_OTHER_RANGE_NOTIFICATION ?>';
    var url_other_range_notification_task_final = `${url_base}${url_other_range_notification_task}`;

    var url_list_comment = '<?php echo URL_LIST_COMMENT_BY_EVENT ?>';
    var url_list_comment_final = `${url_base}${url_list_comment}`;

    var url_create_comment = '<?php echo URL_CREATE_COMMENT ?>';
    var url_create_comment_final = `${url_base}${url_create_comment}`;

    var url_list_piece = '<?php echo URL_LIST_PIECE ?>';
    var url_list_piece_final = `${url_base}${url_list_piece}`;

    var url_list_history = '<?php echo URL_LIST_HISTORY ?>';
    var url_list_history_final = `${url_base}${url_list_history}`;

    var url_create_piece = '<?php echo URL_CREATE_PIECE ?>';
    var url_create_piece_final = `${url_base}${url_create_piece}`;

    var url_update_event_statut = '<?php echo URL_UPDATE_EVENT_STATUT ?>';
    var url_update_event_statut_final = `${url_base}${url_update_event_statut}`;

    var url_create_history = '<?php echo URL_CREATE_HISTORY ?>';
    var url_create_history_final = `${url_base}${url_create_history}`;

    Vue.component('task-planning', {
        template: '#template-task-planning',
        mixins: [common_mixin],
        data: function () {

            return {

                user_logged_in: undefined,
                dialog_planning: false,
                planning_event: null,
                urlBaseImage: url_base,
                list_comments: [],
                newtextcomment: null,
                list_pieces: [],
                list_histories: [],
                headers: [
                    {
                        text: 'Auteur',
                        align: 'left',
                        sortable: false,
                        value: 'autor'
                    },
                    {
                        text: 'Date',
                        align: 'left',
                        sortable: false,
                        value: 'date'
                    },
                    {
                        text: 'Statut',
                        align: 'left',
                        sortable: false,
                        value: 'statut'
                    }
                ],
                pagination_history: {},
                available_next_status: []
            }

        },
        methods: {
            updateLoading(){
                this.closeLoaderAfterDelay();
            },
            showDialogPlanning(eventObj){
                this.dialog_planning = true;
                this.planning_event = eventObj;
                this.getCommentWs(eventObj.ticketId);
                this.getAttachmentWs(eventObj.ticketId);
                this.getHistoryWs(eventObj.ticketId);

                this.available_next_status = [];
                if(eventObj.state === '<?php echo CONSTANT_TASK_TODO ?>'){
                    this.available_next_status.push({
                        state: '<?php echo CONSTANT_TASK_INPROGRESS ?>',
                        stateLabel: this.getStatutLetter('<?php echo CONSTANT_TASK_INPROGRESS ?>')
                    });
                }else if(eventObj.state === '<?php echo CONSTANT_TASK_INPROGRESS ?>'){
                    this.available_next_status.push({
                        state: '<?php echo CONSTANT_TASK_FINISHED ?>',
                        stateLabel: this.getStatutLetter('<?php echo CONSTANT_TASK_FINISHED ?>')
                    });
                }else if(eventObj.state === '<?php echo CONSTANT_TASK_FINISHED ?>'){
                    if(this.user_logged_in.role === 'ADMINISTRATOR'){
                        this.available_next_status.push({
                            state: '<?php echo CONSTANT_TASK_VALIDATED ?>',
                            stateLabel: this.getStatutLetter('<?php echo CONSTANT_TASK_VALIDATED ?>')
                        });
                    }
                }

            },
            getCommentWs(eventId){
                var formData = new FormData();
                formData.append('id_event', eventId);
                axios.post(url_list_comment_final, formData).then((response) => {
                    if(response.data.comments){
                        this.list_comments = response.data.comments;
                        console.log(`RESPONSE COMMENT ====> ${JSON.stringify(response.data.comments)}`);
                    }else{
                        this.list_comments = [];
                        console.log('pas de commentaire');
                    }
                })
                .finally(() => {

                });
            },
            getAttachmentWs(eventId){
                var formData = new FormData();
                formData.append('id_event', eventId);
                axios.post(url_list_piece_final, formData).then((response) => {
                    if(response.data.pieces){
                        this.list_pieces = response.data.pieces;
                        console.log(`RESPONSE PIECES ====> ${JSON.stringify(response.data.pieces)}`);
                    }else{
                        this.list_pieces = [];
                        console.log('pas de pieces');
                    }
                })
                .finally(() => {

                });
            },
            addComment(eventId){
                if(this.newtextcomment){
                    var formData = new FormData();
                    formData.append('text_comment', this.newtextcomment);
                    formData.append('event_id', eventId);
                    formData.append('user_id', this.user_logged_in.id);

                    axios.post(url_create_comment_final, formData).then((response) => {
                        if(response.data.error){
                            console.log(`MESSAGE ERROR=>${response.data.msg}`);
                        }else{
                            console.log(`COMMENT INSERTED ID =>${response.data.inserted_id}`);
                            this.getCommentWs(eventId);
                            this.newtextcomment = null;
                        }
                    });
                }
            },
            getHistoryWs(eventId){
                var formData = new FormData();
                formData.append('id_event', eventId);
                axios.post(url_list_history_final, formData).then((response) => {
                    if(response.data.history){
                        this.list_histories = response.data.history;
                        console.log(`RESPONSE HISTORY ====> ${JSON.stringify(response.data.history)}`);
                    }else{
                        this.list_histories = [];
                        console.log('pas de HISTORY');
                    }
                })
                .finally(() => {

                });
            },
            downloadWithAxios(filename){
                let fileurl = `${this.urlBaseImage}uploads/pieces/${filename}`;
                console.log(fileurl);
                axios({
                    method: 'get',
                    url: fileurl,
                    responseType: 'arraybuffer'
                })
                .then(response => {

                    this.forceFileDownload(filename, response)

                })
                .catch(() => console.log('error occured'))
            },
            forceFileDownload(filename, response){
                const url = window.URL.createObjectURL(new Blob([response.data]))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', filename) //or any other extension
                document.body.appendChild(link)
                link.click()
            },
            pickFile () {
                this.$refs.addfile.click();
                /*let datenorm = new Date();
                let datetimezone = new Date().toLocaleString("fr-FR", {timeZone: "Europe/Paris"});
                let partdate = datetimezone.split(" à ");
                let partdatefrench = partdate[0].split("/");
                let parthour = partdate[1];
                let parthourfrench = parthour.split(":");
                let dateObject = new Date(partdatefrench[2],partdatefrench[1]-1,partdatefrench[0], parthourfrench[0], parthourfrench[1], parthourfrench[2]);
                console.log(datenorm);
                console.log(datetimezone);
                console.log(dateObject);*/
            },
            onFilePicked(event){

                if(typeof event.target.files[0] !== 'undefined'){
                    console.log(`CHANGED IMG=====>${URL.createObjectURL(event.target.files[0])}`);
                    var formData = new FormData();
                    formData.append('files[0]', event.target.files[0]);
                    formData.append('event_id', this.planning_event.ticketId);
                    formData.append('user_id', this.user_logged_in.id);
                    axios.post(url_create_piece_final, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        if(response.data.error){
                            console.log(`MESSAGE ERROR=>${response.data.msg}`);
                        }else{
                            console.log(`PIECE INSERTED ID =>${response.data.inserted_id}`);
                            this.getAttachmentWs(this.planning_event.ticketId);
                        }
                    });

                }

            },
            updateEventStatut: function(eventId, eventStatut){

                var formData = new FormData();
                formData.append('id_event', eventId);
                formData.append('statut_event', eventStatut);
                axios.post(url_update_event_statut_final, formData).then((response) => {
                    console.log(`RESPONSE UPDATE STATUT ====> ${JSON.stringify(response.data.error)}`);
                    this.addHistory(eventStatut, eventId);
                })
                .finally(() => {

                });

            },
            addHistory(status, eventId){

                var formData = new FormData();
                formData.append('status_history', status);
                formData.append('event_id', eventId);
                formData.append('user_id', this.user_logged_in.id);
                axios.post(url_create_history_final, formData)
                    .then((response) => {
                        if(response.data.error){
                            console.log(`MESSAGE ERROR=>${response.data.msg}`);
                        }else{
                            console.log(`HISTORY INSERTED ID =>${response.data.inserted_id}`);

                        }
                    });

            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`PLANNING PAGE USER LOGGED  ===> ${JSON.stringify(connexion.user_logged[0])}`);

                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }

                }else{
                    console.log("PLANNING PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            },

        },
        mounted: function () {
            console.log('planning : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },

    });

    Vue.component('fullcalendar', {
        template: '<div id="cal">{{check}}</div>',
        mixins: [common_mixin],
        props: ['user_id'],
        data () {
            return {
                check: '',
                cal: null,
                allevents: [],
                task_status : [
                    '<?php echo CONSTANT_TASK_TODO ?>',
                    '<?php echo CONSTANT_TASK_INPROGRESS ?>',
                    '<?php echo CONSTANT_TASK_FINISHED ?>',
                    '<?php echo CONSTANT_TASK_VALIDATED ?>',
                ],

            }
        },
        methods: {
            getColorByStatut(stat){
                var res = '#BDBDBD';
                if(stat === '<?php echo CONSTANT_TASK_TODO ?>')
                    res = '#f9944a';
                else if(stat === '<?php echo CONSTANT_TASK_INPROGRESS ?>')
                    res = '#4A9FF9';
                else if(stat === '<?php echo CONSTANT_TASK_FINISHED ?>')
                    res = '#2ac06d';
                return res;
            },
            populateTasks(resp){
                var res = [];
                if(resp && resp.length > 0){

                    //var bystatut = this.groupTaskByStatut(resp, this.task_status[0]);
                    var ids_event = this.takeAllEventId(resp);
                    var filtered_ids_event = ids_event.filter((v,k) => ids_event.indexOf(v) == k);
                    for(var j= 0; j < filtered_ids_event.length; j++){
                        //this.task_managment.push(this.regroupTaskByEvent(this.regroupSpentByEvent(resp, filtered_ids_event[j])));
                        var data = this.regroupTaskByEvent(this.regroupSpentByEvent(resp, filtered_ids_event[j]));
                        //console.log(JSON.stringify(data));
                        let hour = data.details.eventDebut.split(" ")[1];
                        res.push(
                            {
                                title  : data.details.taskLabel,
                                start  : data.details.eventDebut.split(" ")[0] + 'T' + hour ,
                                end  : data.details.eventDebut.split(" ")[0] + 'T' + hour ,
                                backgroundColor : this.getColorByStatut(data.details.eventStatut),
                                borderColor: this.getColorByStatut(data.details.eventStatut),
                                customClass: this.getClassNameByStatut(data.details.eventStatut),
                                ticketId: data.details.eventId,
                                subtaskLabel: data.details.subtaskLabel,
                                heureLabel: hour.substring(0, hour.length-3),
                                placeDescription: data.details.place_description,
                                eventStatut: this.getStatutLetter(data.details.eventStatut),
                                taskspents: data.taskspents,
                                startrange: data.details.eventDebut.split(" ")[0],
                                state: data.details.eventStatut,
                                assignedUser: data.details.assignedUser
                            }
                        );
                    }
                    //console.log(JSON.stringify(res));

                }else{
                    console.log('RIEN');
                    this.data_loader = false;
                }
                //console.log(`RESPONSE 1 ====> ${JSON.stringify(res)}`);

                return res;
            },
            initFullCal(){

                var self = this;
                this.cal = $(self.$el)

                var args = {
                    locale: 'fr',
                    header: {
                        left:   'prev,next today',
                        center: 'title',
                        right:  ''
                    },
                    height: "auto",
                    allDaySlot: false,
                    slotEventOverlap: false,
                    timeFormat: 'HH:mm',
                    showNonCurrentDates: false,
                    slotDuration: '00:30:00',
                    eventRender: function(event, element, view) {
                        /*let begin = event.startrange.split("-")[1];
                        let viewbegin = moment(view.start).format("YYYY-MM-DD");
                        let vewbeginmonth = viewbegin.split("-")[1];
                        console.log(`begin : ${begin}`);
                        console.log(`view : ${vewbeginmonth}`);
                        if(Number(begin) !== Number(vewbeginmonth)) {
                            return false;
                        }*/
                        return self.getTemplate(event.ticketId, event.subtaskLabel, event.heureLabel, event.customClass);
                    },
                    eventClick: function(calEvent, jsEvent, view) {
                        console.log(calEvent);
                        self.$emit('show_dialog_planning', calEvent);
                    },
                    events: function(start, end, timezone, callback) {
                        let start_formated = moment(start).format("YYYY-MM-DD");
                        let end_formated = moment(end).format("YYYY-MM-DD");
                        console.log(`begin : ${start_formated}`);
                        console.log(`end : ${end_formated}`);
                        var events = [];

                        this.data_loader = true;
                        var formData = new FormData();
                        formData.append('user_id', self.user_id);
                        formData.append('notif_date_begin', start_formated);
                        formData.append('notif_date_end', end_formated);
                        axios.post(url_range_notification_task_final, formData).then((response) => {
                            //console.log(`RESPONSE GET NOTIF ====> ${JSON.stringify(response.data.notif)}`);
                            events = self.populateTasks(response.data.notif);
                            //console.log(`RESPONSE 2 ====> ${JSON.stringify(events)}`);
                            callback(events);
                        })
                        .finally(() => {
                            self.$emit('update_loading');
                            //self.closeLoaderAfterDelay();
                        });

                    },
                    dayClick (date, event, view) {
                        /*console.log(date, event, view)
                        console.log(date.format())*/
                    },
                    /*eventAfterAllRender (view) {
                        let start = moment(view.start).format("YYYY-MM-DD");
                        let end = moment(view.end).format("YYYY-MM-DD");
                        console.log('voaantso');
                    }*/
                }
                this.cal.fullCalendar(args);

            },
            getTemplate(ticket, subtask, hour, classname){

                return ['<div style="font-size: 11px; color: white; max-width: 170px; cursor: pointer; border-radius:6px; padding-left: 5px; padding-right: 5px; margin: 1px;" class="' + classname +'">',
                        '<div>',
                        '<b>TIK-' + ticket +  '</b>',
                        '<span style="float: right; padding-right: 2px">' + hour + '</span>',
                        '</br>',
                        subtask,
                        '</br>',

                        '</div>',
                        '</div>'


                ].join('');

            }

        },
        mounted () {
            this.initFullCal();
        }
    });

    Vue.component('accordion-comment', {
        props: ['theme'],
        template: '#template-accordion-comment',
        data() {
            return {
                show: false,
            };
        },

        methods: {
            toggle: function() {
                this.show = !this.show;
            },
            // enter: function(el, done) {
            //   $(el).slideDown(150, done);
            // },
            // leave: function(el, done) {
            //   $(el).slideUp(150, done);
            // },
            beforeEnter: function(el) {
                el.style.height = '0';
            },
            enter: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            beforeLeave: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            leave: function(el) {
                el.style.height = '0';
            },

        },
        created: function () {

        }
    });

</script>