<?php
$this->load->view('components/inputs/expense_category');
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

    .v-time-picker-title {
        color: #fff;
        display: flex;
        height:55px !important;

        justify-content: flex-start !important;
    }

    .user-card-active {
        box-shadow: 2px 3px 5px #388E3C, 2px 3px 5px #388E3C;
    }

    /*google map*/
    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 17px;
        font-weight: 300;
        margin-left: 12px;
        margin-top: 13px;
        min-height: 35px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
        border-color: #CFD8DC;
    }

    #pac-input:focus {
        border-color: #889DA8;
    }

    #title {
        color: #fff;
        background-color: #889DA8;
        font-size: 20px;
        font-weight: 500;
        padding: 6px 7px;
    }

    .marquee {
        width: 250px;
        margin: 0 auto;
        overflow: hidden;
        white-space: nowrap;
        animation: marquee 10s linear infinite;
        -webkit-animation:linear marquee 10s infinite ;
        -moz-animation:linear marquee 10s infinite ;
        -o-animation:linear marquee 10s infinite ;
        -ms-animation:linear marquee 10s infinite ;
        animation:linear marquee 10s infinite ;
    }

    .marquee:hover {
        animation-play-state: paused
    }

    /* Make it move */
    @keyframes marquee {
        0%   { text-indent: 3em }
        100% { text-indent: -9em }
    }

</style>

<script type="text/x-template" id="template-day-task">

    <div style="background:#FFFFFF">

      <template v-if="!data_no_user">

        <v-btn
            fab
            top
            left
            color="blue-grey lighten-1"
            dark
            style="margin-top: -5px; margin-bottom: 29px; width: 48px; height: 48px"
            @click.stop="back_to_previous">
            <v-icon>arrow_back</v-icon>
        </v-btn>

        <span style="color: black; font-family: Roboto,sans-serif; font-size: 27px; margin-left: 25px">Nouvelle tâche</span>

        <template>
            <v-stepper v-model="e13" vertical>
                <v-stepper-step step="1" editable>
                    Libellé et date de la tâche
                </v-stepper-step>

                <v-stepper-content step="1">


                        <v-container grid-list-md style="padding: 0 !important">
                            <v-layout wrap>
                                <v-flex xs12 md4>
                                    <v-card style="margin-left: 7px" height="375px">
                                        <div style="background: #78909C; height: 87px">
                                            <div style="padding-top: 20px; padding-left: 15px">
                                                <span style="color: white; font-family: Roboto,sans-serif; font-size: 32px;">Tâche</span>
                                            </div>
                                        </div>

                                        <v-card style="margin-left: 10px; margin-right: 10px">
                                            <div style="display: flex; flex-direction: row; justify-content: flex-start; align-items: center; height: 75px; margin-left: 12px; margin-top: 35px;">

                                                    <img v-if="selectedTask" :src="urlBaseImage+'uploads/taches/'+selectedTask.taskImage" alt="" style="object-fit:contain" width="90" height="90">
                                                    <v-icon v-else style="font-size: 58px;">face</v-icon>

                                                    <div style="display: flex; justify-content: center; margin-left: 7px; ">
                                                        <h4 v-if="selectedTask" class="text-xs-left">{{ selectedTask.taskLabel }}</h4>
                                                        <h4 v-else>Pas de tâche sélectionné !</h4>
                                                    </div>

                                            </div>
                                        </v-card>
                                        <v-autocomplete
                                            style="margin-left: 10px; margin-top: 33px; margin-right: 10px;"
                                            :items="taches"
                                            :loading="false"
                                            :search-input.sync="search_taches"
                                            chips
                                            clearable
                                            hide-details
                                            hide-selected
                                            item-text="subtaskLabel"
                                            item-value="id"
                                            label="Recherche"
                                            @change="updateContent">

                                            <template

                                                slot="selection"
                                                slot-scope="{ item, selected }">
                                                <v-chip
                                                    :selected="selected"
                                                    color="blue-grey"
                                                    class="white--text"
                                                    style="height: 29px">

                                                    <v-icon left>mdi-coin</v-icon>
                                                    <span v-text="item.subtaskLabel"></span>

                                                </v-chip>
                                            </template>

                                            <template

                                                slot="item"
                                                slot-scope="{ item, tile }">

                                                <v-list-tile-avatar
                                                    color="indigo"
                                                    class="headline font-weight-light white--text">
                                                    {{ item.subtaskLabel.charAt(0) }}
                                                </v-list-tile-avatar>

                                                <v-list-tile-content>
                                                    <v-list-tile-title v-text="item.subtaskLabel"></v-list-tile-title>

                                                </v-list-tile-content>

                                            </template>
                                        </v-autocomplete>
                                    </v-card>
                                </v-flex>
                                <v-flex xs12 md4>
                                    <v-date-picker style="margin-left: 7px" locale="fr-fr" readonly color="blue-grey lighten-1" v-model="picker_date" :landscape="false" full-width :reactive="true"></v-date-picker>
                                </v-flex>
                                <v-flex xs12 md4>
                                    <v-time-picker style="margin-left: 7px" color="blue-grey lighten-1" v-model="picker_time" :landscape="false" full-width format="24hr"></v-time-picker>
                                </v-flex>
                            </v-layout>
                        </v-container>


                    <v-btn style="margin-top: 10px" color="primary" @click="e13 = 2">Continuer</v-btn>
                    <!--<v-btn flat>Cancel</v-btn>-->
                </v-stepper-content>

                <v-stepper-step step="2" editable>
                    Lieu
                </v-stepper-step>

                <v-stepper-content step="2">
                    <v-card class="mb-5" height="350px" style="margin-left: 12px; margin-right: 12px; margin-top: 8px;">

                        <div class="pac-card" id="pac-card">
                            <div>
                                <div id="title">
                                    Recherche
                                </div>
                                <div id="type-selector" class="pac-controls" style="display: none !important">
                                    <input type="radio" name="type" id="changetype-all" checked="checked">
                                    <label for="changetype-all">All</label>

                                    <input type="radio" name="type" id="changetype-establishment">
                                    <label for="changetype-establishment">Establishments</label>

                                    <input type="radio" name="type" id="changetype-address">
                                    <label for="changetype-address">Addresses</label>

                                    <input type="radio" name="type" id="changetype-geocode">
                                    <label for="changetype-geocode">Geocodes</label>
                                </div>
                                <div id="strict-bounds-selector" class="pac-controls" style="display: none !important">
                                    <input type="checkbox" id="use-strict-bounds" value="">
                                    <label for="use-strict-bounds">Strict Bounds</label>
                                </div>
                            </div>
                            <div id="pac-container">
                                <input id="pac-input" type="text" placeholder="Saisissez un lieu">
                            </div>
                        </div>

                        <div style="width: 100%; height: 100%; position: absolute; left:0; bottom:0" id="map"></div>

                        <div id="infowindow-content">
                            <img src="" width="16" height="16" id="place-icon">
                            <span id="place-name"  class="title"></span><br>
                            <span id="place-address"></span>
                        </div>

                    </v-card>
                    <v-btn color="primary" @click="e13 = 3">Continuer</v-btn>
                    <!--<v-btn flat>Cancel</v-btn>-->
                </v-stepper-content>

                <v-stepper-step step="3" editable>
                    Dépenses
                </v-stepper-step>

                <v-stepper-content step="3">

                        <template v-for="(category_item, i) in category_expenses">
                            <expense-category
                                :key="category_item.category.id"
                                :index="i"
                                :selected_category="category_item.category"
                                :type_day="'task'"
                                :item_subcategories="category_item.subcategories"
                                :last_spent_id="last_spent_id"
                                @update_entire_category="from_child_update_entire_category"
                                @delete_entire_category="from_child_delete_entire_category"
                                @update_one_subcategory="from_child_update_one_subcategory"
                                @increment_last_spent_id="from_child_increment_last_spent_id"
                                @remove_one_subcategory="from_child_remove_one_subcategory">
                            </expense-category>
                        </template>
                        <div @click="showDialog" style="display: flex; flex-direction: row; height: 42px; margin-right: 7px; border-top: 6px solid transparent; cursor: pointer; margin-bottom: 10px; max-width: 250px">
                            <div class="button-icon-add">
                                <v-icon>add</v-icon>
                            </div>
                            <div style="display: flex; align-items: center; margin-left: 8px">
                                Ajouter une nouvelle dépense
                            </div>
                        </div>

                    <v-btn color="primary" @click="e13 = 4">Continuer</v-btn>
                    <!--<v-btn flat>Cancel</v-btn>-->
                </v-stepper-content>

                <v-stepper-step step="4" editable>
                    Assignation
                </v-stepper-step>

                <v-stepper-content step="4">


                        <v-flex xs12 style="margin-top: 15px">
                            <v-container fluid grid-list-md>
                                <v-data-iterator
                                    :items="asignation_users"
                                    :pagination.sync="pagination_user"
                                    content-tag="v-layout"
                                    row
                                    wrap
                                    :hide-actions="true"
                                    :total-items="asignation_users.length">
                                    <v-flex
                                        slot="item"
                                        slot-scope="props"
                                        xs12
                                        sm6
                                        md4
                                        lg3>

                                        <v-card :class="getUserClass(props.item)" style="cursor: pointer; background: #fafafa" @click.native="chooseUser(props.item)">

                                            <div v-if="props.item.selected" style="position: absolute; left: 15px; top: 15px; border-radius: 50%; width: 20px; height: 20px; background: green"></div>
                                            <v-layout justify-center>
                                                <v-avatar style="margin-top: 15px"
                                                    :tile="false"
                                                    :size="65"
                                                    color="transparent">

                                                    <template v-if="props.item.user_image && !!props.item.user_image">
                                                        <img :src="urlBaseImage+'uploads/utilisateurs/'+props.item.user_image" alt="" >
                                                    </template>
                                                    <template v-else>
                                                        <v-icon style="font-size: 80px; background: #fafafa">face</v-icon>
                                                    </template>

                                                </v-avatar>

                                            </v-layout>

                                            <v-card-title>
                                                <v-layout align-center justify-center>
                                                    <div>
                                                        <h4 class="text-xs-center">{{ props.item.last_name }}</h4>
                                                        <h4 class="text-xs-center">{{ props.item.first_name }}</h4>
                                                    </div>
                                                </v-layout>
                                            </v-card-title>

                                        </v-card>
                                    </v-flex>
                                </v-data-iterator>
                            </v-container>
                        </v-flex>


                    <v-btn color="primary" @click="addTask()">Enregistrer</v-btn>
                    <!--<v-btn flat>Cancel</v-btn>-->
                </v-stepper-content>
            </v-stepper>
        </template>

        <v-dialog v-model="dialog" width="600px">

            <v-layout row wrap style="background: #f2f3f4">

                <v-flex xs12 style="margin-top: 15px">
                    <v-container fluid grid-list-md>
                        <v-data-iterator
                            :items="category_items"
                            :pagination.sync="pagination"
                            content-tag="v-layout"
                            row
                            wrap
                            :hide-actions="true"
                            :total-items="category_items.length">
                            <v-flex
                                slot="item"
                                slot-scope="props"
                                xs12
                                sm6
                                md4
                                lg3>

                                <v-card style="cursor: pointer;" @click.native="chooseCategory(props.item)">

                                    <v-layout justify-center>
                                        <img :src="urlBaseImage+'uploads/categories/'+props.item.categoryImage" alt="" style="margin-top: 6px;width:42%;height:42%;object-fit:contain">

                                    </v-layout>

                                    <v-card-title>
                                        <template v-if="countCharOK(props.item.categoryLabel)">
                                            <p style="font-weight: bold; font-size: 13px; width: 250px; margin: 0 auto; overflow: hidden; white-space: nowrap; text-align: center">{{ props.item.categoryLabel }}</p>
                                        </template>
                                        <template v-else>
                                            <p style="font-weight: bold; font-size: 13px" class="marquee">{{ props.item.categoryLabel }}</p>
                                        </template>
                                    </v-card-title>

                                </v-card>
                            </v-flex>
                        </v-data-iterator>
                    </v-container>
                </v-flex>
            </v-layout>

        </v-dialog>

      </template>

      <data-no-user v-if="data_no_user"></data-no-user>

      <v-dialog v-model="alert_step" width="350">
        <v-card>
            <v-card-title class="headline grey lighten-2" primary-title>
                Création d'une tâche
            </v-card-title>
            <v-card-text>
                Veuillez terminer tous les étapes !
            </v-card-text>
        </v-card>
      </v-dialog>

      <data-loader v-if="data_loader"></data-loader>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_list_subtask = '<?php echo URL_LIST_SUBTASK ?>';
    var url_list_subtask_final = `${url_base}${url_list_subtask}`;

    var url_list_category = '<?php echo URL_LIST_CATEGORY ?>';
    var url_list_category_final = `${url_base}${url_list_category}`;

    var url_list_task = '<?php echo URL_LIST_TASK ?>';
    var url_list_task_final = `${url_base}${url_list_task}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_list_user = '<?php echo URL_LIST_USER ?>';
    var url_list_user_final = `${url_base}${url_list_user}`;

    var url_select_user = '<?php echo URL_SELECT_USER ?>';
    var url_select_user_final = `${url_base}${url_select_user}`;

    var url_create_event = '<?php echo URL_CREATE_EVENT ?>';
    var url_create_event_final = `${url_base}${url_create_event}`;

    var url_create_place = '<?php echo URL_CREATE_PLACE ?>';
    var url_create_place_final = `${url_base}${url_create_place}`;

    var url_create_spent = '<?php echo URL_SAVE_SPENT ?>';
    var url_create_spent_final = `${url_base}${url_create_spent}`;

    var db_place_id = '101';
    var db_place_description = '';
    var db_place_type = 'mg';
    var db_place_lat = '';
    var db_place_lng = '';
    var db_place_formated_adress = '';

    Vue.component('day-task', {
        template: '#template-day-task',
        mixins: [common_mixin],
        data: function () {

            return{
                e13: 1,
                place: '',
                picker_date: new Date().toISOString().substr(0, 10),
                picker_time: new Date().getHours() + ':' + new Date().getMinutes(),

                lieux: [],
                subcategoryData: undefined,
                search_place: null,
                isLoadingPlace: false,

                taches: [],
                search_taches: null,
                //taskId: 5,

                category_items: [],
                dialog: false,
                pagination: {
                    rowsPerPage: 12
                },
                urlBaseImage: url_base,

                noduplicates: [],
                category_expenses: [],
                last_spent_id: 0,
                expenses_state: undefined,
                expenses_updates: [],

                tacheparents: [],
                selectedTask: undefined,

                user_logged_in: undefined,
                user_choosed_by_admin: undefined,

                asignation_users: [],
                pagination_user: {
                    rowsPerPage: 12
                },

                selected_assignation_user: undefined,
                selected_subtask_id: 0,
                alert_step: false,
                event_consulted: 0,

            }

        },
        methods: {
            wsSubtask(){
                //this.data_loader = true;
                axios.get(url_list_subtask_final).then((response) => {
                    if(response.data.subtask == null){

                    }else{
                        console.log(`SUBTASKS ARE ====>${JSON.stringify(response.data.subtask)}`)
                        this.taches = response.data.subtask;
                    }
                })
                .finally(() => {

                    //this.closeLoaderAfterDelay();

                })
            },
            wsTask(){
                //this.data_loader = true;
                axios.get(url_list_task_final).then((response) => {
                    if(response.data.task == null){

                    }else{
                        //console.log(`TASKS ARE ====>${JSON.stringify(response.data.task)}`)
                        this.tacheparents = response.data.task;
                    }
                })
                .finally(() => {

                    //this.closeLoaderAfterDelay();

                })
            },
            getTaskById(taskId){
                for(var i= 0; i < this.tacheparents.length; i++){
                    if(this.tacheparents[i].id === taskId){
                        return this.tacheparents[i];
                    }
                }
                return null;
            },
            getSubtaskParentId(subtaskId){
                for(var i= 0; i < this.taches.length; i++){
                    if(this.taches[i].id === subtaskId){
                        return this.taches[i].taskId;
                    }
                }
                return null;
            },
            updateContent: function(obj){
                console.log(obj);
                if(typeof obj !== 'undefined'){
                    this.selected_subtask_id = obj;
                    if(this.getSubtaskParentId(obj))
                        this.selectedTask = this.getTaskById(this.getSubtaskParentId(obj));
                }else{
                    this.selectedTask = undefined;
                }

            },
            showDialog: function(){
                this.dialog = true;
            },
            back_to_previous () {
                this.$router.go(-1);
            },
            wsCategory(){
                axios.get(url_list_category_final).then((response) => {
                    if(response.data.category == null){
                        //v.noResult()
                    }else{

                        this.pagination.rowsPerPage = response.data.category.length;
                        this.category_items = response.data.category;
                        //console.log(`CATEGORIES ARE ====>${JSON.stringify(response.data.category)}`)
                    }
                });
            },
            chooseCategory: function(cat_item){

                console.log(`LIST duplicate =======>${this.noduplicates}`);
                if(!this.checkDuplicatedCategory(cat_item, this.noduplicates)){
                    this.noduplicates.push(cat_item.id);
                    this.addCategory(cat_item);
                }else{
                    console.log("already exist");
                }

                this.dialog = false;
            },
            checkDuplicatedCategory: function(cat_item, arr){

                for(var i= 0; i < arr.length; i++){
                    console.log(`ITEM=======>${arr[i]}`);
                    if(arr[i] == cat_item.id){
                        console.log("ENTER IN");
                        return true;
                    }
                }
                return false;
            },
            addCategory: function(cat_item){

                this.category_expenses.push(this.getCategoryByParam(cat_item, []));
                //console.log(`ADDED category--->${JSON.stringify(this.category_expenses)}`);

            },
            getCategoryByParam: function(cat, subcats){
                return {category: cat, subcategories: subcats};
            },
            from_child_increment_last_spent_id: function(){
                this.last_spent_id += 1;
            },
            from_child_update_entire_category: function(type_day, entire_cat){
                for (var i = this.category_expenses.length; i--;) {
                    console.log(`from_child_update_entire_category ====>${i} entire_cat.category_id===>${entire_cat.category_id} category_expenses====> ${this.category_expenses[i].category.id}`);
                    if(this.category_expenses[i].category.id == entire_cat.category_id){
                        //console.log(`tafiditra ao am i ====>${i} length===>${this.category_expenses_morning.length}`);
                        this.category_expenses[i].subcategories = entire_cat.subcategories;
                    }
                }
            },
            from_child_delete_entire_category: function(type_day, selected_category, index){

                //console.log(`from_child_delete_entire_category IDCAT====>${selected_category.id}`);
                this.spliceByCategory(this.category_expenses, selected_category, index, type_day);
                this.removeFromDuplicate(this.noduplicates, selected_category.id);

            },
            from_child_update_one_subcategory: function(type_day, subcat, action_type){

                //console.log(`=====>LISTING MODIFY OBJECT : ${JSON.stringify(subcat)}`);
                subcat.action = action_type;
                if(this.expenses_updates.length > 0){

                    let checked = this.checkSubcategory(subcat, this.expenses_updates);
                    if(checked.exist){
                        this.expenses_updates[checked.index] = subcat;
                    }else{
                        this.expenses_updates.push(subcat);
                    }

                }else{
                    this.expenses_updates.push(subcat);
                }
                //console.log(`EXPENSE updates=====> ${JSON.stringify(this.expenses_updates)}`);
            },
            from_child_remove_one_subcategory: function(type_day, subcat){

                this.ejectupdates(this.expenses_updates, subcat);
                //console.log(`expenses_updates=====> ${JSON.stringify(this.expenses_updates)}`);

            },
            spliceByCategory: function(arr, cat, index, type_day) {

                let subcats = this.getSubcategoriesByCategory(arr, cat);
                console.log(`spliceByCategory subcats====>${JSON.stringify(subcats)}`);
                subcats.forEach((subcat) => {
                    this.from_child_update_one_subcategory(type_day, subcat, '<?php echo CONSTANT_DB_REMOVE ?>');
                })
                arr.splice(index, 1)

            },
            getSubcategoriesByCategory: function(arr, cat){
                var res = [];
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].category.id === cat.id){
                        res = arr[i].subcategories;
                    }
                }
                return res;
            },
            removeFromDuplicate: function(array, item){
                var index = array.indexOf(item);
                if (index !== -1)
                    array.splice(index, 1);
            },
            checkSubcategory: function(subcat_item, arr){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].spent_id === subcat_item.spent_id){
                        return {exist: true, index: i};
                    }
                }
                return {exist: false, index: -1};
            },
            ejectupdates: function(arr, item){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].spent_id == item.spent_id){
                        arr.splice(i, 1);
                    }
                }
            },
            checkUserRole: function(logged, choosed){
                var userobj;
                if(typeof choosed !== 'undefined'){
                    userobj = choosed;
                }else{
                    userobj = logged;
                }
                if(userobj.role === 'USER'){
                    this.getUsersAssignation(userobj.admin_id);
                }else if(userobj.role === 'ADMINISTRATOR'){
                    this.getUsersAssignation(userobj.id);
                }
            },
            getUsersAssignation(admin_id){
                var formData = new FormData();
                formData.append('id_admin', admin_id);
                axios.post(url_list_user_final, formData).then((response) => {
                    if(response.data.users == null){
                        this.selectUser(admin_id, []);
                        console.log("USERS WITHOUT ADMIN NULLLLLLL");
                    }else{
                        this.selectUser(admin_id, response.data.users);
                        console.log(`USERS WITHOUT ADMIN ARE ====>${JSON.stringify(response.data.users)}`)
                    }
                })
                .finally(() => this.data_loader = false);
            },
            selectUser(user_id, users_no_admin){
                var formData = new FormData();
                formData.append('id_user', user_id);
                axios.post(url_select_user_final, formData).then((response) => {
                    if(response.data.user == null){

                        console.log("USER ADMIN NULLLLLLL");

                    }else{
                        users_no_admin.push(response.data.user);
                        this.asignation_users = this.checkIfMe(users_no_admin, this.user_logged_in, this.user_choosed_by_admin);
                        console.log(`USER ADMIN IS ====>${JSON.stringify(response.data.user)}`)
                        console.log(`all of user ====>${JSON.stringify(this.asignation_users)}`)
                        this.pagination_user.rowsPerPage = this.asignation_users.length;
                    }
                });
            },
            checkIfMe: function(all_user, logged, choosed){
                var userobj;
                if(typeof choosed !== 'undefined'){
                    userobj = choosed;
                }else{
                    userobj = logged;
                }
                for(var i= 0; i < all_user.length; i++){
                    if(all_user[i].id === userobj.id){
                        all_user[i].last_name = "Moi";
                        all_user[i].first_name = ".";
                    }
                }
                return this.deselectOther(all_user, userobj.id);
            },
            getUserClass(item){
                var classname = "";
                if(item.selected == true){
                    classname = "user-card-active";
                }
                return classname;
            },
            deselectOther(array, userid){
                for(var i = array.length - 1; i >= 0; i--) {
                    if(array[i].id == userid){
                        array[i].selected = true;
                        this.selected_assignation_user = array[i];
                    }else{
                        array[i].selected = false;
                    }
                }
                return array;
            },
            chooseUser(selectedUser){

                this.deselectOther(this.asignation_users, selectedUser.id);

            },
            createEvent(placeId, dateDebut){
                var formData = new FormData();
                formData.append('event_debut', dateDebut);
                //formData.append('event_fin', admin_id);
                formData.append('event_statut', '<?php echo CONSTANT_TASK_TODO ?>');
                //event_consulted => 1 then no seen
                //event_consulted => 0 then already seen
                formData.append('event_consulted', this.event_consulted);
                formData.append('place_id', placeId);
                formData.append('subtask_id', this.selected_subtask_id);
                formData.append('assigned_user_id', this.selected_assignation_user.id);
                axios.post(url_create_event_final, formData).then((response) => {
                    if(response.data.error){
                        console.log(`MESSAGE ERROR=>${response.data.msg}`);
                    }else{
                        console.log(`EVENT INSERTED ID =>${response.data.inserted_id}`);
                        this.save("task", response.data.inserted_id);
                    }
                });
            },
            addTask(){
                console.log(`this is the date=====>${this.picker_date}`);
                console.log(`this is the hour=====>${this.picker_time}`);
                console.log(`these are EXPENSE updates=====> ${JSON.stringify(this.expenses_updates)}`);
                console.log(`these are ALL category--->${JSON.stringify(this.category_expenses)}`);
                if(this.selected_subtask_id != 0 && !!db_place_formated_adress){
                    if(this.user_logged_in.id != this.selected_assignation_user.id){
                        this.event_consulted = 1;
                        console.log("diff")
                    }else{
                        this.event_consulted = 0;
                        console.log("equal")
                    }
                    let date_debut = `${this.picker_date} ${this.picker_time}`;
                    this.createPlace(date_debut);
                }else{
                    this.alert_step = true;
                }
            },
            createPlace(dateDebut){

                //console.log(`logged===>${this.user_logged_in.id} assigned===>${this.selected_assignation_user.id}`)
                var formData = new FormData();
                formData.append('place_id', db_place_id);
                formData.append('place_description', db_place_description);
                formData.append('place_type', db_place_type);
                formData.append('place_lat', db_place_lat);
                formData.append('place_lng', db_place_lng);
                formData.append('place_formated_adress', db_place_formated_adress);
                axios.post(url_create_place_final, formData).then((response) => {
                    if(response.data.error){
                        console.log(`MESSAGE ERROR=>${response.data.msg}`);
                    }else{
                        console.log(`PLACE INSERTED ID =>${response.data.inserted_id}`);
                        console.log(`MESSAGE =>${response.data.msg}`);
                        this.createEvent(response.data.inserted_id, dateDebut);
                    }
                });

            },
            save(type_day, event_id) {

                var allowPost = false;
                var formData = new FormData();

                if(this.category_expenses.length > 0){
                    console.log(`save this category expenses====>${JSON.stringify(this.category_expenses)}`);
                    formData.append('spents', JSON.stringify(this.formatJson(this.category_expenses, type_day, event_id)));
                    allowPost = true;
                }

                if(allowPost){
                    this.data_loader = true;
                    axios.post(url_create_spent_final, formData).then((response) =>{
                        console.log(`DATA SENT here====>${JSON.stringify(response.data.spents)}`);

                        if(response.data.error){
                            alert("ERROR WHEN SAVING");
                        }else{
                            this.back_to_previous();
                        }

                        this.data_loader = false;
                    });
                }else{
                    console.log("NOTHING TO SAVE OR ADD");
                    this.alert_step = true;
                }

            },
            formatJson: function(array_exp, day_type, event_id) {

                var res_array = [];
                array_exp.forEach((cat) => {
                    cat.subcategories.forEach((subcat) => {
                        res_array.push({unitPrice: subcat.price, number: subcat.number, subcategoryId: subcat.subcategory, userId: this.user_logged_in.id, dayType: day_type, contextId: subcat.utility.id, eventId: event_id });
                    })
                })

                return res_array;

            },
            countCharOK(str){
                if(str && str.length > 17)
                    return false;
                return true;
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`DAYTASK PAGE USER LOGGED  ===> ${JSON.stringify(connexion.user_logged[0])}`);

                    this.user_logged_in = connexion.user_logged[0];
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_choosed_by_admin = connexion.user_choosed[0];
                    }
                    this.checkUserRole(this.user_logged_in, this.user_choosed_by_admin);
                    this.wsSubtask();
                    this.wsTask();
                    this.wsCategory();
                }else{
                    console.log("DAYTASK PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            },

        },
        mounted: function () {
            console.log('daytask : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },
        metaInfo: {
            script: [
                { src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDGX4yrc4jlICDUNxY2rKXubOlpZeMKJYA&libraries=places&callback=initMap', async: true, defer: true },
            ]
        }

    });

    function initMap(){
        setTimeout(() =>{
            initMapAfterDelay();
        },  3500);
    }

    //google map primitive function
    function initMapAfterDelay() {
        var latlng = new google.maps.LatLng(-18.9130152, 47.5172045);
        if(!document.getElementById('map')){
            console.log("map id is null");
            return;
        }

        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);

          /***********VARIABLE FOR DATABASE****************/
          console.log(`PLACE JSON : ${JSON.stringify(place.geometry.location.lat)}`);
          db_place_description = place.name;
          db_place_lat = place.geometry.location.lat();
          db_place_lng = place.geometry.location.lng();
          db_place_formated_adress = address;

        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        /*setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);*/

        /*document.getElementById('use-strict-bounds')
        .addEventListener('click', function() {
            console.log('Checkbox clicked! New state=' + this.checked);
            autocomplete.setOptions({strictBounds: this.checked});
        });*/
    }

</script>

