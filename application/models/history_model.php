<?php

class History_model extends CI_Model
{

    public function createHistory($data)
    {
        $this->db->insert('history', $data);
        if ($this->db->affected_rows() > 0) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        return 0;
    }

    public function showHistoryByEvent($event_id)
    {
        $this->db->select('history.id, history.datehistory, history.statuthistory, history.commenthistory, history.eventId, users.first_name, users.last_name');
        $this->db->from('history');
        $this->db->where('eventId', $event_id);
        $this->db->join('users', 'users.id = history.userId');
        $this->db->order_by('history.datehistory', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
