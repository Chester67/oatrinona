<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spent extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('spent_model','spent');
    }
    
    public function index()
	{
		$this->load->view('welcome');
	}

    public function saveDaySpent()
	{
        $result['error'] = true;
		$result['msg'] ='Error when adding spent ';

		$spents = $this->input->post('spents');

        $result['spents'] = $spents;
        $data = json_decode($spents);

        //uncomment to save spent
        if($this->spent->addSpent($data)){
			$result['error'] = false;
			$result['msg'] ='Spent added successfully';
        }
        
        echo json_encode($result);
                
    }

    public function showDaySpent()
	{
        $userId = $this->input->post('user_id');
        $date = $this->input->post('spent_date');
        //$dayType = $this->input->post('type_day');
        $query = $this->spent->showDaySpent($userId, $date);
		if($query){
			$result['spents'] = $this->spent->showDaySpent($userId, $date);
		}
		echo json_encode($result);
    }

    public function showDayTask()
	{
        $userId = $this->input->post('user_id');
        $date = $this->input->post('spent_date');
        $query = $this->spent->showDayTask($userId, $date);
		if($query){
			$result['taskspents'] = $query;
		}
		echo json_encode($result);
    }

    public function updateDaySpent()
    {
        $result['error'] = true;
        $result['msg'] ='Error when update spent';
        $sqls = $this->input->post('request');
        $result['sqls'] = $sqls;
        $query = $this->spent->updateDaySpent($sqls);
        if($query > 0){
            $result['error'] = false;
            $result['msg'] ='Spent update successfully';
            $result['sqls'] = $sqls;
            $result['nb'] = $query;
		}
        echo json_encode($result);
    }

    public function getLastSpentId(){
        $result['error'] = true;
        $query = $this->spent->getLastSpentId();
        if($query){
            $result['error'] = false;
            $result['spent_id'] = $query;
		}
        echo json_encode($result);
    }

    public function deleteSpentByEvent(){
        $result['error'] = true;
        $result['deleted'] = false;
        $id = $this->input->post('event_id');
        $query = $this->spent->deleteSpentByEvent($id);
        if($query){
            $result['error'] = false;
            $result['deleted'] = true;
		}
        echo json_encode($result);
    }

    public function showMonthWeekSpent(){
        $userId = $this->input->post('user_id');
        $month = $this->input->post('spent_month');
        $year = $this->input->post('spent_year');
        $query = $this->spent->showMonthWeekSpent($userId, $month, $year);
		if($query){
			$result['spents'] = $query;
		}
		echo json_encode($result);
    }

    public function checkNotificationTask(){
        $result['notif'] = [];
        $userId = $this->input->post('user_id');
        $query = $this->spent->checkNotificationTask($userId);
        if($query){
			$result['notif'] = $query;
		}
		echo json_encode($result);
    }

    public function getNotificationTask(){
        $result['notif'] = [];
        $userId = $this->input->post('user_id');
        $date = $this->input->post('notif_date');
        $query = $this->spent->getNotificationTask($userId, $date);
        if($query){
			$result['notif'] = $query;
		}
		echo json_encode($result);
    }

    public function getOtherNotificationTask(){
        $result['notif'] = [];
        $userId = $this->input->post('user_id');
        $date = $this->input->post('notif_date');
        $query = $this->spent->getOtherNotificationTask($userId, $date);
        if($query){
			$result['notif'] = $query;
		}
		echo json_encode($result);
    }

    public function getRangeNotificationTask(){
        $result['notif'] = [];
        $userId = $this->input->post('user_id');
        $begin = $this->input->post('notif_date_begin');
        $end = $this->input->post('notif_date_end');
        $query = $this->spent->getRangeNotificationTask($userId, $begin, $end);
        if($query){
			$result['notif'] = $query;
		}
		echo json_encode($result);
    }

    public function getOtherRangeNotificationTask(){
        $result['notif'] = [];
        $userId = $this->input->post('user_id');
        $begin = $this->input->post('notif_date_begin');
        $end = $this->input->post('notif_date_end');
        $query = $this->spent->getOtherRangeNotificationTask($userId, $begin, $end);
        if($query){
			$result['notif'] = $query;
		}
		echo json_encode($result);
    }

}
