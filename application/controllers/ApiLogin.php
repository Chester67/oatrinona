<?php

require(APPPATH.'/libraries/REST_Controller.php');
require(APPPATH.'/util/WsResponse.php');
require(APPPATH.'/util/UtilDateTime.php');


class ApiLogin extends REST_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('user_model','user');
    }

    public function index_get()
    {
        // Display all books
    }

    public function index_post()
    {
        // Create a new book
    }

    public function test_get(){
        $username = 'admin';
		$password = 'admin';
        $query_result = $this->user->loginUser($username, $password);
        $this->response($query_result);
        //json_output(200, $query_result);
    }

    public function authenticate_post(){
        
        $ws_response = new WsResponse();
        $username = $this->post('username');
		$password = $this->post('password');
        $query_result = $this->user->loginUser($username, $password);
        if($query_result){
            $utilDate = new UtilDateTime();
            $data = array(
                'token' => time(),
                'last_connected' => $utilDate->getDatetimeNow()
            );
            $credential = $this->user->updateCredential($data, $query_result[0]->id);
            if($credential && $credential[0]){
                $message = 'successfully logged in !';
                $code = 200;
                $objkey = 'user';
                $objvalue = $credential[0];
                $entire_response = $ws_response->getSuccess($message, $code, $objkey, $objvalue);
                $this->response($entire_response);
            }
                
            
        }else{
            $this->response($ws_response->getError("login ou mot de passe incorrect !", -1));
        }
        
    }
}