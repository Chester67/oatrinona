<?php
$this->load->view('components/inputs/expense_category');
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

    .button-icon-add {
        margin-left: 8px;
        line-height: 35px;
        width: 35px;
        height: 35px;
        border-radius: 50%;
        border-style: solid;
        border-width: 1px;
        border-color: grey;
        display: flex;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        cursor: pointer;
    }

    .v-stepper__header {
        height: 60px !important;
        align-items: stretch;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
    }

    .marquee {
        width: 250px;
        margin: 0 auto;
        overflow: hidden;
        white-space: nowrap;
        animation: marquee 10s linear infinite;
        -webkit-animation:linear marquee 10s infinite ;
        -moz-animation:linear marquee 10s infinite ;
        -o-animation:linear marquee 10s infinite ;
        -ms-animation:linear marquee 10s infinite ;
        animation:linear marquee 10s infinite ;
    }

    .marquee:hover {
        animation-play-state: paused
    }

    /* Make it move */
    @keyframes marquee {
        0%   { text-indent: 3em }
        100% { text-indent: -9em }
    }

</style>

<script type="text/x-template" id="template-day-expense">

    <div>
        <div v-if="!data_no_user" style="padding-top: 12px; text-align: center; background: #fff; font-weight: bold; font-size: 19px; color: #757575;">

        </div>
        <v-toolbar style="height: 60px;">

            <v-dialog
                ref="dialog"
                v-model="modal"
                :return-value.sync="date"
                persistent
                lazy
                full-width
                width="520px"
                justify-center
                style="width: 100% !important">

                    <!--<div slot="activator" style="display: flex; flex-direction: row; justify-content: center; align-items: center">
                        <v-icon>event</v-icon>
                        <h1 style="margin-left: 20px">Date : {{ date | format_date }}</h1>
                    </div>-->

                    <div slot="activator" class="a">
                        <div class="c">
                            <h1>Date : {{ getDayOfDate(date) }} {{ getMonthByInt(getMonthOfDate(date)) }} {{ getYearOfDate(date) }}</h1>
                        </div>
                        <div class="hidden-sm-and-down" style="display: table-cell; width:83%; height: 20px !important; margin-top: -5px; text-align: right; float: right;">
                            <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 3px; margin-right: 90px">
                                <v-icon>event</v-icon>
                                <h1 style="margin-left: 5px">{{ date | format_date }}</h1>
                            </div>
                        </div>
                    </div>

                    <v-date-picker v-model="date" scrollable full-width locale="fr-fr">
                        <v-spacer></v-spacer>
                        <v-btn flat color="primary" @click="modal = false">Annuler</v-btn>
                        <v-btn flat color="primary" @click="requestByDate(date)">OK</v-btn>
                    </v-date-picker>
            </v-dialog>

        </v-toolbar>
        <v-stepper style="" v-if="!data_no_user" v-model="e1">
            <v-stepper-header>
                <!--morning-->
                <v-stepper-step editable step="1" style="cursor: pointer" @click.native="accessMorningDay">Matin</v-stepper-step>

                <v-divider></v-divider>

                <!--midday-->
                <v-stepper-step editable step="2" style="cursor: pointer" @click.native="accessMidDay">Midi</v-stepper-step>

                <v-divider></v-divider>

                <!--evening-->
                <v-stepper-step editable step="3" style="cursor: pointer" @click.native="accessEvening">Soir</v-stepper-step>
            </v-stepper-header>

            <v-stepper-items style="background: #fffffa">

                <v-stepper-content step="1">
                    <div style="padding-top: 15px; padding-bottom: 15px">
                        <template v-for="(category_item, i) in category_expenses_morning">
                            <expense-category
                                :key="category_item.category.id"
                                :index="i"
                                :selected_category="category_item.category"
                                :type_day="current_type_day"
                                :item_subcategories="category_item.subcategories"
                                :last_spent_id="last_spent_id"
                                @update_entire_category="from_child_update_entire_category"
                                @delete_entire_category="from_child_delete_entire_category"
                                @update_one_subcategory="from_child_update_one_subcategory"
                                @increment_last_spent_id="from_child_increment_last_spent_id"
                                @remove_one_subcategory="from_child_remove_one_subcategory"
                                @show_utility_dialog="from_child_show_utilities">
                            </expense-category>
                        </template>
                        <div @click="showDialog" style="display: flex; flex-direction: row; height: 42px; margin-right: 7px; border-top: 6px solid transparent; cursor: pointer; margin-bottom: 10px; max-width: 250px">
                            <div class="button-icon-add">
                                <v-icon>add</v-icon>
                            </div>
                            <div style="display: flex; align-items: center; margin-left: 8px">
                                Ajouter une nouvelle dépense
                            </div>
                        </div>
                    </div>
                    <v-btn color="blue-grey lighten-1" @click="distinctAction('morning')">Enregistrer</v-btn>
                </v-stepper-content>

                <v-stepper-content step="2">
                    <div style="padding-top: 15px; padding-bottom: 15px">

                        <template v-for="(category_item, i) in category_expenses_midday">
                            <expense-category
                                :key="category_item.category.id"
                                :index="i"
                                :selected_category="category_item.category"
                                :type_day="current_type_day"
                                :item_subcategories="category_item.subcategories"
                                :last_spent_id="last_spent_id"
                                @update_entire_category="from_child_update_entire_category"
                                @delete_entire_category="from_child_delete_entire_category"
                                @update_one_subcategory="from_child_update_one_subcategory"
                                @increment_last_spent_id="from_child_increment_last_spent_id"
                                @remove_one_subcategory="from_child_remove_one_subcategory"
                                @show_utility_dialog="from_child_show_utilities">
                            </expense-category>
                        </template>

                        <div @click="showDialog" style="display: flex; flex-direction: row; height: 42px; margin-right: 7px; border-top: 6px solid transparent; cursor: pointer; margin-bottom: 10px">
                            <div class="button-icon-add">
                                <v-icon>add</v-icon>
                            </div>
                            <div style="display: flex; align-items: center; margin-left: 8px">
                                Ajouter une nouvelle dépense
                            </div>
                        </div>

                    </div>
                    <v-btn color="blue-grey lighten-1" @click="distinctAction('midday')">Enregistrer</v-btn>
                </v-stepper-content>

                <v-stepper-content step="3">
                    <div style="padding-top: 15px; padding-bottom: 15px">

                        <template v-for="(category_item, i) in category_expenses_evening">
                            <expense-category
                                :key="category_item.category.id"
                                :index="i"
                                :selected_category="category_item.category"
                                :type_day="current_type_day"
                                :item_subcategories="category_item.subcategories"
                                :last_spent_id="last_spent_id"
                                @update_entire_category="from_child_update_entire_category"
                                @delete_entire_category="from_child_delete_entire_category"
                                @update_one_subcategory="from_child_update_one_subcategory"
                                @increment_last_spent_id="from_child_increment_last_spent_id"
                                @remove_one_subcategory="from_child_remove_one_subcategory"
                                @show_utility_dialog="from_child_show_utilities">
                            </expense-category>
                        </template>

                        <div @click="showDialog" style="display: flex; flex-direction: row; height: 42px; margin-right: 7px; border-top: 6px solid transparent; cursor: pointer; margin-bottom: 10px">
                            <div class="button-icon-add">
                                <v-icon>add</v-icon>
                            </div>
                            <div style="display: flex; align-items: center; margin-left: 8px">
                                Ajouter une nouvelle dépense
                            </div>
                        </div>

                    </div>
                    <v-btn color="blue-grey lighten-1" @click="distinctAction('evening')">Enregistrer</v-btn>
                </v-stepper-content>

            </v-stepper-items>
        </v-stepper>

        <v-dialog v-model="dialog" width="600px">

            <v-layout row wrap style="background: #f2f3f4">

                <v-flex xs12>
                    <v-container fluid grid-list-md>
                        <v-data-iterator
                            :items="category_items"
                            :pagination.sync="pagination"
                            content-tag="v-layout"
                            row
                            wrap
                            :hide-actions="true"
                            :total-items="category_items.length">
                            <v-flex
                                slot="item"
                                slot-scope="props"
                                xs12
                                sm6
                                md4
                                lg3>

                                <v-card style="cursor: pointer;" @click.native="chooseCategory(props.item)">

                                    <v-layout justify-center>
                                        <img :src="urlBaseImage+'uploads/categories/'+props.item.categoryImage" alt="" style="margin-top: 6px;width:42%;height:42%;object-fit:contain">
                                        <!--<v-avatar style="margin-top: 5px"
                                            :tile="tile"
                                            :size="avatarSize"
                                            color="transparent">

                                            <img src="https://via.placeholder.com/150x150" alt="koko">

                                        </v-avatar>-->
                                    </v-layout>

                                    <v-card-title>
                                        <template v-if="countCharOK(props.item.categoryLabel)">
                                            <p style="font-weight: bold; font-size: 13px; width: 250px; margin: 0 auto; overflow: hidden; white-space: nowrap; text-align: center">{{ props.item.categoryLabel }}</p>
                                        </template>
                                        <template v-else>
                                            <p style="font-weight: bold; font-size: 13px" class="marquee">{{ props.item.categoryLabel }}</p>
                                        </template>
                                    </v-card-title>

                                </v-card>
                            </v-flex>
                        </v-data-iterator>
                    </v-container>
                </v-flex>
            </v-layout>

        </v-dialog>

        <v-dialog v-model="show_utilities" width="320px">

            <v-layout row wrap style="background: #f2f3f4; width: 100%">
                <v-card height="60px" style="width: 100%">
                    <v-card-title class="blue white--text">
                        <span class="headline">Contexte</span>
                    </v-card-title>
                </v-card>
                <v-list style="width: 100%">
                    <v-list-tile v-for="(item, i) in utilities" :key="i" @click="manageUtilities(item)">
                        <v-list-tile-action>
                            <div :style="`border-radius: 50%; width: 20px; height: 20px; background: ${item.contextImage}`"></div>
                        </v-list-tile-action>
                        <v-list-tile-content>
                            <v-list-tile-title>
                                <span class="menu-text-font">{{ item.contextLabel }}</span>
                            </v-list-tile-title>
                        </v-list-tile-content>
                    </v-list-tile>
                </v-list>
            </v-layout>

        </v-dialog>

        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_list_category = '<?php echo URL_LIST_CATEGORY ?>';
    var url_list_category_final = `${url_base}${url_list_category}`;

    var url_create_spent = '<?php echo URL_SAVE_SPENT ?>';
    var url_create_spent_final = `${url_base}${url_create_spent}`;

    var url_list_spent = '<?php echo URL_LIST_SPENT ?>';
    var url_list_spent_final = `${url_base}${url_list_spent}`;

    var url_update_spent = '<?php echo URL_UPDATE_SPENT ?>';
    var url_update_spent_final = `${url_base}${url_update_spent}`;

    var url_last_spent = '<?php echo URL_LAST_SPENT ?>';
    var url_last_spent_final = `${url_base}${url_last_spent}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_list_context = '<?php echo URL_LIST_CONTEXT ?>';
    var url_list_context_final = `${url_base}${url_list_context}`;

    Vue.component('day-expense', {
        template: '#template-day-expense',
        mixins: [common_mixin],
        data: function(){
            return {
                date: new Date().toISOString().slice(0, 10),
                modal: false,
                e1: 0,
                dialog: false,
                category_expenses_morning: [],
                category_expenses_midday: [],
                category_expenses_evening: [],

                pagination: {
                    rowsPerPage: 12
                },
                category_items: [
                    "one", "two", "three", "four", "five", "six", "seven", "hiut", "neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix spet", "dix huit", "dix neuf", "vinght",  "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix spet", "dix huit", "dix neuf", "vinght"
                ],
                tile: false,
                avatarsize: 85,
                current_type_day: undefined,
                noduplicates_morning: [],
                noduplicates_midday: [],
                noduplicates_evening: [],
                urlBaseImage: url_base,

                expenses_morning_state: undefined,
                expenses_midday_state: undefined,
                expenses_evening_state: undefined,

                expenses_morning_updates: [],
                expenses_midday_updates: [],
                expenses_evening_updates: [],

                last_spent_id: 0,
                user_logged_in: undefined,

                show_utilities: false,
                utilities: [],
                data_from_child_utility_id: 0,
                data_from_child_category_id: 0,
                data_from_child_spent_id: 0,

            }
        },
        methods: {
            requestByDate: function(calendarDate){
                this.$refs.dialog.save(calendarDate);
                console.log(`this is the date=====>${calendarDate}`);
                this.wsDepense(calendarDate);
            },
            addCategory: function(type_day, cat_item){

                if(type_day === "morning"){
                    this.category_expenses_morning.push(this.getCategoryByParam(cat_item, []));
                }else if(type_day === "midday"){
                    this.category_expenses_midday.push(this.getCategoryByParam(cat_item, []));
                    console.log(`ADDED MIDDAY--->${JSON.stringify(this.category_expenses_midday)}`);
                }else if(type_day === "evening"){
                    this.category_expenses_evening.push(this.getCategoryByParam(cat_item, []));
                }

            },
            getCategoryByParam: function(cat, subcats){
                return {category: cat, subcategories: subcats};
            },
            getSubCategoryByParam: function(subcat, numb, pr){
                return {subcategory: subcat, number: numb, price: pr};
            },
            showDialog: function(){
                this.dialog = true;
            },
            wsCategory(){
                axios.get(url_list_category_final).then((response) => {
                    if(response.data.category == null){
                        //v.noResult()
                    }else{
                        //v.getData(response.data.category);
                        this.category_items = response.data.category;
                        //console.log(`CATEGORIES ARE ====>${JSON.stringify(response.data.category)}`)
                    }
                });
            },
            wsContext(){
                axios.get(url_list_context_final).then((response) => {
                    if(response.data.context == null){
                        console.log('context list empty')
                    }else{
                        this.utilities = response.data.context;
                        console.log(`CONTEXT ARE ====>${JSON.stringify(response.data.context)}`)
                    }
                });
            },
            chooseCategory: function(cat_item){

                if(this.current_type_day === 'morning'){
                    console.log(`LIST dupli MORNING=======>${this.noduplicates_morning}`);
                    if(!this.checkDuplicatedCategory(cat_item, this.noduplicates_morning)){
                        this.noduplicates_morning.push(cat_item.id);
                        this.addCategory('morning', cat_item);
                    }else{
                        console.log("efa ao morning");
                    }
                }else if(this.current_type_day === 'midday'){
                    console.log(`LIST dupli MIDDAY=======>${this.noduplicates_midday}`);
                    if(!this.checkDuplicatedCategory(cat_item, this.noduplicates_midday)){
                        this.noduplicates_midday.push(cat_item.id);
                        this.addCategory('midday', cat_item);
                    }else{
                        console.log("efa ao midday");
                    }
                }else if(this.current_type_day === 'evening'){
                    console.log(`LIST dupli EVENING=======>${this.noduplicates_evening}`);
                    if(!this.checkDuplicatedCategory(cat_item, this.noduplicates_evening)){
                        this.noduplicates_evening.push(cat_item.id);
                        this.addCategory('evening', cat_item);
                    }else{
                        console.log("efa ao evening");
                    }
                }

                this.dialog = false;
            },
            checkDuplicatedCategory: function(cat_item, arr){

                for(var i= 0; i < arr.length; i++){
                    console.log(`ITEM=======>${arr[i]}`);
                    if(arr[i] == cat_item.id){
                        console.log("ENTER IN");
                        return true;
                    }
                }
                return false;
            },
            checkSubcategory: function(subcat_item, arr){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].spent_id === subcat_item.spent_id){
                        return {exist: true, index: i};
                    }
                }
                return {exist: false, index: -1};
            },
            getSubcategoriesByCategory: function(arr, cat){
                var res = [];
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].category.id === cat.id){
                        res = arr[i].subcategories;
                    }
                }
                return res;
            },
            accessMorningDay: function(){
                this.current_type_day = "morning";
                console.log("ACCESS MORNING");
                this.$router.app.$emit('updateWelcomeSubtitle', "matin");
                //this.e1 = 1;
            },
            accessMidDay: function(){
                this.current_type_day = "midday";
                console.log("ACCESS MIDDAY");
                this.$router.app.$emit('updateWelcomeSubtitle', "midi");
                //this.e1 = 2;
            },
            accessEvening: function(){
                this.current_type_day = "evening";
                console.log("ACCESS EVENING");
                this.$router.app.$emit('updateWelcomeSubtitle', "soir");
                //this.e1 = 3;
            },
            from_child_update_entire_category: function(type_day, entire_cat){
                console.log(`JERENA ====> ${JSON.stringify(entire_cat)}`);

                if(type_day === "morning"){
                    for (var i = this.category_expenses_morning.length; i--;) {
                        console.log(`tafiditra ao am morning ====>${i} entire_cat.category_id===>${entire_cat.category_id} category_expenses_morning====> ${this.category_expenses_morning[i].category.id}`);
                        if(this.category_expenses_morning[i].category.id == entire_cat.category_id){
                            //console.log(`tafiditra ao am i ====>${i} length===>${this.category_expenses_morning.length}`);
                            this.category_expenses_morning[i].subcategories = entire_cat.subcategories;
                        }
                    }
                }else if(type_day === "midday"){
                    for (var i = this.category_expenses_midday.length; i--;) {
                        console.log(`tafiditra ao am midday ====>${i} entire_cat.category_id===>${entire_cat.category_id} category_expenses_midday====> ${JSON.stringify(this.category_expenses_midday)}`);
                        if(this.category_expenses_midday[i].category.id == entire_cat.category_id){
                            //console.log(`tafiditra ao am i ====>${i} length===>${this.category_expenses_midday.length}`);
                            this.category_expenses_midday[i].subcategories = entire_cat.subcategories;
                        }
                    }
                }else if(type_day === "evening"){
                    for (var i = this.category_expenses_evening.length; i--;) {
                        console.log(`tafiditra ao am evening ====>${i} entire_cat.category_id===>${entire_cat.category_id} category_expenses_evening====> ${this.category_expenses_evening[i].category.id}`);
                        if(this.category_expenses_evening[i].category.id == entire_cat.category_id){
                            //console.log(`tafiditra ao am i ====>${i} length===>${this.category_expenses_evening.length}`);
                            this.category_expenses_evening[i].subcategories = entire_cat.subcategories;
                        }
                    }
                }

            },
            from_child_delete_entire_category: function(type_day, selected_category, index){
                if(type_day === "morning"){
                    console.log(`delete_entire_category MORNING IDCAT====>${selected_category.id}`);
                    this.spliceByCategory(this.category_expenses_morning, selected_category, index, type_day);
                    this.removeFromDuplicate(this.noduplicates_morning, selected_category.id);
                }else if(type_day === "midday"){
                    console.log("delete_entire_category MIDDAY");
                    this.spliceByCategory(this.category_expenses_midday, selected_category, index, type_day);
                    this.removeFromDuplicate(this.noduplicates_midday, selected_category.id);
                }else if(type_day === "evening"){
                    console.log("delete_entire_category EVENING");
                    this.spliceByCategory(this.category_expenses_evening, selected_category, index, type_day);
                    this.removeFromDuplicate(this.noduplicates_evening, selected_category.id);
                }
            },
            from_child_update_one_subcategory: function(type_day, subcat, action_type){

                console.log(`=====>LISTING MODIFY OBJECT : ${JSON.stringify(subcat)}`);
                subcat.action = action_type;
                if(type_day === "morning"){

                    if(this.expenses_morning_updates.length > 0){

                        let checked = this.checkSubcategory(subcat, this.expenses_morning_updates);
                        if(checked.exist){
                            this.expenses_morning_updates[checked.index] = subcat;
                        }else{
                            this.expenses_morning_updates.push(subcat);
                        }

                    }else{
                        this.expenses_morning_updates.push(subcat);
                    }
                    console.log(`expenses_morning_updates=====> ${JSON.stringify(this.expenses_morning_updates)}`);

                }else if(type_day === "midday"){

                    if(this.expenses_midday_updates.length > 0){

                        let checked = this.checkSubcategory(subcat, this.expenses_midday_updates);
                        if(checked.exist){
                            this.expenses_midday_updates[checked.index] = subcat;
                        }else{
                            this.expenses_midday_updates.push(subcat);
                        }

                    }else{
                        this.expenses_midday_updates.push(subcat);
                    }
                    console.log(`expenses_midday_updates=====> ${JSON.stringify(this.expenses_midday_updates)}`);

                }else if(type_day === "evening"){

                    if(this.expenses_evening_updates.length > 0){

                        let checked = this.checkSubcategory(subcat, this.expenses_evening_updates);
                        if(checked.exist){
                            this.expenses_evening_updates[checked.index] = subcat;
                        }else{
                            this.expenses_evening_updates.push(subcat);
                        }

                    }else{
                        this.expenses_evening_updates.push(subcat);
                    }
                    console.log(`expenses_evening_updates=====> ${JSON.stringify(this.expenses_evening_updates)}`);

                }

            },
            from_child_increment_last_spent_id: function(){

                this.last_spent_id += 1;

            },
            from_child_remove_one_subcategory: function(type_day, subcat){

                if(type_day === "morning"){
                    this.ejectupdates(this.expenses_morning_updates, subcat);
                    console.log(`expenses_morning_updates=====> ${JSON.stringify(this.expenses_morning_updates)}`);
                }else if(type_day === "midday"){
                    this.ejectupdates(this.expenses_midday_updates, subcat);
                    console.log(`expenses_midday_updates=====> ${JSON.stringify(this.expenses_midday_updates)}`);
                }else if(type_day === "evening"){
                    this.ejectupdates(this.expenses_evening_updates, subcat);
                    console.log(`expenses_evening_updates=====> ${JSON.stringify(this.expenses_evening_updates)}`);
                }

            },
            from_child_show_utilities: function(index, utility_id, type_day, category_id, spent_id){
                //console.log(`SHOW DIALOG UTILITY with index = ${index} with utility id = ${utility_id}`);
                if(this.utilities && this.utilities.length > 0){
                    this.data_from_child_utility_id = utility_id;
                    this.data_from_child_category_id = category_id;
                    this.data_from_child_spent_id = spent_id;
                    this.show_utilities = true;
                }
            },
            ejectupdates: function(arr, item){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].spent_id == item.spent_id){
                        arr.splice(i, 1);
                    }
                }
            },
            transformActionToRequest: function(subcat){
                var req = undefined;
                let action_type = subcat.action;
                if(action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    req = `UPDATE spent SET number = ${subcat.number}, unitPrice = ${subcat.price}, subcategoryId = ${subcat.subcategory}, contextId = ${subcat.utility.id} WHERE id = ${subcat.spent_id};`;
                }else if(action_type === '<?php echo CONSTANT_DB_REMOVE ?>'){
                    req = `DELETE FROM spent WHERE id = ${subcat.spent_id};`;
                }else if(action_type === '<?php echo CONSTANT_DB_INSERT ?>'){
                    req = `INSERT INTO spent (unitPrice, number, subcategoryId, userId, dayType, contextId) VALUES (${subcat.price}, ${subcat.number}, ${subcat.subcategory}, ${this.user_logged_in.id}, '${this.current_type_day}', ${subcat.utility.id});`;
                }
                return req;
            },
            getAllRequest: function(arr){
                var concatenated = "";
                for(var i= 0; i < arr.length; i++){
                    concatenated += this.transformActionToRequest(arr[i]);
                }
                return concatenated;
            },
            removeFromDuplicate: function(array, item){
                var index = array.indexOf(item);
                if (index !== -1)
                    array.splice(index, 1);
            },
            spliceByCategory: function(arr, cat, index, type_day) {

                let subcats = this.getSubcategoriesByCategory(arr, cat);
                console.log(`spliceByCategory subcats====>${JSON.stringify(subcats)}`);
                subcats.forEach((subcat) => {
                    this.from_child_update_one_subcategory(type_day, subcat, '<?php echo CONSTANT_DB_REMOVE ?>');
                })
                arr.splice(index, 1)

            },
            save: function(type_day) {
                this.data_loader = true;
                var allowPost = false;
                var formData = new FormData();
                if(type_day === "morning"){
                    console.log("SAVE MORING");
                    if(this.category_expenses_morning.length > 0){
                        console.log(`category_expenses_morning====>${JSON.stringify(this.category_expenses_morning)}`);
                        formData.append('spents', JSON.stringify(this.formatJson(this.category_expenses_morning, "morning")));
                        allowPost = true;
                    }
                }else if(type_day === "midday"){
                    console.log("SAVE MIDDAY");
                    if(this.category_expenses_midday.length > 0){
                        console.log(`category_expenses_midday====>${JSON.stringify(this.category_expenses_midday)}`);
                        formData.append('spents', JSON.stringify(this.formatJson(this.category_expenses_midday, "midday")));
                        allowPost = true;
                    }
                }else if(type_day === "evening"){
                    console.log("SAVE EVENING");
                    if(this.category_expenses_evening.length > 0){
                        console.log(`category_expenses_evening====>${JSON.stringify(this.category_expenses_evening)}`);
                        formData.append('spents', JSON.stringify(this.formatJson(this.category_expenses_evening, "evening")));
                        allowPost = true;
                    }
                }

                if(allowPost){
                    axios.post(url_create_spent_final, formData).then((response) =>{
                        console.log(`DATA SENT here====>${JSON.stringify(response.data.spents)}`);

                        if(response.data.error){
                            alert("ERROR WHEN SAVING");
                        }else{
                            this.wsDepense(this.date);
                        }
                    });
                }else{
                    console.log("NOTHING TO SAVE OR ADD");
                    this.data_loader = false;
                }

            },
            update: function(type_day) {
                this.data_loader = true;
                var allowPost = false;
                var formData = new FormData();
                if(type_day === "morning"){
                    //console.log("UPDATE MORNING");
                    if(this.expenses_morning_updates.length > 0){
                        console.log(`UPDATE MORING =====> ${JSON.stringify(this.getAllRequest(this.expenses_morning_updates))}`);
                        formData.append("request", this.getAllRequest(this.expenses_morning_updates));
                        allowPost = true;
                    }
                }else if(type_day === "midday"){
                    console.log("UPDATE MIDDAY");
                    if(this.expenses_midday_updates.length > 0){
                        console.log(`UPDATE MIDDAY =====> ${JSON.stringify(this.getAllRequest(this.expenses_midday_updates))}`);
                        formData.append("request", this.getAllRequest(this.expenses_midday_updates));
                        allowPost = true;
                    }
                }else if(type_day === "evening"){
                    console.log("UPDATE EVENING");
                    if(this.expenses_evening_updates.length > 0){
                        console.log(`UPDATE EVENING =====> ${JSON.stringify(this.getAllRequest(this.expenses_evening_updates))}`);
                        formData.append("request", this.getAllRequest(this.expenses_evening_updates));
                        allowPost = true;
                    }
                }

                if(allowPost){
                    axios.post(url_update_spent_final, formData).then((response) =>{
                        console.log(`DATA UPDATE MESSAGE here====>${JSON.stringify(response.data.sqls)}`);
                        console.log(`DATA UPDATE NOMBRE here====>${JSON.stringify(response.data.nb)}`);

                        if(response.data.error){
                            alert("ERROR WHEN UPDATING");
                        }else{
                            this.wsDepense(this.date);
                        }
                    });
                }else{
                    console.log("NOTHING TO UPDATE");
                    this.data_loader = false;
                }

            },
            distinctAction: function(type_day) {

                if(type_day === "morning"){
                    //console.log("DISTINCT MORING");
                    if(this.expenses_morning_state === '<?php echo CONSTANT_LISTING ?>'){
                        this.update('morning');
                    }else{
                        this.save('morning');
                    }

                }else if(type_day === "midday"){
                    //console.log("UPDATE MIDDAY");
                    if(this.expenses_midday_state === '<?php echo CONSTANT_LISTING ?>'){
                        this.update('midday');
                    }else{
                        this.save('midday')
                    }

                }else if(type_day === "evening"){
                    //console.log("UPDATE EVENING");
                    if(this.expenses_evening_state === '<?php echo CONSTANT_LISTING ?>'){
                        this.update('evening');
                    }else{
                        this.save('evening')
                    }

                }


            },
            formatJson: function(array_exp, day_type) {

                var res_array = [];
                array_exp.forEach((cat) => {
                    cat.subcategories.forEach((subcat) => {
                        res_array.push({unitPrice: subcat.price, number: subcat.number, subcategoryId: subcat.subcategory, userId: this.user_logged_in.id, dayType: day_type, contextId: subcat.utility.id, spentDate: this.date });
                    })
                })

                return res_array;

            },
            wsDepense: function(date) {

                this.data_loader = true;
                this.resetAllExpensesData();
                //let today = new Date().toISOString().slice(0, 10);
                var formData = new FormData();
                formData.append('user_id', this.user_logged_in.id);
                console.log(`today ====> ${date}`)
                formData.append('spent_date', date);
                window.axios.post(url_list_spent_final, formData).then((response) => {
                    if(response.data.spents == null){
                        console.log("response.data.spents ==> null");
                    }else{
                        //console.log(`SPENTS LIST=====>${JSON.stringify(response.data.spents)}`)

                        //-----------------------MORNING LIST------------------------------
                        let by_typeday_morning = this.regroupSpentByTypeDay(response.data.spents, 'morning');
                        var ids_morning = this.takeAllCatId(by_typeday_morning);
                        var filtered_morning = ids_morning.filter((v,i) => ids_morning.indexOf(v) == i);
                        //console.log(`filtered_morning====>${JSON.stringify(filtered_morning)}`);
                        if(filtered_morning && filtered_morning.length > 0){
                            for(var i= 0; i < filtered_morning.length; i++){
                                this.category_expenses_morning.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_morning, filtered_morning[i])));
                                this.noduplicates_morning.push(filtered_morning[i]);
                            }
                        }
                        //console.log(`category_expenses_morning from WS====>${JSON.stringify(this.category_expenses_morning)}`);

                        //---------------------MIDDAY LIST----------------------------------
                        let by_typeday_midday = this.regroupSpentByTypeDay(response.data.spents, 'midday');
                        var ids_midday = this.takeAllCatId(by_typeday_midday);
                        var filtered_midday = ids_midday.filter((v,i) => ids_midday.indexOf(v) == i);
                        //console.log(`filtered_midday====>${JSON.stringify(filtered_midday)}`);
                        if(filtered_midday && filtered_midday.length > 0){
                            for(var i= 0; i < filtered_midday.length; i++){
                                this.category_expenses_midday.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_midday, filtered_midday[i])));
                                this.noduplicates_midday.push(filtered_midday[i]);
                            }
                        }

                        //---------------------EVENING LIST----------------------------------
                        let by_typeday_evening = this.regroupSpentByTypeDay(response.data.spents, 'evening');
                        var ids_evening = this.takeAllCatId(by_typeday_evening);
                        var filtered_evening = ids_evening.filter((v,i) => ids_evening.indexOf(v) == i);
                        //console.log(`filtered_evening====>${JSON.stringify(filtered_evening)}`);
                        if(filtered_evening && filtered_evening.length > 0){
                            for(var i= 0; i < filtered_evening.length; i++){
                                this.category_expenses_evening.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_evening, filtered_evening[i])));
                                this.noduplicates_evening.push(filtered_evening[i]);
                            }
                        }

                        //---------------------CHECK IF ADD OR UPDATE--------------------------
                        if(this.category_expenses_morning.length > 0){
                            this.expenses_morning_state = '<?php echo CONSTANT_LISTING ?>';
                        }else{
                            this.expenses_morning_state = '<?php echo CONSTANT_ADDING ?>';
                        }

                        if(this.category_expenses_midday.length > 0){
                            this.expenses_midday_state = '<?php echo CONSTANT_LISTING ?>';
                        }else{
                            this.expenses_midday_state = '<?php echo CONSTANT_ADDING ?>';
                        }

                        if(this.category_expenses_evening.length > 0){
                            this.expenses_evening_state = '<?php echo CONSTANT_LISTING ?>';
                        }else{
                            this.expenses_evening_state = '<?php echo CONSTANT_ADDING ?>';
                        }


                    }
                })
                .catch(err => {
                    console.log(err)
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            resetAllExpensesData: function(){
                this.category_expenses_morning = [];
                this.category_expenses_midday = [];
                this.category_expenses_evening = [];
                this.noduplicates_morning = [];
                this.noduplicates_midday = [];
                this.noduplicates_evening = [];
                this.expenses_morning_updates = [];
                this.expenses_midday_updates = [];
                this.expenses_evening_updates = [];
            },
            wsgetLastSpentId: function(){
                axios.get(url_last_spent_final).then((response) => {
                    if(response.data.error){

                    }else{
                        this.last_spent_id = Number(response.data.spent_id[0].id) + 1;
                        console.log(`LAST SPENT ID ====>${response.data.spent_id[0].id}`)
                    }
                });
            },
            manageUtilities: function(item){
                this.show_utilities = false;

                if(this.current_type_day === "morning"){

                    for(var i= 0; i < this.category_expenses_morning.length; i++){
                        if(this.data_from_child_category_id === this.category_expenses_morning[i].category.id){
                            for(var j= 0; j < this.category_expenses_morning[i].subcategories.length; j++){
                                if(this.category_expenses_morning[i].subcategories[j].spent_id === this.data_from_child_spent_id){

                                    this.$set(this.category_expenses_morning[i].subcategories[j], 'utility', item);

                                }
                            }

                        }
                    }

                }else if(this.current_type_day === "midday"){

                    for(var i= 0; i < this.category_expenses_midday.length; i++){
                        if(this.data_from_child_category_id === this.category_expenses_midday[i].category.id){
                            for(var j= 0; j < this.category_expenses_midday[i].subcategories.length; j++){
                                if(this.category_expenses_midday[i].subcategories[j].spent_id === this.data_from_child_spent_id){

                                    this.$set(this.category_expenses_midday[i].subcategories[j], 'utility', item);

                                }
                            }

                        }
                    }

                }else if(this.current_type_day === "evening"){

                    for(var i= 0; i < this.category_expenses_evening.length; i++){
                        if(this.data_from_child_category_id === this.category_expenses_evening[i].category.id){
                            for(var j= 0; j < this.category_expenses_evening[i].subcategories.length; j++){
                                if(this.category_expenses_evening[i].subcategories[j].spent_id === this.data_from_child_spent_id){

                                    this.$set(this.category_expenses_evening[i].subcategories[j], 'utility', item);

                                }
                            }

                        }
                    }

                }

                this.$forceUpdate();
            },
            countCharOK(str){
                if(str && str.length > 17)
                    return false;
                return true;
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log("DAY PAGE USER LOGGED IN");
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }
                    this.wsgetLastSpentId();
                    this.wsCategory();
                    this.wsDepense(this.date);
                    this.accessMorningDay();
                    this.wsContext();
                }else{
                    console.log("DAY PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            }

        },
        created: function () {
            this.$router.app.$emit('updateWelcomeSubtitle', "matin");
        },
        mounted: function () {
            console.log('day : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        }
    });

</script>