<?php

class Spent_model extends CI_Model{

    public function addSpent($data){
        $insert = $this->db->insert_batch('spent', $data); 
        return $insert;
    }

    public function showDaySpent($userId, $date){
        //without category
        /*$query = $this->db->get_Where('spent', array('userId'=>$userId));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }*/

        //with category by userid & date with format => '2018-11-27'
        $this->db->select('spent.id, spent.unitPrice, spent.number, spent.subcategoryId, spent.userId, spent.spentDate, spent.dayType, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage, spent.contextId, context.contextLabel, context.contextImage, context.contextColor');
        $this->db->from('spent');
        $this->db->where('userId', $userId);
        $this->db->where('DATE(spentDate)', $date);
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('context', 'context.id = spent.contextId');
        $query=$this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function updateDaySpent($sql){
        $i = 0;
        $sqls = explode(';', $sql);
        array_pop($sqls);
        $this->db->trans_start();
        foreach($sqls as $statement){
            $statment = $statement . ";";
            $this->db->query($statement); 
            $i++;  
        }
        $this->db->trans_complete(); 
        return $i;

    }

    public function getLastSpentId(){

        $this->db->select('id');
        $this->db->from('spent');
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }

    }

    public function showDayTask($userId, $date){
        
        $this->db->select('spent.id, spent.unitPrice, spent.number, spent.subcategoryId, spent.userId, spent.spentDate, spent.dayType, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage, spent.contextId, context.contextLabel, context.contextImage, context.contextColor, spent.eventId, event.eventDebut, event.eventStatut, place.id, place.place_description, event.subtaskId, subtask.subtaskLabel, task.taskLabel, event.assignedUserId, users.first_name, users.last_name, users.user_image, users.fcm_id');
        $this->db->from('spent');
        $this->db->where('userId', $userId);
        $this->db->where('dayType', "task");
        $this->db->where('DATE(event.eventDebut)', $date);
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('context', 'context.id = spent.contextId');
        $this->db->join('event', 'event.id = spent.eventId');
        $this->db->join('place', 'place.id = event.placeId');
        $this->db->join('subtask', 'subtask.id = event.subtaskId');
        $this->db->join('task', 'task.id = subtask.taskId');
        $this->db->join('users', 'users.id = event.assignedUserId');
        $query=$this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function showMonthWeekSpent($userId, $month, $year){
        $this->db->select('spent.id, spent.unitPrice, spent.number, spent.subcategoryId, spent.userId, spent.spentDate, spent.dayType, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage, spent.contextId, context.contextLabel, context.contextImage, context.contextColor');
        $this->db->from('spent');
        $this->db->where('userId', $userId);
        $this->db->where('MONTH(spentDate)', $month);
        $this->db->where('YEAR(spentDate)', $year);
        $list_daytype = array('monthly','weekly');
        $this->db->where_in('dayType', $list_daytype);
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('context', 'context.id = spent.contextId');
        $query=$this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    /************************ DELETE SPENT BY EVENT ID***********************/
    public function deleteSpentByEvent($id){
        $deleted = $this->db->delete('spent', array('eventId' => $id));
        return $deleted;
    }

    /*********************** /REPORT SPENT***********************/
    public function reportSimpleSpentGlobal($userId, $spentMonth, $spentYear){
        $this->db->select('id, unitPrice, number, subcategoryId, userId, DATE(spentDate) as spentDate, dayType');
        $this->db->from('spent');
        $this->db->where('userId', $userId);
        $this->db->where('MONTH(spentDate)', $spentMonth);
        $this->db->where('YEAR(spentDate)', $spentYear);
        $list_daytype = array('morning','midday','evening');
        $this->db->where_in('dayType', $list_daytype);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    /*********************** /REPORT TASK***********************/
    public function reportSimpleTaskGlobal($userId, $spentMonth, $spentYear){
        $this->db->select('spent.id, unitPrice, number, subcategoryId, userId, DATE(event.eventDebut) as eventDebut, dayType, spent.eventId');
        $this->db->from('spent');
        $this->db->where('userId', $userId);
        $this->db->where('MONTH(event.eventDebut)', $spentMonth);
        $this->db->where('YEAR(event.eventDebut)', $spentYear);
        $this->db->where('dayType', 'task');
        $this->db->join('event', 'event.id = spent.eventId');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    /*********************** /REPORT CATEGORY***********************/
    public function reportCategorySpent($userId, $spentMonth, $spentYear){
        $this->db->select('unitPrice, number, userId, DATE(spentDate) as spentDate, dayType, subcategoryId, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage');
        $this->db->from('spent');
        $this->db->where('userId', $userId);
        $this->db->where('MONTH(spentDate)', $spentMonth);
        $this->db->where('YEAR(spentDate)', $spentYear);
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $list_daytype = array('morning','midday','evening', 'weekly', 'monthly');
        $this->db->where_in('dayType', $list_daytype);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function reportCategoryTask($userId, $spentMonth, $spentYear){
        $this->db->select('unitPrice, number, userId, DATE(event.eventDebut) as eventDebut, dayType, subcategoryId, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage');
        $this->db->from('spent');
        $this->db->where('userId', $userId);
        $this->db->where('MONTH(event.eventDebut)', $spentMonth);
        $this->db->where('YEAR(event.eventDebut)', $spentYear);
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('event', 'event.id = spent.eventId');
        $this->db->where('dayType', 'task');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    /*********************** /TASK NOTIFICATION***********************/
    public function checkNotificationTask($userId){
        $this->db->select('id, DATE(eventDebut) as eventDebut, eventStatut, eventConsulted, subtaskId, assignedUserId');
        $this->db->from('event');
        $this->db->where('event.assignedUserId', $userId);
        $this->db->where('event.eventConsulted', 1);
        $this->db->where('event.eventStatut', 'TODO');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function getNotificationTask($userId, $date){
        $this->db->select('spent.id, unitPrice, number, spent.subcategoryId, userId, event.eventDebut, dayType, spent.eventId, place.place_description, event.subtaskId, event.eventStatut, event.assignedUserId, subtask.subtaskLabel, task.taskLabel, users.first_name, users.last_name, users.user_image, users.fcm_id, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage, spent.contextId, context.contextLabel, context.contextImage, context.contextColor');
        $this->db->from('spent');
        $this->db->where('event.assignedUserId', $userId);
        //$this->db->where('event.eventConsulted', 1);
        $this->db->where('spent.dayType', 'task');
        $this->db->where('DATE(event.eventDebut)', $date);
        $this->db->join('event', 'event.id = spent.eventId');
        $this->db->join('place', 'place.id = event.placeId');
        $this->db->join('subtask', 'subtask.id = event.subtaskId');
        $this->db->join('task', 'task.id = subtask.taskId');
        $this->db->join('users', 'users.id = spent.userId');
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('context', 'context.id = spent.contextId');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function getOtherNotificationTask($userId, $date){
        $this->db->select('spent.id, unitPrice, number, spent.subcategoryId, userId, event.eventDebut, dayType, spent.eventId, place.place_description, event.subtaskId, event.eventStatut, event.assignedUserId, subtask.subtaskLabel, task.taskLabel, users.first_name, users.last_name, users.user_image, users.fcm_id, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage, spent.contextId, context.contextLabel, context.contextImage, context.contextColor');
        $this->db->from('spent');
        $this->db->where('spent.userId', $userId);
        $this->db->where('event.eventConsulted', 1);
        $this->db->where('spent.dayType', 'task');
        $this->db->where('DATE(event.eventDebut)', $date);
        $this->db->join('event', 'event.id = spent.eventId');
        $this->db->join('place', 'place.id = event.placeId');
        $this->db->join('subtask', 'subtask.id = event.subtaskId');
        $this->db->join('task', 'task.id = subtask.taskId');
        $this->db->join('users', 'users.id = event.assignedUserId');
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('context', 'context.id = spent.contextId');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function getRangeNotificationTask($userId, $begin, $end){
        $this->db->select('spent.id, unitPrice, number, spent.subcategoryId, userId, event.eventDebut, dayType, spent.eventId, place.place_description, event.subtaskId, event.eventStatut, event.assignedUserId, subtask.subtaskLabel, task.taskLabel, users.first_name, users.last_name, users.user_image, users.fcm_id, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage, spent.contextId, context.contextLabel, context.contextImage, context.contextColor');
        $this->db->from('spent');
        $this->db->where('event.assignedUserId', $userId);
        $this->db->where('spent.dayType', 'task');
        $this->db->where('DATE(event.eventDebut) >=', $begin);
        $this->db->where('DATE(event.eventDebut) <=', $end);
        $this->db->join('event', 'event.id = spent.eventId');
        $this->db->join('place', 'place.id = event.placeId');
        $this->db->join('subtask', 'subtask.id = event.subtaskId');
        $this->db->join('task', 'task.id = subtask.taskId');
        $this->db->join('users', 'users.id = spent.userId');
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('context', 'context.id = spent.contextId');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function getOtherRangeNotificationTask($userId, $begin, $end){
        $this->db->select('spent.id, unitPrice, number, spent.subcategoryId, userId, event.eventDebut, dayType, spent.eventId, place.place_description, event.subtaskId, event.eventStatut, event.assignedUserId, subtask.subtaskLabel, task.taskLabel, users.first_name, users.last_name, users.user_image, users.fcm_id, subcategory.subcategoryLabel, subcategory.subcategoryImage, subcategory.categoryId, category.categoryLabel, category.categoryImage, spent.contextId, context.contextLabel, context.contextImage, context.contextColor');
        $this->db->from('spent');
        $this->db->where('spent.userId', $userId);
        $this->db->where('event.eventConsulted', 1);
        $this->db->where('spent.dayType', 'task');
        $this->db->where('DATE(event.eventDebut) >=', $begin);
        $this->db->where('DATE(event.eventDebut) <=', $end);
        $this->db->join('event', 'event.id = spent.eventId');
        $this->db->join('place', 'place.id = event.placeId');
        $this->db->join('subtask', 'subtask.id = event.subtaskId');
        $this->db->join('task', 'task.id = subtask.taskId');
        $this->db->join('users', 'users.id = event.assignedUserId');
        $this->db->join('subcategory', 'subcategory.id = spent.subcategoryId');
        $this->db->join('category', 'category.id = subcategory.categoryId');
        $this->db->join('context', 'context.id = spent.contextId');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

}