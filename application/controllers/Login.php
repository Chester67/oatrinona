<?php

class Login extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('user_model','user');
    }

	public function index()
	{
		$this->load->view('welcome');
		//$this->load->view('components/login/login');
	}
	
	public function authenticate(){

		$result['error'] = true;
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$query_result = $this->user->loginUser($username, $password);
		if($query_result){
			$result['error'] = false;
			$result['user'] = $query_result;
			$this->session->set_userdata('isUserLoggedIn', TRUE);
			$this->session->set_userdata('user', $query_result);
			if($query_result[0]->role === "ADMINISTRATOR"){
				$this->session->set_userdata('chooseduser', $query_result);
			}else{
				$this->session->set_userdata('chooseduser', null);
			}
		}	
		echo json_encode($result);

	}

	public function isLogged(){

		$result['logged'] = false;
		$isLogged = $this->session->userdata("isUserLoggedIn");
		if($isLogged){
			$result['logged'] = true;
			$result['user'] = $this->session->userdata("user");
			$result['chooseduser'] = $this->session->userdata("chooseduser");
		}
		echo json_encode($result);

	}

	public function logout(){
		$result['error'] = true;
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user');
		$this->session->sess_destroy();
		$result['error'] = false;
        echo json_encode($result);
    }
    
}

?>