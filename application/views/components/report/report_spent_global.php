<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
$this->load->view('components/empty/data_empty');
?>

<style type="text/css">
    .sticky-spent-global {
        position: -webkit-sticky;
        position: sticky;
        width: 100%;
        top: 70px;
        top-max: 70px;
        top-min: 70px;
        height: 60px;
    }

    .a {
        width: 100%;
        margin: 0 auto;
        position: relative;
        display: table;
    }

    .c {
        display: table-cell;
        width:83%;
        height: 20px !important;
        margin-top: -5px;
    }

    .d {
        text-align: right;
        float: right;
    }

    .e {
        position: absolute;
        left: 50%;
        display: inline;
        width: auto;
        transform: translateX(-50%);
    }

    .text-head-report-global {

        line-height: 14px;     /* fallback */
        white-space: normal;
        -webkit-box-orient: vertical;
        color: white;
        font-weight: bold;
        border-radius: 19px;
        padding-left: 10px;
        padding-right: 7px;
        padding-top: 1px;
        padding-bottom: 1px;

    }

</style>

<script type="text/x-template" id="template-report-spent-global">

    <div style="background: white; height: 100%">
        <v-container fluid>

            <v-toolbar v-if="!data_no_user" class="sticky-spent-global" style="margin-top: -6px">

                <v-dialog
                    ref="dialog"
                    v-model="modal"
                    :return-value.sync="date"
                    persistent
                    lazy
                    full-width
                    width="520px"
                    justify-center
                    style="width: 100% !important">

                    <div slot="activator">
                        <div style="vertical-align: middle; float: right; margin-right: 10px">
                            <div class="hidden-sm-and-down" style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 3px">
                                <v-icon>event</v-icon>
                                <h1 style="margin-left: 5px">{{ date | format_month }}</h1>
                            </div>
                        </div>
                    </div>

                    <v-date-picker v-model="date" full-width type="month" class="mt-3" locale="fr-fr">
                        <v-spacer></v-spacer>
                        <v-btn flat color="primary" @click="modal = false">Annuler</v-btn>
                        <v-btn flat color="primary" @click="requestByMonth(date)">OK</v-btn>
                    </v-date-picker>

                </v-dialog>

                <!--<v-menu style="position: absolute; right: 25px">
                    <v-btn
                        slot="activator"
                        dark
                        icon>
                        <v-icon style="color: grey">more_vert</v-icon>
                    </v-btn>
                    <v-list>
                        <v-list-tile v-for="(item, i) in menus" :key="i" @click="manageReportContextualMenu(item)">
                            <v-list-tile-action>
                                <v-icon>{{ item.icon }}</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                                <v-list-tile-title>
                                    <span class="menu-text-font">{{ item.title }}</span>
                                </v-list-tile-title>
                            </v-list-tile-content>
                        </v-list-tile>
                    </v-list>
                </v-menu>-->

                <div style="position: absolute; left: 25px; cursor: pointer" @click="modal = true">
                    <h1>Mois : {{ getMonthByInt(getMonthOfDate(date)) }} {{ getYearOfDate(date) }}</h1>
                </div>

            </v-toolbar>
            <template v-if="!data_no_user">
                <accordion theme="grey lighten-1" class="shadowed-part" style="margin-left: -2px; margin-right: -2px">
                    <div slot="header">
                        <span style="color: white">Dépenses & tâches quotidiennes</span>
                        <span class="hidden-sm-and-down" style="position: absolute; right: 7px; top: -10px; color: #455A64; margin-right: 45px"><span class="text-head-report-global hidden-sm-and-down" style="background: #058DC7">Dépenses</span> {{ total_spent_quotidienne }} Ar</span>
                        <span class="hidden-sm-and-down" style="position: absolute; right: 7px; top: 12px; color: #455A64; margin-right: 45px"><span class="text-head-report-global hidden-sm-and-down" style="background: #AADFF3">Tâches</span> {{ total_task_quotidienne }} Ar</span>
                    </div>
                    <div>
                        <template v-if="selected_chart === 'all'" style="height: 380px; width: 100%">
                            <v-layout wrap style="height: 100%">
                                <v-flex xs12 md6 style="">
                                    <template v-if="all_date_filtered_combined.length > 0 && (all_total_spent.length > 0 || all_total_task.length > 0)">
                                        <line-chart style="margin-top: 5px;" :dates="all_date_filtered_combined" :totalspents="all_total_spent" :totaltasks="all_total_task"></line-chart>
                                    </template>
                                    <template v-else>
                                        <data-empty></data-empty>
                                    </template>
                                </v-flex>
                                <v-flex xs12 md6>

                                    <div style="display: flex; flex-direction: column">
                                        <v-flex style="height: 190px;">
                                            <template v-if="all_date_bar.length > 0">
                                                <stacked-bar-chart :height="190" :dates="all_date_bar" :totalmorning="all_total_morning" :totalmidday="all_total_midday" :totalevening="all_total_evening"></stacked-bar-chart>
                                            </template>
                                            <template v-else>
                                                <div style="text-align: center; display: flex; align-items: center; justify-content: center; height: 190px;"><span style="font-weight: bold">Pas de dépenses !</span></div>
                                            </template>
                                        </v-flex>
                                        <v-flex style="height: 190px;">
                                            <template v-if="all_date_task.length > 0">
                                                <bar-chart :height="190" :dates="all_date_task" :total="all_number_task"></bar-chart>
                                            </template>
                                            <template v-else>
                                                <div style="text-align: center; display: flex; align-items: center; justify-content: center; height: 190px;"><span style="font-weight: bold">Pas de tâches !</span></div>
                                            </template>
                                        </v-flex>
                                    </div>

                                </v-flex>
                                <div class="hidden-sm-and-down" style="height: 400px; width: 1px; background: #BDBDBD; position: absolute; left: 0; right: 0; margin: auto"></div>
                            </v-layout>

                        </template>
                        <template v-if="selected_chart === 'line'">
                            <template v-if="all_date_filtered_combined.length > 0 && (all_total_spent.length > 0 || all_total_task.length > 0)">
                                <line-chart style="margin-top: 10px; margin-left: 25px" :dates="all_date_filtered_combined" :totalspents="all_total_spent" :totaltasks="all_total_task"></line-chart>
                            </template>
                            <template v-else>
                                <data-empty></data-empty>
                            </template>
                        </template>
                        <template v-else-if="selected_chart === 'bar'">
                            <template v-if="all_date_bar.length > 0">
                                <div style="text-align: center;"><span style="font-weight: bold; display: inline-block;">Dépenses triées</span></div>
                                <stacked-bar-chart style="margin-top: 5px; margin-left: 25px" :dates="all_date_bar" :totalmorning="all_total_morning" :totalmidday="all_total_midday" :totalevening="all_total_evening"></stacked-bar-chart>
                            </template>
                            <template v-else>
                                <data-empty></data-empty>
                            </template>
                        </template>
                    </div>
                </accordion>

                <v-container grid-list-md style="padding: 0 !important">
                    <v-layout wrap style="margin-left: 0px; margin-right: 0px; margin-top: 0px">
                        <v-flex xs12 md6>
                            <accordion theme="grey lighten-1" class="shadowed-part" style="margin-left: 0px; margin-right: 0px; margin-top: 0px; ">
                                <div slot="header">
                                    <span style="color: white">Dépenses mensuelles</span>
                                    <span style="position: absolute; right: 7px; top: 2px; color: #455A64; margin-right: 45px">{{ total_monthly }} Ar</span>
                                </div>
                                <div>
                                    <v-data-iterator
                                        no-data-text="Pas de données"
                                        :items="category_expenses_monthly"
                                        :rows-per-page-items="rowsPerPageItems"
                                        :pagination.sync="pagination"
                                        content-tag="v-layout"
                                        row
                                        hide-actions
                                        wrap
                                        style="margin-top: 6px">

                                        <v-flex
                                            slot="item"
                                            slot-scope="props"
                                            xs12>

                                            <v-layout row wrap class="card-report card-1-report" style="background:#ECEFF1; margin-bottom: 5px; margin-left: 1px; margin-right: 1px">

                                                <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                                                    <div>
                                                        <v-layout justify-center fill-height align-center>
                                                            <div style="display:block; margin:0 auto; text-align: center;">

                                                                <img :src="urlBaseImage+'/uploads/categories/'+props.item.category.categoryImage" alt="" style="margin-top: 5px;width:42%;height:42%;object-fit:contain">
                                                                <p class="text-xs-center" style="margin-top: -4px; font-size: 12px">{{ props.item.category.categoryLabel }}</p>

                                                            </div>
                                                        </v-layout>
                                                    </div>
                                                </v-flex>

                                                <v-flex d-flex xs12 sm6 md9>

                                                    <v-layout row wrap >

                                                        <template v-for="subcategory in props.item.subcategories">

                                                            <v-layout row style="min-height: 27px; max-height: 41px; width: 100%; margin-top: 2px">
                                                                <v-flex xs12 sm6 md1 style="display: flex; align-items: center;">
                                                                    <div :style="`border-radius: 50%; width: 15px; height: 15px; background: ${subcategory.utility.contextImage}`"></div>
                                                                </v-flex>
                                                                <v-flex xs12 sm6 md5 style="display: flex; align-items: center;">
                                                                    <div>

                                                                        <div class="text">{{ subcategory.subcategory_label }}</div>

                                                                    </div>
                                                                </v-flex>
                                                                <v-flex xs12 sm6 md4>
                                                                    <div class="prix_report">
                                                                        <span style="height: 100%; display: flex; align-items: center; font-size: 13px; color: #78909C">{{ subcategory.price }} Ar</span>
                                                                    </div>
                                                                </v-flex>
                                                                <v-flex xs12 sm6 md2>
                                                                    <span style="text-align: left; height: 100%; display: flex; align-items: center; font-size: 13px; color: #546E7A">x {{ subcategory.number }}</span>
                                                                </v-flex>
                                                            </v-layout>

                                                        </template>

                                                    </v-layout>

                                                </v-flex>

                                            </v-layout>

                                        </v-flex>

                                    </v-data-iterator>
                                </div>
                            </accordion>
                        </v-flex>
                        <v-flex xs12 md6>
                            <accordion theme="grey lighten-1" class="shadowed-part" style="margin-left: 0px; margin-right: 0px; margin-top: 0px; ">
                                <div slot="header">
                                    <span style="color: white">Dépenses hebdomadaires</span>
                                    <span style="position: absolute; right: 7px; top: 2px; color: #455A64; margin-right: 45px">{{ total_weekly }} Ar</span>
                                </div>
                                <div>
                                    <v-data-iterator
                                        no-data-text="Pas de données"
                                        :items="category_expenses_weekly"
                                        :rows-per-page-items="rowsPerPageItems"
                                        :pagination.sync="pagination"
                                        content-tag="v-layout"
                                        row
                                        hide-actions
                                        wrap
                                        style="margin-top: 6px">

                                        <v-flex
                                            slot="item"
                                            slot-scope="props"
                                            xs12>

                                            <v-layout row wrap class="card-report card-1-report" style="background:#ECEFF1; margin-bottom: 5px; margin-left: 1px; margin-right: 1px">

                                                <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                                                    <div>
                                                        <v-layout justify-center fill-height align-center>
                                                            <div style="display:block; margin:0 auto; text-align: center;">

                                                                <img :src="urlBaseImage+'/uploads/categories/'+props.item.category.categoryImage" alt="" style="margin-top: 5px;width:42%;height:42%;object-fit:contain">
                                                                <p class="text-xs-center" style="margin-top: -4px; font-size: 12px">{{ props.item.category.categoryLabel }}</p>

                                                            </div>
                                                        </v-layout>
                                                    </div>
                                                </v-flex>

                                                <v-flex d-flex xs12 sm6 md9>

                                                    <v-layout row wrap >

                                                        <template v-for="subcategory in props.item.subcategories">

                                                            <v-layout row style="min-height: 27px; max-height: 41px; width: 100%; margin-top: 2px">
                                                                <v-flex xs12 sm6 md1 style="display: flex; align-items: center;">
                                                                    <div :style="`border-radius: 50%; width: 15px; height: 15px; background: ${subcategory.utility.contextImage}`"></div>
                                                                </v-flex>
                                                                <v-flex xs12 sm6 md5 style="display: flex; align-items: center;">
                                                                    <div>

                                                                        <div class="text">{{ subcategory.subcategory_label }}</div>

                                                                    </div>
                                                                </v-flex>
                                                                <v-flex xs12 sm6 md4>
                                                                    <div class="prix_report">
                                                                        <span style="height: 100%; display: flex; align-items: center; font-size: 13px; color: #78909C">{{ subcategory.price }} Ar</span>
                                                                    </div>
                                                                </v-flex>
                                                                <v-flex xs12 sm6 md2>
                                                                    <span style="text-align: left; height: 100%; display: flex; align-items: center; font-size: 13px; color: #546E7A">x {{ subcategory.number }}</span>
                                                                </v-flex>
                                                            </v-layout>

                                                        </template>

                                                    </v-layout>

                                                </v-flex>

                                            </v-layout>

                                        </v-flex>

                                    </v-data-iterator>
                                </div>
                            </accordion>
                        </v-flex>
                    </v-layout>
                </v-container>

                <div style="position: absolute; bottom: 0; right: 0; left: 0; margin-left: 20px; margin-right: 20px">
                    <div style="height: 35px; background: #ECEFF1">
                        <v-flex xs12 md12 >
                            <div class="centered-text"><span style="color: #78909C">Total</span> : {{ total_spent_quotidienne + total_task_quotidienne + total_monthly + (total_weekly*4) }} Ar</div>
                        </v-flex>
                    </div>
                </div>
            </template>
            <data-no-user v-if="data_no_user"></data-no-user>
            <data-loader v-if="data_loader"></data-loader>
        </v-container>
    </div>

</script>

<template id="template-accordion">
    <div class="accordion" v-bind:class="theme">
        <div class="header" @click="toggle">
            <slot name="header">HINT</slot>
            <!--<i class="fa fa-2x fa-angle-down header-icon" v-bind:class="{ rotate: show }"></i>-->
            <v-icon class="header-icon" style="font-size: 33px;" v-bind:class="{ rotate: show }">keyboard_arrow_down</v-icon>
        </div>
        <transition name="accordion"
            v-on:before-enter="beforeEnter" v-on:enter="enter"
            v-on:before-leave="beforeLeave" v-on:leave="leave">
            <div class="body" v-show="show">
                <div class="body-inner">
                <slot></slot>
                </div>
            </div>
        </transition>
    </div>
</template>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_report_simple_spent_global = '<?php echo URL_REPORT_SIMPLE_SPENT_GLOBAL ?>';
    var url_report_simple_spent_global_final = `${url_base}${url_report_simple_spent_global}`;

    var url_report_simple_task_global = '<?php echo URL_REPORT_SIMPLE_TASK_GLOBAL ?>';
    var url_report_simple_task_global_final = `${url_base}${url_report_simple_task_global}`;

    var url_list_spent_repeat = '<?php echo URL_LIST_SPENT_REPEAT ?>';
    var url_list_spent_repeat_final = `${url_base}${url_list_spent_repeat}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    Vue.component('report-spent-global', {
        template: '#template-report-spent-global',
        mixins: [common_mixin],
        data: function () {

            return{
                user_logged_in: undefined,
                all_date_filtered_combined:[],
                all_date_bar: [],
                all_date_task: [],
                all_total_spent: [],
                all_total_task: [],
                all_total_morning: [],
                all_total_midday: [],
                all_total_evening: [],
                all_number_task: [],
                objects: [],
                modal: false,
                date: new Date().toISOString().substr(0, 7),
                menus: [
                    { id: 1, icon: 'timeline', title: 'Ligne' },
                    { id: 2, icon: 'equalizer', title: 'Bar' },
                ],
                selected_chart: 'all',
                category_expenses_monthly: [],
                category_expenses_weekly: [],
                rowsPerPageItems: [100],
                pagination: {
                    rowsPerPage: 100
                },
                urlBaseImage: url_base,
                total_monthly: 0,
                total_weekly: 0,
                total_spent_quotidienne: 0,
                total_task_quotidienne: 0,
            }

        },
        methods: {
           wsReportGlobalSpent: function(cur_year, cur_month){
                this.data_loader = true;
                var formData = new FormData();
                //console.log(`USER FOR WS ====>${this.user_logged_in.id}`);
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_month', cur_month);
                formData.append('spent_year', cur_year);
                axios.post(url_report_simple_spent_global_final, formData).then((response) =>{
                    console.log(`REPORT SENT here====>${JSON.stringify(response.data.report)}`);

                        if(response.data.report && response.data.report.length > 0){
                            var all_date = this.takeAllDateSpent(response.data.report);
                            var dateStrings = all_date.filter((v,i) => all_date.indexOf(v) == i);

                            /*linechart*/
                            this.wsReportGlobalTask(cur_year, cur_month, dateStrings, response.data.report);

                            /*barchart*/
                            var sortedStrings = dateStrings.sort(function(a,b) {
                                var aComps = a.split("/");
                                var bComps = b.split("/");
                                var aDate = new Date(aComps[2], aComps[1], aComps[0]);
                                var bDate = new Date(bComps[2], bComps[1], bComps[0]);
                                return aDate.getTime() - bDate.getTime();
                            });

                            this.all_date_bar = sortedStrings;
                            for(var i= 0; i < sortedStrings.length; i++){
                                this.all_total_morning.push(this.regroupPriceSpentByDate(sortedStrings[i], this.regroupSpentByTypeDay(response.data.report, 'morning'), 'spentDate'));
                                this.all_total_midday.push(this.regroupPriceSpentByDate(sortedStrings[i], this.regroupSpentByTypeDay(response.data.report, 'midday'), 'spentDate'));
                                this.all_total_evening.push(this.regroupPriceSpentByDate(sortedStrings[i], this.regroupSpentByTypeDay(response.data.report, 'evening'), 'spentDate'));
                            }
                            /*this.all_total_morning = this.regroupSpentByTypeDay(response.data.report, 'morning');
                            this.all_total_midday = this.regroupSpentByTypeDay(response.data.report, 'midday');
                            this.all_total_evening = this.regroupSpentByTypeDay(response.data.report, 'evening');*/

                        }else{
                            console.log("NO REPORT spent FOUND");
                            this.wsReportGlobalTask(cur_year, cur_month, [], []);
                        }

                })
                .finally(() => {

                    //this.closeLoaderAfterDelay();

                })
           },
           wsReportGlobalTask: function(cur_year, cur_month, dates_spent, response_spent){

                var formData = new FormData();
                //console.log(`DATES SPENT====>${JSON.stringify(dates_spent)}`);
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_month', cur_month);
                formData.append('spent_year', cur_year);
                axios.post(url_report_simple_task_global_final, formData).then((response) =>{
                    console.log(`REPORT task response====>${JSON.stringify(response.data.report)}`);

                        if(response.data.report && response.data.report.length > 0){
                            var all_date = this.takeAllDateTask(response.data.report);

                            /*for number of task*/
                            var dateStringsTask = all_date.filter((v,i) => all_date.indexOf(v) == i);
                            var sortedStringsTask = dateStringsTask.sort(function(a,b) {
                                var aComps = a.split("/");
                                var bComps = b.split("/");
                                var aDate = new Date(aComps[2], aComps[1], aComps[0]);
                                var bDate = new Date(bComps[2], bComps[1], bComps[0]);
                                return aDate.getTime() - bDate.getTime();
                            });
                            this.all_date_task = sortedStringsTask;
                            var ids_event = this.takeAllEventId(response.data.report);
                            var ids_event_filtered = ids_event.filter((v,i) => ids_event.indexOf(v) == i);
                            //console.log(`ids_event_filtered====>${JSON.stringify(ids_event_filtered)}`);
                            var dates_for_number = [];
                            for(var i= 0; i < ids_event_filtered.length; i++){
                                dates_for_number.push(this.getDateByEventId(response.data.report, ids_event_filtered[i]));
                            }
                            for(var i= 0; i < sortedStringsTask.length; i++){
                                this.all_number_task.push(this.countOccurenceOf(dates_for_number, sortedStringsTask[i]));
                            }
                            console.log(`sortedStringsTask ====>${JSON.stringify(sortedStringsTask)}`);
                            console.log(`all_number_task====>${JSON.stringify(this.all_number_task)}`);

                            /*combined dates*/
                            var combined_dates = all_date.concat(dates_spent);
                            var dateStrings = combined_dates.filter((v,i) => combined_dates.indexOf(v) == i);

                            var sortedStrings = dateStrings.sort(function(a,b) {
                                var aComps = a.split("/");
                                var bComps = b.split("/");
                                var aDate = new Date(aComps[2], aComps[1], aComps[0]);
                                var bDate = new Date(bComps[2], bComps[1], bComps[0]);
                                return aDate.getTime() - bDate.getTime();
                            });

                            this.all_date_filtered_combined = sortedStrings;
                            console.log(`DATES COMBINED====>${JSON.stringify(dateStrings)}`);

                            /*spent only*/
                            var temp_tab_spent = [];
                            for(var i= 0; i < dates_spent.length; i++){
                                temp_tab_spent.push({date: dates_spent[i], montant: this.regroupPriceSpentByDate(dates_spent[i], response_spent, 'spentDate'), index: sortedStrings.indexOf(dates_spent[i])});
                            }
                            this.objects.push(temp_tab_spent);

                            /*task only*/
                            var temp_tab_task = [];
                            var temp_date_strings = all_date.filter((v,i) => all_date.indexOf(v) == i);
                            for(var i= 0; i < temp_date_strings.length; i++){
                                temp_tab_task.push({date: temp_date_strings[i], montant: this.regroupPriceSpentByDate(temp_date_strings[i], response.data.report, 'eventDebut'), index: sortedStrings.indexOf(temp_date_strings[i])});
                            }
                            this.objects.push(temp_tab_task);

                            /*log result*/
                            console.log(`objects task====>${JSON.stringify(this.objects)}`);

                            /*montant regroup*/
                            var tabs = [];//tab in tab

                            for(i=0; i<2; i++){
                                var tab = [];
                                for(k=0; k<sortedStrings.length; k++){
                                    tab.push(0);
                                }
                                tab_obj = [];
                                for(j=0; j<sortedStrings.length; j++){

                                    if(this.objects[i][j] && sortedStrings.indexOf(this.objects[i][j].date) > -1){
                                        tab[this.objects[i][j].index] = this.objects[i][j].montant;
                                    }

                                }
                                tabs.push(tab);

                            }
                            console.log(`tabs====>${JSON.stringify(tabs)}`);

                            if(tabs[0]){
                                this.all_total_spent = tabs[0];
                            }
                            if(tabs[1]){
                                this.all_total_task = tabs[1];
                            }

                        }else{
                            console.log("NO REPORT task FOUND");
                            //console.log(`here==>${this.all_date_filtered_combined}`)
                            this.all_date_filtered_combined = dates_spent;
                            if(dates_spent && dates_spent.length > 0 && response_spent && response_spent.length > 0){
                                for(var i= 0; i < dates_spent.length; i++){
                                    this.all_total_spent.push(this.regroupPriceSpentByDate(dates_spent[i], response_spent, 'spentDate'));
                                }
                            }else{
                                console.log("NO DATES OR RESPONSE");
                            }
                        }

                        if(this.all_total_spent.length > 0){
                            this.total_spent_quotidienne = this.sumPrice(this.all_total_spent);
                        }

                        if(this.all_total_task.length > 0){
                            this.total_task_quotidienne = this.sumPrice(this.all_total_task);
                        }

                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
           },
           takeAllDateSpent: function(reports){
                res = [];
                reports.forEach((report, i) => {
                    res.push(this.formatDateToMMDDYYYY(report.spentDate));
                });
                return res;
           },
           wsDepenseMonthWeek: function(spent_date) {

                this.data_loader = true;
                console.log(`today ====> ${spent_date}`)
                var formData = new FormData();
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_month', this.getMonthOfDate(spent_date));
                formData.append('spent_year', this.getYearOfDate(spent_date));
                window.axios.post(url_list_spent_repeat_final, formData).then((response) => {
                    if(response.data.spents == null){
                        console.log("response.data.spents ==> null");
                    }else{
                        console.log(`SPENTS LIST REPEAT=====>${JSON.stringify(response.data.spents)}`)
                        //-----------------------MONTH LIST------------------------------
                        let by_typeday_month = this.regroupSpentByTypeDay(response.data.spents, 'monthly');
                        var ids_month = this.takeAllCatId(by_typeday_month);
                        var filtered_month = ids_month.filter((v,i) => ids_month.indexOf(v) == i);
                        if(filtered_month && filtered_month.length > 0){
                            for(var i= 0; i < filtered_month.length; i++){
                                this.category_expenses_monthly.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_month, filtered_month[i])));

                            }
                        }
                        if(by_typeday_month && by_typeday_month.length > 0){
                            this.total_monthly = this.regroupPriceBySpent(by_typeday_month);
                        }
                        //---------------------WEEK LIST----------------------------------
                        let by_typeday_week = this.regroupSpentByTypeDay(response.data.spents, 'weekly');
                        var ids_week = this.takeAllCatId(by_typeday_week);
                        var filtered_week = ids_week.filter((v,i) => ids_week.indexOf(v) == i);
                        if(filtered_week && filtered_week.length > 0){
                            for(var i= 0; i < filtered_week.length; i++){
                                this.category_expenses_weekly.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_week, filtered_week[i])));

                            }
                        }
                        if(by_typeday_week && by_typeday_week.length > 0){
                            this.total_weekly = this.regroupPriceBySpent(by_typeday_week);
                        }

                    }
                })
                .catch(err => {
                    console.log(err)
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
           },
           regroupPriceBySpent: function(spents){
                var total = 0;
                for(var i= 0; i < spents.length; i++){

                    var montant = 0;
                    let pr = Number(spents[i].number);
                    let pu = Number(spents[i].unitPrice);
                    montant = pu * pr;
                    total += montant;

                }
                return total;
           },
           sumPrice: function(arr){
                var res = 0;
                for(var i= 0; i < arr.length; i++){
                    res += Number(arr[i]);
                }
                return res;
           },
           getDateByEventId: function(arr, evId){
                var res;
                for(var i= 0; i < arr.length; i++){
                    if(parseInt(arr[i].eventId) == evId){
                        res = this.formatDateToMMDDYYYY(arr[i].eventDebut);
                    }
                }
                return res;
           },
           afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log("REPORT GLOBAL PAGE USER LOGGED IN");
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }
                    //var server_current_date = '<?php echo date('Y-m-d'); ?>';//date du server
                    let today = new Date().toISOString().slice(0, 10);
                    console.log(`YEAR====>${this.getYearOfDate(today)}`);
                    console.log(`MONTH====>${this.getMonthOfDate(today)}`);
                    this.wsReportGlobalSpent(this.getYearOfDate(today), this.getMonthOfDate(today));
                    this.wsDepenseMonthWeek(today);
                }else{
                    console.log("REPORT GLOBAL PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
           },
           regroupPriceSpentByDate: function(date, spents, param){
                var total = 0;
                for(var i= 0; i < spents.length; i++){
                    if(this.formatDateToMMDDYYYY(spents[i][param]) === date){
                        var montant = 0;
                        let pr = Number(spents[i].number);
                        let pu = Number(spents[i].unitPrice);
                        montant = pu * pr;
                        total += montant;
                    }
                }
                return total;
           },
           requestByMonth: function(calendarDate){
                this.$refs.dialog.save(calendarDate);
                console.log(`ref save ${calendarDate}`);

                /*common*/
                this.objects = [];

                /*line clear*/
                this.all_date_filtered_combined = [];
                this.all_total_spent = [];
                this.all_total_task = [];

                /*stacked bar clear*/
                this.all_date_bar = [];
                this.all_total_morning = [];
                this.all_total_midday = [];
                this.all_total_evening = [];

                /*bar clear*/
                this.all_date_task = [];
                this.all_number_task = [];

                /*monthly & weekly*/
                this.category_expenses_monthly = [];
                this.category_expenses_weekly = [];
                this.total_monthly = 0;
                this.total_weekly = 0;
                this.total_spent_quotidienne = 0;
                this.total_task_quotidienne = 0;

                this.selected_chart = 'all';
                this.wsReportGlobalSpent(this.getYearOfDate(calendarDate), this.getMonthOfDate(calendarDate));
                this.wsDepenseMonthWeek(calendarDate);
                this.$router.app.$emit('updateWelcomeSubtitle', "courbes");

           },
           manageReportContextualMenu: function (item){
               if(item.id == 1){
                 this.selected_chart = 'line';
                 this.$router.app.$emit('updateWelcomeSubtitle', "courbes");
               }else if(item.id == 2){
                 this.selected_chart = 'bar';
                 this.$router.app.$emit('updateWelcomeSubtitle', "bâtons");
               }
           }

        },
        created: function () {
            this.$router.app.$emit('updateWelcomeSubtitle', "courbes");
        },
        mounted: function () {
            console.log('report spent global : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        }

    });

    Vue.component('line-chart', {
        extends: VueChartJs.Line,
        props: {
            dates: { type: Array, required: true },
            totalspents: { type: Array, required: true },
            totaltasks: { type: Array, required: true }
        },
        mounted () {
            this.renderChart({
                labels: this.dates,
                datasets: [
                    {
                        label: 'Dépenses',
                        backgroundColor: '#058DC740',
                        borderColor: '#058DC7',
                        borderWidth: 1,
                        pointBackgroundColor: '#058DC7',
                        pointRadius: 2,
                        lineTension: 0,
                        data: this.totalspents
                    },
                    {
                        label: 'Tâches',
                        backgroundColor: '#AADFF340',
                        borderColor: '#AADFF3',
                        borderWidth: 1,
                        pointBackgroundColor: '#AADFF3',
                        pointRadius: 2,
                        lineTension: 0,
                        data: this.totaltasks
                    },
                ]
            },
            {
                responsive: true,
                maintainAspectRatio: false,
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: ((tooltipItems, data) => {
                            const label = data.datasets[tooltipItems.datasetIndex].label;
                            const value = tooltipItems.yLabel + ' Ar';
                            return `${label}: ${value}`;
                        })
                    }
                }
            })


        },
        created (){

        }
    });

    Vue.component('stacked-bar-chart', {
        extends: VueChartJs.Bar,
        props: {
            dates: { type: Array, required: true },
            totalmorning: { type: Array, required: true },
            totalmidday: { type: Array, required: true },
            totalevening: { type: Array, required: true }
        },
        beforeMount () {
            this.addPlugin(horizonalLinePlugin)
        },
        mounted () {
            this.renderChart({
                labels: this.dates,
                datasets: [
                    {
                        label: 'Matin',
                        backgroundColor: '#D500F9',
                        data: this.totalmorning
                    },
                    {
                        label: 'Midi',
                        backgroundColor: '#C6FF00',
                        data: this.totalmidday
                    },
                    {
                        label: 'Soir',
                        backgroundColor: '#FF9100',
                        data: this.totalevening
                    }
                ]
            },
            {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        stacked: true,
                        categoryPercentage: 0.5,
                        barPercentage: 1
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: ((tooltipItems, data) => {
                            const label = data.datasets[tooltipItems.datasetIndex].label;
                            const value = tooltipItems.yLabel + ' Ar';
                            return `${label}: ${value}`;
                        })
                    }
                }
            })
        }

    });

    const horizonalLinePlugin = {
        id: 'horizontalLine',
        afterDraw: function(chartInstance) {

            var yValue;
            var yScale = chartInstance.scales["y-axis-0"];
            var canvas = chartInstance.chart;
            var ctx = canvas.ctx;
            var index;
            var line;
            var style;

            if (chartInstance.options.horizontalLine) {

                for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
                    line = chartInstance.options.horizontalLine[index];

                    if (!line.style) {
                        style = "#080808";
                    } else {
                        style = line.style;
                    }

                    if (line.y) {
                        yValue = yScale.getPixelForValue(line.y);
                    } else {
                        yValue = 0;
                    }
                    ctx.lineWidth = 3;

                    if (yValue) {
                        window.chart = chartInstance;
                        ctx.beginPath();
                        ctx.moveTo(0, yValue);
                        ctx.lineTo(canvas.width, yValue);
                        ctx.strokeStyle = style;
                        ctx.stroke();
                    }

                    if (line.text) {
                        ctx.fillStyle = style;
                        ctx.fillText(line.text, 0, yValue + ctx.lineWidth);
                    }
                }
                return;
            }
        }
    };

    Vue.component('accordion', {
        props: ['theme'],
        template: '#template-accordion',
        data() {
            return {
                show: true,
            };
        },

        methods: {
            toggle: function() {
                this.show = !this.show;
                console.log("------>SHOW");
            console.log(this.show);
            },
            beforeEnter: function(el) {
                el.style.height = '0';
            },
            enter: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            beforeLeave: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            leave: function(el) {
                el.style.height = '0';
            }
        },
        mounted: function(){
            this.show = true;
        },
        created: function(){
            console.log("------>SHOW");
            console.log(this.show);
        }

    });

    Vue.component('bar-chart', {
        extends: VueChartJs.Bar,
        props: {
            dates: { type: Array, required: true },
            total: { type: Array, required: true },
        },
        mounted () {
            this.renderChart({
                labels: this.dates,
                datasets: [
                    {
                        label: 'Nombre de tâche(s)',
                        backgroundColor: '#AADFF3',
                        data: this.total
                    },
                ]
            },
            {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            stepSize: 1
                        }
                    }]
                }
            })
        }

    });

</script>