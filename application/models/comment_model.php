<?php

class Comment_model extends CI_Model
{

    public function createComment($data)
    {
        $this->db->insert('comments', $data);
        if ($this->db->affected_rows() > 0) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        return 0;
    }

    public function showByEvent($eventId)
    {
        $this->db->select('comments.id, comments.comment, comments.datecomment, comments.eventId, comments.userId, users.user_image');
        $this->db->from('comments');
        $this->db->where('comments.eventId', $eventId);
        $this->db->join('users', 'users.id = comments.userId');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
