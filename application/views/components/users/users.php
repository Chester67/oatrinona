<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

.top-right-btn{
    width:27px;
    height: 27px
}

.text-ellipsis{
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    display: inherit;
}

.text-colored{
    color: red
}

.centered {
  position: fixed;
  top: 50%;
  left: 50%;
}

.user-card-active {

    box-shadow: 2px 3px 5px #388E3C, 2px 3px 5px #388E3C;
}

.field {
    height: 90px;
}

.overlay-inside-dialog {
    background: #838383 !important;
}

.center-inside-dialog {
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

</style>

<script type="text/x-template" id="template-users">

    <div>
        <v-layout justify-center row wrap v-if="!data_no_user">
            <v-flex xs12 style="display: none">
                <v-text-field
                    flat
                    solo-inverted
                    hide-details
                    prepend-inner-icon="search"
                    label="Recherche par nom ou prénoms"
                    v-model="search.text"
                    @keyup="searchUser">
                </v-text-field>
            </v-flex>

            <v-flex xs12 sm6 style="margin-top: 15px;">
                <v-card :class="getAdminClass()" :ripple="true" style="margin-left: 10px; margin-right: 10px; height: 165px; cursor: pointer;" @click.native="chooseUser(getAdministrator(items), false)">

                    <div v-if="administrator_selected" style="position: absolute; left: 15px; top: 15px; border-radius: 50%; width: 20px; height: 20px; background: green"></div>
                    <div style="display: flex; flex-direction: row; justify-content: flex-end; height: 22px; margin-right: 7px; border-top: 6px solid transparent;">

                        <!--v-btn outline fab color="grey" class="top-right-btn" @click.stop.prevent="onDeleteSelected(getAdministrator(items))">
                            <v-icon style="font-size: 22px;">clear</v-icon>
                        </v-btn-->

                    </div>
                    <v-layout justify-center>
                        <v-avatar style="margin-top: 5px"
                            :tile="tile"
                            :size="avatarSize"
                            color="transparent">

                            <!--<img src="https://via.placeholder.com/150x150" alt="koko">-->
                            <template v-if="getAdministrator(items).user_image && !!getAdministrator(items).user_image">
                                <img :src="urlBaseImage+'uploads/utilisateurs/'+getAdministrator(items).user_image" alt="" >
                            </template>
                            <template v-else>
                                <v-icon style="font-size: 80px; background: #fafafa">face</v-icon>
                            </template>

                        </v-avatar>
                        <v-card-title>
                            <v-layout align-center justify-center>
                                <div>
                                    <div style="text-align: center; font-weight: bold; font-size: 16px; color: #757575;">Administrateur</div>
                                    <h4 class="text-xs-center">{{ getAdministrator(items).first_name }}</h4>
                                    <div class="text-ellipsis">{{ getAdministrator(items).phone }} - {{ getAdministrator(items).email }}</div>
                                    <div class="text-xs-center">{{ getAdministrator(items).register_date }}</div>
                                </div>
                            </v-layout>
                        </v-card-title>
                    </v-layout>



                </v-card>
            </v-flex>
            <v-flex xs12>
                <div style="padding-top: 12px; text-align: center; font-weight: bold; font-size: 18px; color: #757575;">
                Utilisateur(s)
                </div>
            </v-flex>
            <v-flex xs12>
                <v-container fluid grid-list-md>
                    <v-data-iterator
                        no-data-text=""
                        :items="items"
                        :rows-per-page-items="rowsPerPageItems"
                        :pagination.sync="pagination"
                        content-tag="v-layout"
                        :hide-actions="true"
                        row
                        wrap>
                        <v-flex
                            slot="item"
                            slot-scope="props"
                            xs12
                            sm6
                            md4
                            lg3>

                            <v-card v-if="props.item.role !== 'ADMINISTRATOR'" :class="getUserClass(props.item)" :ripple="true" style="height: 230px; cursor: pointer;" @click.native="chooseUser(props.item, true)">

                                <div v-if="props.item.selected" style="position: absolute; left: 15px; top: 15px; border-radius: 50%; width: 20px; height: 20px; background: green"></div>
                                <div style="display: flex; flex-direction: row; justify-content: flex-end; height: 22px; margin-right: 7px; border-top: 6px solid transparent;">

                                    <v-btn outline fab color="grey" class="top-right-btn" @click.stop.prevent="onDeleteSelected(props.item)">
                                        <v-icon style="font-size: 22px;">clear</v-icon>
                                    </v-btn>

                                </div>
                                <v-layout justify-center>
                                    <v-avatar style="margin-top: 5px"
                                        :tile="tile"
                                        :size="avatarSize"
                                        color="transparent">

                                        <!--<img src="https://via.placeholder.com/150x150" alt="koko">-->
                                        <template v-if="props.item.user_image && !!props.item.user_image">
                                            <img :src="urlBaseImage+'uploads/utilisateurs/'+props.item.user_image" alt="" >
                                        </template>
                                        <template v-else>
                                            <v-icon style="font-size: 80px; background: #fafafa">face</v-icon>
                                        </template>

                                    </v-avatar>
                                </v-layout>

                                <v-card-title>
                                    <v-layout align-center justify-center>
                                        <div>
                                            <h4 class="text-xs-center">{{ props.item.first_name }}</h4>
                                            <div class="text-ellipsis">{{ props.item.phone }} - {{ props.item.email }}</div>
                                            <div class="text-xs-center">{{ props.item.register_date }}</div>
                                        </div>
                                    </v-layout>
                                </v-card-title>

                            </v-card>
                        </v-flex>
                    </v-data-iterator>
                </v-container>
            </v-flex>
        </v-layout>

        <v-dialog v-model="dialog" width="800px">

            <v-card :class="getDialogAddClass()">
                <v-card-title style="width: 100%; height: 55px; background: #BDBDBD; font-size: 19px; font-weight: bold; color: #455A64">
                    Créer un utilisateur
                </v-card-title>

                <v-container grid-list-sm class="pa-4">
                    <v-layout row wrap>
                        <v-flex xs4>
                            <v-layout align-center>
                                <v-text-field prepend-icon="face" placeholder="Nom" v-model="newUser.last_name" :disabled="dialog_loading"></v-text-field>
                            </v-layout>
                        </v-flex>
                        <v-flex xs5>
                            <v-text-field placeholder="Prénoms" v-model="newUser.first_name" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field placeholder="Nom utilisateur" v-model="newUser.username" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                        <v-flex xs6>
                            <v-text-field prepend-icon="vpn_key" type="password" placeholder="Mot de passe" v-model="newUser.password" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                        <v-flex xs6>
                            <v-text-field type="password" placeholder="Confirmation mot de passe" v-model="confirm_password" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                        <v-flex xs6>
                            <v-text-field prepend-icon="business" placeholder="Société" v-model="newUser.company" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                        <v-flex xs6>
                            <v-text-field placeholder="Métier" v-model="newUser.job" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                        <v-flex xs12>
                            <v-text-field prepend-icon="mail" placeholder="Email" v-model="newUser.email" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                        <v-flex xs12>
                            <v-text-field
                                type="tel"
                                prepend-icon="phone"
                                placeholder="Téléphone"
                                v-model="newUser.phone" :disabled="dialog_loading"></v-text-field>
                        </v-flex>
                    </v-layout>
                </v-container>

                <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn flat color="blue-grey lighten-1" @click="cancelAddUser" :disabled="dialog_loading">Annuler</v-btn>
                    <v-btn flat @click="addUser" :disabled="dialog_loading">Enregistrer</v-btn>
                </v-card-actions>

                <div v-if="!!errormessage" style="display: flex; flex-direction: row; margin-left: 23px; height: 60px">
                    <v-icon style="display: flex; align-items: center; vertical-align: middle">error_outline</v-icon>
                    <span style="display: flex; align-items: center;">{{ errormessage }}</span>
                </div>

                <v-progress-circular
                    v-if="dialog_loading"
                    class="center-inside-dialog"
                    indeterminate
                    color="primary"></v-progress-circular>

            </v-card>

        </v-dialog>

        <v-dialog v-model="dialog_remove" persistent max-width="290">
            <v-card>
                <v-card-title>Voulez-vous vraiment supprimer cet utilisateur ?</v-card-title>
                <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="green darken-1" flat @click="dialog_remove = false">ANNULER</v-btn>
                <v-btn color="green darken-1" flat @click="deleteUser">SUPPRIMER</v-btn>
                </v-card-actions>
            </v-card>
        </v-dialog>

        <v-btn
            fab
            bottom
            right
            color="blue-grey lighten-1"
            dark
            fixed
            @click.stop="dialog = !dialog"
            v-if="!data_no_user">
            <v-icon>add</v-icon>
        </v-btn>

        <v-alert
            class="centered"
            :value="alert"
            type="success"
            transition="scale-transition">
            {{ flashMessage }}
        </v-alert>

        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>

    </div>

</script>

<script type="text/javascript">
    var url_base = '<?php echo base_url() ?>';

    var url_create_user = '<?php echo URL_CREATE_USER ?>';
    var url_create_user_final = `${url_base}${url_create_user}`;

    var url_list_user = '<?php echo URL_LIST_USER ?>';
    var url_list_user_final = `${url_base}${url_list_user}`;

    var url_search_user = '<?php echo URL_SEARCH_USER ?>';
    var url_search_user_final = `${url_base}${url_search_user}`;

    var url_save_user = '<?php echo URL_SAVE_USER ?>';
    var url_save_user_final = `${url_base}${url_save_user}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_delete_user = '<?php echo URL_DELETE_USER ?>';
    var url_delete_user_final = `${url_base}${url_delete_user}`;

    Vue.component('users', {
        template: '#template-users',
        mixins: [common_mixin],
        data: function(){
            return {
                flashMessage:'',
                alert: false,
                dialog: false,
                rowsPerPageItems: [4, 8, 12],
                pagination: {
                    rowsPerPage: 4
                },
                items: [

                ],

                tile: false,
                avatarsize: 115,

                newUser:{
                    first_name:'',
                    last_name:'',
                    email:'',
                    username:'',
                    password:'',
                    register_date:'',
                    company:'',
                    job:'',
                    phone:'',
                    role:'USER',
                    admin_id: undefined
                },
                confirm_password: '',

                formValidate:[],
                search: {text: ''},
                user_logged_in: undefined,
                user_choosed_by_admin: undefined,
                errormessage: '',
                selected_for_delete: undefined,
                dialog_remove: false,
                urlBaseImage: url_base,
                dialog_loading: false,
                administrator_selected: true,
            }
        },
        computed: {
            avatarSize () {
                return `${this.avatarsize}px`
            }
        },
        methods: {
            getAdminClass(){
                var classname = "";
                if(this.administrator_selected){
                    classname = "user-card-active";
                }
                console.log('CURRENT SELECTED ADMIN======> ' + this.administrator_selected);
                return classname;
            },
            getAdministrator(array){
                for(var i = array.length - 1; i >= 0; i--) {
                    if(array[i].role === 'ADMINISTRATOR'){
                        return array[i];
                    }
                }
                return null;
            },
            closeDialogLoadingDelay(){
                setTimeout(() =>{
                    this.dialog_loading = false;
			    },  1500);
            },
            getDialogAddClass(){
                if(this.dialog_loading)
                    return "overlay-inside-dialog";
                return "";
            },
            done: function(){
                this.dialog = false;
            },
            showAll(){

                var formData = new FormData();
                formData.append('id_admin', this.user_logged_in.id);
                axios.post(url_list_user_final, formData).then((response) => {
                    if(response.data.users == null){
                        //v.noResult()
                        console.log("USERS NULLLLLLL");
                        this.items = this.deselectOther(this.addAdminToUser([]), this.user_logged_in.id);

                    }else{

                        this.items = this.deselectOther(this.addAdminToUser(response.data.users), this.user_choosed_by_admin.id);
                        var arr_rowsPerPageItems = [];
                        arr_rowsPerPageItems.push(this.items.length);
                        this.rowsPerPageItems = arr_rowsPerPageItems;
                        this.pagination.rowsPerPage = this.items.length;
                        //console.log(`USERS ARE ====>${JSON.stringify(response.data.users)}`)
                    }

                })
                .finally(() => this.data_loader = false);
            },
            addUser(){
                if(this.allFieldChecked()){

                    if(this.newUser.password === this.confirm_password){
                        this.errormessage = '';
                        var formData = this.formData(this.newUser);
                        this.dialog_loading = true;
                        axios.post(url_create_user_final, formData).then((response) =>{
                            console.log('user json =>' + JSON.stringify(response));
                            if(response.data.error){
                                if(response.data.code == 0)
                                    this.errormessage = "Erreur lors du traitement !"
                                else if(response.data.code == -1)
                                    this.errormessage = "Utilisateur déjà existant !"
                                this.closeDialogLoadingDelay();
                            }else{
                                this.clearAll();
                                this.dialog = false;
                                this.closeDialogLoadingDelay();
                                this.manageFlashMSG(response.data.msg);
                            }

                        });
                    }else{
                        this.errormessage = "Les deux mots de passe ne sont pas identiques !"
                    }

                }else{
                    this.errormessage = "Veuillez completer les champs !"
                }
            },
            searchUser(){
                /*var formData = this.formData(this.search);
                axios.post(url_search_user_final, formData).then((response) =>{
                    if(response.data.users == null){
                        //v.noResult()
                        console.log(`USER SEARCH ARE ====>${response.data.users}`)
                    }else{
                        //v.getData(response.data.users);
                        console.log(`USER SEARCH ARE ====>${JSON.stringify(response.data.users)}`)
                        this.items = this.deselectOther(this.addAdminToUser(response.data.users), this.user_logged_in.id);
                    }
                })*/

            },
            saveUserSession(selectedUser){

                console.log(`before saved =====>${JSON.stringify(selectedUser)}`)
                this.data_loader = true;
                var formData = this.formData(selectedUser);
                axios.post(url_save_user_final, formData).then((response) =>{
                    if(!response.data.saved){
                        console.log('USER not SAVED')
                    }else{
                        console.log(`USER SAVED IS ====>${JSON.stringify(response.data.content)}`)
                    }
                }).finally(() => {
                    this.closeLoaderAfterDelay();
                });

            },
            formData(obj){
                var formData = new FormData();
                for ( var key in obj ) {
                    formData.append(key, obj[key]);
                }
                return formData;
            },
            clearAll(){

                this.newUser = {
                    first_name:'',
                    last_name:'',
                    email:'',
                    username:'',
                    password:'',
                    register_date:'',
                    company:'',
                    job:'',
                    phone:'',
                    role:'USER',
                    admin_id: this.user_logged_in.id
                };
                this.confirm_password = '';
                this.errormessage = '';

            },
            manageFlashMSG($message){
                this.flashMessage = $message;
                this.alert = true;
                setTimeout(() =>{
                    this.alert = false;
			        this.showAll();
			    },  3000); // disappearing message success in 3 sec
            },
            chooseUser(selectedUser, isUser){

                this.deselectOther(this.items, selectedUser.id);
                this.$router.app.$emit('chooseUser', selectedUser);
                this.saveUserSession(selectedUser);
                //console.log('ITEMS => ' + JSON.stringify(this.items));
                if(!isUser){
                    this.administrator_selected = true;
                } else {
                    this.administrator_selected = false;
                }

            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    //console.log(`USERS PAGE USER LOGGED IN ===> ${JSON.stringify(connexion.user_logged)}`);
                    this.user_logged_in = connexion.user_logged[0];
                    this.user_choosed_by_admin = connexion.user_choosed[0];
                    this.newUser.admin_id = connexion.user_logged[0].id;
                    this.showAll();
                    if(connexion.user_choosed[0].role === 'ADMINISTRATOR'){
                        this.administrator_selected = true;
                    }else{
                        this.administrator_selected = false;
                    }
                }else{
                    console.log("USERS PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            },
            onDeleteSelected(item){
                console.log("ondelete");
                this.selected_for_delete = item;
                this.dialog_remove = true;
            },
            addAdminToUser(array){
                array.push(this.user_logged_in);
                return array;
            },
            deselectOther(array, userid){
                for(var i = array.length - 1; i >= 0; i--) {
                    if(array[i].id == userid){
                        console.log('selected => ' + array[i].role);
                        array[i].selected = true;
                    }else{
                        console.log('deselected => ' + array[i].role);
                        array[i].selected = false;
                    }
                }
                return array;
            },
            getUserClass(item){
                var classname = "";
                if(item.selected == true){
                    classname = "user-card-active";
                }
                return classname;
            },
            allFieldChecked(){
                let check = this.newUser;
                if(check.first_name && check.last_name && check.username && check.email && check.password && this.confirm_password && check.company && check.job && check.phone){
                    return true;
                }
                return false;
            },
            cancelAddUser(){
                this.dialog = false;
                this.clearAll();
            },
            deleteUser(){
                console.log("=====>DELETE")
                this.dialog_remove = false;
                this.data_loader = true;
                var formData = new FormData();
                console.log(`USER_TO_DELETE : ${JSON.stringify(this.selected_for_delete)}`)
                formData.append('id_user', this.selected_for_delete.id);
                axios.post(url_delete_user_final, formData).then((response) =>{
                    console.log(`SUCCESS DELETE====>${response.data.msg}`);
                    if (!response.data.error) {

                        this.manageFlashMSG("Utilisateur supprimé avec succès !");

                    }else {
                        alert(response.data.msg);
                    }
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            }


        },
        created: function () {

        },
        mounted: function () {
            console.log('users : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },
        props: {
            url: String,
        },
    });

</script>
