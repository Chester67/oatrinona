<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('category_model','category');
        $this->load->model('subcategory_model','subcategory');
        $this->load->model('task_model','task');
        $this->load->model('subtask_model','subtask');
    }

	public function index()
	{
		$this->load->view('welcome');
    }
    
    public function uploadImageCategorie(){

        $label_category = $this->input->post('label_category');

        if($this->category->isExist($label_category)){
            //category already exist so dont add this
            $error = array('error' => 'Le catégorie existe déjà !');
            echo json_encode($error);
        }else{
            //add category 
            $config['upload_path']          = './uploads/categories/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 500;
            $config['max_width']            = 2500;
            $config['max_height']           = 1400;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('image_category')) {
                //$this->upload->display_errors()
                $error = array('error' => 'Erreur lors du upload');
                echo json_encode($error);
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $data = array(
                    'categoryLabel'=> $label_category,
                    'categoryImage'=> $file_name,
                );
                $is_inserted = $this->category->addCategory($data);    
                $success = array('success_upload' => $file_name, 'success_insert'=> $is_inserted);
                echo json_encode($success);
            }
        }
        
    }
    
    public function uploadImageSouscategorie(){
        
        $label_subcategory = $this->input->post('label_subcategory');
        $id_category = $this->input->post('id_category');

        if($this->subcategory->isExist($label_subcategory)){
            //subcategory already exist so dont add this
            $error = array('error' => 'Le sous-catégorie existe déjà !');
            echo json_encode($error);
        }else{
            //add category 
            $config['upload_path']          = './uploads/sous-categories/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 500;
            $config['max_width']            = 2500;
            $config['max_height']           = 1400;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('image_subcategory')) {
                //$this->upload->display_errors()
                $error = array('error' => 'Erreur lors du upload');
                echo json_encode($error);
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $data = array(
                    'subcategoryLabel'=> $label_subcategory,
                    'subcategoryImage'=> $file_name,
                    'categoryId'=> $id_category,
                );
                $is_inserted = $this->subcategory->addSubcategory($data);    
                $success = array('success_upload' => $file_name, 'success_insert'=> $is_inserted);
                echo json_encode($success);
            }
        }

    }

    public function uploadImageTask(){

        $label_task = $this->input->post('label_task');

        if($this->task->isExist($label_task)){
            //task already exist so dont add this
            $error = array('error' => 'La tâche existe déjà !');
            echo json_encode($error);
        }else{
            //add category 
            $config['upload_path']          = './uploads/taches/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 500;
            $config['max_width']            = 2500;
            $config['max_height']           = 1400;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('image_task')) {
                //$this->upload->display_errors()
                $error = array('error' => 'Erreur lors du upload');
                echo json_encode($error);
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $data = array(
                    'taskLabel'=> $label_task,
                    'taskImage'=> $file_name,
                );
                $is_inserted = $this->task->addTask($data);    
                $success = array('success_upload' => $file_name, 'success_insert'=> $is_inserted);
                echo json_encode($success);
            }
        }
        
    }

    public function uploadImageSubtask(){
        
        $label_subtask = $this->input->post('label_subtask');
        $id_task = $this->input->post('id_task');

        if($this->subtask->isExist($label_subtask)){
            //subtask already exist so dont add this
            $error = array('error' => 'Le sous-tâche existe déjà !');
            echo json_encode($error);
        }else{
            //add category 
            $config['upload_path']          = './uploads/sous-taches/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 500;
            $config['max_width']            = 2500;
            $config['max_height']           = 1400;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('image_subtask')) {
                //$this->upload->display_errors()
                $error = array('error' => 'Erreur lors du upload');
                echo json_encode($error);
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $data = array(
                    'subtaskLabel'=> $label_subtask,
                    'subtaskImage'=> $file_name,
                    'taskId'=> $id_task,
                );
                $is_inserted = $this->subtask->addSubtask($data);    
                $success = array('success_upload' => $file_name, 'success_insert'=> $is_inserted);
                echo json_encode($success);
            }
        }

    }
	
}
