<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
$this->load->view('components/empty/data_empty');
?>

<style type="text/css">
    .v-card--reveal {
        align-items: center;
        bottom: 0;
        justify-content: center;
        opacity: .4;
        position: absolute;
        width: 100%;
    }
    .bounce-enter-active {
        animation: bounce-in .5s;
    }
    @keyframes bounce-in {
        0% {
            transform: scale(0);
        }
        50% {
            transform: scale(1.1);
        }
        100% {
            transform: scale(1);
        }
    }
</style>

<script type="text/x-template" id="template-report-category">

    <div style="background: white; height: 100%">
        <v-container fluid>
            <v-toolbar v-if="!data_no_user" class="sticky-spent-global" style="margin-top: -6px">

                        <v-dialog
                            ref="dialog"
                            v-model="modal"
                            :return-value.sync="date"
                            persistent
                            lazy
                            full-width
                            width="520px"
                            justify-center
                            style="width: 100% !important">

                            <div slot="activator">
                                <div style="vertical-align: middle; float: right;">
                                    <div class="hidden-sm-and-down" style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 3px">
                                        <v-icon>event</v-icon>
                                        <h1 style="margin-left: 5px">{{ date | format_month }}</h1>
                                    </div>
                                </div>
                            </div>

                            <v-date-picker v-model="date" full-width type="month" class="mt-3" locale="fr-fr">
                                <v-spacer></v-spacer>
                                <v-btn flat color="primary" @click="modal = false">Annuler</v-btn>
                                <v-btn flat color="primary" @click="requestByMonth(date)">OK</v-btn>
                            </v-date-picker>

                        </v-dialog>

                        <div style="position: absolute; left: 25px; cursor: pointer" @click="modal = true">
                            <h1>Mois : {{ getMonthByInt(getMonthOfDate(date)) }} {{ getYearOfDate(date) }}</h1>
                        </div>

            </v-toolbar>

            <template v-if="!data_no_user">
              <v-container grid-list-md>
                <v-layout wrap style="margin-left: 0px; margin-right: 0px;">
                    <v-flex xs12 md6>
                        <v-card>

                            <div style="width: 100%; height: 40px; background: #BDBDBD; font-size: 19px; font-weight: bold; color: #455A64">
                                <div style="float: left; margin-left: 23px; height: 100%; vertical-align: middle; line-height: 40px;">DEPENSES</div>
                                <div style="float: right; margin-right: 23px; height: 100%; vertical-align: middle; line-height: 40px;">{{ spent_title_price }} Ar</div>
                            </div>
                            <template v-if="categories_spent_name.length > 0">
                                <pie-chart
                                    :height="260"
                                    :all_labels="categories_spent_name"
                                    :all_montants="categories_spent_montant"
                                    :all_categ_ids="categories_spent_id"
                                    :all_responses="categories_spent_response"
                                    :all_from="'spent'"
                                    style="margin-top: 25px; margin-left: 25px"
                                    @subgroups="from_child_subgroups">
                                </pie-chart>
                            </template>
                            <template v-else>
                                <div style="display: flex; align-items: center; justify-content: center; height: 40px;">
                                    <span style="font-weight: bold">Oops! Aucune donnée disponible !</span>
                                </div>
                            </template>

                            <div style="margin-top: 15px; height: 35px; background: #BDBDBD; width: 100%; font-size: 17px; font-weight: bold; color: #455A64">
                                <div style="float: left; margin-left: 23px; height: 100%; vertical-align: middle; line-height: 35px;">{{ selected_label_spent }}</div>
                                <div style="float: right; margin-right: 23px; height: 100%; vertical-align: middle; line-height: 35px;">{{ selected_price_spent }}</div>
                            </div>
                            <v-container fluid grid-list-md style="min-height: 35px; margin-top: 0">
                                <v-data-iterator
                                    :items="items_spent"
                                    :rows-per-page-items="rowsPerPageItems"
                                    :pagination.sync="pagination"
                                    no-data-text="Sélectionnez une catégorie"
                                    content-tag="v-layout"
                                    row
                                    hide-actions
                                    wrap
                                    style="margin-top: -15px">

                                    <v-flex
                                        slot="item"
                                        slot-scope="props"
                                        xs12
                                        md3>

                                        <v-card :ripple="true" style="background:#fafafa;">
                                            <transition name="bounce">
                                                <div v-if="props.item.activeSubcategory" class="d-flex black darken-2 v-card--reveal white--text" style="font-size: 9px important!; height: 100%; -webkit-transition: width 2s; transition: width 2s;">
                                                    {{ props.item.priceSubcategory }} Ar
                                                </div>
                                            </transition>
                                            <div @mouseover="mouseOver(props.item.idSubcategory, items_spent)" style="height: 105px; display: flex; justify-content: center; align-items: center; background: #ECEFF1; margin-top: 9px">
                                                <img :src="urlBaseImage+'uploads/sous-categories/'+props.item.imageSubcategory" alt="" style="width:95%; height:95%; object-fit:crop; margin-top: 3px; padding-bottom: 3px;" onerror="this.style.display='none'">
                                            </div>
                                            <div style="height: 43px; display: flex; flex-direction: column; align-items: center; justify-content: center">
                                                <h4 style="line-height: 14px; color: #455A64; margin-bottom: 3px; text-align: center">{{ props.item.labelSubcategory }}</h4>
                                                <span style="line-height: 14px; color: #455A64">{{ props.item.percentSubcategory }}</span>
                                            </div>

                                        </v-card>

                                    </v-flex>
                                </v-data-iterator>
                            </v-container>

                        </v-card>
                    </v-flex>

                    <v-flex xs12 md6>
                        <v-card>
                            <div style="width: 100%; height: 40px; background: #BDBDBD; font-size: 19px; font-weight: bold; color: #455A64">
                                <div style="float: left; margin-left: 23px; height: 100%; vertical-align: middle; line-height: 40px;">TACHES</div>
                                <div style="float: right; margin-right: 23px; height: 100%; vertical-align: middle; line-height: 40px;">{{ task_title_price }} Ar</div>
                            </div>
                            <template v-if="categories_task_name.length > 0">
                                <pie-chart
                                    :height="260"
                                    :all_labels="categories_task_name"
                                    :all_montants="categories_task_montant"
                                    :all_categ_ids="categories_task_id"
                                    :all_responses="categories_task_response"
                                    :all_from="'task'"
                                    style="margin-top: 25px; margin-left: 25px"
                                    @subgroups="from_child_subgroups">
                                </pie-chart>
                            </template>
                            <template v-else>
                                <div style="display: flex; align-items: center; justify-content: center; height: 40px;">
                                    <span style="font-weight: bold">Oops! Aucune donnée disponible !</span>
                                </div>
                            </template>
                            <div style="margin-top: 15px; height: 35px; background: #BDBDBD; width: 100%; font-size: 17px; font-weight: bold; color: #455A64">
                                <div style="float: left; margin-left: 23px; height: 100%; vertical-align: middle; line-height: 35px;">{{ selected_label_task }}</div>
                                <div style="float: right; margin-right: 23px; height: 100%; vertical-align: middle; line-height: 35px;">{{ selected_price_task }}</div>
                            </div>
                            <v-container fluid grid-list-md style="min-height: 35px; margin-top: 0">
                                <v-data-iterator
                                    :items="items_task"
                                    :rows-per-page-items="rowsPerPageItems"
                                    :pagination.sync="pagination"
                                    no-data-text="Sélectionnez une catégorie"
                                    content-tag="v-layout"
                                    row
                                    hide-actions
                                    wrap
                                    style="margin-top: -15px">

                                    <v-flex
                                        slot="item"
                                        slot-scope="props"
                                        xs12
                                        md3>

                                        <v-card :ripple="true" style="background:#fafafa;">
                                            <transition name="bounce">
                                                <div v-if="props.item.activeSubcategory" class="d-flex black darken-2 v-card--reveal white--text" style="font-size: 9px important!; height: 100%; -webkit-transition: width 2s; transition: width 2s;">
                                                    {{ props.item.priceSubcategory }} Ar
                                                </div>
                                            </transition>
                                            <div @mouseover="mouseOver(props.item.idSubcategory, items_task)" style="height: 105px; display: flex; justify-content: center; align-items: center; background: #ECEFF1; margin-top: 9px">
                                                <img :src="urlBaseImage+'uploads/sous-categories/'+props.item.imageSubcategory" alt="" style="width:95%; height:95%; object-fit:crop; margin-top: 3px; padding-bottom: 3px;" onerror="this.style.display='none'">
                                            </div>
                                            <div style="height: 43px; display: flex; flex-direction: column; align-items: center; justify-content: center">
                                                <h4 style="line-height: 14px; color: #455A64; margin-bottom: 3px">{{ props.item.labelSubcategory }}</h4>
                                                <span style="line-height: 14px; color: #455A64">{{ props.item.percentSubcategory }}</span>
                                            </div>

                                        </v-card>

                                    </v-flex>
                                </v-data-iterator>
                            </v-container>
                        </v-card>
                    </v-flex>
                </v-layout>
              </v-container>

            </template>
            <data-no-user v-if="data_no_user"></data-no-user>
            <data-loader v-if="data_loader"></data-loader>

        </v-container>
    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_report_category_spent = '<?php echo URL_REPORT_CATEGORY_SPENT ?>';
    var url_report_category_spent_final = `${url_base}${url_report_category_spent}`;

    var url_report_category_task = '<?php echo URL_REPORT_CATEGORY_TASK ?>';
    var url_report_category_task_final = `${url_base}${url_report_category_task}`;

    Vue.component('report-category', {
        template: '#template-report-category',
        mixins: [common_mixin],
        data: function () {

            return {
                user_logged_in: undefined,
                modal: false,
                date: new Date().toISOString().substr(0, 7),
                spent_title_price: 0,
                categories_spent_name: [],
                categories_spent_montant: [],
                categories_spent_id: [],
                categories_spent_response: [],
                task_title_price: 0,
                categories_task_name: [],
                categories_task_montant: [],
                categories_task_id: [],
                categories_task_response: [],

                rowsPerPageItems: [100],
                pagination: {
                    rowsPerPage: 100
                },
                items_spent: [],
                selected_label_spent: 'Pas de sélection',
                selected_price_spent: '',
                items_task: [],
                selected_label_task: 'Pas de sélection',
                selected_price_task: '',
                urlBaseImage: url_base,
                active: false
            }

        },
        methods: {
            mouseOver: function(id, arr){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].idSubcategory == id)
                        arr[i].activeSubcategory = true;
                    else
                        arr[i].activeSubcategory = false;
                }
            },
            mouseLeave: function(id, arr){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].idSubcategory == id)
                        arr[i].activeSubcategory = false;
                }
            },
            wsReportCategorySpent: function(cur_year, cur_month){
                this.data_loader = true;
                var formData = new FormData();
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_month', cur_month);
                formData.append('spent_year', cur_year);
                axios.post(url_report_category_spent_final, formData).then((response) =>{
                    //console.log(`REPORT CATEGORY====>${JSON.stringify(response.data.report)}`);
                    this.categories_spent_response = response.data.report;
                    if(response.data.report && response.data.report.length > 0){
                        var ids_cat = this.takeAllCatId(response.data.report);
                        var ids_cat_filtered = ids_cat.filter((v,i) => ids_cat.indexOf(v) == i);
                        this.categories_spent_id = ids_cat_filtered;
                        for(var i= 0; i < ids_cat_filtered.length; i++){
                            this.categories_spent_name.push(this.getCatNameById(response.data.report, ids_cat_filtered[i]));
                            this.categories_spent_montant.push(this.regroupPriceByCategory(response.data.report, ids_cat_filtered[i]));
                            this.spent_title_price += this.regroupPriceByCategory(response.data.report, ids_cat_filtered[i]);
                        }
                    }

                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            wsReportCategoryTask: function(cur_year, cur_month){
                this.data_loader = true;
                var formData = new FormData();
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_month', cur_month);
                formData.append('spent_year', cur_year);
                axios.post(url_report_category_task_final, formData).then((response) =>{
                    //console.log(`REPORT CATEGORY====>${JSON.stringify(response.data.report)}`);
                    this.categories_task_response = response.data.report;
                    if(response.data.report && response.data.report.length > 0){
                        var ids_cat = this.takeAllCatId(response.data.report);
                        var ids_cat_filtered = ids_cat.filter((v,i) => ids_cat.indexOf(v) == i);
                        this.categories_task_id = ids_cat_filtered;
                        for(var i= 0; i < ids_cat_filtered.length; i++){
                            this.categories_task_name.push(this.getCatNameById(response.data.report, ids_cat_filtered[i]));
                            this.categories_task_montant.push(this.regroupPriceByCategory(response.data.report, ids_cat_filtered[i]));
                            this.task_title_price += this.regroupPriceByCategory(response.data.report, ids_cat_filtered[i]);
                        }
                    }

                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            regroupPriceByCategory: function(spents, catId){
                var total = 0;
                for(var i= 0; i < spents.length; i++){
                    if(parseInt(spents[i].categoryId) == catId){
                        var montant = 0;
                        let pr = Number(spents[i].number);
                        let pu = Number(spents[i].unitPrice);
                        if(spents[i].dayType === "weekly")
                            montant = (pu * pr) * 4;
                        else
                            montant = pu * pr;
                        total += montant;
                    }
                }
                return total;
            },
            getCatNameById: function(spents, catId){
                for(var i= 0; i < spents.length; i++){
                    if(parseInt(spents[i].categoryId) == catId){
                        return spents[i].categoryLabel;
                    }
                }
                return null;
            },
            from_child_subgroups: function(subgroups, from, selectedLabel, selectedPrice){
                if(from === 'spent'){
                    this.items_spent = subgroups;
                    this.selected_label_spent = selectedLabel;
                    this.selected_price_spent = selectedPrice;
                }else if(from === 'task'){
                    console.log("ato mintsy")
                    this.items_task = subgroups;
                    this.selected_label_task = selectedLabel;
                    this.selected_price_task = selectedPrice;
                }
            },
            requestByMonth: function(calendarDate){
                this.$refs.dialog.save(calendarDate);
                console.log(`ref save ${calendarDate}`);
                this.spent_title_price =  0;
                this.categories_spent_name = [];
                this.categories_spent_montant = [];
                this.categories_spent_id = [];
                this.categories_spent_response = [];
                this.task_title_price = 0;
                this.categories_task_name = [];
                this.categories_task_montant = [];
                this.categories_task_id = [];
                this.categories_task_response = [];
                this.items_spent = [];
                this.selected_label_spent = 'Pas de sélection';
                this.selected_price_spent = '';
                this.items_task = [];
                this.selected_label_task = 'Pas de sélection';
                this.selected_price_task = '';
                this.wsReportCategorySpent(this.getYearOfDate(calendarDate), this.getMonthOfDate(calendarDate));
                this.wsReportCategoryTask(this.getYearOfDate(calendarDate), this.getMonthOfDate(calendarDate));
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }
                    let today = new Date().toISOString().slice(0, 10);
                    //console.log(`YEAR====>${this.getYearOfDate(today)}`);
                    //console.log(`MONTH====>${this.getMonthOfDate(today)}`);
                    this.wsReportCategorySpent(this.getYearOfDate(today), this.getMonthOfDate(today));
                    this.wsReportCategoryTask(this.getYearOfDate(today), this.getMonthOfDate(today));
                }else{
                    console.log("REPORT CATEGORY PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            },

        },
        mounted: function () {
            console.log('report category : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },

    });

    Vue.component('pie-chart', {
        extends: VueChartJs.Pie,
        props: {
            all_labels: { type: Array, required: true },
            all_montants: { type: Array, required: true },
            all_categ_ids: { type: Array, required: true },
            all_responses: { type: Array, required: true },
            all_from: { type: String, required: true }
        },
        mounted () {
            this.renderChart({
                labels: this.all_labels,
                datasets: [
                    {
                        backgroundColor: [
                        '#D81B60',
                        '#CE93D8',
                        '#3949AB',
                        '#1E88E5',
                        '#76FF03',
                        '#FDD835',
                        '#DD1B16',
                        ],
                        data: this.all_montants
                    }
                ]
            },
            {
                responsive: true,
                maintainAspectRatio: false,
                pieceLabel: {
                    mode: 'percentage',
                    precision: 1
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: function (tooltipItem, data) {
                            let dataset = data.datasets[tooltipItem.datasetIndex]
                            let currentValue = dataset.data[tooltipItem.index]
                            let labels = data.labels;
                            let currentLabel = labels[tooltipItem.index];
                            return currentLabel + ': ' + currentValue + ' Ar'
                        }
                    }
                },
                onClick: this.handle,
            })
        },
        methods: {
            handle (point, event) {
                const item = event[0];
                if(item){
                    subcat_ids = this.getAllSubcatIdByCateg(this.all_responses, this.all_categ_ids[item._index]);
                    var subcat_ids_filtered = subcat_ids.filter((v,i) => subcat_ids.indexOf(v) == i);
                    var objs_subgroup = [];
                    for(var i= 0; i < subcat_ids_filtered.length; i++){
                        objs_subgroup.push({
                            idSubcategory: i,
                            labelSubcategory: this.getSubcatNameById(this.all_responses, subcat_ids_filtered[i]),
                            priceSubcategory: this.regroupSubcatPrice(this.all_responses, subcat_ids_filtered[i]),
                            imageSubcategory: this.getSubcatImageById(this.all_responses, subcat_ids_filtered[i]),
                            percentSubcategory: this.computePercent(this.all_montants[item._index], this.regroupSubcatPrice(this.all_responses, subcat_ids_filtered[i])),
                            activeSubcategory: false
                        });
                    }
                    objs_subgroup.sort(function (a, b) {
                        return b.priceSubcategory - a.priceSubcategory;
                    });
                    this.$emit('subgroups', objs_subgroup, this.all_from, this.all_labels[item._index], `${this.all_montants[item._index]} Ar`);
                    let obj = {
                        index: item._index,
                        value: this.all_montants[item._index],
                        label: this.all_labels[item._index],
                        catid: this.all_categ_ids[item._index]
                    }
                    //console.log(`subbbb==========>${JSON.stringify(objs_subgroup)}`);
                }

            },
            getAllSubcatIdByCateg: function(spents, categ_id){
                res = [];
                spents.forEach((spent, i) => {
                    if(categ_id == parseInt(spent.categoryId))
                        res.push(parseInt(spent.subcategoryId));
                });
                return res;
            },
            regroupSubcatPrice: function(spents, subcat_id){
                var total = 0;
                for(var i= 0; i < spents.length; i++){
                    if(parseInt(spents[i].subcategoryId) == subcat_id){
                        var montant = 0;
                        let pr = Number(spents[i].number);
                        let pu = Number(spents[i].unitPrice);
                        if(spents[i].dayType === "weekly")
                            montant = (pu * pr) * 4;
                        else
                            montant = pu * pr;
                        total += montant;
                    }
                }
                return total;
            },
            getSubcatNameById: function(spents, subcatId){
                for(var i= 0; i < spents.length; i++){
                    if(parseInt(spents[i].subcategoryId) == subcatId){
                        return spents[i].subcategoryLabel;
                    }
                }
                return null;
            },
            getSubcatImageById: function(spents, subcatId){
                for(var i= 0; i < spents.length; i++){
                    if(parseInt(spents[i].subcategoryId) == subcatId){
                        return spents[i].subcategoryImage;
                    }
                }
                return null;
            },
            computePercent: function(total, montant){
                return `${Number((montant * 100)/total).toFixed(2)} %`;
            },
        }
    })

</script>