<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

    .event-todo{
        background: #f9944a !important;
    }

    .event-inprogress{
        background: #4A9FF9 !important;
    }

    .event-finished{
        background: #2ac06d !important;
    }

    .event-validated{
        background: #BDBDBD !important;
    }

    .event-rejected{
        background: #c91010 !important;
    }

    .board {
        position: relative;
        margin-left: 1%;
        margin-top: 5px;
    }
    .board-column {
        position: absolute;
        left: 0;
        right: 0;
        width: 18%;
        margin: 0 0.87%;
        background: #f0f0f0;
        border-radius: 3px;
        z-index: 1;
    }
    .board-column.muuri-item-releasing {
        z-index: 2;
    }
    .board-column.muuri-item-dragging {
        z-index: 3;
        cursor: move;
    }
    .board-column-header {
        position: relative;
        height: 50px;
        line-height: 50px;
        overflow: hidden;
        padding: 0 20px;
        text-align: center;
        background: #333;
        color: #fff;
        border-radius: 3px 3px 0 0;
    }
    @media (max-width: 600px) {
        .board-column-header {
            text-indent: -1000px;
        }
    }
    .board-column.todo .board-column-header {
        background: #f9944a;
    }
    .board-column.working .board-column-header {
        background: #4A9FF9;
    }
    .board-column.done .board-column-header {
        background: #2ac06d;
    }
    .board-column.validate .board-column-header {
        background: #BDBDBD;
    }
    .board-column.rejected .board-column-header {
        background: #c91010;
    }
    .board-column-content {
        position: relative;
        border: 10px solid transparent;
    }
    .board-item {
        position: absolute;
        width: 100%;
        margin: 5px 0;
    }
    .board-item.muuri-item-releasing {
        z-index: 9998;
    }
    .board-item.muuri-item-dragging {
        z-index: 9999;
        cursor: move;
    }
    .board-item.muuri-item-hidden {
        z-index: 0;
    }
    .board-item-content {
        position: relative;
        padding: 6px;
        background: #fff;
        border-radius: 4px;
        font-size: 15px;
        cursor: pointer;
        -webkit-box-shadow: 0px 1px 3px 0 rgba(0,0,0,0.2);
        box-shadow: 0px 1px 3px 0 rgba(0,0,0,0.2);
    }
    @media (max-width: 600px) {
        .board-item-content {
            text-align: center;
        }
        .board-item-content span {
            display: none;
        }
    }
    .header-comment {
        padding-left: 19px;
        padding-top: 8px;
        padding-bottom: 8px;
    }

</style>
<script type="text/x-template" id="template-kanban">

    <div>
        <template v-if="!data_no_user">

            <!--<div v-if="!data_no_user" style="padding-top: 12px; text-align: center; font-weight: bold; font-size: 19px; color: #757575;">Aujourd'hui</div> -->
            <div style="display: flex; flex-direction: row; height: 50px; width: 100%; flex: 1">
                <div style="vertical-align: middle; width: 110px; margin-top: -5px; margin-left: 25px">
                    <v-switch v-model="switch_periode" label="Période"></v-switch>
                </div>
                <div v-if="switch_periode" style="padding-top: 12px; font-weight: bold; font-size: 19px; color: #757575; margin-left: 15px;">Du </div>
                <div style="height: 100%">
                    <v-dialog
                        ref="dialog_one"
                        v-model="modal_one"
                        :return-value.sync="date_one"
                        persistent
                        lazy
                        full-width
                        width="520px"
                        justify-center
                        style="width: 100% !important">

                            <div slot="activator">
                                <div style="vertical-align: middle;">
                                    <div class="hidden-sm-and-down" style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 13px; margin-left: 13px">
                                        <v-icon>event</v-icon>
                                        <span style="margin-left: 5px; text-align: center; font-weight: bold; font-size: 19px; color: #757575;">{{ date_one | format_date }}</span>
                                    </div>
                                </div>
                            </div>

                            <v-date-picker v-model="date_one" scrollable full-width locale="fr-fr">
                                <v-spacer></v-spacer>
                                <v-btn flat color="primary" @click="modal_one = false">Annuler</v-btn>
                                <v-btn flat color="primary" @click="requestByDate(date_one)">OK</v-btn>
                            </v-date-picker>

                    </v-dialog>
                </div>
                <div v-if="switch_periode" style="padding-top: 12px; font-weight: bold; font-size: 19px; color: #757575; margin-left: 15px;"> au </div>
                <div v-if="switch_periode" style="height: 100%">
                    <v-dialog
                        ref="dialog_two"
                        v-model="modal_two"
                        :return-value.sync="date_two"
                        persistent
                        lazy
                        full-width
                        width="520px"
                        justify-center
                        style="width: 100% !important">

                            <div slot="activator">
                                <div style="vertical-align: middle; margin-left: 15px">
                                    <div class="hidden-sm-and-down" style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 13px">
                                        <v-icon>event</v-icon>
                                        <span style="margin-left: 5px; text-align: center; font-weight: bold; font-size: 19px; color: #757575;">{{ date_two | format_date }}</span>
                                    </div>
                                </div>
                            </div>

                            <v-date-picker v-model="date_two" scrollable full-width locale="fr-fr">
                                <v-spacer></v-spacer>
                                <v-btn flat color="primary" @click="modal_two = false">Annuler</v-btn>
                                <v-btn flat color="primary" @click="requestByRange(date_two)">OK</v-btn>
                            </v-date-picker>

                    </v-dialog>
                </div>
                <div class="hidden-sm-and-down" style="display: flex; flex-direction: row; justify-content: flex-end;">
                    <div style="padding-top: 12px; font-weight: bold; font-size: 19px; color: #757575; margin-left: 25px;">Assigné à </div>
                    <v-radio-group style="vertical-align: middle; margin-top: 10px; margin-left: 15px" v-model="row_assignation" row>
                        <v-radio label="Moi" value="1"></v-radio>
                        <v-radio label="Autre personne" value="2"></v-radio>
                    </v-radio-group>
                </div>
            </div>

            <div class="board" :style="setVisibility(display)">
                <div class="board-column todo">
                    <div class="board-column-header">A faire</div>
                    <div class="board-column-content">
                        <div v-for="(item, i) in task_managment.todo" :key="i" class="board-item" :data-id="item.details.eventId" data-grid="todo">
                            <div class="board-item-content">
                                <a class="card-detail" @click="showKanbanDialog(transformToEventData(item))" style="font-weight: bold; font-size: 13px; margin-left: 7px" href="#">TIK-{{ item.details.eventId }}</a>
                                <div style="position: absolute; right: 5px; top: 5px">
                                    <img v-if="!!item.details.assignedUserImage" :src="urlBaseImage+'uploads/utilisateurs/'+item.details.assignedUserImage" alt="" width="34" height="34" >
                                    <v-icon v-else style="font-size: 40px;">face</v-icon>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 11px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">assignment</v-icon>
                                    <span class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.subtaskLabel }}</span>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">place</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.place_description }}</h1>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">access_time</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ removeThreeLastChar(formatDateToMMDDYYYY(item.details.eventDebut)) }}</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="board-column working">
                    <div class="board-column-header">En progression</div>
                    <div class="board-column-content">
                        <div v-for="(item, i) in task_managment.inprogress" :key="i" class="board-item" :data-id="item.details.eventId" data-grid="inprogress">
                            <div class="board-item-content">
                                <a class="card-detail" @click="showKanbanDialog(transformToEventData(item))" style="font-weight: bold; font-size: 13px; margin-left: 7px" href="#">TIK-{{ item.details.eventId }}</a>
                                <div style="position: absolute; right: 5px; top: 5px">
                                    <img v-if="!!item.details.assignedUserImage" :src="urlBaseImage+'uploads/utilisateurs/'+item.details.assignedUserImage" alt="" width="34" height="34" >
                                    <v-icon v-else style="font-size: 40px;">face</v-icon>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 11px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">assignment</v-icon>
                                    <span class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.subtaskLabel }}</span>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">place</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.place_description }}</h1>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">access_time</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ removeThreeLastChar(formatDateToMMDDYYYY(item.details.eventDebut)) }}</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="board-column done">
                    <div class="board-column-header">Finies</div>
                    <div class="board-column-content">
                        <div v-for="(item, i) in task_managment.finished" :key="i" class="board-item" :data-id="item.details.eventId" data-grid="finished">
                            <div class="board-item-content">
                                <a class="card-detail" @click="showKanbanDialog(transformToEventData(item))" style="font-weight: bold; font-size: 13px; margin-left: 7px" href="#">TIK-{{ item.details.eventId }}</a>
                                <div style="position: absolute; right: 5px; top: 5px">
                                    <img v-if="!!item.details.assignedUserImage" :src="urlBaseImage+'uploads/utilisateurs/'+item.details.assignedUserImage" alt="" width="34" height="34" >
                                    <v-icon v-else style="font-size: 40px;">face</v-icon>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 11px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">assignment</v-icon>
                                    <span class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.subtaskLabel }}</span>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">place</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.place_description }}</h1>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">access_time</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ removeThreeLastChar(formatDateToMMDDYYYY(item.details.eventDebut)) }}</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="board-column validate">
                    <div class="board-column-header">Validées</div>
                    <div class="board-column-content">
                        <div v-for="(item, i) in task_managment.validated" :key="i" class="board-item" :data-id="item.details.eventId" data-grid="validated">
                            <div class="board-item-content">
                                <a class="card-detail" @click="showKanbanDialog(transformToEventData(item))" style="font-weight: bold; font-size: 13px; margin-left: 7px" href="#">TIK-{{ item.details.eventId }}</a>
                                <div style="position: absolute; right: 5px; top: 5px">
                                    <img v-if="!!item.details.assignedUserImage" :src="urlBaseImage+'uploads/utilisateurs/'+item.details.assignedUserImage" alt="" width="34" height="34" >
                                    <v-icon v-else style="font-size: 40px;">face</v-icon>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 11px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">assignment</v-icon>
                                    <span class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.subtaskLabel }}</span>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">place</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.place_description }}</h1>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">access_time</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ removeThreeLastChar(formatDateToMMDDYYYY(item.details.eventDebut)) }}</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="board-column rejected">
                    <div class="board-column-header">Rejetées</div>
                    <div class="board-column-content">
                        <div v-for="(item, i) in task_managment.rejected" :key="i" class="board-item" :data-id="item.details.eventId" data-grid="rejected">
                            <div class="board-item-content">
                                <a class="card-detail" @click="showKanbanDialog(transformToEventData(item))" style="font-weight: bold; font-size: 13px; margin-left: 7px" href="#">TIK-{{ item.details.eventId }}</a>
                                <div style="position: absolute; right: 5px; top: 5px">
                                    <img v-if="!!item.details.assignedUserImage" :src="urlBaseImage+'uploads/utilisateurs/'+item.details.assignedUserImage" alt="" width="34" height="34" >
                                    <v-icon v-else style="font-size: 40px;">face</v-icon>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 11px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">assignment</v-icon>
                                    <span class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.subtaskLabel }}</span>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">place</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ item.details.place_description }}</h1>
                                </div>
                                <div style="display: flex; flex-direction: row; align-items: center; margin-top: 2px; margin-left: 7px">
                                    <v-icon style="font-size: 18px;">access_time</v-icon>
                                    <h1 class="task-right-text" style="margin-left: 5px; font-size: 13px">{{ removeThreeLastChar(formatDateToMMDDYYYY(item.details.eventDebut)) }}</h1>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </template>

        <v-dialog v-model="dialog_kanban" width="600px" style="background: #ffffff;">

                <v-card class="elevation-4" style="padding: 25px">
                    <div style="display: flex; flex-direction: row; height: 30px;">
                        <h2 v-if="planning_event" style="margin-left: 5px; font-size: 16px; color: #757575; font-weight: bold; line-height:30px; vertical-align:middle">
                            Tâche : {{ planning_event.subtaskLabel }}
                            <!--<span :class="getClassNameByStatut(planning_event.state)" style="border-radius: 2px; margin-left: 5px; font-weight: bold; color: white; padding: 4px"> {{ planning_event.eventStatut }} </span>-->
                        </h2>
                        <v-menu v-if="planning_event" offset-y>
                            <template slot="activator">
                                <v-btn
                                style="height: 25px !important; color: white !important; font-weight: bold"
                                :class="getClassNameByStatut(planning_event.state)"
                                >
                                {{ planning_event.eventStatut }}
                                </v-btn>
                            </template>
                            <v-list style="padding: 3px 0 !important;">
                                <v-list-tile
                                    class="statut-item"
                                    v-for="(item, index) in available_next_status"
                                    :key="index"
                                    @click="updateEventStatut(planning_event.ticketId, item.state)"
                                >
                                    <v-list-tile-title>{{ item.stateLabel }}</v-list-tile-title>
                                </v-list-tile>
                            </v-list>
                        </v-menu>
                    </div>
                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                        <v-icon>assignment_ind</v-icon>
                        <div v-if="planning_event" >
                            <h1 class="task-right-text" style="margin-left: 5px">{{ planning_event.assignedUser }}</h1>
                        </div>


                    </div>
                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 10px">

                        <v-icon>place</v-icon>
                        <h1 v-if="planning_event" class="task-right-text" style="margin-left: 5px">{{ planning_event.placeDescription }}</h1>

                    </div>

                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 5px">
                        <v-icon>attach_money</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px"><span style="width: 150px">Dépenses effectuées</span><hr style='display:inline-block; width:350px;' /></div>
                    </div>

                    <v-layout fluid wrap>

                        <v-flex xs12 md12 v-if="planning_event" style="margin-top: 10px">

                            <template v-for="(taskspent, index) in planning_event.taskspents">

                                <v-flex xs12>

                                    <v-layout row wrap>

                                        <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                                            <div>
                                                <v-layout justify-center fill-height align-center>
                                                    <div style="display:block; margin:0 auto; text-align: center;">

                                                        <img :src="urlBaseImage+'/uploads/categories/'+taskspent.category.categoryImage" alt="" style="margin-top: 5px;width:42%;height:42%;object-fit:contain">
                                                        <p class="text-xs-center" style="margin-top: -4px; font-weight: bold; font-size: 12px">{{ taskspent.category.categoryLabel }}</p>

                                                    </div>
                                                </v-layout>
                                            </div>
                                        </v-flex>

                                        <v-flex d-flex xs12 sm6 md9>

                                            <v-layout row wrap >

                                                <template v-for="subcategory in taskspent.subcategories">

                                                    <v-layout row style="min-height: 27px; max-height: 41px; width: 100%; margin-top: 2px">
                                                        <v-flex xs12 sm6 md1 style="display: flex; align-items: center;">
                                                            <div :style="`border-radius: 50%; width: 15px; height: 15px; background: ${subcategory.utility.contextImage}`"></div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md5 style="display: flex; align-items: center;">
                                                            <div>

                                                                <div class="text-day-task">{{ subcategory.subcategory_label }}</div>

                                                            </div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md4>
                                                            <div class="prix_report">
                                                                <span style="height: 100%; display: flex; align-items: center; font-size: 13px; color: #78909C">{{ subcategory.price }} Ar</span>
                                                            </div>
                                                        </v-flex>
                                                        <v-flex xs12 sm6 md2>
                                                            <span style="height: 100%; text-align: left; display: flex; align-items: center; font-size: 13px; color: #546E7A">x {{ subcategory.number }}</span>
                                                        </v-flex>

                                                    </v-layout>


                                                </template>

                                            </v-layout>

                                        </v-flex>

                                    </v-layout>

                                    <hr v-if="index != (planning_event.taskspents.length - 1)" />

                                </v-flex>

                            </template>

                        </v-flex>

                    </v-layout>

                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 5px">
                        <v-icon>attach_file</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px;"><span style="width: 125px">Pièce(s) jointe(s)</span><hr style='display:inline-block; width:375px;' /></div>
                    </div>

                    <div>
                        <template v-if="list_pieces.length > 0" v-for="(piece, index) in list_pieces">
                            <v-tooltip top>
                                <v-chip
                                    slot="activator"
                                    class="white--text"
                                    style="height: 33px; background: #BDBDBD; cursor: pointer;"
                                    @click="downloadWithAxios(piece.namepiece)">

                                    <v-icon left>mdi-coin</v-icon>
                                    <span style="font-weight: normal; font-size: 13px; color: #455A64; overflow:hidden; text-overflow:ellipsis; max-width:180px;">{{ piece.namepiece }}</span>

                                </v-chip>
                                <span> <b>Auteur : </b>{{ piece.first_name }} {{ piece.last_name }} <br> <b>Date : </b>{{ formatDateToMMDDYYYY(piece.datepiece) }} </span>
                            </v-tooltip>
                        </template>
                        <div style="display: flex; align-items: center">

                            <v-chip
                                class="white--text"
                                style="height: 33px; background: #BDBDBD; cursor: pointer"
                                @click="pickFile()">

                                <v-icon left>add</v-icon>
                                <span style="font-weight: bold; font-size: 15px; color: #fff; margin-left: -10px">Ajouter</span>

                            </v-chip>

                        </div>
                        <input
                            type="file"
                            style="display: none"
                            ref="addfile"
                            multiple
                            @change="onFilePicked"
                        >
                    </div>

                    <div style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 13px">
                        <v-icon>comment</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px"><span style="width: 120px">Commentaire(s)</span><hr style='display:inline-block; width:380px;' /></div>
                    </div>

                    <v-layout fluid wrap style="margin-top: 10px">

                        <v-flex xs12>
                            <template v-for="(comment, index) in list_comments">

                                <v-card v-if="comment" :ripple="true" class="elevation-0" style="border-radius: 5px;margin-top: 6px; margin-left: 6px; margin-right: 6px; padding-top: 10px; padding-bottom: 17px; padding-left: 8px; padding-right: 8px; background: #bcc4c61F">

                                    <div style="padding-top: 6px; padding-left: 10px; padding-right: 50px; padding-bottom: 20px">
                                        {{ comment.comment }}
                                    </div>

                                    <div style="position: absolute; right: 5px; bottom: 1px; height: 40px; display: flex; align-items: center;">
                                        <div style="display: inline-block; float: left; height: 15px; margin-right: 7px; font-size: 10px">
                                        {{ getLocalLongDate(comment.datecomment) }}
                                        </div>
                                        <div style="display: inline-block; float: left; height: 34px;">
                                            <img v-if="!!comment.user_image" :src="urlBaseImage+'uploads/utilisateurs/'+comment.user_image" alt="" width="34" height="34" style="margin-right: 7px">
                                            <v-icon v-else style="font-size: 40px; margin-right: 4px">face</v-icon>
                                        </div>
                                    </div>

                                </v-card>

                            </template>

                            <accordion-kanban class="comment-accordion" theme="grey lighten-5" ref="refresh_week" style="border-radius:5px;">
                                <div slot="header" style="display: flex; flex-direction: row">
                                    <v-icon style="font-size: 22px; ">add_circle_outline</v-icon>
                                    <span style="font-weight: bold; color: rgb(117, 117, 117); margin-left: 8px">Ajouter un commentaire</span>
                                </div>
                                <v-layout row wrap style="padding-bottom: 10px">
                                    <v-flex xs10>
                                        <v-card class="elevation-2" style="margin-left: 7px; margin-top: 10px">
                                            <v-textarea
                                                style="padding-left: 10px; padding-right: 10px; padding-top: 10px"
                                                v-model="newtextcomment"
                                                >
                                            </v-textarea>
                                        </v-card>
                                    </v-flex>

                                    <v-flex xs2>
                                        <div style="display: flex; justify-content: center; margin-top: 14px">
                                            <v-btn
                                                v-if="planning_event"
                                                fab
                                                small
                                                color="blue-grey lighten-1"
                                                dark
                                                @click="addComment(planning_event.ticketId)">
                                                    <v-icon>add</v-icon>
                                            </v-btn>
                                        </div>
                                    </v-flex>
                                </v-layout>
                            </accordion-kanban>

                        </v-flex>

                    </v-layout>

                    <div v-if="list_histories.length > 0" style="display: flex; flex-direction: row; align-items: center; height: 100%; margin-top: 5px">
                        <v-icon>history</v-icon>
                        <div class="task-right-text" style="margin-left: 5px; display: flex; justify-content: center; align-items: center; height: 35px"><span style="width: 100px">Historique(s)</span><hr style='display:inline-block; width:400px;' /></div>
                    </div>

                    <v-data-table
                        v-if="list_histories.length > 0"
                        :headers="headers"
                        :items="list_histories"
                        class="elevation-2"
                        :pagination.sync="pagination_history"
                        :total-items="list_histories.length"
                        :hide-actions="true"
                        style="margin-top: 7px; margin-left: 7px; margin-right: 7px; background: #F7F8F8; border-radius: 6px"
                    >
                        <template
                            slot="items"
                            slot-scope="props"
                            >
                            <td style="color: rgba(0,0,0,.54);">{{ props.item.first_name }} {{ props.item.last_name }}</td>
                            <td style="color: rgba(0,0,0,.54);">{{ formatDateToMMDDYYYY(props.item.datehistory) }}</td>
                            <td style="color: rgba(0,0,0,.54);">{{ getStatutLetter(props.item.statuthistory) }}</td>

                        </template>
                    </v-data-table>

                </v-card>

        </v-dialog>
        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>
        <v-btn
            fab
            bottom
            right
            color="blue-grey lighten-1"
            dark
            fixed
            @click="showAdd"
            v-if="!data_no_user">
            <v-icon>add</v-icon>
        </v-btn>
    </div>

</script>

<template id="template-accordion-kanban">
    <div style="margin-left: 4px; margin-right: 4px" v-bind:class="theme">
        <div class="header-comment" @click="toggle">
            <slot name="header">HINT</slot>
            <!--<i class="fa fa-2x fa-angle-down header-icon" v-bind:class="{ rotate: show }"></i>-->
            <v-icon class="header-icon" style="font-size: 33px;" v-bind:class="{ rotate: show }">keyboard_arrow_down</v-icon>
        </div>
        <transition name="accordion"
            v-on:before-enter="beforeEnter" v-on:enter="enter"
            v-on:before-leave="beforeLeave" v-on:leave="leave">
            <div class="body" v-show="show">
                <div class="body-inner">
                <slot></slot>
                </div>
            </div>
        </transition>
    </div>
</template>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_get_notification_task = '<?php echo URL_GET_NOTIFICATION ?>';
    var url_get_notification_task_final = `${url_base}${url_get_notification_task}`;

    var url_get_other_notification_task = '<?php echo URL_GET_OTHER_NOTIFICATION ?>';
    var url_get_other_notification_task_final = `${url_base}${url_get_other_notification_task}`;

    var url_range_notification_task = '<?php echo URL_GET_RANGE_NOTIFICATION ?>';
    var url_range_notification_task_final = `${url_base}${url_range_notification_task}`;

    var url_other_range_notification_task = '<?php echo URL_GET_OTHER_RANGE_NOTIFICATION ?>';
    var url_other_range_notification_task_final = `${url_base}${url_other_range_notification_task}`;

    var url_list_comment = '<?php echo URL_LIST_COMMENT_BY_EVENT ?>';
    var url_list_comment_final = `${url_base}${url_list_comment}`;

    var url_create_comment = '<?php echo URL_CREATE_COMMENT ?>';
    var url_create_comment_final = `${url_base}${url_create_comment}`;

    var url_list_piece = '<?php echo URL_LIST_PIECE ?>';
    var url_list_piece_final = `${url_base}${url_list_piece}`;

    var url_list_history = '<?php echo URL_LIST_HISTORY ?>';
    var url_list_history_final = `${url_base}${url_list_history}`;

    var url_create_piece = '<?php echo URL_CREATE_PIECE ?>';
    var url_create_piece_final = `${url_base}${url_create_piece}`;

    var url_update_event_statut = '<?php echo URL_UPDATE_EVENT_STATUT ?>';
    var url_update_event_statut_final = `${url_base}${url_update_event_statut}`;

    var url_create_history = '<?php echo URL_CREATE_HISTORY ?>';
    var url_create_history_final = `${url_base}${url_create_history}`;

    /*kanban*/
    const customStringify = function (v) {
        const cache = new Set();
        return JSON.stringify(v, function (key, value) {
            if (typeof value === 'object' && value !== null) {
            if (cache.has(value)) {
                // Circular reference found
                try {
                // If this value does not reference a parent it can be deduped
                return JSON.parse(JSON.stringify(value));
                }
                catch (err) {
                // discard key if value cannot be deduped
                return;
                }
            }
            // Store value in our set
            cache.add(value);
            }
            return value;
        });
    };

    /*condition predicate*/
    function elementMatches(element, selector) {
        var p = Element.prototype;
        return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);
    }

    /*fin kanban*/

    var grid_ids = [];
    Vue.component('kanban', {
        template: '#template-kanban',
        mixins: [common_mixin],
        data: function () {

            return{
                name: '',
                user_logged_in: undefined,
                task_managment : {
                    todo: [],
                    inprogress: [],
                    finished: [],
                    validated: [],
                    rejected:[]
                },
                urlBaseImage: url_base,
                display: false,
                date_one: new Date().toISOString().substr(0, 10),
                date_two: new Date().toISOString().substr(0, 10),
                modal_one: false,
                modal_two: false,
                switch_periode: true,
                row_assignation: "1",
                task_status : [
                    '<?php echo CONSTANT_TASK_TODO ?>',
                    '<?php echo CONSTANT_TASK_INPROGRESS ?>',
                    '<?php echo CONSTANT_TASK_FINISHED ?>',
                    '<?php echo CONSTANT_TASK_VALIDATED ?>',
                    '<?php echo CONSTANT_TASK_REJECTED ?>',
                ],
                drag_enable: true,
                date_of_task: null,

                dialog_kanban: false,
                planning_event: null,
                list_comments: [],
                newtextcomment: null,
                list_pieces: [],
                list_histories: [],
                headers: [
                    {
                        text: 'Auteur',
                        align: 'left',
                        sortable: false,
                        value: 'autor'
                    },
                    {
                        text: 'Date',
                        align: 'left',
                        sortable: false,
                        value: 'date'
                    },
                    {
                        text: 'Statut',
                        align: 'left',
                        sortable: false,
                        value: 'statut'
                    }
                ],
                pagination_history: {},
                available_next_status: [],
                lastRequest: 0
            }

        },
        methods: {
            initKanban: function(){
                var itemContainers = [].slice.call(document.querySelectorAll('.board-column-content'));
                var columnGrids = [];
                var boardGrid;

                // Define the column grids so we can drag those
                // items around.
                var i = 0;
                itemContainers.forEach((container) => {

                    // Instantiate column grid.
                    var grid = new Muuri(container, {
                        items: '.board-item',
                        layoutDuration: 400,
                        layoutEasing: 'ease',
                        dragEnabled: this.drag_enable,
                        dragSort: function () {
                            return columnGrids;
                        },
                        dragSortInterval: 0,
                        dragContainer: document.body,
                        dragStartPredicate: function (item, event) {

                            var isRemoveAction = elementMatches(event.target, '.card-detail, .card-detail span');
                            return !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;

                        },
                        dragReleaseDuration: 400,
                        dragReleaseEasing: 'ease'
                    })
                    .on('dragStart', (item) => {
                        // Let's set fixed widht/height to the dragged item
                        // so that it does not stretch unwillingly when
                        // it's appended to the document body for the
                        // duration of the drag.
                        item.getElement().style.width = item.getWidth() + 'px';
                        item.getElement().style.height = item.getHeight() + 'px';
                    })
                    .on('dragReleaseEnd', (item) => {
                        // Let's remove the fixed width/height from the
                        // dragged item now that it is back in a grid
                        // column and can freely adjust to it's
                        // surroundings.
                        var eventid = item.getElement().getAttribute('data-id');
                        var gridorigin = item.getElement().getAttribute('data-grid');
                        var gridcurrent = item.getGrid()._id;
                        if(grid_ids && grid_ids.length > 0){
                            if(gridorigin === 'todo' && gridcurrent != grid_ids[0]){
                                console.log('NIFINDRA ilay TODO', grid_ids[0]);
                                this.updateEventStatut(eventid, this.getEventStatutByGrid(gridcurrent));
                            }else if(gridorigin === 'inprogress' && gridcurrent != grid_ids[1]){
                                console.log('NIFINDRA ilay INPROGRESS', grid_ids[1]);
                                this.updateEventStatut(eventid, this.getEventStatutByGrid(gridcurrent));
                            }else if(gridorigin === 'finished' && gridcurrent != grid_ids[2]){
                                console.log('NIFINDRA ilay FINISHED', grid_ids[2]);
                                this.updateEventStatut(eventid, this.getEventStatutByGrid(gridcurrent));
                            }else if(gridorigin === 'validated' && gridcurrent != grid_ids[3]){
                                console.log('NIFINDRA ilay VALIDATED', grid_ids[3]);
                                //resend to its origin because validated not updatable
                            }
                        }
                        console.log(`ID====>${eventid}-GRIDORIGIN====>${gridorigin}-GRIDCURRENT====>${customStringify(gridcurrent)}`);
                        //console.log(`AFTER DRAG====>${customStringify(item)}`);
                        item.getElement().style.width = '';
                        item.getElement().style.height = '';
                        // Just in case, let's refresh the dimensions of all items
                        // in case dragging the item caused some other items to
                        // be different size.
                        columnGrids.forEach(function (grid) {
                            grid.refreshItems();
                        });
                    })
                    .on('layoutStart', () => {
                        // Let's keep the board grid up to date with the
                        // dimensions changes of column grids.
                        boardGrid.refreshItems().layout();
                    });

                    // Add the column grid reference to the column grids
                    // array, so we can access it later on.
                    columnGrids.push(grid);
                    i++;

                });

                for(var i = 0; i < columnGrids.length; i++) {
                    grid_ids.push(columnGrids[i]._id);
                }
                console.log(`GRID IDS ====>${grid_ids}`);
                //console.log(`ITEMS====>${columnGrids[2]._id}`);
                //console.log(`NUMBER OF ====>${i}`);
                // Instantiate the board grid so we can drag those
                // columns around.
                boardGrid = new Muuri('.board', {
                    layoutDuration: 400,
                    layoutEasing: 'ease',
                    dragEnabled: true,
                    dragSortInterval: 0,
                    dragStartPredicate: {
                        handle: '.board-column-header'
                    },
                    dragReleaseDuration: 400,
                    dragReleaseEasing: 'ease'
                });
            },
            /*simple date*/
            getNotificationTask(user, date){
                this.resetValues();
                this.lastRequest = 1;
                this.data_loader = true;
                var formData = new FormData();
                formData.append('user_id', user);
                formData.append('notif_date', date);
                axios.post(url_get_notification_task_final, formData).then((response) => {
                    //console.log(`RESPONSE GET NOTIF ====> ${JSON.stringify(response.data.notif)}`);
                    this.populateTasks(response.data.notif);
                })
                .finally(() => {
                    this.closeLoaderAfterDelay();
                });
            },
            getOtherNotificationTask(user, date){
                this.resetValues();
                this.lastRequest = 2;
                this.data_loader = true;
                var formData = new FormData();
                formData.append('user_id', user);
                formData.append('notif_date', date);
                axios.post(url_get_other_notification_task_final, formData).then((response) => {
                    //console.log(`RESPONSE GET NOTIF ====> ${JSON.stringify(response.data.notif)}`);
                    this.populateTasks(response.data.notif);
                })
                .finally(() => {
                    this.closeLoaderAfterDelay();
                });
            },
            /*range date*/
            getRangeNotificationTask(user, begin, end){
                this.resetValues();
                this.lastRequest = 3;
                this.data_loader = true;
                var formData = new FormData();
                formData.append('user_id', user);
                formData.append('notif_date_begin', begin);
                formData.append('notif_date_end', end);
                axios.post(url_range_notification_task_final, formData).then((response) => {
                    //console.log(`RESPONSE GET NOTIF ====> ${JSON.stringify(response.data.notif)}`);
                    this.populateTasks(response.data.notif);
                })
                .finally(() => {
                    this.closeLoaderAfterDelay();
                });
            },
            getOtherRangeNotificationTask(user, begin, end){
                this.resetValues();
                this.lastRequest = 4;
                this.data_loader = true;
                var formData = new FormData();
                formData.append('user_id', user);
                formData.append('notif_date_begin', begin);
                formData.append('notif_date_end', end);
                axios.post(url_other_range_notification_task_final, formData).then((response) => {
                    //console.log(`RESPONSE GET NOTIF ====> ${JSON.stringify(response.data.notif)}`);
                    this.populateTasks(response.data.notif);
                })
                .finally(() => {
                    this.closeLoaderAfterDelay();
                });
            },
            /*common*/
            updateEventStatut: function(eventId, eventStatut){

                this.data_loader = true;
                var formData = new FormData();
                formData.append('id_event', eventId);
                formData.append('statut_event', eventStatut);
                axios.post(url_update_event_statut_final, formData).then((response) => {
                    console.log(`RESPONSE UPDATE STATUT ====> ${JSON.stringify(response.data.error)}`);
                    this.addHistory(eventStatut, eventId);

                    if(this.lastRequest == 1){
                        this.getNotificationTask(this.user_logged_in.id, this.date_one);
                    }else if(this.lastRequest == 2){
                        this.getOtherNotificationTask(this.user_logged_in.id, this.date_one);
                    }else if(this.lastRequest == 3){
                        this.getRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                    }else if(this.lastRequest == 4){
                        this.getOtherRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                    }

                    if(this.dialog_kanban)
                        this.dialog_kanban = false;
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                });

            },
            getEventStatutByGrid: function(gridcurrent){
                var statut = "DEFAULT";
                if(grid_ids.indexOf(gridcurrent) == 0){
                    statut = '<?php echo CONSTANT_TASK_TODO ?>';
                }else if(grid_ids.indexOf(gridcurrent) == 1){
                    statut = '<?php echo CONSTANT_TASK_INPROGRESS ?>';
                }else if(grid_ids.indexOf(gridcurrent) == 2){
                    statut = '<?php echo CONSTANT_TASK_FINISHED ?>';
                }else if(grid_ids.indexOf(gridcurrent) == 3){
                    statut = '<?php echo CONSTANT_TASK_VALIDATED ?>';
                }else if(grid_ids.indexOf(gridcurrent) == 4){
                    statut = '<?php echo CONSTANT_TASK_REJECTED ?>';
                }
                return statut;
            },
            groupTaskByStatut(arr, statut){
                var res = [];
                for(var i = 0; i < arr.length; i++) {
                    if(arr[i].eventStatut === statut)
                        res.push(arr[i]);
                }
                return res;
            },
            populateTasks(resp){
                if(resp && resp.length > 0){
                    for(var i = 0; i < this.task_status.length; i++) {
                        var bystatut = this.groupTaskByStatut(resp, this.task_status[i]);
                        var ids_event = this.takeAllEventId(bystatut);
                        var filtered_ids_event = ids_event.filter((v,k) => ids_event.indexOf(v) == k);
                        for(var j= 0; j < filtered_ids_event.length; j++){
                            this.task_managment[this.task_status[i].toLowerCase()].push(this.regroupTaskByEvent(this.regroupSpentByEvent(bystatut, filtered_ids_event[j])));
                        }
                    }
                }else{
                    this.resetValues();
                    this.$forceUpdate();
                    console.log('RIEN');
                }
                setTimeout(() =>{
                    this.initKanban();
                    this.display = true;
                    var vph = $(window).height();
                    vph = vph - 250;
                    $('.board-column-content').css({'min-height': vph + 'px'});
                },  3000);
                console.log(`RESPONSE TODO ====> ${JSON.stringify(this.task_managment.todo)}`);
                console.log(`RESPONSE INPROGRESS ====> ${JSON.stringify(this.task_managment.inprogress)}`);
            },
            setVisibility(bool){
                if(bool)
                    return "visibility: visible";
                else
                    return "visibility: hidden";
            },
            requestByDate: function(calendarDate){
                this.$refs.dialog_one.save(calendarDate);
                console.log(`this is the date one 1=====>${calendarDate}`);
                console.log(`this is the date one 2=====>${this.date_one}`);
                if(!this.switch_periode && this.row_assignation === "1"){
                    this.getNotificationTask(this.user_logged_in.id, calendarDate);
                }else if(!this.switch_periode && this.row_assignation === "2"){
                    this.getOtherNotificationTask(this.user_logged_in.id, calendarDate);
                }else if(this.switch_periode && this.row_assignation === "1"){
                    this.getRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                }else if(this.switch_periode && this.row_assignation === "2"){
                    this.getOtherRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                }
            },
            requestByRange: function(calendarDate){
                //switch periode always true
                this.$refs.dialog_two.save(calendarDate);
                console.log(`this is the date two 1=====>${calendarDate}`);
                console.log(`this is the date two 2=====>${this.date_two}`);
                if(this.row_assignation === "1"){
                    this.getRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                }else if(this.row_assignation === "2"){
                    this.getOtherRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                }
            },
            resetValues: function(){
                console.log('=======> RESET VALUE CALLED');
                this.task_managment.todo = [];
                this.task_managment.inprogress = [];
                this.task_managment.finished = [];
                this.task_managment.validated = [];
                this.task_managment.rejected = [];
                grid_ids = [];
            },
            transformToEventData: function(data){
                let hour = data.details.eventDebut.split(" ")[1];
                return {
                    title  : data.details.taskLabel,
                    start  : data.details.eventDebut.split(" ")[0] + 'T' + hour ,
                    end  : data.details.eventDebut.split(" ")[0] + 'T' + hour ,
                    ticketId: data.details.eventId,
                    subtaskLabel: data.details.subtaskLabel,
                    heureLabel: hour.substring(0, hour.length-3),
                    placeDescription: data.details.place_description,
                    eventStatut: this.getStatutLetter(data.details.eventStatut),
                    taskspents: data.taskspents,
                    startrange: data.details.eventDebut.split(" ")[0],
                    state: data.details.eventStatut,
                    assignedUser: data.details.assignedUser
                };
            },
            showKanbanDialog: function(eventObj){
                this.dialog_kanban = true;
                this.planning_event = eventObj;
                this.getCommentWs(eventObj.ticketId);
                this.getAttachmentWs(eventObj.ticketId);
                this.getHistoryWs(eventObj.ticketId);

                this.available_next_status = [];
                if(eventObj.state === '<?php echo CONSTANT_TASK_TODO ?>'){
                    this.available_next_status.push({
                        state: '<?php echo CONSTANT_TASK_INPROGRESS ?>',
                        stateLabel: this.getStatutLetter('<?php echo CONSTANT_TASK_INPROGRESS ?>')
                    });
                }else if(eventObj.state === '<?php echo CONSTANT_TASK_INPROGRESS ?>'){
                    this.available_next_status.push({
                        state: '<?php echo CONSTANT_TASK_FINISHED ?>',
                        stateLabel: this.getStatutLetter('<?php echo CONSTANT_TASK_FINISHED ?>')
                    });
                }else if(eventObj.state === '<?php echo CONSTANT_TASK_FINISHED ?>'){
                    if(this.user_logged_in.role === 'ADMINISTRATOR'){
                        this.available_next_status.push({
                            state: '<?php echo CONSTANT_TASK_VALIDATED ?>',
                            stateLabel: this.getStatutLetter('<?php echo CONSTANT_TASK_VALIDATED ?>')
                        });
                    }
                }
            },
            showAdd () {

                this.$router.push('/kanbandaytask');

            },
            addHistory(status, eventId){

                var formData = new FormData();
                formData.append('status_history', status);
                formData.append('event_id', eventId);
                formData.append('user_id', this.user_logged_in.id);
                axios.post(url_create_history_final, formData)
                    .then((response) => {
                        if(response.data.error){
                            console.log(`MESSAGE ERROR=>${response.data.msg}`);
                        }else{
                            console.log(`HISTORY INSERTED ID =>${response.data.inserted_id}`);

                        }
                    });

            },
            getCommentWs(eventId){
                var formData = new FormData();
                formData.append('id_event', eventId);
                axios.post(url_list_comment_final, formData).then((response) => {
                    if(response.data.comments){
                        this.list_comments = response.data.comments;
                        console.log(`RESPONSE COMMENT ====> ${JSON.stringify(response.data.comments)}`);
                    }else{
                        this.list_comments = [];
                        console.log('pas de commentaire');
                    }
                })
                .finally(() => {

                });
            },
            getAttachmentWs(eventId){
                var formData = new FormData();
                formData.append('id_event', eventId);
                axios.post(url_list_piece_final, formData).then((response) => {
                    if(response.data.pieces){
                        this.list_pieces = response.data.pieces;
                        console.log(`RESPONSE PIECES ====> ${JSON.stringify(response.data.pieces)}`);
                    }else{
                        this.list_pieces = [];
                        console.log('pas de pieces');
                    }
                })
                .finally(() => {

                });
            },
            addComment(eventId){
                if(this.newtextcomment){
                    var formData = new FormData();
                    formData.append('text_comment', this.newtextcomment);
                    formData.append('event_id', eventId);
                    formData.append('user_id', this.user_logged_in.id);

                    axios.post(url_create_comment_final, formData).then((response) => {
                        if(response.data.error){
                            console.log(`MESSAGE ERROR=>${response.data.msg}`);
                        }else{
                            console.log(`COMMENT INSERTED ID =>${response.data.inserted_id}`);
                            this.getCommentWs(eventId);
                            this.newtextcomment = null;
                        }
                    });
                }
            },
            getHistoryWs(eventId){
                var formData = new FormData();
                formData.append('id_event', eventId);
                axios.post(url_list_history_final, formData).then((response) => {
                    if(response.data.history){
                        this.list_histories = response.data.history;
                        console.log(`RESPONSE HISTORY ====> ${JSON.stringify(response.data.history)}`);
                    }else{
                        this.list_histories = [];
                        console.log('pas de HISTORY');
                    }
                })
                .finally(() => {

                });
            },
            downloadWithAxios(filename){
                let fileurl = `${this.urlBaseImage}uploads/pieces/${filename}`;
                console.log(fileurl);
                axios({
                    method: 'get',
                    url: fileurl,
                    responseType: 'arraybuffer'
                })
                .then(response => {

                    this.forceFileDownload(filename, response)

                })
                .catch(() => console.log('error occured'))
            },
            forceFileDownload(filename, response){
                const url = window.URL.createObjectURL(new Blob([response.data]))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', filename) //or any other extension
                document.body.appendChild(link)
                link.click()
            },
            pickFile () {
                this.$refs.addfile.click();

            },
            onFilePicked(event){

                if(typeof event.target.files[0] !== 'undefined'){
                    console.log(`CHANGED IMG=====>${URL.createObjectURL(event.target.files[0])}`);
                    var formData = new FormData();
                    formData.append('files[0]', event.target.files[0]);
                    formData.append('event_id', this.planning_event.ticketId);
                    formData.append('user_id', this.user_logged_in.id);
                    axios.post(url_create_piece_final, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        if(response.data.error){
                            console.log(`MESSAGE ERROR=>${response.data.msg}`);
                        }else{
                            console.log(`PIECE INSERTED ID =>${response.data.inserted_id}`);
                            this.getAttachmentWs(this.planning_event.ticketId);
                        }
                    });

                }

            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`KANBAN PAGE USER LOGGED  ===> ${JSON.stringify(connexion.user_logged[0])}`);

                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }
                    var date = new Date();
                    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    var formatedFirst = this.formatDateToYYYYMMdd(firstDay);
                    var formatedLast = this.formatDateToYYYYMMdd(lastDay);
                    this.date_one = formatedFirst;
                    this.date_two = formatedLast;
                    //console.log(`daty first : ${formatedFirst}`);
                    //console.log(`daty last : ${formatedLast}`);
                    //this.getNotificationTask(this.user_logged_in.id, this.date_two);
                    this.getRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);

                }else{
                    console.log("KANBAN PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            },

        },
        mounted: function () {
            console.log('kanban : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
            var dateOffset = (24*60*60*1000) * 1; //1 day

            var query_date = this.$route.query.date;
            if(query_date){
                console.log(`QUERY DATE ===> ${query_date}`);
                var date_one_obj = this.parseFrenchDate(query_date);
                this.date_one = this.formatDateToYYYYMMdd(date_one_obj);
                console.log(`QUERY DATE TRANSFORMED ===> ${date_one_obj}`);
                console.log(`QUERY DATE toISOString ===> ${this.formatDateToYYYYMMdd(date_one_obj)}`);
                //date_one_obj.setTime(date_one_obj.getTime() + dateOffset);
                //this.date_two = date_one_obj.toISOString().substr(0, 10);

            }else{
                console.log('NO QUERY DATE');
                var myDate = new Date();
                myDate.setTime(myDate.getTime() - dateOffset);
                this.date_one = myDate.toISOString().substr(0, 10);
                console.log(`NO QUERY DATE TRANSFORMED ===> ${this.date_one}`);
            }
        },
        created: function (){
            /*setTimeout(() =>{
                this.initKanban();
                this.display = true;
            },  3800);*/
            this.$router.app.$on('updateKanbanDate', (query_date) => {
                console.log("VOAANTSO");
                var date_one_obj = this.parseFrenchDate(query_date);
                this.switch_periode = false;
                this.row_assignation = "1";
                this.date_one = this.formatDateToYYYYMMdd(date_one_obj);
                this.getNotificationTask(this.user_logged_in.id, this.date_one);
                //this.getRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
            });
        },
        watch: {
            row_assignation (val) {
                console.log(`ASSIGNATION CHANGED====>${val}`);
                if(!this.switch_periode){
                    if(val === "1"){
                        this.drag_enable = true;
                        this.getNotificationTask(this.user_logged_in.id, this.date_one);
                    }else if(val === "2"){
                        this.drag_enable = false;
                        this.getOtherNotificationTask(this.user_logged_in.id, this.date_one);
                    }
                }else if(this.switch_periode){
                    if(val === "1"){
                        this.drag_enable = true;
                        this.getRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                    }else if(val === "2"){
                        this.drag_enable = false;
                        this.getOtherRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                    }
                }
            },
            switch_periode (val) {
                console.log(`SWITCH PERIODE CHANGED====>${val}`);
                if(this.row_assignation === "1"){
                    this.drag_enable = true;
                    if(val){
                        this.getRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                    }else{
                        this.getNotificationTask(this.user_logged_in.id, this.date_one);
                    }
                }else if(this.row_assignation === "2"){
                    this.drag_enable = false;
                    if(val){
                        this.getOtherRangeNotificationTask(this.user_logged_in.id, this.date_one, this.date_two);
                    }else{
                        this.getOtherNotificationTask(this.user_logged_in.id, this.date_one);
                    }
                }
            }
        },
        metaInfo: {
            script: [
                { src: '<?php echo base_url() ?>assets/js/kanban/hammer.min.js' },
                { src: '<?php echo base_url() ?>assets/js/kanban/muuri.min.js' },
                { src: '<?php echo base_url() ?>assets/js/kanban/web-animations.min.js' },
            ]
        }

    });

    Vue.component('accordion-kanban', {
        props: ['theme'],
        template: '#template-accordion-kanban',
        data() {
            return {
                show: false,
            };
        },

        methods: {
            toggle: function() {
                this.show = !this.show;
            },
            // enter: function(el, done) {
            //   $(el).slideDown(150, done);
            // },
            // leave: function(el, done) {
            //   $(el).slideUp(150, done);
            // },
            beforeEnter: function(el) {
                el.style.height = '0';
            },
            enter: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            beforeLeave: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            leave: function(el) {
                el.style.height = '0';
            },

        },
        created: function () {

        }
    });

</script>