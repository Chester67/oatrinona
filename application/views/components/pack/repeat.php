<?php
$this->load->view('components/inputs/expense_category');
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

.accordion {

  margin-top: 20px;
  margin-bottom: 20px;
  margin-left: 25px;
  margin-right: 25px;

}

.accordion .header {
  height: 55px;
  line-height: 55px;
  vertical-align: middle;
  padding-left: 15px;
  position: relative;
  color: #455A64;
  cursor: pointer;
  font-size: 15sp;
  font-weight: bold;
}

.accordion .header-icon {
  position: absolute;
  top: 10px;
  right: 8px;
  transform: rotate(0deg);
  transition-duration: 0.3s;
}

.accordion .body {
/*   display: none; */
  overflow: hidden;
  background-color: #fff;
  transition: 150ms ease-out;
}

.accordion .body-inner {
  padding: 8px;
  overflow-wrap: break-word;
/*   white-space: pre-wrap; */
}

.accordion .header-icon.rotate {
  transform: rotate(180deg);
  transition-duration: 0.3s;
}

.accordion.purple {
  background-color: #000000;
}

.shadowed-part {
    box-shadow: 0 3px 3px 0 rgba(0, 0, 0, 0.2), 0 3px 3px 0 rgba(0, 0, 0, 0.19);
}

.marquee {
    width: 250px;
    margin: 0 auto;
    overflow: hidden;
    white-space: nowrap;
    animation: marquee 10s linear infinite;
    -webkit-animation:linear marquee 10s infinite ;
    -moz-animation:linear marquee 10s infinite ;
    -o-animation:linear marquee 10s infinite ;
    -ms-animation:linear marquee 10s infinite ;
    animation:linear marquee 10s infinite ;
}

.marquee:hover {
    animation-play-state: paused
}

/* Make it move */
@keyframes marquee {
    0%   { text-indent: 3em }
    100% { text-indent: -9em }
}

.repeat-accordion .header-icon {
    display: none;
}

</style>

<script type="text/x-template" id="template-expense-repeat">

    <div>
    <div v-if="!data_no_user" style="padding-top: 12px; text-align: center; font-weight: bold; font-size: 20px; color: #757575;">{{ getMonthByInt(getMonthOfDate(date)) }} {{ getYearOfDate(date) }}</div>
        <accordion-repeat v-if="!data_no_user" theme="grey lighten-1" :updateui="update_ui_monthly" class="shadowed-part repeat-accordion" :bus="bus" ref="refresh_month">
            <div slot="header">
                Dépenses mensuelles
                <div style="position: absolute; right: 45px; top: 0;" class="text-xs-center">
                    <v-btn class="hidden-sm-and-down" color="grey darken-1" @click.stop.prevent="distinctAction('month')"><span style="color: transparent">enregistrer</span></v-btn>
                    <span class="hidden-sm-and-down" @click.stop.prevent="distinctAction('month')" style="position: absolute; right: 7px; top: 2px; left: 7px; color: white">ENREGISTRER</span>
                </div>

            </div>
            <div>
                <v-layout row wrap style="margin-left: 12px; margin-right: 12px; margin-top: 8px; padding: 15px">
                    <v-flex xs12 v-for="(category_item, i) in category_expenses_monthly" :key="i">
                        <expense-category
                            :key="category_item.category.id"
                            :index="i"
                            :selected_category="category_item.category"
                            :type_day="'repeat_monthly'"
                            :item_subcategories="category_item.subcategories"
                            :last_spent_id="last_spent_id"
                            @update_entire_category="from_child_update_entire_category_monthly"
                            @delete_entire_category="from_child_delete_entire_category_monthly"
                            @update_one_subcategory="from_child_update_one_subcategory_monthly"
                            @increment_last_spent_id="from_child_increment_last_spent_id"
                            @remove_one_subcategory="from_child_remove_one_subcategory_monthly">
                        </expense-category>
                    </v-flex>
                    <div @click="showDialog('month')" style="display: flex; flex-direction: row; height: 42px; margin-right: 7px; border-top: 6px solid transparent; cursor: pointer; margin-bottom: 10px; max-width: 250px">
                        <div class="button-icon-add">
                            <v-icon>add</v-icon>
                        </div>
                        <div style="display: flex; align-items: center; margin-left: 8px">
                            Ajouter une nouvelle dépense
                        </div>
                    </div>
                </v-layout>
            </div>
        </accordion-repeat>

        <accordion-repeat v-if="!data_no_user" theme="grey lighten-1" :updateui="update_ui_weekly" class="shadowed-part repeat-accordion" :bus="bus" ref="refresh_week">
            <div slot="header">
                Dépenses hebdomadaires
                <div style="position: absolute; right: 45px; top: 0;" class="text-xs-center">
                    <v-btn class="hidden-sm-and-down" color="grey darken-1" @click.stop.prevent="distinctAction('week')"><span style="color: transparent">enregistrer</span></v-btn>
                    <span class="hidden-sm-and-down" @click.stop.prevent="distinctAction('week')" style="position: absolute; right: 7px; top: 2px; left: 7px; color: white">ENREGISTRER</span>
                </div>
            </div>
            <div>
                <v-layout row wrap style="margin-left: 12px; margin-right: 12px; margin-top: 8px; padding: 15px">
                    <v-flex xs12 v-for="(category_item, i) in category_expenses_weekly" :key="i">
                        <expense-category
                            :key="category_item.category.id"
                            :index="i"
                            :selected_category="category_item.category"
                            :type_day="'repeat_monthly'"
                            :item_subcategories="category_item.subcategories"
                            :last_spent_id="last_spent_id"
                            @update_entire_category="from_child_update_entire_category_weekly"
                            @delete_entire_category="from_child_delete_entire_category_weekly"
                            @update_one_subcategory="from_child_update_one_subcategory_weekly"
                            @increment_last_spent_id="from_child_increment_last_spent_id"
                            @remove_one_subcategory="from_child_remove_one_subcategory_weekly">
                        </expense-category>
                    </v-flex>
                    <div @click="showDialog('week')" style="display: flex; flex-direction: row; height: 42px; margin-right: 7px; border-top: 6px solid transparent; cursor: pointer; margin-bottom: 10px; max-width: 250px">
                        <div class="button-icon-add">
                            <v-icon>add</v-icon>
                        </div>
                        <div style="display: flex; align-items: center; margin-left: 8px">
                            Ajouter une nouvelle dépense
                        </div>
                    </div>
                </v-layout>
            </div>
        </accordion-repeat>

        <v-dialog v-model="dialog" width="600px">

            <v-layout row wrap style="background: #f2f3f4">

                <v-flex xs12 style="margin-top: 15px">
                    <v-container fluid grid-list-md>
                        <v-data-iterator
                            :items="category_items"
                            :pagination.sync="pagination"
                            content-tag="v-layout"
                            row
                            wrap
                            :hide-actions="true"
                            :total-items="category_items.length">
                            <v-flex
                                slot="item"
                                slot-scope="props"
                                xs12
                                sm6
                                md4
                                lg3>

                                <v-card style="cursor: pointer;" @click.native="chooseCategory(props.item)">

                                    <v-layout justify-center>
                                        <img :src="urlBaseImage+'uploads/categories/'+props.item.categoryImage" alt="" style="margin-top: 6px;width:42%;height:42%;object-fit:contain">

                                    </v-layout>

                                    <v-card-title>
                                        <template v-if="countCharOK(props.item.categoryLabel)">
                                            <p style="font-weight: bold; font-size: 13px; width: 250px; margin: 0 auto; overflow: hidden; white-space: nowrap; text-align: center">{{ props.item.categoryLabel }}</p>
                                        </template>
                                        <template v-else>
                                            <p style="font-weight: bold; font-size: 13px" class="marquee">{{ props.item.categoryLabel }}</p>
                                        </template>
                                    </v-card-title>

                                </v-card>
                            </v-flex>
                        </v-data-iterator>
                    </v-container>
                </v-flex>
            </v-layout>

        </v-dialog>

        <v-dialog v-model="dialog_confirm" persistent max-width="290">
            <v-card>
                <v-card-title>{{ dialog_confirm_message }}</v-card-title>
                <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn color="green darken-1" flat @click="dialog_confirm = false">OK</v-btn>
                </v-card-actions>
            </v-card>
        </v-dialog>
        <data-no-user v-if="data_no_user"></data-no-user>

    </div>

</script>

<template id="template-accordion-repeat">
    <div class="accordion" v-bind:class="theme">
        <div class="header" @click="toggle">
            <slot name="header">HINT</slot>
            <!--<i class="fa fa-2x fa-angle-down header-icon" v-bind:class="{ rotate: show }"></i>-->
            <v-icon class="header-icon" style="font-size: 33px;" v-bind:class="{ rotate: show }">keyboard_arrow_down</v-icon>
        </div>
        <transition name="accordion"
            v-on:before-enter="beforeEnter" v-on:enter="enter"
            v-on:before-leave="beforeLeave" v-on:leave="leave">
            <div class="body" v-show="show">
                <div class="body-inner">
                <slot></slot>
                </div>
            </div>
        </transition>
    </div>
</template>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_list_category = '<?php echo URL_LIST_CATEGORY ?>';
    var url_list_category_final = `${url_base}${url_list_category}`;

    var url_create_spent = '<?php echo URL_SAVE_SPENT ?>';
    var url_create_spent_final = `${url_base}${url_create_spent}`;

    var url_list_spent_repeat = '<?php echo URL_LIST_SPENT_REPEAT ?>';
    var url_list_spent_repeat_final = `${url_base}${url_list_spent_repeat}`;

    var url_update_spent = '<?php echo URL_UPDATE_SPENT ?>';
    var url_update_spent_final = `${url_base}${url_update_spent}`;

    Vue.component('expense-repeat', {
        template: '#template-expense-repeat',
        mixins: [common_mixin],
        data: function () {

            return{
                category_expenses_monthly: [],
                category_expenses_weekly: [],

                category_items: [],
                dialog: false,
                pagination: {
                    rowsPerPage: 12
                },
                urlBaseImage: url_base,
                user_logged_in: undefined,
                user_choosed_by_admin: undefined,

                noduplicates_monthly: [],
                noduplicates_weekly: [],

                last_spent_id: 0,

                update_ui_monthly: false,
                update_ui_weekly: false,

                expenses_updates_monthly: [],
                expenses_updates_weekly: [],

                expenses_month_state: undefined,
                expenses_week_state: undefined,

                actual_iteration: undefined,

                dialog_confirm: false,
                dialog_confirm_message: '',

                date: new Date().toISOString().substr(0, 7),

                bus: new Vue({}),
            }

        },
        methods: {
            showDialog: function(it){
                this.dialog = true;
                this.actual_iteration = it;
            },
            wsCategory(){
                axios.get(url_list_category_final).then((response) => {
                    if(response.data.category == null){
                        //v.noResult()
                    }else{

                        this.pagination.rowsPerPage = response.data.category.length;
                        this.category_items = response.data.category;
                        //console.log(`CATEGORIES ARE ====>${JSON.stringify(response.data.category)}`)
                    }
                });
            },
            getCategoryByParam: function(cat, subcats){
                return {category: cat, subcategories: subcats};
            },
            checkDuplicatedCategory: function(cat_item, arr){

                for(var i= 0; i < arr.length; i++){
                    console.log(`ITEM=======>${arr[i]}`);
                    if(arr[i] == cat_item.id){
                        console.log("ENTER IN");
                        return true;
                    }
                }
                return false;

            },
            from_child_increment_last_spent_id: function(){
                this.last_spent_id += 1;
            },
            //-----------------------monthly---------------------
            from_child_update_entire_category_monthly: function(type_day, entire_cat){
                for (var i = this.category_expenses_monthly.length; i--;) {
                    console.log(`from_child_update_entire_category_monthly ====>${i} entire_cat.category_id===>${entire_cat.category_id} category_expenses_monthly====> ${this.category_expenses_monthly[i].category.id}`);
                    if(this.category_expenses_monthly[i].category.id == entire_cat.category_id){
                        this.category_expenses_monthly[i].subcategories = entire_cat.subcategories;
                    }
                }
            },
            from_child_delete_entire_category_monthly: function(type_day, selected_category, index){

                this.spliceByCategoryMonthly(this.category_expenses_monthly, selected_category, index, type_day);
                this.removeFromDuplicate(this.noduplicates_monthly, selected_category.id);

            },
            from_child_update_one_subcategory_monthly: function(type_day, subcat, action_type){

                //console.log(`=====>LISTING MODIFY OBJECT : ${JSON.stringify(subcat)}`);
                subcat.action = action_type;
                if(this.expenses_updates_monthly.length > 0){

                    let checked = this.checkSubcategory(subcat, this.expenses_updates_monthly);
                    if(checked.exist){
                        this.expenses_updates_monthly[checked.index] = subcat;
                    }else{
                        this.expenses_updates_monthly.push(subcat);
                    }

                }else{
                    this.expenses_updates_monthly.push(subcat);
                }
                //console.log(`EXPENSE updates=====> ${JSON.stringify(this.expenses_updates_monthly)}`);
            },
            from_child_remove_one_subcategory_monthly: function(type_day, subcat){

                this.ejectupdates(this.expenses_updates_monthly, subcat);
                //console.log(`expenses_updates_monthly=====> ${JSON.stringify(this.expenses_updates_monthly)}`);

            },
            spliceByCategoryMonthly: function(arr, cat, index, type_day) {

                let subcats = this.getSubcategoriesByCategory(arr, cat);
                console.log(`spliceByCategoryMonthly subcats====>${JSON.stringify(subcats)}`);
                subcats.forEach((subcat) => {
                    this.from_child_update_one_subcategory_monthly(type_day, subcat, '<?php echo CONSTANT_DB_REMOVE ?>');
                })
                arr.splice(index, 1)

            },
            chooseCategoryMonthly: function(cat_item){

                console.log(`LIST duplicate =======>${this.noduplicates_monthly}`);
                if(!this.checkDuplicatedCategory(cat_item, this.noduplicates_monthly)){
                    console.log('tafa0');
                    this.noduplicates_monthly.push(cat_item.id);
                    this.addCategoryMonthly(cat_item);
                }else{
                    console.log("already exist");
                }

                this.dialog = false;
            },
            addCategoryMonthly: function(cat_item){

                console.log('tafa1')
                this.bus.$emit('refresh');
                this.category_expenses_monthly.push(this.getCategoryByParam(cat_item, []));
                this.update_ui_monthly = !this.update_ui_monthly;
                this.$forceUpdate();
                console.log(`log=====>${this.update_ui_monthly}`);

            },
            //-------------------------weekly--------------------------
            from_child_update_entire_category_weekly: function(type_day, entire_cat){
                for (var i = this.category_expenses_weekly.length; i--;) {
                    console.log(`from_child_update_entire_category_weekly ====>${i} entire_cat.category_id===>${entire_cat.category_id} category_expenses_weekly====> ${this.category_expenses_weekly[i].category.id}`);
                    if(this.category_expenses_weekly[i].category.id == entire_cat.category_id){
                        this.category_expenses_weekly[i].subcategories = entire_cat.subcategories;
                    }
                }
            },
            from_child_delete_entire_category_weekly: function(type_day, selected_category, index){

                this.spliceByCategoryWeekly(this.category_expenses_weekly, selected_category, index, type_day);
                this.removeFromDuplicate(this.noduplicates_weekly, selected_category.id);

            },
            from_child_update_one_subcategory_weekly: function(type_day, subcat, action_type){

                subcat.action = action_type;
                if(this.expenses_updates_weekly.length > 0){

                    let checked = this.checkSubcategory(subcat, this.expenses_updates_weekly);
                    if(checked.exist){
                        this.expenses_updates_weekly[checked.index] = subcat;
                    }else{
                        this.expenses_updates_weekly.push(subcat);
                    }

                }else{
                    this.expenses_updates_weekly.push(subcat);
                }

            },
            from_child_remove_one_subcategory_weekly: function(type_day, subcat){

                this.ejectupdates(this.expenses_updates_weekly, subcat);

            },
            spliceByCategoryWeekly: function(arr, cat, index, type_day) {

                let subcats = this.getSubcategoriesByCategory(arr, cat);
                console.log(`spliceByCategoryWeekly subcats====>${JSON.stringify(subcats)}`);
                subcats.forEach((subcat) => {
                    this.from_child_update_one_subcategory_weekly(type_day, subcat, '<?php echo CONSTANT_DB_REMOVE ?>');
                })
                arr.splice(index, 1)

            },
            chooseCategoryWeekly: function(cat_item){

                console.log(`LIST duplicate =======>${this.noduplicates_weekly}`);
                if(!this.checkDuplicatedCategory(cat_item, this.noduplicates_weekly)){
                    this.noduplicates_weekly.push(cat_item.id);
                    this.addCategoryWeekly(cat_item);
                }else{
                    console.log("already exist");
                }

                this.dialog = false;
            },
            addCategoryWeekly: function(cat_item){

                this.category_expenses_weekly.push(this.getCategoryByParam(cat_item, []));
                this.update_ui_weekly = !this.update_ui_weekly;
                this.$forceUpdate();
            },
            //------------------------common---------------------------
            chooseCategory: function(cat_item){
                if(this.actual_iteration === 'month'){
                    this.chooseCategoryMonthly(cat_item);
                    console.log('month')
                }else if(this.actual_iteration === 'week'){
                    this.chooseCategoryWeekly(cat_item);
                    console.log('week')
                }
            },
            getSubcategoriesByCategory: function(arr, cat){
                var res = [];
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].category.id === cat.id){
                        res = arr[i].subcategories;
                    }
                }
                return res;
            },
            removeFromDuplicate: function(array, item){
                var index = array.indexOf(item);
                if (index !== -1)
                    array.splice(index, 1);
            },
            checkSubcategory: function(subcat_item, arr){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].spent_id === subcat_item.spent_id){
                        return {exist: true, index: i};
                    }
                }
                return {exist: false, index: -1};
            },
            ejectupdates: function(arr, item){
                for(var i= 0; i < arr.length; i++){
                    if(arr[i].spent_id == item.spent_id){
                        arr.splice(i, 1);
                    }
                }
            },
            formatJson: function(array_exp, day_type) {

                var res_array = [];
                array_exp.forEach((cat) => {
                    cat.subcategories.forEach((subcat) => {
                        res_array.push({unitPrice: subcat.price, number: subcat.number, subcategoryId: subcat.subcategory, userId: this.user_logged_in.id, dayType: day_type, contextId: subcat.utility.id });
                    })
                })

                return res_array;

            },
            save: function(iteration) {
                this.data_loader = true;
                var allowPost = false;
                var formData = new FormData();
                if(iteration === "month"){
                    console.log("SAVE month");
                    if(this.category_expenses_monthly.length > 0){
                        console.log(`category_expenses_monthly====>${JSON.stringify(this.category_expenses_monthly)}`);
                        formData.append('spents', JSON.stringify(this.formatJson(this.category_expenses_monthly, "monthly")));
                        allowPost = true;
                    }
                }else if(iteration === "week"){
                    console.log("SAVE week");
                    if(this.category_expenses_weekly.length > 0){
                        console.log(`category_expenses_weekly====>${JSON.stringify(this.category_expenses_weekly)}`);
                        formData.append('spents', JSON.stringify(this.formatJson(this.category_expenses_weekly, "weekly")));
                        allowPost = true;
                    }
                }

                if(allowPost){
                    axios.post(url_create_spent_final, formData).then((response) =>{
                        console.log(`DATA SENT here====>${JSON.stringify(response.data.spents)}`);

                        if(response.data.error){
                            alert("ERROR WHEN SAVING");
                        }else{
                            //this.wsDepense();
                            this.dialog_confirm_message = 'Ajouté aves succès !';
                            this.dialog_confirm = true;
                        }
                    });
                }else{
                    console.log("NOTHING TO SAVE OR ADD");
                    this.data_loader = false;
                }

            },
            wsDepense: function() {

                this.data_loader = true;
                //this.resetAllExpensesData();
                let today = new Date().toISOString().slice(0, 10);
                console.log(`today ====> ${today}`)
                var formData = new FormData();
                formData.append('user_id', this.user_logged_in.id);
                formData.append('spent_month', this.getMonthOfDate(today));
                formData.append('spent_year', this.getYearOfDate(today));
                window.axios.post(url_list_spent_repeat_final, formData).then((response) => {
                    if(response.data.spents == null){
                        console.log("response.data.spents ==> null");
                    }else{
                        console.log(`SPENTS LIST REPEAT=====>${JSON.stringify(response.data.spents)}`)
                        //-----------------------MONTH LIST------------------------------
                        let by_typeday_month = this.regroupSpentByTypeDay(response.data.spents, 'monthly');
                        var ids_month = this.takeAllCatId(by_typeday_month);
                        var filtered_month = ids_month.filter((v,i) => ids_month.indexOf(v) == i);
                        if(filtered_month && filtered_month.length > 0){
                            for(var i= 0; i < filtered_month.length; i++){
                                this.category_expenses_monthly.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_month, filtered_month[i])));
                                this.noduplicates_monthly.push(filtered_month[i]);
                            }
                        }
                        //---------------------WEEK LIST----------------------------------
                        let by_typeday_week = this.regroupSpentByTypeDay(response.data.spents, 'weekly');
                        var ids_week = this.takeAllCatId(by_typeday_week);
                        var filtered_week = ids_week.filter((v,i) => ids_week.indexOf(v) == i);
                        if(filtered_week && filtered_week.length > 0){
                            for(var i= 0; i < filtered_week.length; i++){
                                this.category_expenses_weekly.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(by_typeday_week, filtered_week[i])));
                                this.noduplicates_weekly.push(filtered_week[i]);
                            }
                        }
                        //---------------------CHECK IF ADD OR UPDATE--------------------------
                        if(this.category_expenses_monthly.length > 0){
                            this.expenses_month_state = '<?php echo CONSTANT_LISTING ?>';
                        }else{
                            this.expenses_month_state = '<?php echo CONSTANT_ADDING ?>';
                        }

                        if(this.category_expenses_weekly.length > 0){
                            this.expenses_week_state = '<?php echo CONSTANT_LISTING ?>';
                        }else{
                            this.expenses_week_state = '<?php echo CONSTANT_ADDING ?>';
                        }
                    }
                })
                .catch(err => {
                    console.log(err)
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            update: function(iteration) {
                this.data_loader = true;
                var allowPost = false;
                var formData = new FormData();
                if(iteration === "month"){
                    console.log("UPDATE MONTH");
                    if(this.expenses_updates_monthly.length > 0){
                        console.log(`UPDATE MONTH =====> ${JSON.stringify(this.getAllRequest(this.expenses_updates_monthly, 'monthly'))}`);
                        formData.append("request", this.getAllRequest(this.expenses_updates_monthly, 'monthly'));
                        allowPost = true;
                    }
                }else if(iteration === "week"){
                    console.log("UPDATE WEEK");
                    if(this.expenses_updates_weekly.length > 0){
                        console.log(`UPDATE WEEK =====> ${JSON.stringify(this.getAllRequest(this.expenses_updates_weekly, 'weekly'))}`);
                        formData.append("request", this.getAllRequest(this.expenses_updates_weekly, 'weekly'));
                        allowPost = true;
                    }
                }

                if(allowPost){
                    axios.post(url_update_spent_final, formData).then((response) =>{
                        console.log(`DATA UPDATE MESSAGE here====>${JSON.stringify(response.data.sqls)}`);
                        console.log(`DATA UPDATE NOMBRE here====>${JSON.stringify(response.data.nb)}`);

                        if(response.data.error){
                            alert("ERROR WHEN UPDATING");
                        }else{
                            //this.wsDepense();
                            this.dialog_confirm_message = 'Modifié aves succès !';
                            this.dialog_confirm = true;
                        }
                    });
                }else{
                    console.log("NOTHING TO UPDATE");
                    this.data_loader = false;
                }

            },
            transformActionToRequest: function(subcat, day_type){
                var req = undefined;
                let action_type = subcat.action;
                if(action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    req = `UPDATE spent SET number = ${subcat.number}, unitPrice = ${subcat.price}, subcategoryId = ${subcat.subcategory}, contextId = ${subcat.utility.id} WHERE id = ${subcat.spent_id};`;
                }else if(action_type === '<?php echo CONSTANT_DB_REMOVE ?>'){
                    req = `DELETE FROM spent WHERE id = ${subcat.spent_id};`;
                }else if(action_type === '<?php echo CONSTANT_DB_INSERT ?>'){
                    req = `INSERT INTO spent (unitPrice, number, subcategoryId, userId, dayType, contextId) VALUES (${subcat.price}, ${subcat.number}, ${subcat.subcategory}, ${this.user_logged_in.id}, '${day_type}', ${subcat.utility.id});`;
                }
                return req;
            },
            getAllRequest: function(arr, day_type){
                var concatenated = "";
                for(var i= 0; i < arr.length; i++){
                    concatenated += this.transformActionToRequest(arr[i], day_type);
                }
                return concatenated;
            },
            distinctAction: function(iteration) {

                if(iteration === "month"){

                    if(this.expenses_month_state === '<?php echo CONSTANT_LISTING ?>'){
                        this.update('month');
                    }else{
                        this.save('month');
                    }

                }else if(iteration === "week"){

                    if(this.expenses_week_state === '<?php echo CONSTANT_LISTING ?>'){
                        this.update('week');
                    }else{
                        this.save('week')
                    }

                }

            },
            countCharOK(str){
                if(str && str.length > 17)
                    return false;
                return true;
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`REPEAT PAGE USER LOGGED  ===> ${JSON.stringify(connexion.user_logged[0])}`);

                    this.user_logged_in = connexion.user_logged[0];
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_choosed_by_admin = connexion.user_choosed[0];
                    }
                    this.wsCategory();
                    this.wsDepense();

                }else{
                    console.log("REPEAT PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            },

        },
        mounted: function () {
            console.log('repeat : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },


    });

    Vue.component('accordion-repeat', {
        props: ['theme', 'updateui', 'bus'],
        template: '#template-accordion-repeat',
        data() {
            return {
                show: true,
            };
        },

        methods: {
            toggle: function() {
                //this.show = !this.show;
            },
            // enter: function(el, done) {
            //   $(el).slideDown(150, done);
            // },
            // leave: function(el, done) {
            //   $(el).slideUp(150, done);
            // },
            beforeEnter: function(el) {
                el.style.height = '0';
            },
            enter: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            beforeLeave: function(el) {
                el.style.height = el.scrollHeight + 'px';
            },
            leave: function(el) {
                el.style.height = '0';
            },
            refresh: function() {
                console.log('arrived in REFRESH');
                this.toggle();
                setTimeout(() =>{
                    this.toggle();
                },  500);
            },
            trying() {
                console.log('arrived in REFRESH');
            }
        },
        watch: {
            updateui (val) {
                console.log(`VAL====>${val}`);
                this.toggle();
                setTimeout(() =>{
                    this.toggle();
                },  500);

            },
        },
        created: function () {
            this.bus.$on('refresh', this.trying);
        }
    });


</script>