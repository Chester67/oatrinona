<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('event_model','event');
    }

	public function createEvent(){
        $result['error'] = true;
		$result['msg'] ='Error when adding event';
        $event = array(
            'eventDebut' => $this->input->post('event_debut'),
            //'eventFin' => $this->input->post('event_fin'),
            'eventStatut' => $this->input->post('event_statut'),
            'eventConsulted' => $this->input->post('event_consulted'),
            'placeId' => $this->input->post('place_id'),
            'subtaskId' => $this->input->post('subtask_id'),
            'assignedUserId' => $this->input->post('assigned_user_id'),
        );
        $id_inserted = $this->event->createEvent($event); 
		if($id_inserted != 0){
            $result['error'] = false;
            $result['inserted_id'] = $id_inserted;
			$result['msg'] ='Event added successfully';
		}
		echo json_encode($result);
	}
    
    public function deleteEvent(){
        $result['error'] = true;
        $result['deleted'] = false;
        $id = $this->input->post('event_id');
        $query = $this->event->deleteEvent($id);
        if($query){
            $result['error'] = false;
            $result['deleted'] = true;
		}
        echo json_encode($result);
    }

    public function updateEventStatut(){
        $result['error'] = true;
		$result['msg'] = "failed when update event";

		$id = $this->input->post('id_event');
		$statut = $this->input->post('statut_event');
		$data = array(
			'eventStatut' => $statut,
        );
        $query = $this->event->updateEventStatut($data, $id);
        if($query){
            $result['error'] = false;
            $result['msg'] = "success update event";
		}
        echo json_encode($result);
    }
}
