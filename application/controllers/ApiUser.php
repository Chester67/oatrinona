<?php

require(APPPATH.'/libraries/REST_Controller.php');
require(APPPATH.'/util/WsResponse.php');
require(APPPATH.'/util/UtilDateTime.php');


class ApiUser extends REST_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('user_model','user');
    }

    public function index_get()
    {
        // Display all books
    }

    public function index_post()
    {
        // Create a new book
    }

    public function users_post(){
        $adminId = 4;
        $user_id_url = $this->post('user');
        $token_url = $this->query('token');
        $query_result = $this->user->checkCredential($user_id_url, $token_url);
        if($query_result){
            $users_result = $this->user->showByAdmin($adminId);
            $this->response($users_result);
        }else{
            $ws_response = new WsResponse();
            $this->response($ws_response->getError("token invalide !", -1));
        }
        
    }

}