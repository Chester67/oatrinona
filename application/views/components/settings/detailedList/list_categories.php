<?php

?>

<style type="text/css">
    hr {
        background: #e0d9d9;
        height: 1px;
        border: 0;
        border-top: 1px solid #e0d9d9;
        margin: 1em 0;
        padding: 0;
    }
</style>

<script type="text/x-template" id="template-setting-category-list">

    <v-container fluid grid-list-md>
        <v-data-iterator
            :items="getListAfterRegroup()"
            :rows-per-page-items="rowsPerPageItems"
            :pagination.sync="pagination"
            content-tag="v-layout"
            row
            wrap>
            <v-flex
                slot="item"
                slot-scope="props"
                xs12
                sm6
                md4
                lg3>
                <v-card style="background:#f5f5f5">
                    <v-card-title style="color: #546E7A"><h4>{{ props.item.categoryLabel }}</h4></v-card-title>
                    <!--<v-divider></v-divider>-->
                    <v-list dense style="background:#eceff1">
                        <template v-if="props.item.subcategories.length > 0">
                            <v-list-tile :key="subcat.id" v-for="subcat in props.item.subcategories">
                                <v-list-tile-content>- {{ subcat.subcategoryLabel }}</v-list-tile-content>    
                            </v-list-tile>
                        </template>
                        <template v-else>
                            <div style="height: 40px; width: 100%; display: flex; align-items: center; margin-left: 15px">Pas de donnée</div>
                        </template>
                        
                    </v-list>
                </v-card>
            </v-flex>
        </v-data-iterator>
    </v-container>

</script>

<script type="text/javascript">

    Vue.component('setting-category-list', {
        template: '#template-setting-category-list',
        props: {
            subcategories: {type: Array, required: true},
            categories: {type: Array, required: true}
        },
        data: function () {
            
            return{
                rowsPerPageItems: [6],
                pagination: {
                    rowsPerPage: 6
                },
                
            }
            
        },
        methods: {
           getListAfterRegroup: function() {
                var res = [];
                for(var i= 0; i < this.categories.length; i++){
                    var obj = this.categories[i];
                    obj.subcategories = this.regroupByCategory(this.categories[i].id);
                    res.push(obj);
                }
                return res;
            
           },
           regroupByCategory(catId){
                var res = [];
                for(var i= 0; i < this.subcategories.length; i++){
                    if(this.subcategories[i].categoryId === catId){
                        res.push(this.subcategories[i]);
                    }
                }
                return res;
           }
           
        },
        mounted: function () {
            
        },
        created: function () {
            var arr_rowsPerPageItems = [];
            arr_rowsPerPageItems.push(this.categories.length);
            this.rowsPerPageItems = arr_rowsPerPageItems;
            this.pagination.rowsPerPage = this.categories.length;
            //console.log(`FUNCTION ????? ====>${JSON.stringify(this.getListAfterRegroup())}`)
        }
        
    });

</script>