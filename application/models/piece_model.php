<?php

class Piece_model extends CI_Model
{

    public function createPiece($data)
    {
        $this->db->insert('piece', $data);
        if ($this->db->affected_rows() > 0) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
        return 0;
    }

    public function showPieceByEvent($event_id)
    {
        $this->db->select('piece.id, piece.namepiece, piece.datepiece, piece.visibilitypiece, piece.eventId, users.first_name, users.last_name');
        $this->db->from('piece');
        $this->db->where('eventId', $event_id);
        $this->db->join('users', 'users.id = piece.userId');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
