<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data_to_send['name'] = 'this is an try 1';
		$this->load->view('welcome', $data_to_send);
	}

	public function message(){
		$this->load->view('welcome_message');
	}

	public function saveLastMenu(){
		$result['saved']  = false;
		$lastMenuId = $this->input->post('menu_id');
		$this->session->set_userdata('last_menu', $lastMenuId);
		$result['saved']  = true;	
		echo json_encode($result);	
	}

	public function getLastMenu(){
		$result['error']  = true;
		$menu_id = $this->session->userdata("last_menu");
		if($menu_id)
			$result['menu'] = $menu_id;
		else
			$result['menu'] = 1;
		$result['error']  = false;	
		echo json_encode($result);
	}
	
}
