<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Context extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('context_model','context');
    }

	public function showAll(){
        
		$query = $this->context->showAll();
		if($query){
			$result['context']  = $query;
		}
        echo json_encode($result);
        
	}
	
}
