<?php

class UtilDateTime {

	public function getDatetimeNow() {
        date_default_timezone_set('Indian/Antananarivo');
        $datetime = new DateTime();
        $tz_object = new DateTimeZone('Indian/Antananarivo');
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y\-m\-d\ h:i:s');
    }
    
}

?>