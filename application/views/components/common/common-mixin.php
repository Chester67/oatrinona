<script type="text/javascript">

  /*
   *
   *
   *   COMMON MIXIN
   *
   *
   */

  var common_mixin = {
    data: function () {
        return {
            data_loader: true,
            data_no_user: false,
        }
    },
    filters: {

        /*
        Turn '2016-1-31' into '31/01/2016'
        */
        format_date: function (date = '') {
            let date_parts = date.match(/^(\d{4})-(\d+)-(\d+)(.*)$/);
            let year;
            let month;
            let day;

            if (date_parts) {
                year = ('00' + date_parts[1]).slice(-4);
                month = ('0' + date_parts[2]).slice(-2);
                day = ('0' + date_parts[3]).slice(-2);

                return  day + '/' + month + '/' + year + date_parts[4];
            } else {
                return date;
            }
        },

        format_month: function (date = '') {
            let date_parts = date.match(/^(\d{4})-(\d+)(.*)$/);
            let year;
            let month;

            if (date_parts) {
                year = ('00' + date_parts[1]).slice(-4);
                month = ('0' + date_parts[2]).slice(-2);

                return month + '/' + year;
            } else {
                return date;
            }
        },

        getHourOfDate: function(date){
            var str = date.substr(date.length - 8);
            var res = str.substring(0, str.length-3);
            return res;
        }

    },
    methods: {
        /*SPENT*/
        regroupSpentByCategory: function(spents, idcat) {
            var by_cat = [];
            spents.forEach((spent, i) => {
                if(spent.categoryId == idcat){
                    by_cat.push(spent);
                }
            });
            return by_cat;
        },
        regroupExpenseBySpent: function(spents){
            var by_expense = { category: {}, subcategories: []};
            by_expense.category = { id: spents[0].categoryId, categoryLabel: spents[0].categoryLabel, categoryImage: spents[0].categoryImage};
            spents.forEach((spent, i) => {
                by_expense.subcategories.push({index: i, type: 'content-longtext', model: '', subcategory: spent.subcategoryId, subcategory_label: spent.subcategoryLabel, number: spent.number, price: spent.unitPrice, spent_id: spent.id, from: '<?php echo CONSTANT_LISTING ?>', utility: {id: spent.contextId, contextLabel: spent.contextLabel, contextImage: spent.contextImage, contextColor: spent.contextColor} });
            });
            return by_expense;
        },
        regroupSpentByTypeDay: function(spents, type_day) {
            var by_type_day = [];
            spents.forEach((spent, i) => {
                if(spent.dayType === type_day){
                    by_type_day.push(spent);
                }
            });
            return by_type_day;
        },
        regroupSpentByEvent: function(spents, idevent) {
            var by_event = [];
            spents.forEach((spent, i) => {
                if(spent.eventId == idevent){
                    by_event.push(spent);
                }
            });
            return by_event;
        },
        regroupTaskByEvent: function(spents){
            var by_event = { hoursort: Number(this.getTwoFirstCharOfString(this.getHourMinOfDate(spents[0].eventDebut))), details: {}, taskspents: []};
            by_event.details = { taskLabel: spents[0].taskLabel, subtaskLabel: spents[0].subtaskLabel, place_description: spents[0].place_description, eventStatut: spents[0].eventStatut, eventDebut: spents[0].eventDebut, eventId: spents[0].eventId, assignedUser: `${spents[0].first_name} ${spents[0].last_name}`, assignedUserId: spents[0].assignedUserId, assignedUserImage: spents[0].user_image};

                var ids_category = this.takeAllCatId(spents);
                var filtered_ids_category = ids_category.filter((v,i) => ids_category.indexOf(v) == i);
                if(filtered_ids_category && filtered_ids_category.length > 0){
                    for(var i= 0; i < filtered_ids_category.length; i++){
                        by_event.taskspents.push(this.regroupExpenseBySpent(this.regroupSpentByCategory(spents, filtered_ids_category[i])));
                    }
                }

            return by_event;
        },
        takeAllCatId: function(spents){
            res = [];
            spents.forEach((spent, i) => {
                res.push(parseInt(spent.categoryId));
            });
            return res;
        },
        takeAllEventId: function(spents){
            res = [];
            spents.forEach((spent, i) => {
                res.push(parseInt(spent.eventId));
            });
            return res;
        },
        takeAllDateTask: function(reports){
            res = [];
            reports.forEach((report, i) => {
                res.push(this.formatDateToMMDDYYYY(report.eventDebut));
            });
            return res;
        },
        countOccurenceOf: function(arr, dat){
            return arr.filter(function(x){ return x === dat; }).length;
        },
        takeAllDateIdTask: function(reports){
            res = [];
            reports.forEach((report, i) => {
                res.push(report.id);
            });
            return res;
        },
        closeLoaderAfterDelay: function(){
            setTimeout(() =>{
                this.data_loader = false;
            },  1000); // disappearing message success in 1 sec
        },
        getClassNameByStatut(stat){
            var res = 'event-validated';
            if(stat === '<?php echo CONSTANT_TASK_TODO ?>')
                res = 'event-todo';
            else if(stat === '<?php echo CONSTANT_TASK_INPROGRESS ?>')
                res = 'event-inprogress';
            else if(stat === '<?php echo CONSTANT_TASK_FINISHED ?>')
                res = 'event-finished';
            else if(stat === '<?php echo CONSTANT_TASK_REJECTED ?>')
                res = 'event-rejected';
            return res;
        },

        /*LOGIN*/
        checkIfLogged: function(url_logged){
            /*let common_url_base = '<?php echo base_url() ?>';
            let common_url_is_logged: '<?php echo URL_IS_LOGGED ?>';
            var common_url_is_logged_final = `${common_url_base}${common_url_is_logged}`;*/
            axios.get(url_logged).then((response) => {
                if(response.data.logged){
                    //console.log(`USER LOGGED IN ===> ${JSON.stringify(response.data.user)}`);
                    this.data_no_user = false;
                    this.afterCheckLogged({is_logged: true, user_logged: response.data.user, user_choosed: response.data.chooseduser});
                }else{
                    //console.log("NO USER LOGGED");
                    this.data_no_user = true;
                    this.afterCheckLogged({is_logged: false, user_logged: undefined, user_choosed: undefined});
                }
            });
        },

        /*DATE 2010-08-10 to 10/08/2010*/
        formatDateToMMDDYYYY: function(date){
            let date_parts = date.match(/^(\d{4})-(\d+)-(\d+)(.*)$/);
            let year;
            let month;
            let day;

            if (date_parts) {
                year = ('00' + date_parts[1]).slice(-4);
                month = ('0' + date_parts[2]).slice(-2);
                day = ('0' + date_parts[3]).slice(-2);

                return  day + '/' + month + '/' + year + date_parts[4];
            } else {
                return date;
            }
        },

        /*MONTH 01 to JANVIER*/
        getMonthByInt: function(month_int){
            var months = [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
               "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" ];
            return months[month_int-1];
        },

        /*MONTH 2019-01 to 01*/
        getMonthOfDate: function(date){
            let tabs = date.split("-");
            return Number(tabs[1]);
        },

        /*YEAR 2019-01 to 2019*/
        getYearOfDate: function(date){
            let tabs = date.split("-");
            return Number(tabs[0]);
        },

        /*DAY 2019-01-02 to 02*/
        getDayOfDate: function(date){
            let tabs = date.split("-");
            return Number(tabs[2]);
        },

        getHourMinOfDate: function(date){
            var str = date.substr(date.length - 8);
            var res = str.substring(0, str.length-3);
            return res;
        },

        getTwoFirstCharOfString: function(str){
            return str.substring(0,2);
        },

        parseDate: function (input) {
            // parse a date in yyyy-mm-dd format
            var parts = input.match(/(\d+)/g);
            return new Date(parts[0], parts[1] - 1, parts[2]); // months are 0-based
        },

        array_move: function(arr, old_index, new_index) {
            if (new_index >= arr.length) {
                var k = new_index - arr.length + 1;
                while (k--) {
                    arr.push(undefined);
                }
            }
            arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
            return arr;
        },

        removeThreeLastChar(str){
            return str.substring(0, str.length-3);
        },

        getStatutLetter(str){
            var res = '';
            if(str === 'TODO')
                res = 'A faire';
            else if(str === 'INPROGRESS')
                res = 'En progression';
            else if(str === 'FINISHED')
                res = 'Terminée';
            else if(str === 'VALIDATED')
                res = 'Validée';
            else if(str === 'REJECTED')
                res = 'Rejetée';
            return res;
        },

        formatDateToYYYYMMdd(current_datetime){
            var m = current_datetime.getMonth() + 1;
            var mois = m<10 ? '0'+m : m;
            var j = current_datetime.getDate();
            var jour = j<10 ? '0'+j : j;
            return current_datetime.getFullYear() + "-" + mois + "-" + jour;
        },

        parseFrenchDate(str){
            let parts = str.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        },

        getLocalLongDate(str){
            let parts = str.split(" ");
            let subpartsdate = parts[0].split("-");
            let subpartshour = parts[1].split(":");
            var event = new Date(Date.UTC(subpartsdate[0], subpartsdate[1]-1, subpartsdate[2], subpartshour[0], subpartshour[1], 0));
            console.log(event);
            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            let datefr = event.toLocaleDateString('fr-FR', options);
            return datefr + ' à ' + subpartshour[0] + ':' + subpartshour[1];
        },



    }
  }


</script>
