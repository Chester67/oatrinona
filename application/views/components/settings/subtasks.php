<?php
$this->load->view('components/empty/data_empty');
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
?>

<style type="text/css">

    .sticky-subtask {
        position: -webkit-sticky;
        position: sticky;
        width: 100%;
        top: 70px;
        top-max: 70px;
        top-min: 70px;
        height: 70px;
        z-index: 10;
    }

    .subtasktext {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        line-height: 16px;     /* fallback */
        white-space: normal;
        word-wrap: break-word;
        -webkit-line-clamp: 2; /* number of lines to show */
        -webkit-box-orient: vertical;
        color: black;
        text-align: center;
    }

    .marquee {
        width: 250px;
        margin: 0 auto;
        overflow: hidden;
        white-space: nowrap;
        animation: marquee 10s linear infinite;
        -webkit-animation:linear marquee 10s infinite ;
        -moz-animation:linear marquee 10s infinite ;
        -o-animation:linear marquee 10s infinite ;
        -ms-animation:linear marquee 10s infinite ;
        animation:linear marquee 10s infinite ;
    }

    .marquee:hover {
        animation-play-state: paused
    }

    /* Make it move */
    @keyframes marquee {
        0%   { text-indent: 3em }
        100% { text-indent: -9em }
    }

</style>

<script type="text/x-template" id="template-setting-subtasks">

    <div style="background:#ffffff; height: 100%">

        <v-container fluid>

            <v-toolbar class="sticky-subtask">
                <v-btn icon @click="back_to_previous">
                <v-icon>arrow_back</v-icon>
                </v-btn>

                <div style="width: 120px; height: 100%; display: flex; align-items: center;">
                    <img :src="image_url" alt="" style="width:85%;height:85%;object-fit:contain;">
                </div>

                <h4>{{ subtask_label }}</h4>

            </v-toolbar>

            <template v-if="items && items.length > 0">
                <v-container fluid grid-list-md style="min-height: 434px; margin-top: 0">
                    <v-data-iterator
                        :items="items"
                        :rows-per-page-items="rowsPerPageItems"
                        :pagination.sync="pagination"
                        content-tag="v-layout"
                        row
                        hide-actions
                        wrap>

                        <v-flex
                            slot="item"
                            slot-scope="props"
                            xs12
                            md2>

                            <v-card :ripple="true" style="background:#fafafa;">
                                <div v-if="user_logged_in.role === 'ADMINISTRATOR'" style="display: flex; flex-direction: row; justify-content: flex-end; height: 19px; margin-right: 7px">
                                    <v-btn outline fab color="grey" class="edits-btn-sub" @click.stop.prevent="showUpdate(props.item)" style="background: red">
                                        <v-icon style="font-size: 15px;">create</v-icon>
                                    </v-btn>
                                    <v-btn outline fab color="grey" class="edits-btn-sub" @click.stop.prevent="onDeleteSelected(props.item)">
                                        <v-icon style="font-size: 18px;">clear</v-icon>
                                    </v-btn>
                                </div>
                                <div style="height: 145px; display: flex; justify-content: center; align-items: center; background: #ECEFF1; margin-top: 9px">
                                    <img :src="urlBaseImage+'uploads/sous-taches/'+props.item.subtaskImage" alt="" style="width:95%; height:95%; object-fit:crop; margin-top: 3px; padding-bottom: 3px;">
                                </div>
                                <v-card-title style="display: flex; justify-content: center; align-items: center;">
                                <template v-if="countCharOK(props.item.subtaskLabel)">
                                    <p style="font-weight: bold; font-size: 13px; width: 250px; margin: 0 auto; overflow: hidden; white-space: nowrap; text-align: center">{{ props.item.subtaskLabel }}</p>
                                </template>
                                <template v-else>
                                    <p style="font-weight: bold; font-size: 13px" class="marquee">{{ props.item.subtaskLabel }}</p>
                                </template>
                                </v-card-title>
                            </v-card>

                        </v-flex>
                    </v-data-iterator>
                </v-container>
            </template>
            <template v-else>
                <data-empty></data-empty>
            </template>

        </v-container>

        <v-dialog v-model="dialog" width="420px">
            <v-card>
                <v-card-title class="grey lighten-4 py-4 title">
                    {{ dialog_title }}
                </v-card-title>

                <v-container grid-list-sm class="pa-4">
                    <v-layout row wrap>

                        <form id="upload-form" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                            <v-flex xs12>
                                <v-text-field prepend-icon="text_fields" placeholder="Sous-tâche" v-model="label"></v-text-field>
                            </v-flex>

                            <v-flex xs12>
                                <div style="display: flex; flex-direction: row;">
                                    <v-icon>add_a_photo</v-icon>
                                    <div style="display: flex; flex-direction: row; align-items: center">
                                        <input id="file" style="margin-left: 8px" type="file" :class="class_name_state" @change="onFileSelected">
                                        <label style="margin-left: 4px" id="fileLabel">{{ file_name_update }}</label>
                                    </div>
                                </div>
                            </v-flex>

                        </form>

                    </v-layout>
                </v-container>

                <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn flat color="blue-grey lighten-1" @click="dialog = false">Annuler</v-btn>
                    <v-btn flat @click="distinctAction">Enregistrer</v-btn>
                </v-card-actions>

                <div v-if="errormessage" style="display: flex; flex-direction: row; margin-left: 25px; height: 40px">
                    <v-icon style="display: flex; align-items: center; vertical-align: middle">error_outline</v-icon>
                    <span style="margin-left: 8px; display: flex; align-items: center;">{{ errormessage }}</span>
                </div>

            </v-card>
        </v-dialog>

        <v-btn
            fab
            bottom
            right
            color="blue-grey lighten-1"
            dark
            fixed
            @click.stop="showAdd">
            <v-icon>add</v-icon>
        </v-btn>

        <v-dialog v-model="dialog_remove" persistent max-width="290">
            <v-card>
                <v-card-title>Voulez-vous vraiment supprimer cete sous-tâche?</v-card-title>
                <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="green darken-1" flat @click="dialog_remove = false">ANNULER</v-btn>
                <v-btn color="green darken-1" flat @click="deleteSubtask">SUPPRIMER</v-btn>
                </v-card-actions>
            </v-card>
        </v-dialog>

        <data-loader v-if="data_loader"></data-loader>

        <v-alert
            class="centered"
            :value="alert"
            type="success"
            transition="scale-transition">
            {{ flashMessage }}
        </v-alert>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_list_subtask_by_taskId = '<?php echo URL_LIST_SUBTASK_BY_ID ?>';
    var url_list_subtask_by_taskId_final = `${url_base}${url_list_subtask_by_taskId}`;

    var url_upload_image_subtask = '<?php echo URL_UPLOAD_IMAGE_SUBTASK ?>';
    var url_upload_image_subtask_final = `${url_base}${url_upload_image_subtask}`;

    var url_update_subtask = '<?php echo URL_UPDATE_SUBTASK ?>';
    var url_update_subtask_final = `${url_base}${url_update_subtask}`;

    var url_delete_subtask = '<?php echo URL_DELETE_SUBTASK ?>';
    var url_delete_subtask_final = `${url_base}${url_delete_subtask}`;

    Vue.component('setting-subtasks', {
        template: '#template-setting-subtasks',
        mixins: [common_mixin],
        data: function () {

            return{
                flashMessage:'',
                alert: false,
                dialog: false,
                dialog_remove: false,
                dialog_title: '',
                rowsPerPageItems: [6, 12, 24],
                pagination: {
                    rowsPerPage: 6
                },
                items: [],
                image_url: '',
                subtask_label: '',
                task_id: 0,
                urlBaseImage: url_base,
                selectedFile: null,
                label: null,
                errormessage: '',
                selected_for_delete: undefined,
                action_type: '',
                class_name_state: '',
                file_name_update: '',
                old_file_name_update: '',
                selected_for_update: undefined,
                user_logged_in: undefined,
            }

        },
        methods: {
            back_to_previous () {
                this.$router.go(-1);
            },
            ws_subtasks () {
                var formData = new FormData();
                formData.append('task_id', this.task_id);
                window.axios.post(url_list_subtask_by_taskId_final, formData).then((response) => {
                    if(response.data.subtask == null){
                        console.log('SUBTASKS NULL');
                        this.items = [];
                    }else{
                        console.log(`SUBTASKS ARE ====>${JSON.stringify(response.data.subtask)}`);
                        var arr_rowsPerPageItems = [];
                        arr_rowsPerPageItems.push(response.data.subtask.length);
                        this.rowsPerPageItems = arr_rowsPerPageItems;
                        this.pagination.rowsPerPage = response.data.subtask.length;
                        this.items = response.data.subtask;
                    }
                })
                .catch(err => {
                    console.log(err)
                })
                .finally(() => {
                    this.closeLoaderAfterDelay();
                })
            },
            onDeleteSelected(item){
                this.selected_for_delete = item;
                this.dialog_remove = true;
            },
            onFileSelected(event){
                this.selectedFile = event.target.files[0];
                if(this.action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    this.file_name_update = this.selectedFile.name;
                }
            },
            manageFlashMSG(message){
                this.flashMessage = message;
                this.alert = true;
                setTimeout(() =>{
                    this.alert = false;
			        this.ws_subtasks();
			    },  3000); // disappearing message success in 3 sec
            },
            addSubtask(){
                console.log("addSubtask")
                if(this.label && this.selectedFile && this.selectedFile.name){
                    this.data_loader = true;
                    this.errormessage = '';
                    var formData = new FormData();
                    formData.append('image_subtask', this.selectedFile);
                    formData.append('label_subtask', this.label);
                    formData.append('id_task', this.task_id);
                    axios.post(url_upload_image_subtask_final, formData).then((response) =>{
                        console.log(`SUCCESS UPLOAD====>${response.data.success_upload} SUCCESS INSERT====>${response.data.success_insert}`);
                        if (response.data.success_upload && response.data.success_insert) {
                            //alert("SUCCESS");
                            this.dialog = false;
                            this.manageFlashMSG("Votre sous-tâche a été ajouté avec succès !");

                        }
                        if (response.data.error) {
                            //alert(`ERROR====>${response.data.error}`);
                            this.errormessage = response.data.error;
                        }
                    })
                    .finally(() => {

                        this.closeLoaderAfterDelay();

                    })
                }else{
                    this.errormessage = "Veuillez completer les champs";
                }
            },
            updateSubctask(){
                console.log("updateSubtask")

                if(this.label){

                    this.data_loader = true;
                    this.errormessage = '';
                    var formData = new FormData();

                    formData.append('id_subtask', this.selected_for_update.id);
                    formData.append('label_subtask', this.label);
                    formData.append('new_image_name_subtask', this.file_name_update);
                    formData.append('old_image_name_subtask', this.old_file_name_update);

                    if(this.selectedFile && this.selectedFile.name){
                        console.log("========>UPDATE BOTH SUBTASK LABEL & IMAGE");
                        formData.append('has_image', 'TRUE');
                        formData.append('image_subtask', this.selectedFile);

                    }else{
                        console.log("========>UPDATE ONLY SUBTASK LABEL");
                        formData.append('has_image', 'FALSE');
                        formData.append('image_subtask', null);
                    }

                    axios.post(url_update_subtask_final, formData).then((response) =>{
                        console.log(`CODE ERROR EDIT====>${response.data.code_error}`);
                        if (!response.data.error) {

                            this.dialog = false;
                            this.manageFlashMSG("Votre sous-tâche a été modifiée avec succès !");

                        }else{
                            alert("ERROR");
                        }

                    })
                    .finally(() => {

                        this.closeLoaderAfterDelay();

                    })

                }else{
                    this.errormessage = "Veuillez completer la sous-tâche !";
                }
            },
            deleteSubtask(){
                console.log("deleteSubtask")
                this.dialog_remove = false;
                this.data_loader = true;
                var formData = new FormData();
                console.log(`SUBTASK_TO_DELETE : ${JSON.stringify(this.selected_for_delete)}`)
                formData.append('image_subtask', this.selected_for_delete.subtaskImage);
                formData.append('id_subtask', this.selected_for_delete.id);
                axios.post(url_delete_subtask_final, formData).then((response) =>{
                    console.log(`SUCCESS DELETE====>${response.data.msg}`);
                    if (response.data.file_deleted && !response.data.error) {

                        this.manageFlashMSG("Votre sous-tâche a été supprimée avec succès !");

                    }
                    if (response.data.error) {
                        alert(response.data.msg);
                    }
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            distinctAction: function() {
                if(this.action_type === '<?php echo CONSTANT_DB_INSERT ?>'){
                    this.addSubtask();
                }else if(this.action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    this.updateSubctask();
                }
            },
            showUpdate(subtask){

                this.selected_for_update = subtask;
                this.dialog_title = "Modifier une sous-tâche";
                this.label = subtask.subtaskLabel;
                //set input file image programmatically
                this.class_name_state = 'hidden-placeholder';
                this.dialog = true;
                this.errormessage =  '';
                this.file_name_update = subtask.subtaskImage;
                this.old_file_name_update = subtask.subtaskImage;
                this.action_type = '<?php echo CONSTANT_DB_UPDATE ?>';
                this.selectedFile = null;

            },
            showAdd(){

                //reset input file if non null (after open update)
                if(document.querySelector('#file')){
                    document.querySelector('#file').value = '';
                }
                this.selectedFile = null;
                this.dialog_title = "Créer une sous-tâche";
                this.class_name_state = "";
                this.file_name_update = "";
                this.dialog = true;
                this.label = null;
                this.errormessage =  '';
                this.action_type = '<?php echo CONSTANT_DB_INSERT ?>';

            },
            countCharOK(str){
                if(str.length > 20)
                    return false;
                return true;
            },

        },
        mounted: function () {

        },
        created: function() {
            this.image_url = this.$route.query.image;
            this.subtask_label = this.$route.query.label;
            this.task_id = this.$route.query.taskid;
            this.user_logged_in = this.$route.query.userlogged;
            this.ws_subtasks();
            this.$router.app.$emit('updateWelcomeSubtitle', "Sous-tâches");
            console.log(`PARAMS=====>${this.$route.params.taskId} IMAGE=====>${this.$route.query.image}`);
        }

    });

</script>