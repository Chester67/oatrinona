<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Comments extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('comment_model', 'comment');
    }

    public function createComment()
    {
        $result['error'] = true;
        $result['msg'] = 'Error when adding comment';
        $newcomment = array(
            'comment' => $this->input->post('text_comment'),
            'eventId' => $this->input->post('event_id'),
            'userId' => $this->input->post('user_id'),
        );
        $id_inserted = $this->comment->createComment($newcomment);
        if ($id_inserted != 0) {
            $result['error'] = false;
            $result['inserted_id'] = $id_inserted;
            $result['msg'] = 'Comment added successfully';
        }
        echo json_encode($result);
    }

    public function showByEvent()
    {
        $eventId = $this->input->post('id_event');
        $query = $this->comment->showByEvent($eventId);
        if ($query) {
            $result['comments'] = $query;
        }
        echo json_encode($result);
    }

}
