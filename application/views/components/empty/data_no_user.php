<style type="text/css">

    .nouser {
        display: block;
        margin-left: auto;
        margin-right: auto
    }

</style>

<script type="text/x-template" id="template-data-no-user">

    <div style="width: 100%; height: 100%">

        <div class="nouser">

            <div style="display: flex; flex-direction: column; justify-content: center; align-items: center; height: 350px;">

                <v-icon size="72px">people</v-icon>
                <label>Pas d'utilisateur connecté !</label>
                <!--v-btn color="blue-grey lighten-1" @click="connect">Se connecter</v-btn-->

            </div>

        </div>

    </div>

</script>

<script type="text/javascript">

    Vue.component('data-no-user', {
        template: '#template-data-no-user',
        data: function () {

            return{
                name: '',
            }

        },
        methods: {
           connect: function(){
                this.$router.push('/login');
           }
        },

    });

</script>
