<?php

class Subcategory_model extends CI_Model{

    public function showAll(){
        $query = $this->db->get('subcategory');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function showByCategoryId($categId){
        $query = $this->db->get_Where('subcategory', array('categoryId'=>$categId));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function addSubcategory($data){
        $insert = $this->db->insert('subcategory', $data);
        return $insert;
    }

    public function isExist($label_subcategory){
        $this->db->select('*');
        $this->db->from('subcategory');
        $this->db->where('subcategoryLabel', $label_subcategory);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

    public function updateSubcategory($data, $id){
        $updated = $this->db->update('subcategory', $data, array('id' => $id));
        return $updated;
    }

    public function deleteSubcategory($id){
        $deleted = $this->db->delete('subcategory', array('id' => $id));
        return $deleted;
    }

    public function isExistInSpent($id_subcategory){
        $this->db->select('*');
        $this->db->from('spent');
        $this->db->where('subcategoryId', $id_subcategory);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

}

?>