<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('history_model', 'history');
    }

    public function showHistoryByEvent()
    {
        $eventId = $this->input->post('id_event');
        $query = $this->history->showHistoryByEvent($eventId);
        if ($query) {
            $result['history'] = $query;
        }
        echo json_encode($result);

    }

    public function createHistory()
    {
        $result['error'] = true;
        $result['msg'] = 'Error when adding history';
        $id_inserted = 0;

        $newhistory = array(
            'statuthistory' => $this->input->post('status_history'),
            'eventId' => $this->input->post('event_id'),
            'userId' => $this->input->post('user_id'),
        );
        //commenthistory

        $id_inserted = $this->history->createHistory($newhistory);

        if ($id_inserted != 0) {
            $result['error'] = false;
            $result['inserted_id'] = $id_inserted;
            $result['msg'] = 'History added successfully';
        }
        echo json_encode($result);
    }

}
