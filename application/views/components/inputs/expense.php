<?php

?>

<script type="text/x-template" id="template-expense">

    <div>
    
        <v-container>
            <v-layout wrap>

              <v-flex xs12 md4>
                <v-autocomplete v-model="friends" :items="people" box chips color="blue-grey lighten-2" label="Select" item-text="name" item-value="name" multiple>
                    <template slot="selection" slot-scope="data">
                        <v-chip :selected="data.selected" close class="chip--select-multi" @input="remove(data.item)">
                          <v-avatar>
                            <img :src="data.item.avatar">
                          </v-avatar>
                          {{ data.item.name }}
                        </v-chip>
                    </template>
                    <template slot="item" slot-scope="data">
                        <template v-if="typeof data.item !== 'object'">
                            <v-list-tile-content v-text="data.item"></v-list-tile-content>
                        </template>
                        <template v-else>
                            <v-list-tile-avatar>
                                <img :src="data.item.avatar">
                            </v-list-tile-avatar>
                            <v-list-tile-content>
                                <v-list-tile-title v-html="data.item.name"></v-list-tile-title>
                                <v-list-tile-sub-title v-html="data.item.group"></v-list-tile-sub-title>
                            </v-list-tile-content>
                        </template>
                    </template>
                </v-autocomplete>
              </v-flex>

              <v-flex xs12 md4>
                <v-text-field v-model="title" box color="blue-grey lighten-2" label="Title"></v-text-field>
              </v-flex>

              <v-flex xs12 md4>
                <v-text-field v-model="title" box color="blue-grey lighten-2" label="Title"></v-text-field>
              </v-flex>

            </v-layout>
        </v-container>
    
    </div>

</script>

<script type="text/javascript">

    Vue.component('expense', {
        template: '#template-expense',
        data: function () {
            
                return{
                friends: ['Sandra Adams', 'Britta Holt'],
                name: 'Midnight Crew',
                people: [
                { header: 'Group 1' },
                { name: 'Sandra Adams', group: 'Group 1', avatar: 'https://cdn.vuetifyjs.com/images/lists/1.jpg' },
                { name: 'Ali Connors', group: 'Group 1', avatar: 'https://cdn.vuetifyjs.com/images/lists/2.jpg' },
                { name: 'Trevor Hansen', group: 'Group 1', avatar: 'https://cdn.vuetifyjs.com/images/lists/3.jpg' },
                { name: 'Tucker Smith', group: 'Group 1', avatar: 'https://cdn.vuetifyjs.com/images/lists/4.jpg' },
                { divider: true },
                { header: 'Group 2' },
                { name: 'Britta Holt', group: 'Group 2', avatar: 'https://cdn.vuetifyjs.com/images/lists/4.jpg' },
                { name: 'Jane Smith ', group: 'Group 2', avatar: 'https://cdn.vuetifyjs.com/images/lists/4.jpg' },
                { name: 'John Smith', group: 'Group 2', avatar: 'https://cdn.vuetifyjs.com/images/lists/4.jpg' },
                { name: 'Sandra Williams', group: 'Group 2', avatar: 'https://cdn.vuetifyjs.com/images/lists/4.jpg' }
              ],
              title: 'The summer breeze'}
            
        },
        methods: {
           remove (item) {
               const index = this.friends.indexOf(item.name)
               if (index >= 0) this.friends.splice(index, 1)
           },
           
        },
        mounted: function () {
            
        },
        
    });

</script>