<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
 */
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', true);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
 */
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
 */
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
 */
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| URLS FOR DATABASE CRUD & FOR USER CHOOSE
|--------------------------------------------------------------------------
|
| Used to specify urls of database crud and database model
|
|
 */
defined('URL_CREATE_USER') or define('URL_CREATE_USER', "users/register");
defined('URL_LIST_USER') or define('URL_LIST_USER', "users/showByAdmin");
defined('URL_SEARCH_USER') or define('URL_SEARCH_USER', "users/searchUser");
defined('URL_SAVE_USER') or define('URL_SAVE_USER', "users/saveUserChoosedSession");
defined('URL_CHOOSE_USER') or define('URL_CHOOSE_USER', "welcome/currentuser");
defined('URL_DELETE_USER') or define('URL_DELETE_USER', "users/deleteUser");
defined('URL_UPDATE_USER') or define('URL_UPDATE_USER', "users/updateUser");
defined('URL_SELECT_USER') or define('URL_SELECT_USER', "users/selectUser");
defined('URL_LIST_CATEGORY') or define('URL_LIST_CATEGORY', "categories/showAll");
defined('URL_LIST_SUBCATEGORY') or define('URL_LIST_SUBCATEGORY', "subcategories/showAll");
defined('URL_LIST_SUBCATEGORY_BY_ID') or define('URL_LIST_SUBCATEGORY_BY_ID', "subcategories/showByCategoryId");
defined('URL_LIST_SUBTASK_BY_ID') or define('URL_LIST_SUBTASK_BY_ID', "subtasks/showByTaskId");
defined('URL_SAVE_SPENT') or define('URL_SAVE_SPENT', "spent/saveDaySpent");
defined('URL_LIST_SPENT') or define('URL_LIST_SPENT', "spent/showDaySpent");
defined('URL_LIST_SPENT_REPEAT') or define('URL_LIST_SPENT_REPEAT', "spent/showMonthWeekSpent");
defined('URL_UPDATE_SPENT') or define('URL_UPDATE_SPENT', "spent/updateDaySpent");
defined('URL_LAST_SPENT') or define('URL_LAST_SPENT', "spent/getLastSpentId");
defined('URL_UPLOAD_IMAGE_CATEGORIE') or define('URL_UPLOAD_IMAGE_CATEGORIE', "upload/uploadImageCategorie");
defined('URL_UPLOAD_IMAGE_SOUSCATEGORIE') or define('URL_UPLOAD_IMAGE_SOUSCATEGORIE', "upload/uploadImageSouscategorie");
defined('URL_DELETE_CATEGORY') or define('URL_DELETE_CATEGORY', "categories/deleteCategory");
defined('URL_UPDATE_CATEGORY') or define('URL_UPDATE_CATEGORY', "categories/updateCategory");
defined('URL_REPORT_SIMPLE_SPENT_GLOBAL') or define('URL_REPORT_SIMPLE_SPENT_GLOBAL', "report/reportSimpleSpentGlobal");
defined('URL_AUTHENTICATE') or define('URL_AUTHENTICATE', "login/authenticate");
defined('URL_LOGOUT') or define('URL_LOGOUT', "login/logout");
defined('URL_IS_LOGGED') or define('URL_IS_LOGGED', "login/isLogged");
defined('URL_SAVE_MENU') or define('URL_SAVE_MENU', "welcome/saveLastMenu");
defined('URL_GET_MENU') or define('URL_GET_MENU', "welcome/getLastMenu");
defined('URL_UPDATE_SUBCATEGORY') or define('URL_UPDATE_SUBCATEGORY', "subcategories/updateSubcategory");
defined('URL_DELETE_SUBCATEGORY') or define('URL_DELETE_SUBCATEGORY', "subcategories/deleteSubcategory");
defined('URL_LIST_TASK') or define('URL_LIST_TASK', "tasks/showAll");
defined('URL_UPDATE_TASK') or define('URL_UPDATE_TASK', "tasks/updateTask");
defined('URL_DELETE_TASK') or define('URL_DELETE_TASK', "tasks/deleteTask");
defined('URL_LIST_SUBTASK') or define('URL_LIST_SUBTASK', "subtasks/showAll");
defined('URL_UPDATE_SUBTASK') or define('URL_UPDATE_SUBTASK', "subtasks/updateSubtask");
defined('URL_DELETE_SUBTASK') or define('URL_DELETE_SUBTASK', "subtasks/deleteSubtask");
defined('URL_CREATE_EVENT') or define('URL_CREATE_EVENT', "event/createEvent");
defined('URL_LIST_CONTEXT') or define('URL_LIST_CONTEXT', "context/showAll");
defined('URL_CREATE_PLACE') or define('URL_CREATE_PLACE', "place/createPlace");
defined('URL_LIST_DAY_TASK') or define('URL_LIST_DAY_TASK', "spent/showDayTask");
defined('URL_DELETE_SPENT_BY_EVENT') or define('URL_DELETE_SPENT_BY_EVENT', "spent/deleteSpentByEvent");
defined('URL_DELETE_EVENT') or define('URL_DELETE_EVENT', "event/deleteEvent");
defined('URL_REPORT_SIMPLE_TASK_GLOBAL') or define('URL_REPORT_SIMPLE_TASK_GLOBAL', "report/reportSimpleTaskGlobal");
defined('URL_REPORT_CATEGORY_SPENT') or define('URL_REPORT_CATEGORY_SPENT', "report/reportCategorySpent");
defined('URL_REPORT_CATEGORY_TASK') or define('URL_REPORT_CATEGORY_TASK', "report/reportCategoryTask");
defined('URL_CHECK_NOTIFICATION') or define('URL_CHECK_NOTIFICATION', "spent/checkNotificationTask");
defined('URL_GET_NOTIFICATION') or define('URL_GET_NOTIFICATION', "spent/getNotificationTask");
defined('URL_GET_OTHER_NOTIFICATION') or define('URL_GET_OTHER_NOTIFICATION', "spent/getOtherNotificationTask");
defined('URL_GET_RANGE_NOTIFICATION') or define('URL_GET_RANGE_NOTIFICATION', "spent/getRangeNotificationTask");
defined('URL_GET_OTHER_RANGE_NOTIFICATION') or define('URL_GET_OTHER_RANGE_NOTIFICATION', "spent/getOtherRangeNotificationTask");
defined('URL_UPDATE_EVENT_STATUT') or define('URL_UPDATE_EVENT_STATUT', "event/updateEventStatut");
defined('URL_UPLOAD_IMAGE_TASK') or define('URL_UPLOAD_IMAGE_TASK', "upload/uploadImageTask");
defined('URL_UPLOAD_IMAGE_SUBTASK') or define('URL_UPLOAD_IMAGE_SUBTASK', "upload/uploadImageSubtask");
defined('URL_CREATE_COMMENT') or define('URL_CREATE_COMMENT', "comments/createComment");
defined('URL_LIST_COMMENT_BY_EVENT') or define('URL_LIST_COMMENT_BY_EVENT', "comments/showByEvent");
defined('URL_CREATE_PIECE') or define('URL_CREATE_PIECE', "piece/createPiece");
defined('URL_LIST_PIECE') or define('URL_LIST_PIECE', "piece/showPieceByEvent");
defined('URL_CREATE_HISTORY') or define('URL_CREATE_HISTORY', "history/createHistory");
defined('URL_LIST_HISTORY') or define('URL_LIST_HISTORY', "history/showHistoryByEvent");
defined('URL_LIST_PLACES') or define('URL_LIST_PLACES', "place/showAll");

/*
|--------------------------------------------------------------------------
| CONSTANT WORD
|--------------------------------------------------------------------------
|
| Used to specify WORD
|
|
 */
defined('CONSTANT_ADDING') or define('CONSTANT_ADDING', "adding");
defined('CONSTANT_LISTING') or define('CONSTANT_LISTING', "listing");
defined('CONSTANT_DB_INSERT') or define('CONSTANT_DB_INSERT', "insert");
defined('CONSTANT_DB_UPDATE') or define('CONSTANT_DB_UPDATE', "update");
defined('CONSTANT_DB_REMOVE') or define('CONSTANT_DB_REMOVE', "remove");
defined('CONSTANT_TASK_TODO') or define('CONSTANT_TASK_TODO', "TODO");
defined('CONSTANT_TASK_INPROGRESS') or define('CONSTANT_TASK_INPROGRESS', "INPROGRESS");
defined('CONSTANT_TASK_FINISHED') or define('CONSTANT_TASK_FINISHED', "FINISHED");
defined('CONSTANT_TASK_VALIDATED') or define('CONSTANT_TASK_VALIDATED', "VALIDATED");
defined('CONSTANT_TASK_REJECTED') or define('CONSTANT_TASK_REJECTED', "REJECTED");
defined('CONSTANT_TASK_ARCHIVED') or define('CONSTANT_TASK_ARCHIVED', "ARCHIVED");
