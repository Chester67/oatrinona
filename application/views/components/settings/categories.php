<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
$this->load->view('components/settings/detailedList/list_categories');
?>

<style type="text/css">

.centered_flash {
  position: fixed ;
  top: 50%;
  left: 50%;
}

.edits-btn{
    width:22px;
    height:22px
}

.hidden-placeholder {
    width:120px;
    color:transparent;
}

.marquee {
    width: 250px;
    margin: 0 auto;
    overflow: hidden;
    white-space: nowrap;
    animation: marquee 10s linear infinite;
    -webkit-animation:linear marquee 10s infinite ;
	-moz-animation:linear marquee 10s infinite ;
	-o-animation:linear marquee 10s infinite ;
	-ms-animation:linear marquee 10s infinite ;
	animation:linear marquee 10s infinite ;
}

.marquee:hover {
  animation-play-state: paused
}

/* Make it move */
@keyframes marquee {
    0%   { text-indent: 3em }
    100% { text-indent: -9em }
}

</style>

<script type="text/x-template" id="template-setting-category">

<!--sur v-card @click.native="goToSub(props.item.id, urlBaseImage+'/uploads/categories/'+props.item.categoryImage, props.item.categoryLabel, props.item.id)"-->
    <div style="background:#ffffff; height: 100%; marging: 30px">

        <div v-if="!data_no_user">
            <v-btn-toggle v-model="icon" style="position: relative; left: 50%; transform: translateX(-50%); margin-top: 15px">
              <v-btn flat value="left" @click="changeListDisplay('list_edit')" :class="edit_class">
                <span>Modification</span>
                <v-icon>edit</v-icon>
              </v-btn>
              <v-btn flat value="right" @click="changeListDisplay('list_detailed')" :class="list_class">
                <span>Liste détaillée</span>
                <v-icon>view_list</v-icon>
              </v-btn>
            </v-btn-toggle>
        </div>

        <template v-if="!data_no_user">

            <v-container fluid grid-list-md v-if="activelist === 'list_edit'">
                <v-data-iterator
                    no-data-text=""
                    :items="items"
                    :rows-per-page-items="rowsPerPageItems"
                    :pagination.sync="pagination"
                    content-tag="v-layout"
                    row
                    hide-actions
                    wrap>
                    <v-flex
                        slot="item"
                        slot-scope="props"
                        xs12
                        md2>

                        <v-card :ripple="true" style="cursor: pointer; background:#fafafa;" @click.native="goToSub(props.item.id, urlBaseImage+'/uploads/categories/'+props.item.categoryImage, props.item.categoryLabel, props.item.id)">

                            <div style="width: 100%; height: 100%;">
                                <div v-if="user_logged_in.role === 'ADMINISTRATOR'" style="display: flex; flex-direction: row; justify-content: flex-end; height: 22px; margin-right: 7px; border-top: 6px solid transparent;">
                                    <v-btn outline fab color="grey" class="edits-btn" @click.stop.prevent="showUpdate(props.item)">
                                        <v-icon style="font-size: 15px;">create</v-icon>
                                    </v-btn>
                                    <v-btn outline fab color="grey" class="edits-btn" @click.stop.prevent="onDeleteSelected(props.item)">
                                        <v-icon style="font-size: 18px;">clear</v-icon>
                                    </v-btn>
                                </div>

                                <v-layout justify-center>
                                    <img :src="urlBaseImage+'uploads/categories/'+props.item.categoryImage" alt="" style="margin-top: 24px;object-fit:contain" width="160" height="90">
                                </v-layout>

                                <v-card-title>
                                    <template v-if="countCharOK(props.item.categoryLabel)">
                                        <p style="font-weight: bold; font-size: 13px; width: 250px; margin: 0 auto; overflow: hidden; white-space: nowrap; text-align: center">{{ props.item.categoryLabel }}</p>
                                    </template>
                                    <template v-else>
                                        <p style="font-weight: bold; font-size: 13px" class="marquee">{{ props.item.categoryLabel }}</p>
                                    </template>
                                </v-card-title>
                            </div>

                        </v-card>
                    </v-flex>
                </v-data-iterator>
            </v-container>

            <setting-category-list
                v-else-if="activelist === 'list_detailed' && items.length > 0 && all_subcategories.length > 0"
                :categories="items"
                :subcategories="all_subcategories">
            </setting-category-list>

        </template>

        <v-dialog v-model="dialog" width="420px">
            <v-card>
                <v-card-title class="grey lighten-4 py-4 title">
                    {{ dialog_title }}
                </v-card-title>

                <v-container grid-list-sm class="pa-4">
                    <v-layout row wrap>

                        <form id="upload-form" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                            <v-flex xs12>
                                <v-text-field prepend-icon="text_fields" placeholder="Catégorie" v-model="label"></v-text-field>
                            </v-flex>

                            <v-flex xs12>
                                <div style="display: flex; flex-direction: row;">
                                    <v-icon>add_a_photo</v-icon>
                                    <div style="display: flex; flex-direction: row; align-items: center">
                                        <input id="file" style="margin-left: 8px" type="file" :class="class_name_state" @change="onFileSelected">
                                        <label style="margin-left: 4px" id="fileLabel">{{ file_name_update }}</label>
                                    </div>
                                </div>
                            </v-flex>

                        </form>

                    </v-layout>
                </v-container>

                <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn flat color="blue-grey lighten-1" @click="dialog = false">Annuler</v-btn>
                    <v-btn flat @click="distinctAction">Enregistrer</v-btn>
                </v-card-actions>

                <div v-if="errormessage" style="display: flex; flex-direction: row; margin-left: 25px; height: 40px">
                    <v-icon style="display: flex; align-items: center; vertical-align: middle">error_outline</v-icon>
                    <span style="margin-left: 8px; display: flex; align-items: center;">{{ errormessage }}</span>
                </div>

            </v-card>
        </v-dialog>

        <v-btn
            fab
            bottom
            right
            color="blue-grey lighten-1"
            dark
            fixed
            @click="showAdd"
            v-if="!data_no_user && activelist === 'list_edit'">
            <v-icon>add</v-icon>
        </v-btn>

        <v-dialog v-model="dialog_remove" persistent max-width="290">
            <v-card>
                <v-card-title>Voulez-vous vraiment supprimer ce catégorie?</v-card-title>
                <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="green darken-1" flat @click="dialog_remove = false">ANNULER</v-btn>
                <v-btn color="green darken-1" flat @click="deleteCategory">SUPPRIMER</v-btn>
                </v-card-actions>
            </v-card>
        </v-dialog>

        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>

        <v-alert
            class="centered_flash"
            :value="alert"
            type="success"
            transition="scale-transition">
            {{ flashMessage }}
        </v-alert>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_list_category = '<?php echo URL_LIST_CATEGORY ?>';
    var url_list_category_final = `${url_base}${url_list_category}`;

    var url_upload_image_category = '<?php echo URL_UPLOAD_IMAGE_CATEGORIE ?>';
    var url_upload_image_category_final = `${url_base}${url_upload_image_category}`;

    var url_delete_category = '<?php echo URL_DELETE_CATEGORY ?>';
    var url_delete_category_final = `${url_base}${url_delete_category}`;

    var url_update_category = '<?php echo URL_UPDATE_CATEGORY ?>';
    var url_update_category_final = `${url_base}${url_update_category}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_list_subcategories = '<?php echo URL_LIST_SUBCATEGORY ?>';
    var url_list_subcategories_final = `${url_base}${url_list_subcategories}`;

    Vue.component('setting-category', {
        template: '#template-setting-category',
        mixins: [common_mixin],
        data: function () {

            return{
                flashMessage:'',
                alert: false,
                dialog: false,
                dialog_title: '',
                dialog_remove: false,
                rowsPerPageItems: [6, 12, 24],
                pagination: {
                    rowsPerPage: 6
                },
                items: [],
                tile: false,
                avatarsize: 115,
                selectedFile: null,
                label: null,
                errormessage: '',
                urlBaseImage: url_base,
                selected_for_delete: undefined,
                class_name_state: '',
                file_name_update: '',
                action_type: '',
                old_file_name_update: '',
                selected_for_update: undefined,
                icon: 'justify',
                activelist: 'list_edit',
                edit_class: 'v-btn--active',
                list_class: '',
                all_subcategories: [],
                user_logged_in: undefined,
            }

        },
        methods: {
            wsCategory(){
                this.data_loader = true;
                axios.get(url_list_category_final).then((response) => {
                    if(response.data.category == null){
                        this.items = [];
                    }else{
                        //console.log(`CATEGORIES ARE ====>${JSON.stringify(response.data.category)}`)
                        var arr_rowsPerPageItems = [];
                        arr_rowsPerPageItems.push(response.data.category.length);
                        this.rowsPerPageItems = arr_rowsPerPageItems;
                        this.pagination.rowsPerPage = response.data.category.length;
                        this.items = response.data.category;
                    }
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            wsSubcategories() {
                window.axios.get(url_list_subcategories_final).then((response) => {
                    if(response.data.subcategory == null){
                        console.log('SUBCATEGORIES NULL');
                        this.all_subcategories = [];
                    }else{
                        //console.log(`SUBCATEGORIES ARE ====>${JSON.stringify(response.data.subcategory)}`);
                        this.all_subcategories = response.data.subcategory;
                    }
                })
                .catch(err => {
                    console.log(err)
                })
            },
            addCategory(){
                console.log(`LABEL======>${this.label}`);
                if(this.label && this.selectedFile && this.selectedFile.name){
                    this.data_loader = true;
                    this.errormessage = '';
                    var formData = new FormData();
                    formData.append('image_category', this.selectedFile);
                    formData.append('label_category', this.label);
                    axios.post(url_upload_image_category_final, formData).then((response) =>{
                        console.log(`SUCCESS UPLOAD====>${response.data.success_upload} SUCCESS INSERT====>${response.data.success_insert}`);
                        if (response.data.success_upload && response.data.success_insert) {
                            //alert("SUCCESS");
                            this.dialog = false;
                            this.manageFlashMSG("Votre catégorie a été ajouté avec succès !");

                        }
                        if (response.data.error) {
                            //alert(`ERROR====>${response.data.error}`);
                            this.errormessage = response.data.error;
                        }
                    })
                    .finally(() => {

                        this.closeLoaderAfterDelay();

                    })
                }else{
                    this.errormessage = "Veuillez completer les champs";
                }

            },
            deleteCategory(){

                this.dialog_remove = false;
                this.data_loader = true;
                var formData = new FormData();
                console.log(`CATEG_TO_DELETE : ${JSON.stringify(this.selected_for_delete)}`)
                formData.append('image_category', this.selected_for_delete.categoryImage);
                formData.append('id_category', this.selected_for_delete.id);
                axios.post(url_delete_category_final, formData).then((response) =>{
                    console.log(`SUCCESS DELETE====>${response.data.msg}`);
                    if (response.data.file_deleted && !response.data.error) {

                        this.manageFlashMSG("Votre catégorie a été supprimée avec succès !");

                    }
                    if (response.data.error) {

                        alert(response.data.msg);

                    }
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })

            },
            updateCategory(){

                if(this.label){

                    this.data_loader = true;
                    this.errormessage = '';
                    var formData = new FormData();

                    formData.append('id_category', this.selected_for_update.id);
                    formData.append('label_category', this.label);
                    formData.append('new_image_name_category', this.file_name_update);
                    formData.append('old_image_name_category', this.old_file_name_update);

                    if(this.selectedFile && this.selectedFile.name){
                        console.log("========>UPDATE BOTH CATEGORY LABEL & IMAGE");
                        formData.append('has_image', 'TRUE');
                        formData.append('image_category', this.selectedFile);

                    }else{
                        console.log("========>UPDATE ONLY CATEGORY LABEL");
                        formData.append('has_image', 'FALSE');
                        formData.append('image_category', null);
                    }

                    axios.post(url_update_category_final, formData).then((response) =>{
                        console.log(`CODE ERROR EDIT====>${response.data.code_error}`);
                        if (!response.data.error) {

                            this.dialog = false;
                            this.manageFlashMSG("Votre catégorie a été modifiée avec succès !");

                        }else{
                            alert("ERROR");
                        }

                    })
                    .finally(() => {

                        this.closeLoaderAfterDelay();

                    })

                }else{
                    this.errormessage = "Veuillez completer la catégorie !";
                }

            },
            distinctAction: function() {
                if(this.action_type === '<?php echo CONSTANT_DB_INSERT ?>'){
                    this.addCategory();
                }else if(this.action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    this.updateCategory();
                }
            },
            onDeleteSelected(item){
                this.selected_for_delete = item;
                this.dialog_remove = true;
            },
            onFileSelected(event){
                this.selectedFile = event.target.files[0];
                if(this.action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    this.file_name_update = this.selectedFile.name;
                }
            },
            manageFlashMSG(message){
                this.flashMessage = message;
                this.alert = true;
                setTimeout(() =>{
                    this.alert = false;
			        this.wsCategory();
			    },  3000); // disappearing message success in 3 sec
            },
            goToSub(subId, image_url, sub_label, catId){
                //this.$router.push('/subcategories');
                this.$router.push({ path: `/subcategories/${subId}`, query:{image: image_url, label: sub_label, categoryid: catId, userlogged: this.user_logged_in} });

            },
            showUpdate(cat){

                this.selected_for_update = cat;
                this.dialog_title = "Modifier une catégorie";
                this.label = cat.categoryLabel;
                //set input file image programmatically
                this.class_name_state = 'hidden-placeholder';
                this.dialog = true;
                this.errormessage =  '';
                this.file_name_update = cat.categoryImage;
                this.old_file_name_update = cat.categoryImage;
                this.action_type = '<?php echo CONSTANT_DB_UPDATE ?>';
                this.selectedFile = null;

            },
            showAdd(){

                //reset input file if non null (after open update)
                if(document.querySelector('#file')){
                    document.querySelector('#file').value = '';
                }
                this.selectedFile = null;
                this.dialog_title = "Créer une catégorie";
                this.class_name_state = "";
                this.file_name_update = "";
                this.dialog = true;
                this.label = null;
                this.errormessage =  '';
                this.action_type = '<?php echo CONSTANT_DB_INSERT ?>';

            },
            updateSegmentClass(actif){
                if(actif === 'list_edit'){
                    this.edit_class = 'v-btn--active'
                    this.list_class = '';
                }else if(actif === 'list_detailed'){
                    this.edit_class = '';
                    this.list_class = 'v-btn--active'
                }
            },
            changeListDisplay(actif){
                this.activelist = actif;
                this.updateSegmentClass(actif);
            },
            countCharOK(str){
                if(str.length > 22)
                    return false;
                return true;
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`CATEGORIES PAGE USER LOGGED IN ===> ${JSON.stringify(connexion)}`);
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }
                    this.wsCategory();
                    this.wsSubcategories();
                }else{
                    console.log("CATEGORIES PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            }

        },
        computed: {
            avatarSize () {
                return `${this.avatarsize}px`
            },

        },
        created: function () {
            //this.$router.app.$emit('updateWelcomeSubtitle', undefined);
        },
        mounted: function () {
            console.log('categories : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        }

    });

</script>