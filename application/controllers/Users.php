<?php

require APPPATH . '/util/UtilDateTime.php';

class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
    }

    public function index()
    {
        $this->load->view('welcome');
    }

    public function register()
    {
        $result['error'] = true;
        $result['code'] = 0;
        $result['msg'] = 'erreur lors de la création';
        $email = $this->input->post('email');
        $username = $this->input->post('username');

        $result = $this->user->isExist($username, $email);
        if ($result && $result[0]) {
            $result['error'] = true;
            $result['code'] = -1;
            $result['inserted_id'] = $result[0]->id;
            $result['msg'] = 'User already exist';
        } else {
            $utilDate = new UtilDateTime();
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $email,
                'username' => $username,
                'password' => md5($this->input->post('password')),
                'company' => $this->input->post('company'),
                'job' => $this->input->post('job'),
                'phone' => $this->input->post('phone'),
                'role' => $this->input->post('role'),
                'admin_id' => $this->input->post('admin_id'),
                'register_date' => $utilDate->getDatetimeNow(),
            );
            $result['tosend'] = $data;
            if ($this->user->addUser($data)) {
                $result['error'] = false;
                $result['code'] = 1;
                $result['msg'] = 'User added successfully';
            }
        }
        echo json_encode($result);
    }

    public function showByAdmin()
    {
        $result['users'] = [];
        $adminId = $this->input->post('id_admin');
        $query = $this->user->showByAdmin($adminId);
        if ($query) {
            $result['users'] = $query;
        }
        echo json_encode($result);
    }

    public function searchUser()
    {
        $value = $this->input->post('text');
        $query = $this->user->searchUser($value);
        if ($query) {
            $result['users'] = $query;
        }
        echo json_encode($result);
    }

    public function deleteUser()
    {
        $result['error'] = true;
        $result['msg'] = 'error when deleting!';
        $userId = $this->input->post('id_user');
        $deleted = $this->user->deleteUser($userId);
        if ($deleted) {
            $result['error'] = false;
            $result['msg'] = 'success deleting!';
        }
        echo json_encode($result);
    }

    public function updateUser()
    {
        $result['error'] = true;
        $result['msg'] = 'error when updating!';

        $id = $this->input->post('id_user');
        $has_image = $this->input->post('has_image');
        $actual_password = $this->input->post('password');

        if ($has_image === 'TRUE') {

            $config['upload_path'] = './uploads/utilisateurs/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 500;
            $config['max_width'] = 2500;
            $config['max_height'] = 1400;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image_user')) {
                $result['msg'] = 'Erreur lors du upload';
            } else {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'email' => $this->input->post('email'),
                    'company' => $this->input->post('company'),
                    'job' => $this->input->post('job'),
                    'phone' => $this->input->post('phone'),
                    'user_image' => $file_name,
                );
                if ($actual_password !== 'placeholder') {
                    $data['password'] = md5($actual_password);
                }
                $is_updated = $this->user->updateUser($data, $id);
                if ($is_updated) {
                    $result['error'] = false;
                }

            }
        } else {

            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'company' => $this->input->post('company'),
                'job' => $this->input->post('job'),
                'phone' => $this->input->post('phone'),
            );
            if ($actual_password !== 'placeholder') {
                $data['password'] = md5($actual_password);
            }
            $is_updated = $this->user->updateUser($data, $id);
            if ($is_updated) {
                $result['error'] = false;
            }

        }
        echo json_encode($result);
    }

    public function selectUser()
    {
        $result['user'] = false;
        $userId = $this->input->post('id_user');
        $query = $this->user->selectUser($userId);
        if ($query) {
            $result['user'] = $query[0];
        }
        echo json_encode($result);
    }

    //--------------------------------------SESSION-----------------------------
    public function saveUserChoosedSession()
    {
        $result['saved'] = false;
        $userId = $this->input->post('id');
        $data = $this->user->selectUser($userId);
        $this->session->set_userdata('chooseduser', $data);
        //$this->session->set_userdata('user', $data);
        $result['saved'] = true;
        $result['content'] = $data;
        echo json_encode($result);
    }

}
