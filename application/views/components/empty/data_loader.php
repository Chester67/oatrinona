<?php

?>

<style type="text/css">
    
.modalui {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: #757575;
    display: flex;
    justify-content: center;
    align-items: center;
    opacity: 0.8;
    z-index: 1;
}

.spinner {
  width: 60px;
  height: 60px;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 10;
  animation: rotate 1.4s infinite ease-in-out, background 1.4s infinite ease-in-out alternate;
}

@keyframes rotate {
  0% {
    transform: perspective(120px) rotateX(0deg) rotateY(0deg);
  }
  50% {
    transform: perspective(120px) rotateX(-180deg) rotateY(0deg);
  }
  100% {
    transform: perspective(120px) rotateX(-180deg) rotateY(-180deg);
  }
}
@keyframes background {
  0% {
  background-color: #78909C;
  }
  50% {
    background-color: #455A64;
  }
  100% {
    background-color: #263238;
  }
}

</style>

<script type="text/x-template" id="template-data-loader">

    <v-flex xs12 class="modalui">
            
            <div class="spinner"></div>
        
    </v-flex>
    
</script>

<script type="text/javascript">

    Vue.component('data-loader', {
        template: '#template-data-loader',
        data: function () {
            
            return{
                title: '',
            }
            
        },
                
    });

</script>