<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="<?php echo base_url(); ?>assets/js/vue/dev/vue.js"></script>
        <link href="<?php echo base_url(); ?>assets/js/vuetify/vuetify.min.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/vuetify/vuetify.js"></script>
    </head>

    <?php $this->load->view('components/login/login');?>

    <body>	
        
        <div id="vue-container">
            
            <v-app>
                <v-content>
                    <v-container>Hello world</v-container>
                    <ul>
                        <li v-for="ob in logindata">

                        <div>
                            <label>{{ ob }}</label>
                        </div>

                        </li>
                    </ul>
                    <a href="#" v-on:click="signin()">Login</a>
                    <page-login></page-login>

                </v-content>
                
            </v-app>
            
        </div>

        <script type="text/javascript">
            new Vue({
                el: '#vue-container',
                data: {
                    message: '',
                    logindata: [],
                },
                methods:{
                    signin: function(){
                        this.logindata.push(
                            {
                                thelogin: 'exemple',
                            }
                        )
                    },
                }
                
            });
        </script>
    
    </body>
</html>

