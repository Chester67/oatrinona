<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html>
    <head>

        <title>Oatrinona</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">-->
        <script src="<?php echo base_url(); ?>assets/js/vue/dev/vue.js"></script>
        <link href="<?php echo base_url(); ?>assets/js/vuetify/vuetify.min.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/vuetify/vuetify.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vue-router/vue-router.js"></script>
        <script src="<?php echo base_url() ?>assets/js/axios/axios.min.js"></script>
        <!--<script src="<?php echo base_url() ?>assets/js/moment/moment.min.js"></script>-->
        <script src="<?php echo base_url() ?>assets/js/moment/moment-2.8.1.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/fullcalendar/fullcalendar.js"></script>
        <script src="<?php echo base_url() ?>assets/js/fullcalendar/fr.js"></script>
        <script src="<?php echo base_url() ?>assets/js/chart/Chart.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/chart/vue-chartjs.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/googlemap/vue-google-maps.js"></script>
        <script src="<?php echo base_url() ?>assets/js/chrono/index.js"></script>
        <!--<script src="https://unpkg.com/vue-meta@1.5.8/lib/vue-meta.js"></script>-->
        <script src="<?php echo base_url() ?>assets/js/meta/vue-meta.js"></script>
        <link href="<?php echo base_url(); ?>assets/css/welcome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/fullcalendar/fullcalendar.min.css" rel="stylesheet">

    </head>

    <?php
$this->load->view('components/login/login');
$this->load->view('components/aliment/aliments');
$this->load->view('components/users/users');
$this->load->view('components/pack/day');
$this->load->view('components/pack/daytask');
$this->load->view('components/pack/daytasklist');
$this->load->view('components/pack/realtime');
$this->load->view('components/pack/repeat');
$this->load->view('components/settings/categories');
$this->load->view('components/settings/subcategories');
$this->load->view('components/settings/tasks');
$this->load->view('components/settings/subtasks');
$this->load->view('components/report/report_category');
$this->load->view('components/report/report_day');
$this->load->view('components/report/report_spent_global');
$this->load->view('components/propos/detail');
$this->load->view('components/common/common-mixin');
$this->load->view('components/profil/user_profil');
$this->load->view('components/taskmanager/kanban');
$this->load->view('components/taskmanager/planning');
$this->load->view('components/taskmanager/kanbandaytask');
?>

    <body>

        <div id="vue-container-welcome">
        <!--<login></login>-->
            <v-app>
                <v-navigation-drawer
                    v-if="!data_no_user"
                    :clipped="$vuetify.breakpoint.lgAndUp"
                    v-model="drawer"
                    fixed
                    style="background: #FAFAFA; z-index: 10;"
                    app>

                    <!--<div>{{ JSON.stringify(selectedUser) }}</div>-->

                    <div style="display: flex; flex-direction: row; justify-content: flex-start; height: 78px; padding-left: 16px; padding-top: 35px; background: #757575; box-shadow: 0 6px 6px 0 rgba(0, 0, 0, 0.2), 0 0px 0px 0 rgba(0, 0, 0, 0.19);">
                        <v-avatar
                            :tile="tile"
                            :size="avatarSize"
                            color="transparent"
                            >
                            <!--<img src="https://via.placeholder.com/150x150" alt="">-->
                            <template v-if="selectedUser.user_image && !!selectedUser.user_image">
                                <img :src="urlBaseImage+'uploads/utilisateurs/'+selectedUser.user_image" alt="" >
                            </template>
                            <template v-else>
                                <v-icon style="font-size: 38px; background: #CFD8DC">face</v-icon>
                            </template>
                        </v-avatar>
                        <div style="display: flex; flex-direction: column; justify-content: center; margin-left: 19px; ">
                            <h4 class="text-xs-left" style="color: white">{{ selectedUser.first_name }}</h4>
                            <span class="text-xs-left" style="color: white">{{ selectedUser.last_name }}</span>
                        </div>

                    </div>

                    <v-list dense style="margin-top: 60px">
                        <template v-for="item in items">
                            <v-layout
                                v-if="item.heading"
                                :key="item.heading"
                                row
                                align-center>
                                <v-flex xs6>
                                <v-subheader v-if="item.heading">
                                    {{ item.heading }}
                                </v-subheader>
                                </v-flex>
                                <v-flex xs6 class="text-xs-center">
                                <a href="#!" class="body-2 black--text">EDIT</a>
                                </v-flex>
                            </v-layout>
                            <v-list-group
                                v-else-if="item.children"
                                v-model="item.model"
                                :key="item.text"
                                :prepend-icon="item.model ? item.icon : item['icon-alt']"
                                append-icon="">
                                <v-list-tile slot="activator">
                                    <v-list-tile-content>
                                        <v-list-tile-title class="menu-text-font">
                                        {{ item.text }}
                                        </v-list-tile-title>
                                    </v-list-tile-content>
                                </v-list-tile>
                                <v-list-tile
                                    :style="getMenuItemStyle(child)"
                                    v-for="(child, i) in item.children"
                                    :key="i"
                                    @click="refreshSessionAndGoto(child)">
                                    <v-list-tile-action v-if="child.icon">
                                        <v-icon>{{ child.icon }}</v-icon>
                                    </v-list-tile-action>
                                    <v-list-tile-content>
                                        <v-list-tile-title>
                                            <p class="sous-menu-text-font">{{ child.text }}</p>
                                        </v-list-tile-title>
                                    </v-list-tile-content>
                                </v-list-tile>
                            </v-list-group>
                            <v-list-tile :style="getMenuItemStyle(item)" v-else :key="item.text" @click="refreshSessionAndGoto(item)">
                                <v-list-tile-action>
                                    <v-icon>{{ item.icon }}</v-icon>
                                </v-list-tile-action>
                                <v-list-tile-content>
                                    <v-list-tile-title>
                                        <span class="menu-text-font">{{ item.text }}</span>
                                    </v-list-tile-title>
                                </v-list-tile-content>
                            </v-list-tile>
                        </template>
                    </v-list>
                </v-navigation-drawer>
                <v-toolbar
                    :clipped-left="$vuetify.breakpoint.lgAndUp"
                    color="grey darken-1"
                    dark
                    app
                    fixed>

                    <v-toolbar-title style="width: 365px; float: left" class="ml-0 pl-3">
                        <v-toolbar-side-icon v-if="!data_no_user" @click.stop="drawer = !drawer"></v-toolbar-side-icon>
                        <img src="<?php echo base_url(); ?>assets/img/oatrinona_logo.png" alt="logo" width="30" style="vertical-align: middle">

                    </v-toolbar-title>

                    <!--<v-spacer></v-spacer>-->
                    <div v-if="!data_no_user" class="hidden-sm-and-down" style="vertical-align: middle; line-height: 100%; display: inline-block; width:100%;">

                        <v-chip
                            v-if="typeof selectedTitleParent !== 'undefined'"
                            class="white--text"
                            style="height: 33px; background: #BDBDBD">

                            <v-icon left>mdi-coin</v-icon>
                            <span style="font-weight: bold; font-size: 15px; color: #455A64;">{{ selectedTitleParent }}</span>

                        </v-chip>
                        <v-chip
                            v-if="typeof selectedTitle !== 'undefined'"
                            class="white--text"
                            style="height: 33px; background: #BDBDBD">

                            <v-icon left>mdi-coin</v-icon>
                            <span style="font-weight: bold; font-size: 15px; color: #455A64;">{{ selectedTitle }}</span>

                        </v-chip>
                        <v-chip
                            v-if="typeof selectedSubTitle !== 'undefined'"
                            class="white--text"
                            style="height: 33px; background: #BDBDBD">

                            <v-icon left>mdi-coin</v-icon>
                            <span style="font-weight: bold; font-size: 15px; color: #455A64;">{{ selectedSubTitle }}</span>

                        </v-chip>
                    </div>

                    <div v-if="user_logged_in && selectedUser" class="hidden-sm-and-down" style="position: absolute; right: 130px">
                        <template v-if="user_logged_in.id !== selectedUser.id">
                            <span style="font-weight: bold">(Administrateur) {{ user_logged_in.first_name }} {{ user_logged_in.last_name }}</span>  > {{ selectedUser.first_name }} {{ selectedUser.last_name }}
                        </template>
                        <template v-else>
                            <span style="font-weight: bold">(Administrateur) {{ user_logged_in.first_name }} {{ user_logged_in.last_name }}</span>
                        </template>
                    </div>
                    <!--div v-else-if="selectedUser" class="hidden-sm-and-down" style="position: absolute; right: 130px">
                        {{ selectedUser.first_name }} {{ selectedUser.last_name }}
                    </div-->
                    <div style="float: right;">
                      <div style="display: flex; flex-direction: row">
                        <v-menu v-if="!data_no_user" offset-y>
                            <v-btn icon slot="activator">
                                <v-icon>notifications</v-icon>
                                <div v-if="notif_number > 0" class="numberCircle">{{ notif_number }}</div>
                            </v-btn>

                            <v-list subheader v-if="notif_tasks.length > 0">
                                <v-subheader>Assigné à moi</v-subheader>
                                <v-list-tile v-for="(item, i) in notif_tasks" :key="i" @click="manageNotifContextualMenu(item)">
                                    <v-list-tile-content>
                                        <v-list-tile-title>
                                            <span class="menu-text-font">{{ getTaskPhrase(item.numbtask, item.date) }}</span>
                                        </v-list-tile-title>
                                    </v-list-tile-content>
                                </v-list-tile>
                            </v-list>
                            <v-list v-else>
                                <v-list-tile v-for="(item, i) in notif_task_empty" :key="i">
                                    <v-list-tile-content>
                                        <v-list-tile-title>
                                            <span class="menu-text-font">{{ item.title }}</span>
                                        </v-list-tile-title>
                                    </v-list-tile-content>
                                </v-list-tile>
                            </v-list>

                        </v-menu>
                        <!--<v-btn v-if="!data_no_user" icon large>
                            <v-avatar size="32px" tile>
                            <img
                                src="https://cdn.vuetifyjs.com/images/logos/logo.svg"
                                alt="Vuetify"
                            >
                            </v-avatar>
                        </v-btn>-->
                        <v-menu v-if="!data_no_user" offset-y style="z-index: 99 !important;">
                            <v-btn
                                slot="activator"
                                dark
                                icon>
                                <v-icon>account_circle</v-icon>
                            </v-btn>
                            <v-list>
                                <v-list-tile v-for="(item, i) in menus" :key="i" @click="manageUserContextualMenu(item)">
                                    <v-list-tile-action>
                                        <v-icon>{{ item.icon }}</v-icon>
                                    </v-list-tile-action>
                                    <v-list-tile-content>
                                        <v-list-tile-title>
                                            <span class="menu-text-font">{{ item.title }}</span>
                                        </v-list-tile-title>
                                    </v-list-tile-content>
                                </v-list-tile>
                            </v-list>
                        </v-menu>
                      </div>
                    </div>
                </v-toolbar>
                <v-content>
                    <router-view></router-view>
                </v-content>


            </v-app>

        </div>

        <script type="text/javascript">

            //urls
            var url_base = '<?php echo base_url() ?>';

            var url_logout = '<?php echo URL_LOGOUT ?>';
            var url_logout_final = `${url_base}${url_logout}`;

            var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
            var url_is_logged_final = `${url_base}${url_is_logged}`;

            var url_save_menu = '<?php echo URL_SAVE_MENU ?>';
            var url_save_menu_final = `${url_base}${url_save_menu}`;

            var url_get_menu = '<?php echo URL_GET_MENU ?>';
            var url_get_menu_final = `${url_base}${url_get_menu}`;

            var url_check_notification_task = '<?php echo URL_CHECK_NOTIFICATION ?>';
            var url_check_notification_task_final = `${url_base}${url_check_notification_task}`;

            var url_get_notification_task = '<?php echo URL_GET_NOTIFICATION ?>';
            var url_get_notification_task_final = `${url_base}${url_get_notification_task}`;

            //routes
            const Login = { template: '<page-login></page-login>' }
            const Aliment = { template: '<aliments></aliments>' }
            const User = { template: '<users url="<?php echo base_url(); ?>" ></users>' }
            const DayExpense = { template: '<day-expense></day-expense>' }
            const DayTask = { template: '<day-task></day-task>'}
            const TimeExpense = { template: '<time-expense></time-expense>' }
            const Categories = { template: '<setting-category></setting-category>' }
            const Subcategories = { template: '<setting-subcategory></setting-subcategory>' }
            const Subtasks = { template: '<setting-subtasks></setting-subtasks>' }
            const Tasks = { template: '<setting-tasks></setting-tasks>'}
            const ReportCategory = { template: '<report-category></report-category>' }
            const ReportDay = { template: '<report-day></report-day>' }
            const ReportSpentGlobal = { template: '<report-spent-global></report-spent-global>' }
            const AppDetail = { template: '<application-detail></application-detail>'}
            const UserProfil = { template: '<user-profil></user-profil>' }
            const DayTaskList = { template: '<day-tasklist></day-tasklist>' }
            const RepeatExpense = { template: '<expense-repeat></expense-repeat>'}
            const Kanban = { template: '<kanban></kanban>'}
            const Planning = { template: '<task-planning></task-planning>'}
            const KanbanDayTask = { template: '<kanban-day-task></kanban-day-task>'}

            const router = new VueRouter({
                mode: 'history',
                base: '',
                routes: [
                    { path: '/login', component: Login },
                    { path: '/propos', component: AppDetail },
                    { path: '/aliments', component: Aliment },
                    { path: '/users', component: User },
                    { path: '/day', component: DayExpense },
                    { path: '/day/task', component: DayTask },
                    { path: '/day/tasklist', component: DayTaskList },
                    { path: '/realtime', component: TimeExpense },
                    { path: '/users/currentuser', component: User },
                    { path: '/categories', component: Categories },
                    { path: '/subcategories/:subId', component: Subcategories, name: 'subcategories' },
                    { path: '/subtasks/:taskId', component: Subtasks, name: 'subtasks' },
                    { path: '/tasks', component: Tasks },
                    { path: '/report/category', component: ReportCategory },
                    { path: '/report/day', component: ReportDay },
                    { path: '/report/spentglobal', component: ReportSpentGlobal },
                    { path: '/userprofil', component: UserProfil },
                    { path: '/repeat', component: RepeatExpense },
                    { path: '/kanban', component: Kanban },
                    { path: '/planning', component: Planning },
                    { path: '/kanbandaytask', component: KanbanDayTask },
                    { path: '*', redirect: '/login'}
                ],

            })

            //chrono
            Vue.use(window.crono);

            new Vue({
                router,
                el: '#vue-container-welcome',
                mixins: [common_mixin],
                data: {
                    user_logged_in: undefined,
                    dialog: false,
                    drawer: null,
                    selectedUser: undefined,
                    items: [],
                    tile: false,
                    avatarsize: 76,
                    menus: [
                        { id: 1, icon: 'face', title: 'Profil' },
                        { id: 2, icon: 'power_settings_new', title: 'Deconnexion' },
                    ],
                    notif_tasks: [],
                    notif_task_empty: [{ id: -1, title: 'Aucune notification !'}],
                    notif_number: 0,
                    urlBaseImage: url_base,
                    selectedTitle: "A propos",
                    selectedTitleParent: undefined,
                    selectedSubTitle: undefined,
                    cronRunning: false,
                    notifdatesIds: []
                },
                methods:{
                    chooseUser: function(selectedUser){
                        alert("selectedUser");
                        //this.$router.app.$on('productStored', () => { alert("none"); })
                    },
                    refreshSessionAndGoto: function(item){
                        this.$router.push(item.href);
                        console.log(`=======>selected id: ${item.id}`);
                        this.items = this.deselectOther(this.items, item.id);
                        //refresh entire page
                        //this.$router.go(phref);
                    },
                    logout: function(){
                        axios.get(url_logout_final).then((response) => {
                            if(!response.data.error){
                                console.log("LOGOUT SUCCESSFULL!");
                                this.$router.push('/login');
                                //update menu state
                                this.checkIfLogged(url_is_logged_final);
                                //reset userloggedin
                                this.user_logged_in = undefined;
                            }
                        });
                    },
                    afterCheckLogged(connexion){
                        if(connexion.is_logged){
                            console.log("WELCOME PAGE USER LOGGED IN");
                            //-------------MENU------------------------
                            if(connexion.user_logged[0].role === 'USER'){
                                //remove menu utilisateurs
                                var array = this.getMenu();
                                for(var i = array.length - 1; i >= 0; i--) {
                                    if(array[i].icon === 'people') {
                                        array.splice(i, 1);
                                    }
                                }
                                this.items = array;
                                console.log("USER TSOTRA");
                            }else if(connexion.user_logged[0].role === 'ADMINISTRATOR'){
                                this.items = this.getMenu();
                                this.user_logged_in = connexion.user_logged[0];
                                console.log("USER admin");
                            }

                            this.getLastMenu();

                            //-------------CHOSED USER------------------
                            if(connexion.user_logged[0].role === 'ADMINISTRATOR'){
                                this.selectedUser = connexion.user_choosed[0];
                            }else{
                                this.selectedUser = connexion.user_logged[0];
                            }
                            this.$forceUpdate();

                            //LOAD CRONO TO CHECK UPDATED TASK NOTIFICATION
                            this.checkNotificationTask(this.selectedUser.id);
                            this.startTimer();

                        }else{
                            console.log("WELCOME PAGE NO USER LOGGED");
                            this.$router.push('/login');
                        }
                    },
                    getMenu: function(){

                        let menus = [
                            { id: 1, icon: 'info', text: "A propos", href:'/propos' },
                            { id: 2, icon: 'people', text: "Utilisateurs", href:'/users' },
                            {
                                id: -1,
                                icon: 'keyboard_arrow_up',
                                'icon-alt': 'keyboard_arrow_down',
                                text: 'Rapports',
                                model: false,
                                children: [
                                    { id: 3, icon: 'date_range', text: 'Rapport par date', href:'/report/day', parent: "Rapports" },
                                    { id: 4, icon: 'insert_chart', text: 'Rapport total par mois', href:'/report/spentglobal', parent: "Rapports" },
                                    { id: 5, icon: 'pie_chart', text: 'Rapport par catégorie', href:'/report/category', parent: "Rapports" }
                                ]
                            },
                            {
                                id: -1,
                                icon: 'keyboard_arrow_up',
                                'icon-alt': 'keyboard_arrow_down',
                                text: 'Dépenses',
                                model: false,
                                children: [
                                    { id: 6, icon: 'money_off', text: 'Dépense journalière simple', href:'/day', parent: "Dépenses" },
                                    { id: 7, icon: 'monetization_on', text: 'Dépense journalière par tâches', href:'/day/tasklist', parent: "Dépenses" },
                                    { id: 8, icon: 'all_inclusive', text: 'Dépense répétitif', href:'/repeat', parent: "Dépenses" },
                                ]
                            },
                            {
                                id: -1,
                                icon: 'keyboard_arrow_up',
                                'icon-alt': 'keyboard_arrow_down',
                                text: 'Paramètre',
                                model: false,
                                children: [
                                    { id: 9, icon: 'dashboard', text: 'Catégories', href:'/categories', parent: "Paramètre" },
                                    { id: 10, icon: 'assignment', text: 'Tâches', href:'/tasks', parent: "Paramètre" },
                                    //{ icon: 'add', text: 'Dépense en temps réel', href:'/realtime' },
                                ]
                            },
                            { id: 11, icon: 'view_column', text: "Gestion des tâches", href:'/kanban' },
                            { id: 12, icon: 'date_range', text: "Planning", href:'/planning' },
                        ];
                        return menus;

                    },
                    deselectOther(array, menuid){
                        this.saveLastMenu(menuid);
                        for(var i = array.length - 1; i >= 0; i--) {
                            if(array[i].id == menuid){
                                array[i].selected = true;
                                this.selectedTitle = array[i].text;
                                this.selectedTitleParent = undefined;
                            }else{
                                array[i].selected = false;
                                if(array[i].children){
                                    for(var j = 0; j < array[i].children.length; j++){
                                        if(array[i].children[j].id == menuid){
                                            array[i].children[j].selected = true;
                                            this.selectedTitle = array[i].children[j].text;
                                            this.selectedTitleParent = array[i].children[j].parent;
                                        }else{
                                            array[i].children[j].selected = false;
                                        }
                                    }
                                }
                            }
                        }
                        return array;
                    },
                    getMenuItemStyle(item){
                        var stylecontent = "background: transparent;";
                        if(item.selected == true){
                            stylecontent = "background: #CFD8DC;";
                        }
                        return stylecontent;
                    },
                    saveLastMenu(menuid){
                        var formData = new FormData();
                        formData.append('menu_id', menuid);
                        axios.post(url_save_menu_final, formData).then((response) => {
                            console.log("last menu saved");
                        });
                    },
                    getLastMenu(){
                        axios.get(url_get_menu_final).then((response) => {
                            if(!response.data.error){
                                this.items = this.deselectOther(this.items, response.data.menu);
                                this.$forceUpdate();
                                var parent_to_select = 0;
                                let item_id = parseInt(response.data.menu);

                                if(this.selectedUser.role === 'ADMINISTRATOR'){
                                    if(item_id == 3 || item_id == 4 || item_id == 5)
                                        parent_to_select = 2;
                                    else if(item_id == 6 || item_id == 7 || item_id == 8)
                                        parent_to_select = 3;
                                    else if(item_id == 9 || item_id == 10)
                                        parent_to_select = 4;
                                }else if(this.selectedUser.role === 'USER'){
                                    if(item_id == 3 || item_id == 4 || item_id == 5)
                                        parent_to_select = 1;
                                    else if(item_id == 6 || item_id == 7 || item_id == 8)
                                        parent_to_select = 2;
                                    else if(item_id == 9 || item_id == 10)
                                        parent_to_select = 3;
                                }
                                setTimeout(() =>{
                                    this.items[parent_to_select].model = true;
                                },  1500);

                            }
                        });
                    },
                    manageUserContextualMenu(item){
                        if(item.id == 1){
                            //Profil
                            this.goToUserProfil();
                        }else if(item.id == 2){
                            //Deconexion
                            this.notif_tasks = [];
                            this.stopTimer();
                            this.logout();
                        }
                    },
                    goToUserProfil(){
                        this.items = this.deselectOther(this.items, -2);
                        this.$router.push('/userprofil');
                    },
                    checkNotificationTask(user){
                        var formData = new FormData();
                        formData.append('user_id', user);
                        axios.post(url_check_notification_task_final, formData).then((response) => {
                            console.log(`RESPONSE CHECK NOTIF ====> ${JSON.stringify(response.data.notif)}`);
                            var dates_ids = this.takeAllDateIdTask(response.data.notif);
                            console.log(`dates_ids ====> ${JSON.stringify(dates_ids)}`);
                            var dates_task = [];
                            /*if(this.notifdatesIds && this.notifdatesIds.length > 0){
                                for(var i= 0; i < dates_ids.length; i++){
                                    if(!this.checkDuplicatedNotification(dates_ids[i], this.notifdatesIds)){

                                        this.notifdatesIds.push(dates_ids[i]);
                                        dates_task.push(this.takeDateTaskById(response.data.notif, dates_ids[i]));

                                    }
                                }
                            }else{
                                for(var i= 0; i < dates_ids.length; i++){
                                    this.notifdatesIds.push(dates_ids[i]);
                                    dates_task.push(this.takeDateTaskById(response.data.notif, dates_ids[i]));
                                }
                            }*/
                            //console.log(`notifdatesIds ====> ${JSON.stringify(this.notifdatesIds)}`);
                            for(var i= 0; i < dates_ids.length; i++){
                                dates_task.push(this.takeDateTaskById(response.data.notif, dates_ids[i]));
                            }
                            if(dates_task.length > 0){
                                this.notif_tasks = [];
                                console.log(`dates_task ====> ${JSON.stringify(dates_task)}`);
                                var dates_task_filtered = dates_task.filter((v,i) => dates_task.indexOf(v) === i);
                                for(var i = 0; i < dates_task_filtered.length; i++) {
                                    this.notif_tasks.push({ date: dates_task_filtered[i], numbtask: this.countOccurenceOf(dates_task, dates_task_filtered[i]) });
                                }
                                this.notif_number = this.notif_tasks.length;
                                this.$forceUpdate();
                            }else{
                                this.notif_tasks = [];
                                this.notif_number = 0;
                            }
                            console.log(`dates_task_filtered ====> ${JSON.stringify(this.notif_tasks)}`);
                        });
                    },
                    takeDateTaskById: function(arr, id){
                        res = '';
                        for(var i= 0; i < arr.length; i++){
                            if(arr[i].id === id)
                                res = this.formatDateToMMDDYYYY(arr[i].eventDebut);
                        }
                        return res;
                    },
                    checkDuplicatedNotification: function(notif_id, arr){

                        for(var i= 0; i < arr.length; i++){
                            console.log(`ITEM=======>${arr[i]}`);
                            if(arr[i] == notif_id){
                                console.log("ENTER IN");
                                return true;
                            }
                        }
                        return false;

                    },
                    getTaskPhrase: function(number, date){
                        if(number > 1)
                            return `${number} tâches le ${date}`;
                        else
                            return `${number} tâche le ${date}`;
                    },
                    manageNotifContextualMenu: function(item){
                        //this.getNotificationTask(this.selectedUser.id, '2019-03-07');
                        this.items = this.deselectOther(this.items, 11);
                        //this.$router.push('/kanban');
                        console.log(this.$route.path);
                        if(this.$route.path === '/kanban')
                            this.$router.app.$emit('updateKanbanDate', item.date);
                        else
                            this.$router.replace({ path: '/kanban', query:{date: item.date} });
                    },
                    getNotificationTask(user, date){
                        var formData = new FormData();
                        formData.append('user_id', user);
                        formData.append('notif_date', date);
                        axios.post(url_get_notification_task_final, formData).then((response) => {
                            console.log(`RESPONSE GET NOTIF ====> ${JSON.stringify(response.data.notif)}`);

                        });
                    },
                    startTimer(){
                        this.$cron.start('load');
                        this.cronRunning = true;
                    },
                    stopTimer(){
                        this.$cron.stop('load');
                        this.cronRunning = false;
                    },
                    load(){
                        console.log('every 45 seconds');
                        if(this.selectedUser)
                            this.checkNotificationTask(this.selectedUser.id);
                    }


                },props: {
                    source: String
                },
                watch: {
                    '$route': function(to, from) {
                        console.log('To :', to.path); // current route
                        console.log('From :', from.path); // old route

                        let path_from = from.path;
                        if(path_from.indexOf('subcategories') != -1 || path_from.indexOf('subtasks') != -1 || path_from.indexOf('report') != -1 || path_from === '/day'){
                            this.selectedSubTitle = undefined;
                        }

                        let path_to = to.path;
                        if(path_to.indexOf('userprofil') != -1){
                            this.selectedTitle = "Profil utilisateur";
                            this.selectedTitleParent = undefined;

                        }
                    }
                },
                created(){
                    this.$router.app.$on('chooseUser', (selectedUser) => {
                        this.selectedUser = selectedUser;
                        if(this.selectedUser)
                            this.checkNotificationTask(this.selectedUser.id);
                    });
                    //this.$router.app.$emit('updateMenuState', response.data.user);
                    this.$router.app.$on('updateMenuState', () => {

                        this.checkIfLogged(url_is_logged_final);

                    });
                    this.$router.app.$on('updateWelcomeSubtitle', (subtitle) => { this.selectedSubTitle = subtitle; });
                    this.$router.app.$on('logoutSession', () => {
                        this.notif_tasks = [];
                        this.stopTimer();
                        this.logout();
                    });
                },
                computed: {
                    avatarSize () {
                        return `${this.avatarsize}px`
                    }
                },
                mounted: function () {
                    console.log('welcome : checkIfLogged called in mounted');
                    this.checkIfLogged(url_is_logged_final);
                },
                cron: {
                    time: 45000,
                    method: 'load'
                }

            });

        </script>

    </body>
</html>
