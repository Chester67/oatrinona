<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('task_model','task');
    }

	public function index()
	{
		$this->load->view('welcome');
	}

	public function showAll(){
		$query = $this->task->showAll();
		if($query){
			$result['task']  = $query;
		}
		echo json_encode($result);
	}

	public function updateTask(){
		
		$result['error'] = true;
		$result['msg'] = "failed when update task";
		$result['code_error'] = 0;

		$id = $this->input->post('id_task');
		$label = $this->input->post('label_task');
		$has_image = $this->input->post('has_image');
		$new_image_name = $this->input->post('new_image_name_task');
		$old_image_name = $this->input->post('old_image_name_task');
		
		$data = array(
			'taskLabel' => $label,
			'taskImage' => $new_image_name
        );
		$query = $this->task->updateTask($data, $id);

		if($has_image === 'TRUE'){
			
			$file = "uploads/taches/" . $old_image_name;
 			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				
				//add task 
				$config['upload_path']          = './uploads/taches/';
				$config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['max_size']             = 500;
				$config['max_width']            = 2500;
				$config['max_height']           = 1400;
	
				$this->load->library('upload', $config);
	
				if ( ! $this->upload->do_upload('image_task')) {
			
					$result['msg'] = 'Erreur lors du upload';
					$result['code_error'] = 2;
					
				} else {

					$result['msg'] = 'Upload success';
					$result['code_error'] = 1;

				}
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
				$result['code_error'] = 3;
			}

		}

		if($query){
			$result['error'] = false;
		}
		echo json_encode($result);

	}

	public function deleteTask(){

		$result['error'] = true;
		$result['file_deleted'] = false;
		$result['msg'] = 'error when deleting!';

		$image = $this->input->post('image_task');
		$id = $this->input->post('id_task');
		$file = "uploads/taches/" . $image;
 
		if($this->task->containSubtask($id)){
			$result['msg'] = "Cette tâche contient un ou plusieurs sous-tâche et ne peut pas être supprimée";
		}else{
			if (is_readable($file) && unlink($file)) {
				$result['file_deleted'] = true;
				
				$query = $this->task->deleteTask($id);
				if($query){
					$result['error'] = false;
				}
				$result['msg'] = "file has been deleted.";
			} else {
				$result['msg'] = "The file was not found or not readable and could not be deleted.";
			}
		}
		
		echo json_encode($result);

	}
	
}
