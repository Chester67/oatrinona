<?php
$this->load->view('components/common/common-mixin');
?>

<script type="text/x-template" id="template-login">

    <div style="height: 100%">
    <v-container fluid style="position: absolute; top: 20%;">
        <v-layout justify-center>
            <v-flex xs12 sm8 md4>
                <v-card class="elevation-12">
                    <v-toolbar dark color="grey darken-1">
                        <v-toolbar-title>Authentification</v-toolbar-title>
                        &nbsp;&nbsp;&nbsp;<v-progress-circular v-if="dialog_loading" indeterminate color="white"></v-progress-circular>
                    </v-toolbar>
                    <v-card-text>
                        <v-form>
                        <v-text-field prepend-icon="person" name="login" label="Login" type="text" v-model="username"></v-text-field>
                        <v-text-field id="password" prepend-icon="lock" name="password" label="Password" type="password" v-model="password"></v-text-field>
                        </v-form>
                    </v-card-text>
                    <v-card-actions>
                        <v-spacer></v-spacer>
                        <v-btn color="grey darken-1" @click="authenticate">Se connecter</v-btn>
                    </v-card-actions>

                    <div v-if="!!errormessage" style="display: flex; flex-direction: row; margin-left: 17px; height: 40px">
                        <v-icon style="display: flex; align-items: center; vertical-align: middle">error_outline</v-icon>
                        <span style="display: flex; align-items: center;">{{ errormessage }}</span>
                    </div>
                </v-card>
            </v-flex>
        </v-layout>
    </v-container>
    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_authenticate = '<?php echo URL_AUTHENTICATE ?>';
    var url_authenticate_final = `${url_base}${url_authenticate}`;

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    Vue.component('page-login', {
        template: '#template-login',
        mixins: [common_mixin],
        data: function(){
            return {
                username: '',
                password: '',
                errormessage: '',
                dialog_loading: false,
            }
        },
        methods: {
            authenticate: function(){
                if(this.username && this.password){
                    this.dialog_loading = true;
                    this.errormessage = '';
                    var formData = new FormData();
                    formData.append('username', this.username);
                    formData.append('password', this.password);
                    axios.post(url_authenticate_final, formData).then((response) =>{
                        //console.log(`USER LOGGED====>${JSON.stringify(response.data.error)}`);
                        console.log(`USER LOGGED====>${JSON.stringify(response.data.user)}`);

                        if(response.data.error){
                            setTimeout(() =>{
                                this.dialog_loading = false;
                                this.errormessage = "Login ou mot de passe incorrect !"
                            },  1500);
                        }else{
                            setTimeout(() =>{
                                //hide loading
                                this.dialog_loading = false;
                                //show a propos page
                                this.$router.push('/propos');
                                //update menu state
                                this.$router.app.$emit('updateMenuState');
                            },  1500);
                        }

                    });

                }else{
                    this.errormessage = "Veuillez completer les champs !"
                }
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`LOGIN PAGE USER LOGGED IN ===> ${JSON.stringify(connexion.user_logged)}`);
                    this.$router.push('/propos');
                }else{
                    console.log("LOGIN PAGE NO USER LOGGED");
                }
            }
        },
        props: [],
        computed: {},
        mounted: function () {
            console.log('login : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        }
    });

</script>
