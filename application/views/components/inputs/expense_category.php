<?php

?>

<style type="text/css">
    .input-expense{
        height: 35px;
        min-height: 35px !important;
        max-height: 35px !important;
        font-size: 10pt;
    }

    .v-text-field.v-text-field--solo .v-input__control {
        max-height: 35px;        
        min-height: 35px;
        padding: 0;
    }

    .button-icon-add {
        margin-left: 8px;
        line-height: 35px;
        width: 35px;
        height: 35px;
        border-radius: 50%;
        border-style: solid;
        border-width: 1px;
        border-color: grey;
        display: flex; 
        -webkit-box-align: center; 
        align-items: center; 
        -webkit-box-pack: center; 
        justify-content: center;
        cursor: pointer;
    }

    .top-right-btn{
        width: 27px; 
        height: 27px;
        margin-top: 5px;
    }
    
    .card {
        background: #fff;
        border-radius: 2px;
        margin: 1rem;
        position: relative;
    }

    .card-1 {
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    }

    .card-1:hover {
        box-shadow: 0 2px 2px rgba(0,0,0,0.15), 0 2px 2px rgba(0,0,0,0.12);
    }

    .prix {
        position: relative;
        margin-left: 7px;
    }

    .prix .pricespan { 
        position: absolute; 
        right: 0; 
        top: 1px ;
        background: #DCDCDC; 
        width: 45px; 
        height: 100%; 
        margin-top: -1px;
        border-radius: 0px 3px 3px;
    }
    
    .textprice {
        position: absolute; 
        top: 7px;
        left: 3px;
    }

    div.middle-div {
        position: relative;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        margin-left: 12px;
    }

    /*.v-select.v-select--is-menu-active .v-input__icon--append .v-icon {
        display: none !important;
    }*/

</style>

<script type="text/x-template" id="template-expense-category">

    <div style="background:#fafafa; display: flex; flex-direction: row" class="card card-1">
        <v-layout row wrap style="margin-bottom: 7px; padding-top: 7px; padding-bottom: 7px">

            <v-flex d-flex xs12 sm6 md3 class="hidden-sm-and-down">
                <div>
                    <v-layout justify-center fill-height align-center>
                        <div style="display:block; margin:0 auto; text-align: center; ">
                            
                            <img :src="urlBaseImage+'/uploads/categories/'+selected_category.categoryImage" alt="" style="margin-top: 19px;width:42%;height:42%;object-fit:contain">
                            <p class="text-xs-center" style="margin-top: -4px; font-weight: bold; font-size: 14px">{{ selected_category.categoryLabel }}</p>
                             
                        </div>
                    </v-layout>
                    
                </div>
            </v-flex>

            <v-flex d-flex xs12 sm6 md9>
                
                    <!--debut exemple-->
                    <section style="margin-top: 20px">
                        <content-editor :item_subcategories="item_subcategories" :category_id="selected_category.id" :type_day="type_day" :last_spent_id="last_spent_id"></content-editor>
                    </section>
                    <!--fin exemple-->
                
            </v-flex>

        </v-layout>

        <div style="position: relative">
            <v-icon @click="deleteEntireCategory" :data-index="index" style="position: absolute; top: 5px; right: 10px; font-size: 22px; color: grey; cursor: pointer">clear</v-icon>
        </div>
        
    </div>

</script>

<!--Exemple for id-->
<template id="content-list-template">
  <section class="content-list">
    <div v-for="(currentView, index) in views" :key="currentView.index">
        
            <div style="display: flex; flex-direction: row">
                <component :is="currentView.type" :update="updateContent(index)" :indexp="index" :model="currentView.model" :category_id="category_id" :number="currentView.number" :price="currentView.price" :subcategory="currentView.subcategory" :utility="currentView.utility" :showutility="showUtilityDialog(index)"></component>
                <!--<a v-on:click="removeContent(index)">Remove</a>-->
                
                <div style="height: 100%; margin-top: 4px; margin-left: 5px">
                    <v-btn @click="removeContent(index)" outline fab color="grey" class="top-right-btn" style="margin-left: 3px !important">
                        <v-icon style="font-size: 22px; color: red">clear</v-icon>
                    </v-btn>
                </div>
            </div>
        
    </div>
  </section>
</template>

<template id="content-editor">
  <section class="content-editor">
    <content-list :views="views" :remove="removeContentBlock" :update="updateContentBlock" :category_id="category_id" :showutility="showUtilityDialog"></content-list>
    <div @click="newContentBlock()" style="display: flex; flex-direction: row; height: 42px; margin-right: 7px; border-top: 6px solid transparent; cursor: pointer; margin-bottom: 10px; max-width: 240px">
        <div class="button-icon-add">
            <v-icon>add</v-icon>
        </div>
        <div style="display: flex; align-items: center; margin-left: 8px">
            Ajouter une nouvelle ligne
        </div>
    </div>
  </section>
</template>

<template id="content-longtext">

    <v-layout row wrap style="margin-top: 7px;">
        <v-flex xs12 sm6 md2>
            
            <v-chip
                style="width: 100%"
                class="v-chip--select-multi"
                @click.stop="showUtilityDialog(utilityData)">
                <v-avatar :color="utilityData.contextColor">
                    
                </v-avatar>
                <span style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; max-width: 90px">{{ utilityData.contextLabel }}</span>
            </v-chip>
            <!--
                <v-btn round color="primary" dark>
                Indispensable
                <v-icon left dark>cloud_upload</v-icon>
            </v-btn>-->
                
        </v-flex>
        <v-flex xs12 sm6 md5>
            <v-autocomplete          
                :items="items"
                :loading="isLoading"
                :search-input.sync="search"
                chips
                clearable
                hide-details
                hide-selected
                item-text="subcategoryLabel"
                item-value="id"
                label="Sous-catégorie..."
                solo
                :value="subcategoryData"
                style="margin-left: 7px"
                @change="(event) => updateContent(event, 'subcategory')">
                
                <template
                    
                    slot="selection"
                    slot-scope="{ item, selected }">
                    <v-chip
                        :selected="selected"
                        color="blue-grey"
                        class="white--text"
                        style="height: 29px">

                        <v-icon left>mdi-coin</v-icon>
                        <span v-text="item.subcategoryLabel"></span>

                    </v-chip>
                </template>

                <template
                    style="display: flex; flex-direction: column"
                    slot="item"
                    slot-scope="{ item, tile }">

                    <div style="position:relative; top: -21px">
                        <v-list-tile-avatar
                            style="position:absolute"
                            color="indigo"
                            :size="45"
                            class="headline font-weight-light white--text">
                            {{ item.subcategoryLabel.charAt(0) }}
                        </v-list-tile-avatar>
                        <v-avatar
                                style="position:absolute"
                                :tile="false"
                                :size="45"
                                color="transparent"
                                >
                            <img :src="urlBaseImage+'/uploads/sous-categories/'+item.subcategoryImage" alt="" style="width:89%;height:89%;object-fit:contain" onerror="this.style.display='none'"/>
                        </v-avatar>
                    </div>
                    <v-list-tile-content style="margin-left: 35px">
                        <v-list-tile-title style="margin-left: 15px !important" v-text="item.subcategoryLabel"></v-list-tile-title>
                    </v-list-tile-content>

                    <!--<v-list-tile-action>
                        <v-icon>mdi-coin</v-icon>
                    </v-list-tile-action>-->

                </template>
            </v-autocomplete>
        </v-flex>
        <v-flex xs12 sm6 md3>
            <div class="prix">
                <v-text-field
                    class="inputprice"
                    label="Prix"
                    @input.native="(event) => updateContent(event, 'price')"
                    :value="priceData"
                    ref="price"
                    solo>
                    
                </v-text-field>
                <div class="pricespan"><span class="textprice">Ariary</span></div>
            </div>
        </v-flex>
        <v-flex xs12 sm6 md2>
            <v-text-field
                style="margin-left: 7px"
                label="Nombre"
                @input.native="(event) => updateContent(event, 'number')"
                :value="numberData"
                ref="nombre"
                solo>
            </v-text-field>
        </v-flex>

    </v-layout>
  
</template>

<script type="text/javascript">

    var url_base = '<?php echo base_url()?>';
    
    var url_list_subcategories = '<?php echo URL_LIST_SUBCATEGORY ?>';
    var url_list_subcategories_final = `${url_base}${url_list_subcategories}`;

    var url_list_subcategories_by_categId = '<?php echo URL_LIST_SUBCATEGORY_BY_ID ?>';
    var url_list_subcategories_by_id_final = `${url_base}${url_list_subcategories_by_categId}`;

    Vue.component('expense-category', {
        template: '#template-expense-category',
        props: {
            selected_category: {type: Object, required: true},
            type_day: {type: String, required: true},
            item_subcategories: {type: Array, required: true},
            index: {type: Number, required: true},
            last_spent_id: {type: Number, required: true},
        },
        data: function () {
            return{
                tile: false,
                avatarsize: 115,
                isLoading: false,
                items: [],
                search: null,
                formsubcat: 
                {
                    model: [],
                    unitprice: [],
                    number: []
                },
                increment: 0,
                number: 0,
                price: 0.0,
                urlBaseImage: url_base
            }
        },
        methods: {
            deleteEntireCategory: function(e){
                
                var index = e.target.getAttribute('data-index');
                this.$emit('delete_entire_category', this.type_day, this.selected_category, index);
                console.log(`delete_entire_category======>${index}`);
                
            }
        },
        created: function () {
            //console.log(`selected_category====>${JSON.stringify(this.selected_category)}`);
        }
    });

    //-----------------------------example for id----------------------------------
    Vue.component('content-longtext', {
      template: '#content-longtext',
      props: {
        subcategory: { type: String, required: false },
        number: { type: String, required: false },
        price: { type: String, required: false },
        model: { type: String, required: false },
        utility: { type: Object, required: false },
        update: { type: Function, required: true },
        showutility: { type: Function, required: true },
        indexp: { type: Number, required: true },
        category_id: { type: String, required: true }
      },
      data() {
        return {
          tile: false,
          avatarsize: 115,
          isLoading: false,
          inputData: this.model,
          items: [],
          search: null,
          formsubcat: 
          {
            model: [],
            unitprice: [],
            number: []
          },
          subcategoryData: this.subcategory,
          numberData: this.number,
          priceData: this.price,
          utilityData: this.utility,
          urlBaseImage: url_base,

          //context
          /*select_context: 'Programming',
          all_context: [
            'Programming',
            'Design',
            'Vue',
            'Vuetify'
          ]*/
                
        }
      },
      methods: {
        updateContent(event, type) {
          
          if(type === 'subcategory'){
            this.update(event, type)
          }else{
            this.update(event.target.value, type)
          }

          //console.log(`EVENT=====>${JSON.stringify(event)}`);
          //console.log(`TYPE=====>${type}`);
        },
        startsWith(str, word) {
            return str.lastIndexOf(word, 0) === 0;
        },
        showUtilityDialog(obj_util){
            this.showutility(obj_util);
        }
        
      },
      watch: {
        search (val) {
            // Items have already been loaded
            if (this.items.length > 0) return

            this.isLoading = true
            var formData = new FormData();
            formData.append('category_id', this.category_id);
            window.axios.post(url_list_subcategories_by_id_final, formData).then((response) => { 
                if(response.data.subcategory == null){
                    //v.noResult()
                }else{
                    //v.getData(response.data.users);
                    this.items = response.data.subcategory;
                    //console.log(`SUBCATEGORIES ARE ====>${JSON.stringify(response.data.subcategory)}`)
                }
            })
            .catch(err => {
                console.log(err)
            })
            .finally(() => (this.isLoading = false));

        },
        'utility': {
  	        handler: function(val) { 
                
                this.utilityData = val;
                this.update(val, 'utility');
                
            },
            deep: true
        }
      },
      created: function(){
          this.search = "abcd";
      }
    })

    Vue.component('content-list', {
      template: '#content-list-template',
      data() {
        return {
            test: "12"  
        }
      },
      props: {
        remove: { type: Function, required: true },
        update: { type: Function, required: true },
        showutility: { type: Function, required: true },
        views: { type: Array, required: true },
        category_id: { type: String, required: true }
      },
      methods: {
        removeContent(index) {
          this.remove(index)
        },
        updateContent(index) {
          return (content, type) => this.update(index, content, type)
        },
        showUtilityDialog(index){
          return (obj_util) => this.showutility(index, obj_util);
        }
      },
    })

    Vue.component('content-editor', {
      template: '#content-editor',
      props: ['category_id', 'item_subcategories', 'type_day', 'last_spent_id'],
      data() {
        return {
          item: {},
          views: [
            //{index: 0, type: 'content-longtext', model: '', subcategory: undefined, number: undefined, price: undefined},     
          ],
        }
      },
      methods: {
        newContentBlock(type) {
            let to_add = {index: this.views.length, type: 'content-longtext', model: '', subcategory: undefined, number: undefined, price: undefined, spent_id: this.last_spent_id, from: '<?php echo CONSTANT_ADDING ?>', utility: {id: 1, contextLabel: 'Indispensable', 	contextImage: '#43A047', contextColor: 'green darken-1'} };
            this.views.push(to_add)
            this.updateEntireCategory();
            this.updateOneSubcategory(to_add);
            this.$parent.$emit('increment_last_spent_id');
        },
        updateContentBlock(index, model, type) {
            this.views[index][type] = model;
            console.log(`======>TYPE = ${type} with value = ${JSON.stringify(model)} with index = ${index}`)
            this.updateEntireCategory();
            this.updateOneSubcategory(this.views[index]);
            this.item = {category_id: this.category_id, subcategories: this.item_subcategories};
                
        },
        removeContentBlock(index) {
            this.removeOneCategory(this.views[index]);

            this.views
            .splice(index, 1)
            .map((view, index) => view.index = index)

            this.updateEntireCategory();
          
        },
        updateEntireCategory: function(){
            var entire_cat_to_send = {category_id: this.category_id, subcategories: this.views};
            this.$parent.$emit('update_entire_category', this.type_day, entire_cat_to_send);
        },
        updateOneSubcategory: function(view){
            if(view.from === '<?php echo CONSTANT_LISTING ?>'){
                console.log(`=====>LISTING MODIFY ID : ${view.spent_id}`);
                this.$parent.$emit('update_one_subcategory', this.type_day, view, '<?php echo CONSTANT_DB_UPDATE ?>');
            }else{
                console.log('=====>ADDING');
                this.$parent.$emit('update_one_subcategory', this.type_day, view, '<?php echo CONSTANT_DB_INSERT ?>');
            }
        },
        removeOneCategory: function(view){
            if(view.from === '<?php echo CONSTANT_LISTING ?>'){
                console.log(`=====>LISTING REMOVE ID : ${view.spent_id}`);
                this.$parent.$emit('update_one_subcategory', this.type_day, view, '<?php echo CONSTANT_DB_REMOVE ?>');
            }else{
                this.$parent.$emit('remove_one_subcategory', this.type_day, view);
            }
        },
        showUtilityDialog(index, obj_util){
            console.log(`=====>ARRIVED HERE : ${JSON.stringify(obj_util)}`);
            this.$parent.$emit('show_utility_dialog', index, obj_util.id, this.type_day, this.category_id, this.views[index].spent_id);
        }
      },
      watch: { 
        'item_subcategories': {
  	        handler: function(val) { 
                
                this.views = val;
                this.$forceUpdate();
                
            },
            deep: true
        }
      },
      created: function () {
          //eto no mi liste depense
          if(this.item_subcategories && this.item_subcategories.length > 0){
            this.views = this.item_subcategories;
            //console.log(`category_expenses_morning from WS====>${JSON.stringify(this.views)}`);
          }
            
      }
      
    })

</script>