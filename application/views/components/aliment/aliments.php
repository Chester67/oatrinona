<?php

?>

<script type="text/x-template" id="template-aliments">

    <v-container fluid grid-list-md>
        <v-data-iterator
            :items="items"
            :rows-per-page-items="rowsPerPageItems"
            :pagination.sync="pagination"
            content-tag="v-layout"
            row
            wrap>
            <v-flex
                slot="item"
                slot-scope="props"
                xs12
                sm6
                md4
                lg3>
                <v-card>
                    <v-card-title><h4>{{ props.item.name }}</h4></v-card-title>
                    <v-divider></v-divider>
                    <v-list dense>
                        <v-list-tile v-for="it in props.item.iron">
                            <v-list-tile-content>Calories:</v-list-tile-content>
                            <v-list-tile-content class="align-end">{{ props.item.calories }}</v-list-tile-content>
                        </v-list-tile>
                        <!--<v-list-tile>
                            <v-list-tile-content>Fat:</v-list-tile-content>
                            <v-list-tile-content class="align-end">{{ props.item.fat }}</v-list-tile-content>
                        </v-list-tile>
                        <v-list-tile>
                            <v-list-tile-content>Carbs:</v-list-tile-content>
                            <v-list-tile-content class="align-end">{{ props.item.carbs }}</v-list-tile-content>
                        </v-list-tile>
                        <v-list-tile>
                            <v-list-tile-content>Protein:</v-list-tile-content>
                            <v-list-tile-content class="align-end">{{ props.item.protein }}</v-list-tile-content>
                        </v-list-tile>
                        <v-list-tile>
                            <v-list-tile-content>Sodium:</v-list-tile-content>
                            <v-list-tile-content class="align-end">{{ props.item.sodium }}</v-list-tile-content>
                        </v-list-tile>
                        <v-list-tile>
                            <v-list-tile-content>Calcium:</v-list-tile-content>
                            <v-list-tile-content class="align-end">{{ props.item.calcium }}</v-list-tile-content>
                        </v-list-tile>
                        <v-list-tile>
                            <v-list-tile-content>Iron:</v-list-tile-content>
                            <v-list-tile-content class="align-end">{{ props.item.iron }}</v-list-tile-content>
                        </v-list-tile>-->
                    </v-list>
                </v-card>
            </v-flex>
        </v-data-iterator>
    </v-container>
    
</script>

<script type="text/javascript">

    Vue.component('aliments', {
        template: '#template-aliments',
        data: function(){
            return {
                rowsPerPageItems: [6],
                pagination: {
                    rowsPerPage: 6
                },
                items: [
                    {
                        value: false,
                        name: 'Frozen Yogurt',
                        calories: 159,
                        fat: 6.0,
                        carbs: 24,
                        protein: 4.0,
                        sodium: 87,
                        calcium: '14%',
                        iron: [1,2,3,4,5],
                        
                    },
                    {
                        value: false,
                        name: 'Ice cream sandwich',
                        calories: 237,
                        fat: 9.0,
                        carbs: 37,
                        protein: 4.3,
                        sodium: 129,
                        calcium: '8%',
                        iron: [1,2,3],
                    },
                    {
                        value: false,
                        name: 'Eclair',
                        calories: 262,
                        fat: 16.0,
                        carbs: 23,
                        protein: 6.0,
                        sodium: 337,
                        calcium: '6%',
                        iron: [1,2,3,4,5,6,7],
                    },
                    {
                        value: false,
                        name: 'Cupcake',
                        calories: 305,
                        fat: 3.7,
                        carbs: 67,
                        protein: 4.3,
                        sodium: 413,
                        calcium: '3%',
                        iron: [1,2,3,4,5],
                    },
                    {
                        value: false,
                        name: 'Gingerbread',
                        calories: 356,
                        fat: 16.0,
                        carbs: 49,
                        protein: 3.9,
                        sodium: 327,
                        calcium: '7%',
                        iron: [1,2],
                    },
                    {
                        value: false,
                        name: 'Jelly bean',
                        calories: 375,
                        fat: 0.0,
                        carbs: 94,
                        protein: 0.0,
                        sodium: 50,
                        calcium: '0%',
                        iron: [1,2,3,4,5,6,7,8],
                    },
                    
                ]
            }
        },
        props: [],
        computed: {}
    });

</script>