<?php
$this->load->view('components/common/common-mixin');
$this->load->view('components/empty/data_loader');
$this->load->view('components/empty/data_no_user');
?>

<style type="text/css">

    .marquee {
        width: 250px;
        margin: 0 auto;
        overflow: hidden;
        white-space: nowrap;
        animation: marquee 10s linear infinite;
        -webkit-animation:linear marquee 10s infinite ;
        -moz-animation:linear marquee 10s infinite ;
        -o-animation:linear marquee 10s infinite ;
        -ms-animation:linear marquee 10s infinite ;
        animation:linear marquee 10s infinite ;
    }

    .marquee:hover {
        animation-play-state: paused
    }

    /* Make it move */
    @keyframes marquee {
        0%   { text-indent: 3em }
        100% { text-indent: -9em }
    }

</style>

<script type="text/x-template" id="template-setting-tasks">

    <div style="background:#ffffff; height: 100%; marging: 30px">
        <v-container fluid grid-list-md v-if="!data_no_user">
            <v-data-iterator
                no-data-text=""
                :items="items"
                :rows-per-page-items="rowsPerPageItems"
                :pagination.sync="pagination"
                content-tag="v-layout"
                row
                hide-actions
                wrap>
                <v-flex
                    slot="item"
                    slot-scope="props"
                    xs12
                    md2>

                    <v-card :ripple="true" style="cursor: pointer; background:#fafafa;" @click.native="goToSub(props.item.id, urlBaseImage+'/uploads/taches/'+props.item.taskImage, props.item.taskLabel)">

                        <div style="width: 100%; height: 100%;">
                            <div v-if="user_logged_in.role === 'ADMINISTRATOR'" style="display: flex; flex-direction: row; justify-content: flex-end; height: 22px; margin-right: 7px; border-top: 6px solid transparent;">
                                <v-btn outline fab color="grey" class="edits-btn" @click.stop.prevent="showUpdate(props.item)">
                                    <v-icon style="font-size: 15px;">create</v-icon>
                                </v-btn>
                                <v-btn outline fab color="grey" class="edits-btn" @click.stop.prevent="onDeleteSelected(props.item)">
                                    <v-icon style="font-size: 18px;">clear</v-icon>
                                </v-btn>
                            </div>

                            <v-layout justify-center>
                                <img :src="urlBaseImage+'uploads/taches/'+props.item.taskImage" alt="" style="margin-top: 29px;object-fit:contain" width="160" height="90">
                            </v-layout>

                            <v-card-title>
                                <template v-if="countCharOK(props.item.taskLabel)">
                                        <p style="font-weight: bold; font-size: 13px; width: 250px; margin: 0 auto; overflow: hidden; white-space: nowrap; text-align: center">{{ props.item.taskLabel }}</p>
                                </template>
                                <template v-else>
                                    <p style="font-weight: bold; font-size: 13px" class="marquee">{{ props.item.taskLabel }}</p>
                                </template>
                            </v-card-title>
                        </div>

                    </v-card>
                </v-flex>
            </v-data-iterator>
        </v-container>

        <v-dialog v-model="dialog" width="420px">
            <v-card>
                <v-card-title class="grey lighten-4 py-4 title">
                    {{ dialog_title }}
                </v-card-title>

                <v-container grid-list-sm class="pa-4">
                    <v-layout row wrap>

                        <form id="upload-form" enctype="multipart/form-data" method="post" accept-charset="utf-8">

                            <v-flex xs12>
                                <v-text-field prepend-icon="text_fields" placeholder="Tâche" v-model="label"></v-text-field>
                            </v-flex>

                            <v-flex xs12>
                                <div style="display: flex; flex-direction: row;">
                                    <v-icon>add_a_photo</v-icon>
                                    <div style="display: flex; flex-direction: row; align-items: center">
                                        <input id="file" style="margin-left: 8px" type="file" :class="class_name_state" @change="onFileSelected">
                                        <label style="margin-left: 4px" id="fileLabel">{{ file_name_update }}</label>
                                    </div>
                                </div>
                            </v-flex>

                        </form>

                    </v-layout>
                </v-container>

                <v-card-actions>
                    <v-spacer></v-spacer>
                    <v-btn flat color="blue-grey lighten-1" @click="dialog = false">Annuler</v-btn>
                    <v-btn flat @click="distinctAction">Enregistrer</v-btn>
                </v-card-actions>

                <div v-if="errormessage" style="display: flex; flex-direction: row; margin-left: 25px; height: 40px">
                    <v-icon style="display: flex; align-items: center; vertical-align: middle">error_outline</v-icon>
                    <span style="margin-left: 8px; display: flex; align-items: center;">{{ errormessage }}</span>
                </div>

            </v-card>
        </v-dialog>

        <v-btn
            fab
            bottom
            right
            color="blue-grey lighten-1"
            dark
            fixed
            @click="showAdd"
            v-if="!data_no_user">
            <v-icon>add</v-icon>
        </v-btn>

        <v-dialog v-model="dialog_remove" persistent max-width="290">
            <v-card>
                <v-card-title>Voulez-vous vraiment supprimer cette tâche ?</v-card-title>
                <v-card-actions>
                <v-spacer></v-spacer>
                <v-btn color="green darken-1" flat @click="dialog_remove = false">ANNULER</v-btn>
                <v-btn color="green darken-1" flat @click="deleteTask">SUPPRIMER</v-btn>
                </v-card-actions>
            </v-card>
        </v-dialog>

        <data-no-user v-if="data_no_user"></data-no-user>
        <data-loader v-if="data_loader"></data-loader>

        <v-alert
            class="centered_flash"
            :value="alert"
            type="success"
            transition="scale-transition">
            {{ flashMessage }}
        </v-alert>

    </div>

</script>

<script type="text/javascript">

    var url_base = '<?php echo base_url() ?>';

    var url_is_logged = '<?php echo URL_IS_LOGGED ?>';
    var url_is_logged_final = `${url_base}${url_is_logged}`;

    var url_list_task = '<?php echo URL_LIST_TASK ?>';
    var url_list_task_final = `${url_base}${url_list_task}`;

    var url_upload_image_task = '<?php echo URL_UPLOAD_IMAGE_TASK ?>';
    var url_upload_image_task_final = `${url_base}${url_upload_image_task}`;

    var url_update_task = '<?php echo URL_UPDATE_TASK ?>';
    var url_update_task_final = `${url_base}${url_update_task}`;

    var url_delete_task = '<?php echo URL_DELETE_TASK ?>';
    var url_delete_task_final = `${url_base}${url_delete_task}`;

    Vue.component('setting-tasks', {
        template: '#template-setting-tasks',
        mixins: [common_mixin],
        data: function () {

          return{
            flashMessage:'',
            alert: false,
            dialog: false,
            dialog_title: '',
            dialog_remove: false,
            rowsPerPageItems: [6, 12, 24],
            pagination: {
                rowsPerPage: 6
            },
            items: [],
            selectedFile: null,
            label: null,
            errormessage: '',
            urlBaseImage: url_base,
            selected_for_delete: undefined,
            class_name_state: '',
            file_name_update: '',
            action_type: '',
            old_file_name_update: '',
            selected_for_update: undefined,
            user_logged_in: undefined,
          }

        },
        methods: {
            distinctAction: function() {
                if(this.action_type === '<?php echo CONSTANT_DB_INSERT ?>'){
                    this.addTask();
                }else if(this.action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    this.updateTask();
                }
            },
            addTask(){
                console.log(`LABEL======>${this.label}`);
                if(this.label && this.selectedFile && this.selectedFile.name){
                    this.data_loader = true;
                    this.errormessage = '';
                    var formData = new FormData();
                    formData.append('image_task', this.selectedFile);
                    formData.append('label_task', this.label);
                    axios.post(url_upload_image_task_final, formData).then((response) =>{
                        console.log(`SUCCESS UPLOAD====>${response.data.success_upload} SUCCESS INSERT====>${response.data.success_insert}`);
                        if (response.data.success_upload && response.data.success_insert) {
                            //alert("SUCCESS");
                            this.dialog = false;
                            this.manageFlashMSG("Votre tâche a été ajouté avec succès !");

                        }
                        if (response.data.error) {
                            //alert(`ERROR====>${response.data.error}`);
                            this.errormessage = response.data.error;
                        }
                    })
                    .finally(() => {

                        this.closeLoaderAfterDelay();

                    })
                }else{
                    this.errormessage = "Veuillez completer les champs";
                }
            },
            updateTask(){
                console.log("updateTask");
                if(this.label){

                    this.data_loader = true;
                    this.errormessage = '';
                    var formData = new FormData();

                    formData.append('id_task', this.selected_for_update.id);
                    formData.append('label_task', this.label);
                    formData.append('new_image_name_task', this.file_name_update);
                    formData.append('old_image_name_task', this.old_file_name_update);

                    if(this.selectedFile && this.selectedFile.name){
                        console.log("========>UPDATE BOTH TASK LABEL & IMAGE");
                        formData.append('has_image', 'TRUE');
                        formData.append('image_task', this.selectedFile);

                    }else{
                        console.log("========>UPDATE ONLY TASK LABEL");
                        formData.append('has_image', 'FALSE');
                        formData.append('image_task', null);
                    }

                    axios.post(url_update_task_final, formData).then((response) =>{
                        console.log(`CODE ERROR EDIT====>${response.data.code_error}`);
                        if (!response.data.error) {

                            this.dialog = false;
                            this.manageFlashMSG("Votre tâche a été modifiée avec succès !");

                        }else{
                            alert("ERROR");
                        }

                    })
                    .finally(() => {

                        this.closeLoaderAfterDelay();

                    })

                }else{
                    this.errormessage = "Veuillez completer la tâche !";
                }
            },
            deleteTask(){
                console.log("deleteTask");
                this.dialog_remove = false;
                this.data_loader = true;
                var formData = new FormData();
                console.log(`TASK_TO_DELETE : ${JSON.stringify(this.selected_for_delete)}`)
                formData.append('image_task', this.selected_for_delete.taskImage);
                formData.append('id_task', this.selected_for_delete.id);
                axios.post(url_delete_task_final, formData).then((response) =>{
                    console.log(`SUCCESS DELETE====>${response.data.msg}`);
                    if (response.data.file_deleted && !response.data.error) {

                        this.manageFlashMSG("Votre catégorie a été supprimée avec succès !");

                    }
                    if (response.data.error) {

                        alert(response.data.msg);

                    }
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            onFileSelected(event){
                this.selectedFile = event.target.files[0];
                if(this.action_type === '<?php echo CONSTANT_DB_UPDATE ?>'){
                    this.file_name_update = this.selectedFile.name;
                }
            },
            onDeleteSelected(item){
                this.selected_for_delete = item;
                this.dialog_remove = true;
            },
            showUpdate(task){

                this.selected_for_update = task;
                this.dialog_title = "Modifier une tâche";
                this.label = task.taskLabel;
                //set input file image programmatically
                this.class_name_state = 'hidden-placeholder';
                this.dialog = true;
                this.errormessage =  '';
                this.file_name_update = task.taskImage;
                this.old_file_name_update = task.taskImage;
                this.action_type = '<?php echo CONSTANT_DB_UPDATE ?>';
                this.selectedFile = null;

            },
            showAdd(){

                //reset input file if non null (after open update)
                if(document.querySelector('#file')){
                    document.querySelector('#file').value = '';
                }
                this.selectedFile = null;
                this.dialog_title = "Créer une tâche";
                this.class_name_state = "";
                this.file_name_update = "";
                this.dialog = true;
                this.label = null;
                this.errormessage =  '';
                this.action_type = '<?php echo CONSTANT_DB_INSERT ?>';

            },
            wsTask(){
                this.data_loader = true;
                axios.get(url_list_task_final).then((response) => {
                    if(response.data.task == null){

                    }else{
                        console.log(`TASKS ARE ====>${JSON.stringify(response.data.task)}`)
                        var arr_rowsPerPageItems = [];
                        arr_rowsPerPageItems.push(response.data.task.length);
                        this.rowsPerPageItems = arr_rowsPerPageItems;
                        this.pagination.rowsPerPage = response.data.task.length;
                        this.items = response.data.task;
                    }
                })
                .finally(() => {

                    this.closeLoaderAfterDelay();

                })
            },
            goToSub(taskId, image_url, sub_label){

                this.$router.push({ path: `/subtasks/${taskId}`, query:{image: image_url, label: sub_label, taskid: taskId, userlogged: this.user_logged_in} });

            },
            countCharOK(str){
                if(str.length > 22)
                    return false;
                return true;
            },
            manageFlashMSG(message){
                this.flashMessage = message;
                this.alert = true;
                setTimeout(() =>{
                    this.alert = false;
			        this.wsTask();
			    },  3000); // disappearing message success in 3 sec
            },
            afterCheckLogged(connexion){
                if(connexion.is_logged){
                    console.log(`TASKS PAGE USER LOGGED IN ===> ${JSON.stringify(connexion)}`);
                    if(connexion.user_choosed && connexion.user_choosed[0]){
                        this.user_logged_in = connexion.user_choosed[0];
                    }else{
                        this.user_logged_in = connexion.user_logged[0];
                    }
                    this.wsTask();
                }else{
                    console.log("TASKS PAGE NO USER LOGGED");
                    this.$router.app.$emit('logoutSession');
                }
            }
        },
        mounted: function () {
            console.log('tasks : checkIfLogged called in mounted');
            this.checkIfLogged(url_is_logged_final);
        },
        created: function () {
            //this.$router.app.$emit('updateWelcomeSubtitle', undefined);
        },

    });


</script>