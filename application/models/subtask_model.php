<?php

class Subtask_model extends CI_Model{

    public function showByTaskId($taskId){
        $query = $this->db->get_Where('subtask', array('taskId'=>$taskId));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function showAll(){
        $query = $this->db->get('subtask');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function addSubtask($data){
        $insert = $this->db->insert('subtask', $data);
        return $insert;
    }

    public function isExist($label_subtask){
        $this->db->select('*');
        $this->db->from('subtask');
        $this->db->where('subtaskLabel', $label_subtask);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

    public function updateSubtask($data, $id){
        $updated = $this->db->update('subtask', $data, array('id' => $id));
        return $updated;
    }

    public function deleteSubtask($id){
        $deleted = $this->db->delete('subtask', array('id' => $id));
        return $deleted;
    }

    public function isExistInSpent($id_subtask){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('subtaskId', $id_subtask);
        $query = $this->db->get();
        $numrows = $query->num_rows();
        if($numrows > 0)
            return true;
            
        return false;
    }

}

?>